
/**********************************************************************//**
 * @file p2/main.c
 * @author Francisco Javier Roman Escorza
 * @brief Using the leds depending of the 3 buttoms of the iCEBREAKER associated with this configuration of the neorv:

  gpio_i(0)  <= iCEBreakerv10_PMOD2_9_Button_1; 
  gpio_i(1)  <= iCEBreakerv10_PMOD2_4_Button_2;  
  gpio_i(2)  <= iCEBreakerv10_PMOD2_10_Button_3;
  iCEBreakerv10_PMOD2_1_LED_left   <= gpio_o(0);
  iCEBreakerv10_PMOD2_2_LED_right  <= gpio_o(1);
  iCEBreakerv10_PMOD2_8_LED_up     <= gpio_o(2);
  iCEBreakerv10_PMOD2_3_LED_down   <= gpio_o(3);
  iCEBreakerv10_PMOD2_7_LED_center <= gpio_o(4);
 **************************************************************************/

#include <neorv32.h>

#define ADR 0x90000000
#define WORD 4
/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200


//PINES DE TECLADO
#define TECCOL1 8
#define TECCOL2 7
#define TECCOL3 6
#define TECCOL4 5
#define TECROW1 6
#define TECROW2 5
#define TECROW3 4
#define TECROW4 3

uint64_t TECROW[]={TECROW1, TECROW2, TECROW3, TECROW4}; 
uint64_t TECCOL[]={TECCOL1, TECCOL2, TECCOL3, TECCOL4}; 
uint64_t MASKTECIN = ((1<<TECROW1) | (1<<TECROW2) | (1<<TECROW3) | (1<<TECROW4));
uint64_t MASKTECOUT = ((1<<TECCOL1) | (1<<TECCOL2) | (1<<TECCOL3) | (1<<TECCOL4));
char tecla = 0; 
char matrix[4][4]=
  {
    {'1','2','3','A'}, 
    {'4','5','6','B'}, 
    {'7','8','9','C'},
    {'0','F','E','D'}
  }; 

char teclado(void);  //Funcion de lectura del teclado, 
                    //devuelva el caracter ASCII de la tecla presionada

int estado = 0; 
char impresion = 0;
int operador = 0;  
int aux = 0; 
char signo[]= "+-*/";
 int op[3]; 
 int res; 
int main() {

  // init UART (primary UART = UART0; if no id number is specified the primary UART is used) at default baud rate, no parity bits, ho hw flow control
  neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

  // check if GPIO unit is implemented at all
  if (neorv32_gpio_available() == 0) {
    neorv32_uart0_print("Error! No GPIO unit synthesized!\n");
    return 1; // nope, no GPIO unit synthesized
  }

  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  
  neorv32_uart0_print("Practica 3 SEPA: CALCULADORA \n");
   
  neorv32_uart0_printf("-------CONTROLES-------\n"); 
  neorv32_uart0_printf("A => Avanzar\n");
  neorv32_uart0_printf("B => Retroceder(back)\n");
  neorv32_uart0_printf("C => Limpiar todo el termino\n");
  neorv32_uart0_printf("D => Eliminar ultima cifra\n");
  neorv32_uart0_printf("E => Reset\n\n");

  for (int i=0; i<4; i++) 
  {
    neorv32_cpu_store_unsigned_word(ADR+i*WORD, 0);

  }

  while(1)
  {
    switch (estado){
      case 0:                                                         //Seleccinar operación
        if (impresion == 0){                                          //Impresion una vez de las instrucciones
          
          neorv32_uart0_printf("------SELECCIONE OPERACION------\n"); 
          neorv32_uart0_printf("Suma => 1\n");
          neorv32_uart0_printf("Resta => 2\n");
          neorv32_uart0_printf("Multiplicacion => 3\n");
          neorv32_uart0_printf("Division => 4 \n");
          impresion = 1; 
        }
        tecla=teclado();                                              //Esperar a que se pulse una tecla

        if (tecla <= '4' && tecla >= '1')     operador = tecla -'0';  //Obtener tipo de operación según las instrucciones previas
        else if (tecla == 'A')                                        //Avanzar de estado
          {
            estado = 1; 
            impresion = 0; 
            tecla = 0;
            neorv32_cpu_store_unsigned_word(ADR, operador);           //Guardar operación en registro
            operador = 0;   
          }
        else                                                          //Reiniciar sistema
        { 
          impresion = 0; 
          tecla = 0;
          operador = 0; 
          }
      break;
      case 1:                                                         //Seleccionar primer término
        if (impresion == 0)
        {
          neorv32_uart0_printf("Teclee el primer operador\n");        
          impresion = 1; 
        }
        tecla = teclado();                                            //Esperar a que se pulse una tecla
        if (tecla <= '9' && tecla >= '0')
        { 
          aux = tecla - '0';                                          //Calcular digito presionado
          operador = operador*10 + aux;                               //Añadir digito a numero presionado
        }
        else if (tecla == 'A')                                       //Avanzar de estado
          {
            estado ++; 
            impresion = 0; 
            tecla = 0;
            neorv32_cpu_store_unsigned_word(ADR+WORD, operador);     //Guardar primer término en registro
            operador = 0;   
          }
        else if (tecla == 'B')                                       //Volver al estado anterior
        {
            estado --; 
            impresion = 0; 
            tecla = 0;
            operador = neorv32_cpu_load_unsigned_word(ADR+1*WORD);  //Recuperar término del estado anterior   
          } 

        else if (tecla == 'C') operador = 0;                         //Eliminar término actual
        else if (tecla == 'D')                                       //Borrar último digito   
        {
          aux = operador%10; 
          operador = (operador-aux)/10;                               
        }
        else if (tecla == 'E')                                        //Reiniciar sistema                             
        {
            estado = 0; 
            impresion = 0; 
            tecla = 0;
            operador = 0; 
            for (int i=0; i<4; i++) 
            neorv32_cpu_store_unsigned_word(ADR+i*WORD, 0);           //Vaciar registro
          }
      break;
      case 2:                                                         //Selección segundo término
                                                                      //Análogo al uno
        if (impresion == 0)
        {
          neorv32_uart0_printf("Teclee el segundo operador\n"); 
          impresion = 1; 
        }
        tecla = teclado();                                            
        if (tecla <= '9' && tecla >= '0')
        { 
          aux = tecla - '0'; 
          operador = operador*10 + aux;  
        }
        else if (tecla == 'A')
          {
            estado = 3; 
            impresion = 0; 
            tecla = 0;
            neorv32_cpu_store_unsigned_word(ADR+2*WORD, operador);
            operador = 0;   
          }
        else if (tecla == 'B')
        {
            estado = 2; 
            impresion = 0; 
            tecla = 0;
            operador = neorv32_cpu_load_unsigned_word(ADR+2*WORD);   
          } 

        else if (tecla == 'C') operador = 0;
        else if (tecla == 'D')
        {
          aux = operador%10; 
          operador = (operador-aux)/10; 
        }
        else if (tecla == 'E')
        {
            estado = 0; 
            impresion = 0; 
            tecla = 0;
            operador = 0; 
            for (int i=0; i<4; i++) 
            neorv32_cpu_store_unsigned_word(ADR+i*WORD, 0);
          }
      break;
      case 3:                                              //Cálculo
       
        for (int i=0; i < 3; i++) op[i]=neorv32_cpu_load_unsigned_word(ADR+i*WORD);     //Recuperar elementos de la memoria
        if (op[2]==0 && op[0] == 4)                                                     //Division entre infinito
        {
        neorv32_uart0_printf("NaN\n");
        estado = 0; 
        impresion = 0; 
        operador = 0; 
        tecla = 0; 
        for (int i=0; i<4; i++)                           //Limpiar memoria
        neorv32_cpu_store_unsigned_word(ADR+i*WORD, 0);
        }
        else         
        {
          res = (op[0]==1)*(op[1]+op[2])+(op[0]==2)*(op[1]-op[2])+(op[0]==3)*(op[1]*op[2])+(op[0]==4)*(op[1]/op[2]);
          neorv32_cpu_store_unsigned_word(ADR+3*WORD, res);                               //Guardar resultado
          estado = 4;} 
      break;              
      case 4:                                              //Mostrar resusltado
        neorv32_uart0_printf("El resulto de %i %c %i = %i \n",op[1],signo[op[0]-1],op[2], res);
        estado = 0; 
        impresion = 0; 
        operador = 0; 
        tecla = 0; 
        for (int i=0; i<4; i++)                           //Limpiar memoria
        neorv32_cpu_store_unsigned_word(ADR+i*WORD, 0);
      break;  
      default:
        estado = 0; 
        impresion = 0; 
        operador = 0; 
        tecla = 0; 
        for (int i=0; i<4; i++)                           //Limpiar memoria
        neorv32_cpu_store_unsigned_word(ADR+i*WORD, 0);
    }
  }
  return 0;
}

char teclado(void)
{
  char r = 0; 
  while(r == 0){  
    for (int i=0; i<4; i++)
      {
        neorv32_gpio_port_set((MASKTECOUT&(~(1<<TECCOL[i]))));            //Apagar la columna i y encender los leds correspondientes
        uint64_t  INTEC = ((~neorv32_gpio_port_get())&MASKTECIN);         //Leer y procesar los inputs para obtener unicamente el pin
                                                                          //de la tecla pulsada a '1'
        if (INTEC != 0x00000000)                                          //Comprobar si tecla pulsada
        {
          for (int j=0; j<4; j++)
            {
              if ((INTEC & 1<<TECROW[j]))                                 //Tecla col = i, row = j pulsada
              {
                neorv32_uart0_printf("%c\n", matrix[j][i]);
                r=matrix[j][i];                                           //Salvar valor ASCII de tecla pulsada 
                while(INTEC == ((~neorv32_gpio_port_get())&MASKTECIN)) ;   //Guardar mascara de leds correspondiente a la tecla pulsada
              }
            }
        }
      } 
  }
  return r; 
}

