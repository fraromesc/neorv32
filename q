[33mcommit cd5cfc9b728246a7b02f6998ec439db3cb793df3[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: salas <fraromesc@gmail.com>
Date:   Fri Dec 16 17:21:39 2022 +0100

    p2 a medias 3

[33mcommit 4d3783a4a47d8ed09ad09b0e3b6fddca3e7bb966[m
Author: salas <fraromesc@gmail.com>
Date:   Fri Dec 16 17:14:44 2022 +0100

    p2 a medias 2

[33mcommit b6dc7e98bdd61ada48ac3cd42348094fc840010c[m
Author: salas <fraromesc@gmail.com>
Date:   Fri Dec 16 17:10:32 2022 +0100

    p2 a medias

[33mcommit bed9a2718651b81682b741f7c353fb3e3baf20d2[m
Author: salas <fraromesc@gmail.com>
Date:   Fri Dec 16 13:33:57 2022 +0100

    p1 - documentacion

[33mcommit b7ffaed81a106fb747e3f2a202727532fd0d14e0[m
Author: salas <fraromesc@gmail.com>
Date:   Wed Dec 14 20:34:02 2022 +0100

    p1 falta probarlo

[33mcommit c7c0fd68386f1e6aaad4a06d2b9edb9540f3240d[m[33m ([m[1;31morigin/icebreaker[m[33m)[m
Author: Hipolito Guzman <hguzman@us.es>
Date:   Mon Nov 29 13:01:32 2021 +0100

    Fix neorv32_gpio_port_get()
    
    This function should read from:
     - NEORV32_GPIO.INPUT_LO and NEORV32_GPIO.INPUT_HI
    
    But previously it was reading from:
     - NEORV32_GPIO.OUTPUT_LO and NEORV32_GPIO.OUTPUT_HI
    
    So it wasn't working correctly. The function neorv32_pin_get() was
    reading the correct values (from INPUT instead of OUTPUT) and thus it
    was working correctly.

[33mcommit 4eb0efde4b5270ddd15e4657798f564d78efe3cd[m
Author: Hipolito Guzman <hguzman@us.es>
Date:   Thu Nov 18 22:52:45 2021 +0100

    icebreaker: board top instances the processor core and not a wrapper

[33mcommit 421d1ce93ea4980ca0db8cb1cf8a23c6560e86c7[m
Author: Hipolito Guzman <hguzman@us.es>
Date:   Fri Nov 5 22:40:33 2021 +0100

    icebreaker: remove internal oscillator and PLL, and add --freq to PNRFLAGS
    
    The internal oscillator and PLL were removed to make the code more
    readable for students. How to instance the PLL and make the processor
    run faster will be explained in the FAQ.
    
    Removed the PNR flag that allowed the timing to fail, we don't want
    that! Instead we now have --freq 13 (push a bit harsher than just 12
    MHz, but it should also work with 12)

[33mcommit a05c32a0f2628320f7b3e839dfc07952b063c6b4[m
Author: Hipolito Guzman <hguzman@us.es>
Date:   Fri Nov 5 11:06:56 2021 +0100

    icebreaker: default DMEM_SIZE now matches default linker script

[33mcommit 98dcf6444647194e7b4c8a2129470424625bd740[m
Author: Hipolito Guzman <hguzman@us.es>
Date:   Wed Nov 3 18:13:53 2021 +0100

    icebreaker: neorv is now reset by on-board button
    
    The on-board button (UBUTTON) now resets the microprocessor
    
    Not to be mistaken with the PMOD2 buttons

[33mcommit 162c6e65358948b4a047b4a65616c2188feb1371[m
Author: Hipolito Guzman <hguzman@us.es>
Date:   Wed Oct 27 16:00:38 2021 +0200

    Add support for iCEBreaker board (MinimalBoot design)

[33mcommit 2de0a3140f2d60a1f620903b6e688ee8069cce13[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 17:13:47 2021 +0100

    [rtl/core/mem/README] minor edits
    
    it is a toolchain problem - not an FPGA/technology problem. the cyclone 2 block RAMs are (nearly) identical to the current FPGA one's.

[33mcommit e50d1315fb34ada5a87a05ab29025ace916189f4[m
Merge: 6cc88d51 d7a8e1ea
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 12:47:39 2021 +0100

    Merge pull request #198 from stnolting/add_cyclone2_legacy_memfiles
    
    Add cyclone2 legacy mem-files

[33mcommit d7a8e1eaccbae6b9ccde7d61a0d79eb618447513[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 12:44:46 2021 +0100

    [rtl/core/mem/README] typo fixes

[33mcommit 6cc88d515aebaec3e0de2f6504ef79e22550b8f2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 10:39:37 2021 +0100

    [docs] minor edits and fixes

[33mcommit 942f88632fa65642701c4fc4f5387a7dc130247a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 00:52:27 2021 +0100

    updated CHANGELOG and version

[33mcommit 19edd14b0db8cac95eb7952025ffc323c39ea846[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 00:49:02 2021 +0100

    [rtl/core/mem] added README

[33mcommit d3b716dafb811cddd22305e52faed482a10685c1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 00:48:48 2021 +0100

    [docs] minor edits regarding DEFAULT mem sources

[33mcommit 72b00be5f6459cd58897b9959f3d8ee39d4ba1fb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 00:46:08 2021 +0100

    [rtl/core/mem] minor edits

[33mcommit a35609ad8abd1890673d57092299ca38f0396325[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 4 00:45:42 2021 +0100

    [rtl/core/mem] added cyclone2-optimized mem files #197
    
    these files DO NOT use any FPGA-specific primitves or macros at all! - just a different HDL style

[33mcommit a498d66a2e7dcd5d314bc8c2633cd6e619a7e653[m
Merge: 6acbf8d2 b2d3c478
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 3 15:58:33 2021 +0100

    Merge pull request #195 from stnolting/add_general_purpose_timer
    
    Add general purpose timer GPTMR

[33mcommit 6acbf8d2c8351b3e3e042ed25b750ae87525f506[m
Merge: 0c4fdcef da2893a2
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 3 13:48:46 2021 +0100

    Merge pull request #196 from gitter-badger/gitter-badge
    
    Add a Gitter chat badge

[33mcommit da2893a252b6259e55009e6289a103dbcb501b4f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 3 13:45:31 2021 +0100

    [docs] added gitter badge

[33mcommit 8aed093b34f6e6ab54c32c4d9c13990a4b1ef7af[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 3 13:45:15 2021 +0100

    [README] badge layout edits

[33mcommit c6e071a300a94d83d4212dadcbda1e0fdb9ebbec[m
Author: The Gitter Badger <badger@gitter.im>
Date:   Wed Nov 3 12:14:42 2021 +0000

    Add Gitter badge

[33mcommit b2d3c478d0175428f69ae1d64fbda4ec20125671[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 22:39:06 2021 +0100

    [CHANGELOG] added v1.6.3.1

[33mcommit 145f082ab9d29223e92bfd07d4dc19b0ce2595b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 19:13:53 2021 +0100

    [docs] added GPTMR documentation

[33mcommit 119b63e8a1c7ce7f80d2676721c977201061188f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 19:02:56 2021 +0100

    [docs/datasheet] minor edits & fixes

[33mcommit 885e7a8c9a93188ced8a82536968f96a077bc46c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:29:55 2021 +0100

    [sw/example] added GPTMR example/demo program

[33mcommit 7029554499e89fcd914be43ce808848d0021c120[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:26:51 2021 +0100

    [rtl/core] fixed broken default application image
    
    my fault...

[33mcommit e721f5357d9eebaba2eb2db5825c9bae0b8aa2b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:24:26 2021 +0100

    [sw/examples/processor_check] added GPTMR test case

[33mcommit d0917dc982b41648644e6f4906305fca295348d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:24:03 2021 +0100

    [sw/lib] RTE: added GPTMR to HW check

[33mcommit 9c536ae7cef2f198dc26ced1b8e6aa69a9a9c7bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:23:38 2021 +0100

    [sw/libs] added GPTMR hardware driver

[33mcommit c8e5560c86d60d2947c15fffd824e897b7a43fb2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:23:18 2021 +0100

    [sw/lib] added GPTMR register and bit definitions

[33mcommit 2afe6c6b1d8a417c49be24d6cf01417c8a8d42fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:22:48 2021 +0100

    [setups] added GPTMR VHDL source

[33mcommit 7e95e0270794d4328239a5a3f3cdcb81edee3b2a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:22:21 2021 +0100

    [sim] added GPTMR to testbenches

[33mcommit a04965467c2c0f1436972721b0a2f0d1947351bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:22:03 2021 +0100

    [rtl/system_integration] added GPTMR

[33mcommit 61fa1ceca8a69e3bc6b3d40d1e98fb7b853c9ba8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 18:21:43 2021 +0100

    [rtl/core] added GPTMR to processor

[33mcommit 4f6c86094319f684b69974c94a87870f16cab969[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 16:58:17 2021 +0100

    [docs/figures] added GPTMR

[33mcommit 0c4fdcef9edaa1693be376a8bc0bbaca2085e497[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 15:37:28 2021 +0100

    [sw/common/common.mk] added header

[33mcommit 874ed983e34acd4c2f08364e4c02f7215501d519[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 2 10:19:16 2021 +0100

    :rocket: preparing new release v1.6.3

[33mcommit 0860f389de6b8df16e09d2150d3e2170fe7809d5[m
Merge: 34d9ab17 fdc885f6
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 1 14:32:52 2021 +0100

    Merge pull request #192 from stnolting/Zicnt_Zihpm_update
    
    RISC-V "Zicntr" and "Zihpm" extensions

[33mcommit fdc885f6dec0d615be11783dbe785d90d188f83b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 1 14:05:25 2021 +0100

    [CHANGELOG] added v1.6.2.13

[33mcommit 254b170b38cc6b7c6e7aa014f33d5af08a9dfcdf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 1 14:05:05 2021 +0100

    [docs] minor edits

[33mcommit 1b9d8e5bbf9a5e591c786c49921f8847d8a19e2b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 1 13:57:25 2021 +0100

    [README] added Zicntr and Zihpm

[33mcommit d46b1d93b1df39e4bcfb35b1472734ce70f55f05[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 1 13:57:12 2021 +0100

    [docs] minor edits and updates

[33mcommit 34a7882794d7420273ac493af7c8ad68ce102766[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 1 13:56:43 2021 +0100

    [docs] added Zicntr & Zihpm ISA extensions

[33mcommit bf07c12728609daf0200a9425808cbd9545460a8[m
Merge: 245d8b23 34d9ab17
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 31 22:44:31 2021 +0100

    Merge branch 'master' into Zicnt_Zihpm_update

[33mcommit 34d9ab179c5d911abca40cb86d481e0cf6c71903[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 31 19:48:16 2021 +0100

    [sw] README typo fixes

[33mcommit 1892c98b4fde0ab3f7a2d4c52c91e5833051ed4d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 31 18:03:27 2021 +0100

    [docs/figures] typo fix

[33mcommit 245d8b2383ad8b25bd32e506dfc31523b50737c4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 14:08:03 2021 +0200

    [setups] minor edits

[33mcommit bb25c57afee4791865f393f68d1961bb795e58c7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 13:12:48 2021 +0200

    [docs/figures] added Zicntr and Zihpm

[33mcommit 77d38c3552f144b7e5ce9e058d1bf6d6e4d2935d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 13:12:13 2021 +0200

    [setups] minor update

[33mcommit fdbe63b6368c1886e9311662fd14b9ace70e9bf2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 12:15:45 2021 +0200

    [rtl] updated test setups, templates and integration wrappers

[33mcommit bb437056b6e8f8f14c257c30cdc2a9b46aa518aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 12:06:58 2021 +0200

    [sim] updated testbenches

[33mcommit b7fa996c1c7e8ddbaa57e187eaf0d932ca8513f3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 11:54:33 2021 +0200

    [sw/lib/source] updated Zicntr & Zihpm flags

[33mcommit 91f7b22fb43390639d94a090ed66424b82cb1c50[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 11:53:57 2021 +0200

    SYSINFO: renamed flags
    
    * `SYSINFO_CPU_HPM` => `SYSINFO_CPU_ZIHPM`
    * `SYSINFO_CPU_ZXNOCNT` => not `SYSINFO_CPU_ZICNTR`

[33mcommit 008663176cf327dfcce3b7ccfbd91249bd571572[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 11:52:47 2021 +0200

    [rtl/core] added Zicntr and Zihpm config. generics

[33mcommit 87ca10ecae32437f2c1ce95bbd23ca43bffd61e5[m
Merge: f7abb222 9669796e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 10:50:37 2021 +0200

    Merge branch 'master' into Zicnt_Zihpm_update

[33mcommit 9669796ee219542cd1990c2aa9bf73faa53427cc[m
Merge: 9d5fe55b ff26141a
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 10:49:07 2021 +0200

    Merge pull request #191 from stnolting/bus_keeper_update
    
    [BUSKEEPER] add memory-mapped status register

[33mcommit f7abb2226ffeeea5ffd45305fdc364330b5a936a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 01:17:07 2021 +0200

    [docs/references] upadted RISC-V specs.

[33mcommit ff26141a9e7e90a40dc516e2081a7d0ecbbfb55d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 01:10:27 2021 +0200

    [BUSKEEPER] sticky error flag auto-clear on read-access

[33mcommit 462ffda775b19e1faa04c8b389f4a96ecb72fac2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:53:06 2021 +0200

    [CHANGELOG] added v1.6.2.12

[33mcommit 884b912f490e54cb571e134a520584dbde8ce60b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:52:44 2021 +0200

    [rtl/core] BUSKEEPER: minor edit

[33mcommit 42c573f0fc02b022961969e12ddb8b50931a167c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:39:48 2021 +0200

    [docs] added new BUSKEEPER documentation

[33mcommit e6281d2fb9dab20dd2ab238a5751867131ae3f18[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:39:03 2021 +0200

    [sw/example] processor_test: minor clean-ups

[33mcommit 860f798658dc65f526efa60fc576b8fe1415094b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:38:48 2021 +0200

    [sw/lib] added BUSKEEPER error typo output to RTE trap debug handler

[33mcommit 61b0e74a1525b29ebc722e264745615522f82cab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:38:07 2021 +0200

    [sw/lib] added BUSKEEPER register and bit definitions

[33mcommit f754eb280156e178dab66f8b36069a781cbf3af1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 30 00:37:47 2021 +0200

    [rtl/core] BUSKEEPER: added memory-mapped status register

[33mcommit 9d5fe55b63e68ef1875d7ea1d06b950bfe5e2677[m
Merge: b8a2a677 f6870262
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 18:07:04 2021 +0200

    Merge pull request #190 from stnolting/bit_manipulation_zba
    
    [B ISA Extension] Rework and addition of Zba subset

[33mcommit f68702624e54f2d772aa1a32848cd413771798a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 16:50:10 2021 +0200

    [CHANGELOG] added v1.6.2.11

[33mcommit e843b84675eda214e722fef895ff8c7721935eec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 16:49:51 2021 +0200

    removed whitepaces

[33mcommit 3151eb57c90f1d46fb073132313ced9ce5f0746f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 03:01:45 2021 +0200

    [docs] updated documentation

[33mcommit a85674b37773fccd788fb2393472f58772534c0e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 03:01:22 2021 +0200

    [sw/lib] removed deprecated Zbb flag from SYSINFO

[33mcommit f9119439cbfc27b838658f305fb54a7673a46c58[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 03:00:22 2021 +0200

    [sw/Example/bitmanip] updated README

[33mcommit 75f32177979a9df5073fc41a215dd35587f75a02[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 03:00:05 2021 +0200

    [sim] updated B ISA extension generic

[33mcommit d3037b754b43aa53d4099000a3a295ce03c86a21[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 02:59:36 2021 +0200

    [setups/quartus] updated B ISA extension generic

[33mcommit 552463108d850e0b1df0fa60d3bcef014b17b2d6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 02:59:22 2021 +0200

    [rtl/system_integration] updated B ISA extension generic

[33mcommit 961f6f69392ed914bafc6de575db1ef30ddea014[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 02:58:31 2021 +0200

    :warning: [rtl/core] reworked bit-manip configuration generic
    
    removed `CPU_EXTENSION_RISCV_Zbb` generic; added `CPU_EXTENSION_RISCV_B` that will implement all available B subsets (Zbb and Zba) when "true"

[33mcommit 87b6dc8b5293820112959000ef893d80af31ac1d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 02:56:49 2021 +0200

    [sw/example/bitmanip_test] added Zba tests and intrinsics

[33mcommit 3a9c2332a110b0cbb6f9314a1492bda79bd218ff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 29 02:54:31 2021 +0200

    [rtl/core] bit-manip CP: added Zba subset
    
    Zba = address generation instructions, subset of bit-manipulation (B) ISA extensions

[33mcommit b8a2a677d49f79f85d768911be368612b73acc6c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 28 21:58:16 2021 +0200

    [docs] minor edits -> "execution safety"

[33mcommit 94837562cfa30b8742a92ee13e858cfbc526dc3e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 28 17:26:11 2021 +0200

    [rtl/core] minor comment edits

[33mcommit 97d8345ba56dc99e59a8c5b11776a7b74b005903[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 28 17:25:54 2021 +0200

    [CHANGELOG] typo fix

[33mcommit a7c34410fa1c8e6a453a68cac154c23251c14cad[m
Merge: a5c2754e a4b412d1
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 28 14:58:20 2021 +0200

    Merge pull request #188 from stnolting/ulx3s_increase_mem_size
    
    [setups/ULX3S] increase memory sizes to "default"

[33mcommit a4b412d133d91770da5ba86572235f645317266c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 28 14:29:01 2021 +0200

    increase memory sizes to "default"

[33mcommit a5c2754eeeb150094c10ce0846adbf43f95d2c64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 23:52:04 2021 +0200

    :bug: [rtl/core] fixed imprecise illegal ALU-class instruction exceptions
    
    `MEPC` and `MTAVL` did not reflect the correct exception-causing data for illegal ALU-class (non-multi-cycle like `SUB`) operations; minor critical path improvements

[33mcommit 32bcd9d8ebfa23f82295ad207d6db575391639b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 23:50:50 2021 +0200

    bootloader: added exception error code #169

[33mcommit 532b8d5d90e21fdf3c2efd4fa580eaba54b81b80[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 23:26:12 2021 +0200

    [sw/example] processor_test: added PRECISE illegal instr. checks
    
    MTVAL and MEPC always have to reflect the _PRECISE_ state of the exception-causing instruction

[33mcommit a5f33eebc46f938ec004dea3689d86f4f4abed5f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 23:24:10 2021 +0200

    [docs] clearified execution architecture (pipeline vs multi-cycle)

[33mcommit 935f43c26a89a35db0e035550ac636a06af823af[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 15:34:38 2021 +0200

    [docs/datasheet] faster execution of FENCE.I

[33mcommit 817820ae759feb3cccbf003883f18eeb9364cf04[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 15:34:13 2021 +0200

    [rtl/core] CPU: minor control logic optimizations
    
    * faster execution of `fence.i`
    * reduced HW footprint
    * shortened critical path (PC update logic)

[33mcommit afcba6249f8d655adb5a882e07acb9cc139df7ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 00:36:52 2021 +0200

    :bug: fixed bootloader stack pointer init #169

[33mcommit 71697367b4ca1644b9956b9f2627825e71e157e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 00:34:57 2021 +0200

    :bug: [sw/common/crt0.S] fixed bootloader SP init
    
    always use SP symbol from linker script; linker script automatically defines a data memory size of 512 bytes when making the bootloader

[33mcommit 3c82001740e270082e96847f824441b4ca3c3cd9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 27 00:33:45 2021 +0200

    [sw/bootloader] minor edits and reverts

[33mcommit 694b329aeb59025650b8b02a3b1fe59bcd0c8798[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 26 23:57:27 2021 +0200

    [rtl/core] minor VHDL code clean-up

[33mcommit ca2b1bcd45397580235c81193526e386aaecb1e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 26 18:07:37 2021 +0200

    [sw/lib] comment typo fix

[33mcommit 19d26276a80f4c3b149f4e18e2df632d0483b9ae[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 25 12:43:26 2021 +0200

    [CHANGELOG] minor edits

[33mcommit 9bf398820f82f78d333639d172b203011f2c1d28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 20:00:48 2021 +0200

    [CHANGELOG] added v1.6.2.7

[33mcommit 905031959bcdc4b6d1381cf377fe4030217620b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:59:53 2021 +0200

    [rtl/core] updated pre-build memory images
    
    due to updates in sw libraries and crt0.S

[33mcommit 0afa308f0f6cdb9841d5909d54ee22cc94d46800[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:57:05 2021 +0200

    [sw/common] improved crt0 for bootloader case
    
    If compiling the bootloader, the stack pointer initialization is made based on the actual physical memory configuration. Hence, the bootloader is now even more independent of the platform-configuration.

[33mcommit 9f158b77960d6afe3b2a970bced30d342b9879c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:55:44 2021 +0200

    [sw/bootloader] stall if unexpected exception

[33mcommit ef8432cdec4d3894667fcc545304539136b5b7a4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:54:46 2021 +0200

    [rtl/core] UART: minor edits
    
    The updated logic seems to require less logic (tested on Quartus and Radiant)

[33mcommit 045b979800bd6b6748cd49fd5d951ed8139c5ea1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:53:27 2021 +0200

    [sw/lib] UART: faster execution of UART setup function
    
    let gcc handle division operations if M instruction is not available; except for compiling bootloader: here we need MINIMAL code size

[33mcommit 2718894d466582a7db9367904fe376c5f521c91f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:51:20 2021 +0200

    [rtl/core] minor ALU logic optimization

[33mcommit 36c0445b9f2126c1bf57957e792ab88b4336e2e8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:50:41 2021 +0200

    [rtl/core] minor control unit fixes
    
    When loading two half-words of an unaligned 32-bit instruction, the instruction issue unit now also checks that both of the fetched half-words did not cause any bus exceptions.

[33mcommit 490ab163e0f11fc74b139c24fd009c8e250a7b49[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 24 19:40:33 2021 +0200

    :bug: [rtl/core] fixed bug (introduced in v1.6.2.4)

[33mcommit cde357b5486662050d72a3c7a91c7085caa553f9[m[33m ([m[1;33mtag: nightly[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 16:06:45 2021 +0200

    [rtl/core] UART: improved start-bit detection
    
    -> more "spike"-resistant

[33mcommit 75307a2257050f40032b3230efcf813ea4827453[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 16:04:51 2021 +0200

    [rtl/core] SPI minor edits

[33mcommit e9ddf8b596c650cdbae9283b4f828d44f098cf16[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 16:00:07 2021 +0200

    [docs/datasheet] minor edits

[33mcommit 418d2c94b3348d8a0e1add6585821e6538b65c64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 07:40:33 2021 +0200

    [rtl/core] comment fixes / clean-ups

[33mcommit bd1efbe4557631289886aca1693324e96f10dc77[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 07:40:07 2021 +0200

    [rtl/core] minor VHDL code fixes / clean-ups

[33mcommit e2ff9077834ff3651119c1389b2719cbc5df79bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 07:36:24 2021 +0200

    [sw/lib} RTE: removed terminal whitespaces

[33mcommit c2f8bab4f7fb2ad18e1af4196cc97652892f43b4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 21 07:35:43 2021 +0200

    [docs] typo fixes

[33mcommit 3cbf78056a9f6d5492775f0ddd395f110e9267f6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 20 15:36:49 2021 +0200

    [rtl/core/SPI] hardware optimization / code clean-up

[33mcommit 4def0602e1695ceae2309f9259efbdc45a5eabc3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 20 15:36:02 2021 +0200

    [sw/example/hex_viewer] added byte/half/word modes #169

[33mcommit f0fea2da739d0909bebadcc34aa4b3c6645db41b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 20 15:34:26 2021 +0200

    [docs/datasheet] minor additions

[33mcommit 9aa0b07d62ae9cd111b3f04db204f0bc26183165[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 20 15:33:51 2021 +0200

    [sw/lib] added non-blocking SPI access functions

[33mcommit 7e7da1434f2c5781969ed48e16d97a61e427f5ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 19 21:28:25 2021 +0200

    [docs/figures] minor edits
    
    -> layout fixes, clean-up

[33mcommit 4e3d49521934a95c1f8ae1c4b3dd99da3f16788e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 19 17:19:03 2021 +0200

    [docs/userguide] minor typo fixes

[33mcommit 114a20203585f2e5d083aeb774f6c03f3efc940b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 19 17:18:50 2021 +0200

    :sparkles: [sw/lib] added central list of module's FIRQ aliases
    
    * mie CSR bit
    * mip CSR bit
    * mcause trap code
    * RTE trap ID

[33mcommit 50c659073c2387cc64fa19cd60d91822a746bd2d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 19 17:17:48 2021 +0200

    [sw/lib] doxygen format fixes

[33mcommit fdaaba3a0dfdf167066f79383d9497b6fb1c177b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 18:36:08 2021 +0200

    🪁 added link to upstream Zephyr RTOS support
    
    https://docs.zephyrproject.org/latest/boards/riscv/neorv32/doc/index.html
    
    port provided by @henrikbrixandersen

[33mcommit 5a50a38158a427028ad4ed354b4f61808e41fa78[m
Merge: 36824afa be6b54ab
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 18:31:39 2021 +0200

    Merge pull request #186 from gottschalkm/reduce_function_fix
    
    fix: reduce_f won't work with single bit operands

[33mcommit be6b54abd6f8645b975ee9e0851466bbed0e4ac3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 18:23:59 2021 +0200

    updated version (v1.6.2.2)

[33mcommit 36824afae2eceecf56c7c571c332b9ad3413be23[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 17:35:53 2021 +0200

    [docs/datasheet] SPI: minor fixes

[33mcommit d258a3e5f9bd61eafbde78f89f0a3552e16b35ae[m
Author: gottschalkm <marcus.gottschalk@posteo.de>
Date:   Mon Oct 18 17:11:08 2021 +0200

    fix: reduce_f won't work with single bit operands

[33mcommit d2fb1cec8becabfde47ca2d4337dac25cb220c7d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 15:44:02 2021 +0200

    [docs/userguide] added note: tri-state driver in vivado block designs

[33mcommit 217244fcaa78a6c74b013163a1944d99b85f192c[m
Merge: 9f468655 c02cdd7e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 15:10:49 2021 +0200

    Merge pull request #185 from stnolting/add_spi_cpol
    
    [SPI] Add CPOL (clock polarity) configuration option

[33mcommit 9f46865559073cc5451bb9324338880170995633[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 00:15:44 2021 +0200

    [README] minor edits

[33mcommit c02cdd7ec23b655e6b7f775ae2e6be2b4d5bdb9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 00:14:14 2021 +0200

    [CHANGELOG] date fix

[33mcommit 000ba468c6fd9c33b95aadd2ad5bd3b2c30bbd19[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 00:13:56 2021 +0200

    [docs/datasheet] typo fixes

[33mcommit a8af1db2ea090de6795712d4e945da89e9bd3898[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 18 00:13:36 2021 +0200

    [rtl/core] removed whitespaces

[33mcommit 5de07aee5daff234254648a45f99c0baa199de68[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 23:55:43 2021 +0200

    [docs/datasheet] added SPI clock mode table

[33mcommit 1c94f054f3af1dc2a86b5b980fe75f8995ea4be1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 10:32:29 2021 +0200

    [CHANGELOG] added v1.6.2.1

[33mcommit 8c2562c3890e7512e85b29ed1398c33834726070[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 10:32:12 2021 +0200

    [rtl/core] added CPOL bit; code clean-up & optimization

[33mcommit 682f4cb4d7b798560e5c5c545f6e48b92c09133a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 10:30:41 2021 +0200

    [rtl/core] updated pre-built bootloader image
    
    due to updated SPI setup function

[33mcommit 614a1cc27273a73529bec10161a61d2d13856e03[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 10:11:33 2021 +0200

    [docs/datasheet] update

[33mcommit 59ec2b557f17c26b1ed36988998b51e904ae7dd6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 09:58:31 2021 +0200

    [sw/bootloader] updated SPI setup of bootloader

[33mcommit da58d05767ab5c6c6cb245eda951e98781b89ecc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 09:58:15 2021 +0200

    [sw/example] updated processor_check

[33mcommit 5367b377a0d15dc37c614b6a395bf152378c70d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 09:57:49 2021 +0200

    [sw/lib] updated SPI drivers (setup function)

[33mcommit 4106ae821be739ed9464e0affe2c10d42290fd9a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 09:36:47 2021 +0200

    [sw/lib] added new SPI CPOL bit defintion

[33mcommit cc3d65286674bec7c65ca977b31e4bc34f2dc515[m
Merge: 784ae96b f1fadaa7
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 09:18:56 2021 +0200

    Merge branch 'master' into add_spi_cpol

[33mcommit f1fadaa7697e0118b7071ad45240ffe3607b939e[m[33m ([m[1;33mtag: v1.6.2[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 09:07:37 2021 +0200

    preparing new release (v1.6.2)

[33mcommit 784ae96ba725ad7bf7a63924a775dc5386499925[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 08:46:43 2021 +0200

    [docs/figures] added license.mde

[33mcommit c16a433afe65a67f609a2dd32add7362dfe06bef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 08:45:17 2021 +0200

    [docs/figures] added SPI clock modes diagram
    
    source: https://en.wikipedia.org/wiki/File:SPI_timing_diagram2.svg
    
    :copyright: license:
    * Creative Commons: https://en.wikipedia.org/wiki/Creative_Commons
    * Attribution-Share Alike 3.0 Unported: https://creativecommons.org/licenses/by-sa/3.0/deed.en

[33mcommit 0533c0cfc3bf900bd63dd389408a2866ce90cf35[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 06:53:40 2021 +0200

    [sw/lib/SPI] fixed confusion CPOL vs CPHA
    
    the clock phase CPHA of the SPI module can be programmed, the clock polarity CPOL is fixed (but can be modified using an external inverter)

[33mcommit b5578d05cd85cd2303c1f0142594f5a3cb3f5195[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 06:51:58 2021 +0200

    [sw/datasheet/SPI] fixed confusion CPOL vs CPHA; typo fixes

[33mcommit 5b6b9d5f61d0d50bf7e6383e0cb2b3a01afe45c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 06:51:00 2021 +0200

    [sw/lib] comment typo fixes

[33mcommit 126054be00755572b310a33fb9c05becd7b571a2[m
Merge: c4d18030 3955f4bd
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 06:19:56 2021 +0200

    Merge pull request #184 from stnolting/make_march_variable
    
    :warning: [makefile] modify handling of MARCH and MABI variables

[33mcommit 3955f4bd316784207e3ccd0f44841319f60d1e6c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 17 06:10:45 2021 +0200

    fixed version number

[33mcommit c4d1803070e30f900941a5fa4b88bc0414381a42[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 16 11:40:52 2021 +0200

    [README] added link to Avalon-MM wrapper

[33mcommit 64e8b0e1945f560e72c10aa1db0bf6a67dd30fc6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 17:53:17 2021 +0200

     [CHANGELOG] added note regarding this PR

[33mcommit d97df66b39bc386614d8170efdb3a8aa4cf7e5e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 17:20:55 2021 +0200

    :warning: modify handling of makefile's MARCH and MABI variables
    
    Pre-commit usage: `MARCH=-march=rv32i`, the `-march` should not be here
    
    Post-commit usage: `MARCH=rv32i`, this is more straightforward
    
    Same for `MABI=-mabi=...` -> `MABI=...`

[33mcommit 0c19addb1907697c04bd70abab5c1f0aa55aa16c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 16:57:37 2021 +0200

    [CHANGELOG] added v1.6.1.12

[33mcommit 1543e947455708c1233d3fc899fb348267d01ffe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 15:53:14 2021 +0200

    [docs/datasheet] CFS: minor rework

[33mcommit 1fc7c4e93446746896b5a00aed58598b942c8a4f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 15:52:52 2021 +0200

    [sw/lib] comment typo fixes

[33mcommit 130d3f60915c8478f01a276e869754cd24eb0f30[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 15:52:36 2021 +0200

    [rtl/core] minor CPU control logic optimization

[33mcommit d73b919b6b44bc650a93b1fad37f17771eeefe28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 15:52:21 2021 +0200

    [core/rtl] removed CFS sleep_i input

[33mcommit f90bcf17bdd0a1e1256ff0f74c2f7a617ed63fd6[m
Merge: 06d1a7cd 756acff1
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 15:44:41 2021 +0200

    Merge pull request #183 from stnolting/add_uart_fifos
    
    [UARTs] add RX & TX FIFOs

[33mcommit 756acff10ffae622a0e5908e10cd23ee51bdcb96[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 15 15:32:16 2021 +0200

    [sw/lib] comment typo fix

[33mcommit 2e7a69378bf938fd224943a277b209895cd03efb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 22:50:05 2021 +0200

    [CHANGELOG] added v1.6.1.11

[33mcommit ea0230240c960aa84458c42a6c122bb8edeb2b65[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 22:49:12 2021 +0200

    [docs/datasheet/soc_uart] update/rework

[33mcommit 3e3b67c456fd6d0ec603839b1bdd04c94544242d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 22:48:05 2021 +0200

    [rtl/core] updated pre-buld memory images
    
    * bootload and blink_led example
    * now using updated UART functions (using FIFOs if implemented)

[33mcommit 4c6a6322298adad10a2e7a8f53eb8c4ac84c14fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 22:46:39 2021 +0200

    [rtl/core] tie IRQ config to zero if according FIFO size is 1

[33mcommit 7f027b8e5860c10c6c47136c417d0f16a2abc968[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 21:30:01 2021 +0200

    [sw/lib/source/neorv32_uart.c] updated UART drivers
    
    * `neorv32_uart*_putc`is now checking if TX FIFO is full before densing new data (-> faster)
    * `neorv32_uart*_tx_busy` also checks if TX FIFO is empty
    * reworked `neorv32_uart*_getc_safe` (return codes)

[33mcommit 2581d876fbba6be56f70faafa81d8af5af66d3c4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 21:27:51 2021 +0200

    [docs/datasheet/soc] added new UART FIFO configuration generics

[33mcommit 3d5e931129d33469f42dc4d5d53557ac8db16a47[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 17:36:04 2021 +0200

    [sim] added UART FIFO generics to TBs

[33mcommit cea9171af77101e4613f0cab74a04ba4b29911b3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 17:35:39 2021 +0200

    [rtl/system_integration] added new UART FIFO generics

[33mcommit a220c62ce185e554536ff404c83e5be7845b966d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 17:34:54 2021 +0200

    [rtl/core] added UART FIFO configuration generics

[33mcommit 3c13981a585cf4af6fdcb9a95aa25036979ca837[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 17:12:31 2021 +0200

    [rtl/core] added UART rx & tx FIFOs
    
    Note that the FIFO config generics have not been exposed yet. The current VHDL code implements FIFOs with 4 entries each. Since _NO_ software driver have been modified yet, this commit is some kind of backwards-compatibility check

[33mcommit a2e8f7c87d46c9a353fb88a1d9f3ac4b03084d20[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 16:37:44 2021 +0200

    [sw/lib/include/neorv32.h] added new UART FIFO flag definitions

[33mcommit 06d1a7cd28ac4456d6926e96926413cb0d37b040[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 16:35:29 2021 +0200

    [docs/userguide] fixed official RISC-V gcc github link

[33mcommit 38ed476bd5806c3ce7dad4bc3eada734c41336b3[m
Merge: 0914e104 9524be7d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 16:27:05 2021 +0200

    Merge pull request #182 from stnolting/slink_fine_grained_irq
    
    [SLINK] add fine-grained IRQ configuration

[33mcommit 9524be7d01fac75daac224bacba63bccfa875175[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 01:38:40 2021 +0200

    [CHANGELOG] added v1.6.1.10

[33mcommit 4ba5afcd6d4bfb73754ec6dd914a1bf8bc84f9ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 01:38:06 2021 +0200

    [rtl/core] version update

[33mcommit 1473e4766b7e39600fff6950f7771e78b9b4d062[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 01:37:40 2021 +0200

    [rtl/core] commet edits

[33mcommit 20754f0c7747265b4b997202465ff795cc61b579[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 14 01:37:27 2021 +0200

    [rtl/core] minor FIFO component optimization (SYNC read)

[33mcommit aa858daef056f45ae8243dbebe2898ee7a984a32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 19:32:03 2021 +0200

    [docs/datasheet] updated documentation

[33mcommit 082a08b57a42ef9f174c24b6a5fb32696a2da984[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 19:01:08 2021 +0200

    [sw/example/processor_check] updated IRQ config

[33mcommit bad04c6f7565a003aaee5637e076a0e8b5b36b9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 18:53:54 2021 +0200

    [sw/lib] added SLINK IRQ configuration functions

[33mcommit 92813abfcd169218f99bf35739e257adc37d6dbe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 18:53:22 2021 +0200

    [sw] added new SLINK IRQ register and bit definitions

[33mcommit d94b4f295826423a09f49151c32c8e4ca0f77e9f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 18:52:55 2021 +0200

    [rtl] tie IRQ type bits to one of FIFO_SIZE = 1

[33mcommit 18315432bae4528a73774d9d84b8fd29e07a9658[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 18:31:50 2021 +0200

    [rtl] added fine-grained IRQ configuration
    
    for each RX & TX link:
    * IRQ enable
    * IRQ type (FIFO state)

[33mcommit 0914e104253bd5e49b14d27dcfdb46ccdccc3734[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 16:39:54 2021 +0200

    [CHANGELOG] added v1.6.1.9

[33mcommit 82a82293e122fb7bf75d07463c91c2fff876bcc3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 16:39:14 2021 +0200

    [docs/datasheet] added NEOLED IRQ config flag
    
    + em/en dash clean-up #181

[33mcommit 4de071d0a894296965bd01a1446280a142cc6a2d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 16:38:32 2021 +0200

    [sw/lib] added NEOLED IRQ config flag
    
    + minor comments clean-up

[33mcommit 0110348ffda6991c5674b7e20750008de7e1cae2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 16:38:07 2021 +0200

    :sparkles: [rtl/core] NEOLED: added IRQ condition config flag
    
    bit 27 of NEOLED control register (r/w):
    
    * `0`: IRQ if TX FIFO if less than half-full
    * `1`: IRQ if TX FIFO is empty

[33mcommit a31a24e94086ac558f710e57c663ad745b1066e3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 13 11:31:24 2021 +0200

    [docs] fixing #181 (em dash vs. en dash)

[33mcommit 9e18b2eb6fcb385e477cf48a1cb5e3579ec90859[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 12 16:24:55 2021 +0200

    [sim/TB] added 8-entry NEOLED FIFO configuration

[33mcommit 758411d941d2ded4138cdd293969dee3881a71fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 12 16:24:27 2021 +0200

    :sparkles: [rtl/core] simplified FIFO level monitoring logic
    
    now using dedicated "half_o" FIFO status signal; reduces area footprint and critical path of level monitoring logic

[33mcommit 3f9e608b8a8e5ec82236b2a7439952224867982b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 12 16:21:41 2021 +0200

    [docs] removed legacy primary UART wrappers

[33mcommit 7310a944c7c3e6375ea49db334ec851403f3d9f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 12 16:20:30 2021 +0200

    [rtl/core] added dedicated "half_o" signal to FIFO component
    
    indicating "FIFO at least half-full" condition; simplifies FIFO-external level monitoring logic

[33mcommit e44cb4381994a15f8e7dfe59a334878b08a5c68c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 12 16:19:18 2021 +0200

    [sw] no more use of "neorv32_uart_*" legacy wrappers
    
    all of the software is now using `neorv32_uart0_*` instead of the legacy wrappers `neorv32_uart_*`; however, the legacy wrappers are still implemented

[33mcommit f81370ab2cbfc0e95309e3ce31474cfc276e5f13[m
Merge: cc9a2652 c0d3222e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 11 16:15:26 2021 +0200

    Merge pull request #180 from henrikbrixandersen/neoled_datasheet_fix_formatting
    
    [docs/datasheet] fix neoled register table formatting

[33mcommit c0d3222ea6004bd57a9bcac3e617240921783d51[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Mon Oct 11 10:14:29 2021 +0200

    [docs/datasheet] fix neoled register table formatting
    
    Fix the formatting of the NEOLED register table after adding missing bit decriptions.
    
    Fixes: 70ae1b4df33eac2e5a90fee54c922d3ad26d6875

[33mcommit cc9a26523b842cc5775e7b5920e0531eb8e278dc[m
Merge: 77f9321e 9f1fdd51
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 11 08:55:21 2021 +0200

    Merge pull request #178 from henrikbrixandersen/axi_wrapper_add_neoled_fifo
    
    [rtl/system_integration] add NEOLED FIFO depth generic to AXI4 wrapper

[33mcommit 77f9321e13a67f67854f0badb11758581903b7c3[m
Merge: f2a261c3 70ae1b4d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 11 08:54:31 2021 +0200

    Merge pull request #177 from henrikbrixandersen/neoled_fix_datasheet
    
    [docs/datasheet] Fix neoled register bits documentation

[33mcommit 9f1fdd51f8198f75550e986059f83ae6143bfc94[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Sun Oct 10 12:17:17 2021 +0200

    [rtl/system_integration] add NEOLED FIFO depth generic to AXI4 wrapper

[33mcommit 70ae1b4df33eac2e5a90fee54c922d3ad26d6875[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Sun Oct 10 12:14:25 2021 +0200

    [docs/datasheet] Fix neoled register bits documentation
    
    Add the missing NEOLED register bits to the datasheet and fix the naming.

[33mcommit f2a261c3eb16be83b7db6be8ccc66acf71919f37[m
Merge: 3dc43e82 8c92b2f0
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 9 16:05:09 2021 +0200

    Merge pull request #176 from stnolting/firq_level_triggered
    
    [rtl/core] make FIRQs level-triggered

[33mcommit 8c92b2f0c3d3d82cdcb7de2cc90e1dd8e0d0c15f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 9 10:58:34 2021 +0200

    [sw/example/processor_check] minor runtime optimizations
    
    enable FIRQs only when they are actually relevant for evaluation

[33mcommit 27b54384d2cce1f311e0c35b1291e181406ea465[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 9 10:14:00 2021 +0200

    WDT: fixed race condition in IRQ ack
    
    a pending WDT interrupt is cleared by resetting the watchdog (or by disabling it)

[33mcommit 11f9f61daeca1d9dfe71dd69dad63efd0508eda4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 9 08:42:58 2021 +0200

    [CHANGELOG] added v1.6.1.7

[33mcommit 8bed94c8abcee46fc18419011d2aaaf9e6c0e6b6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 9 08:42:12 2021 +0200

    [docs/datasheet] OCD: added WFI note
    
    a debugger halt request will also resume CPU operation after it was send to sleep mode via 'wfi' instruction

[33mcommit 786dc0e0f79198077b1fd476ac6431269c0bbdfd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 23:03:04 2021 +0200

    [sw] minor edits/fixes
    
    - console output edits
    - procesor_check: test if PMP test fails because of locked PMP entry
    - fixed minor issue in number of PMP entries evaluation

[33mcommit 948c4dc045b530a8910bdb8747aea7f8b6d64a64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 20:12:15 2021 +0200

    [sw/example/processor_check] minor console output edits

[33mcommit 0aff08cb8cb00ecf641a8b4a057f260d962ca26b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 19:40:44 2021 +0200

    [NEOLED] fixed IRQ corner case if FIFO_DEPTH=1

[33mcommit 982c5e566e5f1aabf19a76707d569d4e49f2eab4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 19:26:18 2021 +0200

    :wrench: updated processor_check test program

[33mcommit a4f1e5c1fbded0cad70ce86bd6a96260598f1865[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 19:25:12 2021 +0200

    :books: [docs/datasheet] FIRQ updates

[33mcommit 7f9fc732863d618afe2ac737eeb6167248aa770c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 19:24:10 2021 +0200

    [rtl/core/WDT] fixed IRQ request clear
    
    request is cleared by reading or writing control register

[33mcommit 3dc43e82be860b4f8304298d7497034d1e29c08c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 18:56:01 2021 +0200

    [sw/common] fixed 'sim' target
    
    also requires 'install'

[33mcommit 29f88b733309142560874266f3528920e8398962[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 18:02:59 2021 +0200

    [rtl/core] version update

[33mcommit b275c31c0bf186434306a6845e5f89b0fabaed6b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 18:02:46 2021 +0200

    [rtl/core/WDT] FIRQ update

[33mcommit ecfde22b9af2d183bf06a29d2e3fe39b1604afd6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 18:00:42 2021 +0200

    [rtl/core/SLINK] FIRQ update

[33mcommit 4064232673402977c8fee7795fc683ea4985dd42[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:59:41 2021 +0200

    [rtl/core/UART] FIRQ update

[33mcommit bd00b5fb797a83beb98cd8c67e13792380e12ce5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:56:57 2021 +0200

    [rtl/core/NEOLED] FIRQ update

[33mcommit 0c49e5e303d373c9dd0f7ff3540a768d1391389b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:56:27 2021 +0200

    [rtl/core/TWI] FIRQ update

[33mcommit e2d24ad493fac32d87c6822728ec18a3144db2d4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:55:57 2021 +0200

    [rtl/core/SPI] FIRQ update

[33mcommit 99a9e52e5926739866c8648cc5b620f1b7895b1a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:52:48 2021 +0200

    [rtl/core/XIRQ] FIRQ update

[33mcommit e48b1506db1b09b451b21ee1c6b0fa0fb0ba0e83[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:52:29 2021 +0200

    [rtl/core/CFS] FIRQ update

[33mcommit 5bd040c9831c7e8a9f73e28f251d4defd90bef44[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:30:21 2021 +0200

    [rtl/core] removed FIRQ buffer; FIRQs are now level-triggered
    
    the CPU fast interrupt requests are now level-triggered and need to stay asserted until explicitly acknowledged by the CPU (by reading/writing specific memory-mapped registers in the IRQ-causing peripheral module)
    
    -> to comply with the RISC-V priv. spec; especially the behavior of MIE and MIP CSRs

[33mcommit 22125a66fe14a5e8a68b65eec8771cc2421e118d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:25:36 2021 +0200

    [sw] minor UART console output formatting edits

[33mcommit 19ef952621a594fa9c3af68c79f64e76caaea838[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:24:53 2021 +0200

    [setups/README] minor format edits

[33mcommit c34416b80b34d448c0e4da4f3691c16a8b190359[m
Merge: ac2d8e07 8d7b95a0
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:09:13 2021 +0200

    Merge pull request #175 from henrikbrixandersen/vivado_setups_filesets
    
    [setups/vivado] Fix fileset paths

[33mcommit ac2d8e07c66c0db2bb03f399d13d2f16190d9b4d[m
Merge: dd62d540 c41b5b28
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 17:08:23 2021 +0200

    Merge pull request #174 from henrikbrixandersen/vivado_xhub_stores
    
    [setups/vivado] Mention the XHub Stores menu item for installing board support

[33mcommit 8d7b95a0d3266ca2cead47ca9f155964814514f3[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Fri Oct 8 16:53:15 2021 +0200

    [setups/vivado] Fix fileset paths

[33mcommit c41b5b285f71990960978e6a1947a7c0fd73183c[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Fri Oct 8 16:34:32 2021 +0200

    [setups/vivado] Mention the XHub Stores menu item for installing board support

[33mcommit dd62d540bd571e00800b624ccb742fac7a48546f[m
Merge: 29e33a4b b8aa03d4
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 8 15:22:01 2021 +0200

    Merge pull request #173 from torerams/make-ci-flow-argument
    
    Add argument to makefile to simplify Continuous Integration flow

[33mcommit b8aa03d4f499df158a55423ea8b03ce75231c67f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 7 13:24:01 2021 +0200

    [docs] updated SW documentation

[33mcommit 8fd760e3b7bf38e73f616e9c2f720ed5a9946b31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 7 13:23:41 2021 +0200

    minor fixes/updates

[33mcommit 959c90597366008a8d957122eaf5b8a7cde8dabc[m
Author: Tore Ramsland <tore.ramsland@firmwaredesign.no>
Date:   Thu Oct 7 09:39:02 2021 +0200

    Added targets to makefile to make local application and bootloader images

[33mcommit 29e33a4b075d4b1c742059123201bcd8b7d13b28[m
Merge: 69d5a54c 4e69670f
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 15:32:27 2021 +0200

    Merge pull request #157 from stnolting/custom_cfs_example_setup
    
    [setups] added CRC32 custom module tutorial (link), by @motius

[33mcommit 69d5a54c48a9e0e8ccf78c646d0ee8befe0118cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 12:10:01 2021 +0200

    [CHANGELOG] added v1.6.1.6

[33mcommit 8478d919dac7a1f3eea7aa18a7c502eec45e6446[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 12:06:57 2021 +0200

    [setups/radiant/UPduino] fixed processor configuration

[33mcommit c52fad87bbc69b27ae49d4707ab3ac8e1cead8c1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 12:05:40 2021 +0200

    [rtl/core] TWI signals are tri-state if not used

[33mcommit a034c95edcd12dacb0880556a0eac4453070d338[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 03:56:54 2021 +0200

    [rtl/core] minor fix in CPU HPM counter
    
    -> multi-cycle ALU wait cycles

[33mcommit 90da4991634d012fe91ef81f4b607f57ba2eefcd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 03:56:05 2021 +0200

    [README] minor edits

[33mcommit 460fcbd0b43e122aa71e321f42792f298af05c92[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 6 03:55:33 2021 +0200

    :bug: [setups/radiant] fixed signal assignment

[33mcommit 2f6c8aa57b1801d34c585c6ee059cadcec426859[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 5 21:06:34 2021 +0200

    [sw/example] benchmarks: minor fixes

[33mcommit ff7874eabdb0b8b8f5fbae9077a5cec7cf1ec132[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 5 15:57:40 2021 +0200

    [docs/datasheet] OCD: added "watchpoints" note

[33mcommit bef3e1380bcd7421c661bcc3a88430bd1ec739e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 5 15:57:08 2021 +0200

    [bootloader] minor edits
    
    "neorv32_cpu_get_systime" executes faster than "neorv32_mtime_get_time" and also returns system time

[33mcommit 7c746ceebcefea8dfdbeb5108c53d6f70043eed0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 5 15:56:27 2021 +0200

    :lock: [rtl/core] illegal instructions do not commit state changes
    
    + CPU control logic optimization (smaller footprint)

[33mcommit fc103f71fdaebb7dca794040f47cd68e601c74d6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 5 07:58:51 2021 +0200

    [rtl/core] logic optimization / cleanup

[33mcommit 6120589cff62748215ca202bf75dc083c6e25bfa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 4 21:01:39 2021 +0200

    [rtl] moved CPU comparator to ALU

[33mcommit b79840f6486367a9205637650912f09c14215fdf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 4 21:00:00 2021 +0200

    [sw/lib] added OCD address space define

[33mcommit c98cc210497cd2e4f75f0aa45d913ab37757ed43[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 4 19:02:17 2021 +0200

    minor typo fixes

[33mcommit 3917e544d457f1f16131f9d9a85b5570a644cb1d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 4 19:02:06 2021 +0200

    updated riscv-arch-test submodule

[33mcommit b0c4b6cb19be712f2584556c7469f70a63132c35[m
Merge: 1ade1367 da9b250c
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 3 22:17:29 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit da9b250cad785681f1234b0eefa7373beab66db9[m
Merge: d10b4956 fda87119
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 3 22:13:34 2021 +0200

    Merge pull request #170 from henrikbrixandersen/sysint_axi4_uart1_swap_fix
    
    :bug: [rtl/system_integration] fix uart1 rx/tx signals

[33mcommit fda87119569686ad49f7cf35220d24272a8c1489[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 3 22:01:29 2021 +0200

    updated version number

[33mcommit 635811e957a07443a59c25dfae4f6433972259e7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 3 22:01:16 2021 +0200

    [rtl/system_integration] fixed missing UART signal connection

[33mcommit 1e756df6362ecc1c805f911f4a99504ba10c4eab[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Sun Oct 3 21:35:37 2021 +0200

    :bug: [rtl/system_integration] fix uart1 rx/tx signals
    
    Fix routing of the UART1 rx/tx signals in the AXI4Lite system
    integration top.

[33mcommit 1ade1367a88d76aca9302cc27529d10b3aa7d267[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 3 18:21:02 2021 +0200

    [docs] OCD: minor edits

[33mcommit d10b4956072ef587ea60dd253b896b761432a798[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 18:16:25 2021 +0200

    :warning: removed mstatus.TW flag
    
    timeout wait: WFI instruction is now always allowed to execute in any privilege mode; minor control unit logic optimization

[33mcommit 6b2436307999686c01d9cffcc31d2a61c22d6a96[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 18:03:38 2021 +0200

    [docs] minor edits

[33mcommit 7004185d1135730d68dcb28d16fe46073c6a298c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 17:18:43 2021 +0200

    layout/typo fixes

[33mcommit 4ea246d8ff33eb77424b2982859424385172d3ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 17:14:56 2021 +0200

    [docs] updated section "adress space"
    
    added OCD address space

[33mcommit 661f8a332391907a6ce6eacd2b7ec533b594884c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 16:39:43 2021 +0200

    [CHANGELOG] typo fix

[33mcommit c406ff6061dfb2c1126f3ab79bc0262daafddb8d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 16:33:46 2021 +0200

    [OCD] wfi acts as nop in debug mode & single-stepping
    
    required by RISC-V debug spec

[33mcommit 8c9bd42a479051908a7ff14674435e48ed2c4ff5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 16:30:39 2021 +0200

    [sw] minor edits in atomic instruction documentation and testing

[33mcommit 2fa42f037e2a32870a365194ec00a8e3f9496fea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 16:28:59 2021 +0200

    [docs/datasheet] minor content & typo fixes

[33mcommit 322a635e83c26ebe3a248ec2c1a9216b956ec419[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 16:27:41 2021 +0200

    [sw/lib/include/neorv32.h] added OCD debug module definitions
    
    should not be used by application software; this is just for documentation

[33mcommit db07b3d2ed52ab979fa1630f02e25b1c7ffd826b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 1 16:08:29 2021 +0200

    [docs] removed google ID experiment

[33mcommit bdfebeaeede1b2583767e96453ee41c638712b7b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 29 18:42:11 2021 +0200

    [docs] testing google identification

[33mcommit 1c35a20556810b7dd94a381202e46b341f2871b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 29 13:11:21 2021 +0200

    [docs] added some keywords

[33mcommit 4e69670f65afef374b5e92d3e25d4b743c931879[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 29 13:10:10 2021 +0200

    [setups] removed term "CFS"

[33mcommit 257d1c8f77ed76f26aa3aa936040d00504e7cf5a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 16:55:59 2021 +0200

    [docs] DOI fix
    
    link now always points to the latest version

[33mcommit c938c6c2882e63b370f458cb2b0ddbcd121ea06e[m[33m ([m[1;33mtag: v1.6.1[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 16:26:32 2021 +0200

    [rtl/core] updated pre-built application image

[33mcommit af0f029fa293117e71a871d845d9f653823fbbfa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 16:25:12 2021 +0200

    :rocket: preparing new release (v1.6.1)

[33mcommit 60415610c09afde9f62760d90f26221f8438ce61[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 16:24:37 2021 +0200

    [docs/userguide] added how do breakpoints work

[33mcommit 96c7462ddc2c915dfbf60c450f04dc1933befeba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 13:17:35 2021 +0200

    :bug: [rtl/core] fixed bug in mtime coparator logic
    
    "time >= timecmp" was not evaluated correctly (the split subword comparator was incorrect), which missed the interrupt for some time/timecmp combinations

[33mcommit e3a07321ded0a6d9f6ad228b9ad9605a3b6330ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 11:56:07 2021 +0200

    [docs/userguide] minot edits
    
    compilation _with_ debug symbols

[33mcommit b97f6f2533719b30c0013db90faddf9a9083f4f6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 09:09:42 2021 +0200

    :bug: [rtl] fixed debug-mode IRQ prioritization

[33mcommit 6cebb7b20a01e8bda0a7ffa28cfcb3b1c88edfd8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 28 08:39:40 2021 +0200

    [docs] minor edits

[33mcommit 38becd40cdd227aa78a9cb89e9ff0de84a9e04b8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 27 21:17:00 2021 +0200

    [sw/isa-test] always enable Zifencei
    
    required by the riscv-arch-test framework

[33mcommit c34ac9a09a303a104cb4bb1f6ba2d466308dedb2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 27 20:48:06 2021 +0200

    [rtl] fence.i will trap if Zifencei is disabled
    
    * minor rtl comment edits
    * :warning: on-chip debugger / debug mode requires Zifencei ISA extension

[33mcommit 4d1585ddb7afeaf37a8073a5e5b4f6784e98eaaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 27 20:46:14 2021 +0200

    [docs] on-chip debugger requires Zifencei

[33mcommit 3e9ccfc62b77b6006cc3be844903c0e9224a32de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 27 20:45:48 2021 +0200

     [docs] fence.i will trap if Zifencei extension is disabled

[33mcommit de4afd601fabda762b8c6549e97b6592e7c1cc39[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 23 22:11:57 2021 +0200

    minor typo fixes

[33mcommit 925b7259d642d99e53a88801ec3111ac4fd9cab7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 23 21:23:44 2021 +0200

    [README] layout edits

[33mcommit fea4c73424d590e94b48efce341f5c1fe4a7e821[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 23 17:22:03 2021 +0200

    [setups/quartus] removed non-maskable interrupt

[33mcommit e683d54d3d38d1c89ef11edb26a19ab10b707bdc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 23 17:21:41 2021 +0200

    [docs/userguide] new section "Adding Custom Hardware Modules"

[33mcommit 213bb2c203390fad1110f15f037d543eb76a05f8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 23 14:49:13 2021 +0200

    [docs/figures] removed non-maskable interrupt

[33mcommit 346a530afda32bc85e65c4b9be415957e359682a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 22 20:48:03 2021 +0200

    [sw/isa-test] minor edits

[33mcommit c7596c3a102f70021525d2fc07ef21e9137db79b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 22 20:47:46 2021 +0200

    reworked external interrupts controller (XIRQ) handshake

[33mcommit 2673ac7b1a8952daffafc767d37db7b084677095[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 22 20:44:10 2021 +0200

    [docs] minor edits

[33mcommit 0acaa50ec439b57e72a902df4d6aa9ce823641ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 22 20:43:59 2021 +0200

    reduced CPU counter size does not trigger a trap anymore
    
    + minor control logic optimizations

[33mcommit cd81575ea0134bbc5b6bb64cbc535edc1dbf0838[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 22 10:04:12 2021 +0200

    :bug: fixed instruction alignment exception bug
    
    introduced in version 1.6.0.7

[33mcommit c646fa82fe25280365cd0ef029ee6254f625eb25[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 23:09:57 2021 +0200

    :warning: [docs] updates due to reworked CPU trap/exception/IRQ system

[33mcommit 0125f601cce90424ba09ffe95a146a3993da4e68[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 23:06:02 2021 +0200

    [rtl/core] updated pre-built blink_led image

[33mcommit 6cbabd6aee7eadb1a6ed18503957813759689dba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:56:00 2021 +0200

    [sw/example] reworked processor_check test program

[33mcommit 7ef5014884fce8d2dacc95f5a011311c243f5690[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:55:05 2021 +0200

    :warning: [rtl/core] mjor update
    
    * removed non-maskable IRQ (NMI)
    * changed trap priority order (to comply with RISC-V specs.)
    * RISC-V interrupts (MEI, MTI, MSI) are high-level-triggered and have to stay asserted until explicitly acknowledged
    * fixed minor bug in misaligned instruction detection logic

[33mcommit f5a246ea6be3add23c7cbda2012d61cb32318eb1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:48:59 2021 +0200

    :bug: [rtl/core] fixed instruction misalignment check logic
    
    PC(0) = '1' has to raise a misalignment exception, too

[33mcommit 3b76dbacbb9ef5da5a7d75ab6d07509486861be1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:48:07 2021 +0200

    [rtl] minor edits

[33mcommit b8de33997f4a5120705c81f78f541a97c78ddc40[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:46:20 2021 +0200

    [rtl/core] top: removed NMI, removed MEI & MSI edge detectors
    
    all RISC-V interrupts (MEI, MTI, MSI) are high-level-triggered and have to stay asserted until explicitly acknowledged

[33mcommit e111dcc342cf520ef7c6fddb44ba9bab966a8f0c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:42:58 2021 +0200

    [sim] testbenches: removed NMI, reworked MEI & MSI IRQ triggers

[33mcommit 9628a169cfa385c338e668d3e0f79ea3eb460f64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:39:29 2021 +0200

    [sw/lib] removed non-maskable interrupt (NMI)

[33mcommit 41c95c4a1214fce2cd11036089bcae9edc058fd6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:37:05 2021 +0200

    [sw/bootloader] minor edits

[33mcommit 0a9302a5ef3626f9ac564e56883a71fb1f2fea2c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:35:44 2021 +0200

    :warning: [rtl] removed non-maskable interrupt (NMI)
    
    -> was not RISC-V spec. compliant (at all)

[33mcommit defcd421a4ef49e87667fff85bc5860864ab2b66[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 21 22:34:45 2021 +0200

    [rtl] mtime IRQ stays asserted until ack

[33mcommit 0cf657f3722977eb33e6c179bc057014865849cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 20 15:52:25 2021 +0200

    mip and mtval CSRs do not cause trap on write anymore
    
    these CSRs are read-only for the NEORV32, but write accesses do not cause an illegal instruction exception anymore (to be compatible with RISC-V specs.)

[33mcommit 4446f15fff51f2d700ab4ffa8f0889bbdf83ea57[m
Merge: 2acb8182 edf86075
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 23:13:55 2021 +0200

    Merge pull request #165 from umarcor/doit-install-check
    
    [doit] add task BuildAndInstallSoftwareFrameworkTests

[33mcommit 2acb8182a116285c4eddea75b1b694df387fd4c2[m
Merge: d9aab991 9098a31d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 23:12:41 2021 +0200

    Merge pull request #166 from henrikbrixandersen/datasheet_soc_sysinfo_bits
    
    docs: datasheet: soc_sysinfo: fix NEORV32_SYSINFO.SOC bits description

[33mcommit d9aab9917a01b1363ab80bde2f9d54f2c657ff02[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 23:05:07 2021 +0200

    added "menvcfg[h]" CSRs
    
    * not actually used yet - hardwired to zero
    * required by the RISC-V priv. spec.

[33mcommit 9098a31dd7254301e4b804f6d91835728af8b347[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Sun Sep 19 22:51:36 2021 +0200

    docs: datasheet: soc_sysinfo: fix NEORV32_SYSINFO.SOC bits description
    
    Fix the descriptions for NEORV32_SYSINFO.SOC register bits 14 and 15.

[33mcommit edf86075abf2dfbe101fd482285023ee9398eb75[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jul 8 04:29:01 2021 +0200

    [doit] add task BuildAndInstallSoftwareFrameworkTests

[33mcommit 4a1ffa869de2b6014ec17bbfc0c9fbe1a47afc64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 21:30:27 2021 +0200

    [sw/lib] minor edits

[33mcommit 5e909ba78e5068f7bab4ba766e74105fdf1d620a[m
Merge: cbcd8eb3 531d2688
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 20:55:13 2021 +0200

    Merge pull request #164 from umarcor/doit-arch-tests
    
    [doit] add task RunRISCVArchitectureTests

[33mcommit cbcd8eb37ca1131ebef4b3c34917bf93af8380eb[m
Merge: 51f3b86c 1b1ae19e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 20:53:34 2021 +0200

    Merge pull request #152 from torerams/qsys_component
    
    Adding NEORV32 Qsys/Platform Designer component and AvalonMM Master Interface wrapper

[33mcommit 1b1ae19e630522b4a1c8fc0b652e555e6d0d7f3b[m
Author: Tore Ramsland <tore.ramsland@firmwaredesign.no>
Date:   Tue Sep 14 22:04:22 2021 +0200

    Created Qsys component, AvalonMM wrapper and example designs
    
    Moved Qsys component and rebased to new IMEM/DMEM split
    
    Fixed documentation

[33mcommit 531d26889801a6c302c15dd041e4ec4383275eee[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jul 8 04:32:49 2021 +0200

    [doit] add task RunRISCVArchitectureTests

[33mcommit 51f3b86c1491e22d9a28e9831c8928f61fcb3054[m
Merge: 9d3fc2e1 62228574
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 19 01:13:06 2021 +0200

    Merge pull request #163 from umarcor/doit-docs
    
    [doit] add task Documentation

[33mcommit 6222857465eba0df39595c27aad80efdad9cbefd[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Sep 18 05:39:07 2021 +0200

    [doit] add task Documentation

[33mcommit 9d3fc2e1dae53ff967521cce2c381ef83cad6650[m
Merge: e5952d55 ad076386
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 23:44:26 2021 +0200

    Merge pull request #158 from stnolting/rework_low_level_hw_access
    
    :warning: rework of low-level hardware access

[33mcommit e5952d5510a7a94e72045a72ac0b2b11c9f4baa1[m
Merge: b542c382 b0ea62d0
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 23:33:48 2021 +0200

    Merge pull request #162 from umarcor/doit-pages
    
    [doit] add initial doit file; add task DeployToGitHubPages

[33mcommit b0ea62d04325f3709b57726c2341e2e955c1ee58[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jul 7 20:10:39 2021 +0200

    update .gitignore

[33mcommit ad076386766cc320ecebb8551cf64bc11e5452f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 23:07:28 2021 +0200

    updated version and changelog

[33mcommit d9ad214a125f3bf0c6ccdae54db0f58b43fac02d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 22:50:23 2021 +0200

    [sw] added deprecated control register defines

[33mcommit 5f9bc45c5fa58d2277b2c9fd35c558157641d79a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Sep 18 05:39:07 2021 +0200

    [doit] rename dodo.py to do.py and make it executable

[33mcommit daff79af189a1661bcc03128221977da147b4c05[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Sep 18 22:32:02 2021 +0200

    [doit] add initial doit file; add task DeployToGitHubPages

[33mcommit b542c38278a1d8b26d13d4f458765fecbba413d2[m
Merge: 3a9558c3 31aff329
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 22:01:51 2021 +0200

    Merge pull request #161 from umarcor/containers-bullseye
    
    [containers] update from Debian Buster to Debian Bullseye

[33mcommit 31aff3290bcb4570112f08cb07742259b992c00a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Sep 18 05:27:39 2021 +0200

    [containers] update from Debian Buster to Debian Bullseye

[33mcommit 3a9558c33e5dfd0ea3f840f8d53774461d8abb16[m
Merge: 27014042 6f6e95f5
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 20:42:13 2021 +0200

    Merge pull request #160 from umarcor/containers-doit
    
    add pydoit to custom dockerfiles

[33mcommit 270140424588db6c04992cbf999928ab38e364d8[m
Merge: 01fa47b2 183f69a5
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 18 20:32:51 2021 +0200

    Merge pull request #159 from umarcor/explicit-mem
    
    [setups/osflow/filesets] do not provide default NEORV32_MEM_SRC, require it to be explicitly set

[33mcommit 6f6e95f5da583886025cde8f797d69da8c766b93[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jul 7 03:49:13 2021 +0200

    add pydoit to custom dockerfiles

[33mcommit 183f69a5d5edf48cd08c4628f722d69d78a86459[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jul 7 18:43:09 2021 +0200

    [setups/osflow/filesets] do not provide default NEORV32_MEM_SRC, require it to be explicitly set

[33mcommit 0ac508d3519cdd965a22c3980578aea3aab37931[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:48:25 2021 +0200

    [rtl/core] updated pre-built images
    
    default app (blink_led) and bootloader

[33mcommit ecafcd4d82ed13029a4ba3687332d95c881cd336[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:42:10 2021 +0200

    [docs/datasheet] updated low-level registers

[33mcommit e4f14c014321ebf6171ac6a9459d2810971c4cab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:30:10 2021 +0200

    [sw/bootloader] converted

[33mcommit 2cf4a59200885e3408307299822cf1e084f0b3a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:29:57 2021 +0200

    [sw/example] converted example programs

[33mcommit aa215acbe95ff541b66c03df1a7bf0064fb4e225[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:29:26 2021 +0200

    [sw/lib] added back-compatibility layer

[33mcommit d435b3e9ff3a006868b80a6f906dda649d594581[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:28:58 2021 +0200

    [sw/lib] ported all low-level drivers

[33mcommit 7fd3d747f827143e658010d678d6dbf9b5ab6bcb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:17:50 2021 +0200

    [sw/lib] now using struct-based low-level HW access
    
    instead of single "define" pointers

[33mcommit 01fa47b2fec764dc24c8a2004e26b93c9e4f6b63[m
Merge: 5ff9581b 372a3f0a
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:13:22 2021 +0200

    Merge pull request #156 from umarcor/doc-sim
    
    [docs/userguide] update section 'Simulating the Processor'

[33mcommit da4f30dae69bb09750bddf9a14d218729c689ebb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 17 18:04:08 2021 +0200

    [setups] added CRC32 CFS tutorial (link), by @motius

[33mcommit 372a3f0a4addfbe9f083475cad867c47d2773999[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Sep 17 16:10:47 2021 +0200

    [docs/userguide] update section 'Simulating the Processor'

[33mcommit 8e5d985976a2bf177b34cfe47aeaaf73adea8001[m
Author: Rafael Corsi <rafael.corsi@insper.edu.br>
Date:   Thu Sep 16 14:58:11 2021 -0300

    [docs/userguide] update simulate instructions (new path)

[33mcommit 5ff9581b9e3f2bbc44e515137258a68baec6585a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 16 20:59:45 2021 +0200

    :bug: [rtl/system_integration] fixed missing NMI connection

[33mcommit c5dce9ae21e17a6bb9b61ef27554a929f2749c4f[m
Merge: 4d6a926a 60695c79
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 16 20:51:32 2021 +0200

    Merge pull request #151 from umarcor/test-split-arch
    
    [rtl/core] split dmem/imem entities and architectures to separated files

[33mcommit 4d6a926a92fcc56e5387accc53a5ba243ba8770a[m
Merge: 3be750c5 ec7c3838
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 16 20:03:40 2021 +0200

    Merge pull request #154 from rafaelcorsi/bug-fix-wb_mem_a
    
    [sim] fix typo wb_mem_a.cyc in testbenches

[33mcommit ec7c3838f6981ced2c17c361ef797218953438dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 16 17:45:22 2021 +0200

    fixed same typo in "simple" testbench

[33mcommit 2c673331cfa626ed5000afccb33ebd176fd9cdb3[m
Author: Rafael Corsi <corsiferrao@gmail.com>
Date:   Wed Sep 15 23:07:07 2021 -0300

    fix typo wb_mem_a.cyc on sim example

[33mcommit 60695c7934def8999e950fbdc9e3d88b59193438[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 15 17:57:12 2021 +0200

    [CHANGELOG] date fix; added PR

[33mcommit a08be0be2a3dc1f3bce0f9a43a0e9b0f4c8119c8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 14 23:17:40 2021 +0200

    updated sanity checks / added architecture style reports

[33mcommit c05fc14894dc57e427b5ed0cd484c4cd11e2a6bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 14 23:08:30 2021 +0200

    [CHANGELOG] date fix

[33mcommit 8636398aa99fdadf2cc599914e039db8b9edeb70[m
Merge: cac66444 8d60f86e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 14 22:56:34 2021 +0200

    Merge branch 'test-split-arch' of https://github.com/umarcor/neorv32 into pr/151

[33mcommit cac664444fe4ecb0316abd8c8e907012d9972b2c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 14 22:56:10 2021 +0200

    [docs/datasheet] add DMEM/IMEM files + note

[33mcommit 8d60f86ea574f1f055fe64c098f7c7c0e40e4c5c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 21:40:20 2021 +0200

    updated version and changelog

[33mcommit 8129c932df1514af0c353dd6f52052247e446f9b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 21:17:57 2021 +0200

    [setups/radiant] path fix

[33mcommit d9a7bef3c7fc684c4261b6cf9b6a4cee58c5aa0f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 21:06:24 2021 +0200

    [rtl/README] added note regarding mem folder

[33mcommit 3ea109d7083d545cb1dec47701bbb98af24ad48d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 20:59:03 2021 +0200

    [docs] added split IMEM/DMEM

[33mcommit b548fb3c2f2b2f59099270ee0fe3f793ea7690c1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Sep 12 05:30:53 2021 +0200

    [rtl/core] split dmem/imem entities and architectures to separated files

[33mcommit 78d767899db3e1c1f3aaed32e5afa8ed905c5dad[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 21:40:20 2021 +0200

    updated version and changelog

[33mcommit 199007847397927ae7dd2320d5b00519d319c4c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 21:17:57 2021 +0200

    [setups/radiant] path fix

[33mcommit fea54ecb11a2e477c65c231a938d34dedf406ac8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 21:06:24 2021 +0200

    [rtl/README] added note regarding mem folder

[33mcommit 9c9495c69ed025ba6fea4f61f984959f09dec3b0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 20:59:03 2021 +0200

    [docs] added split IMEM/DMEM

[33mcommit 0a8f2b2e8db348a4c83df636f4a37c7aeb33f858[m
Merge: b3e2788c 287fbb60
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 20:18:12 2021 +0200

    Merge branch 'test-split-arch' of https://github.com/umarcor/neorv32 into pr/151

[33mcommit 287fbb6064f9d902145d2f62936ff12376a44dad[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Sep 12 05:30:53 2021 +0200

    [rtl/core] split dmem/imem entities and architectures to separated files

[33mcommit 3be750c5e286aca58ae488a78dbecb9ade105269[m
Merge: 0fd8e9e1 526af95b
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 19:02:15 2021 +0200

    Merge pull request #150 from umarcor/arch-simple
    
    [sim] create subdir 'simple', avoid making a local copy of 'sw' and 'sim'

[33mcommit 526af95b27aec216a3ff4101715fec9506ff6d52[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 18:48:03 2021 +0200

    [sw/common] adapted sim target
    
    * simulation using vunit could be added using another target like "sim_vunit"

[33mcommit 0fd8e9e15b7b65811eceb1abf9b0c16f4de5b4dd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 13 18:36:03 2021 +0200

    :bug: [rtl/system_integration] fixed connection of missing IRQ signals
    
    * machine software interrupt
    * external interrupt controller interrupts

[33mcommit b3e2788c352d7cd7f92f7a8e29426b704ebc67b3[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Sep 12 05:30:53 2021 +0200

    [rtl/core] split dmem/imem entities and architectures to separated files

[33mcommit 8f8027015a36daf488f9054ba0253d08a7721399[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Sep 12 04:38:51 2021 +0200

    [sim] update README

[33mcommit bee29be1ad9fbbd50460c520aa8aa714c5e73dc5[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Sep 12 03:20:19 2021 +0200

    [sim/run_riscv_test] do not make a local copy of sim/simple

[33mcommit 72dd8776073d2008d667d0acef3fe34db2a79ad7[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Sep 12 00:39:56 2021 +0200

    [sim/run_riscv_arch_test] do not make a local copy of sw

[33mcommit ccac8fc8cea0ede1bbf1966c55f9ecc3d13b4ae4[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Sep 10 16:31:00 2021 +0200

    [sim] create subdir 'simple'

[33mcommit eb4ba9f608eac706f731ca197656401624deb09a[m[33m ([m[1;33mtag: v1.6.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 19:08:44 2021 +0200

    preparing new release 1.6.0

[33mcommit 0fc4f5cacbd07a90852949ab19992040058514d1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 18:22:33 2021 +0200

    [docs] minor edits

[33mcommit 511e37b6b676779f716592c7b90f8e3f00e709a4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 18:22:22 2021 +0200

    [sw] optimized intrinsic libraries
    
    Intirnsics (pseudo custom instructions) for "Zfinx" and "Zbb" extensions
    * now using true inlining
    * no more pseudo operations
    -> faster execution; closer to the original/native instructions

[33mcommit 2bbb2f50f1ec964bd082a6b2b90f029e20baf844[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 08:00:56 2021 +0200

    [README] minor edits

[33mcommit c65dcc872eeaa806e9b9c82412edd8e080add2d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 08:00:23 2021 +0200

    [docs/ug] reworked simulation section

[33mcommit b28dbbd927e75c32da53792cb095f47c3fe901a0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 07:30:25 2021 +0200

    removed mstatus.sd and mstatus.fs bits
    
    according to the revised specs. these mstatus bits are hardwired to zero / not used by the Zfinx extension

[33mcommit bd3acc41053b888fe8c541ee6a670c360cef2aaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 11 07:28:38 2021 +0200

    [docs] updated RISC-V specs.

[33mcommit 33d67f2d745de5437862768cdd89fccae9fafe06[m
Merge: 6246f0e8 e5181b77
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 10 17:55:46 2021 +0200

    Merge pull request #149 from umarcor/fix-windows
    
    [setup/osflow/boards/index.mk] do not set GHDL_PLUGIN_MODULE unconditionally

[33mcommit e5181b77b125098244ed76b87f1c1e3d771b2685[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Sep 10 16:43:15 2021 +0200

    [setup/osflow/boards/index.mk] do not set GHDL_PLUGIN_MODULE unconditionally

[33mcommit 6246f0e8c0fe059bcf9f07af37ab71285ef764e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 10 16:24:06 2021 +0200

    [sw/common] added new makefile target "sim"
    
    can be used for in-console simulation of current application using GHDL and the default processor testbench ("simple"/script-based testbench setup)

[33mcommit 2c66af5cf1b5e97d59cd8cef09ea3c213d5336f8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 10 12:27:20 2021 +0200

    [docs] clean-upa and fixes

[33mcommit e07f1f38781689089a61979f775f162f4af22efe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 11:38:30 2021 +0200

    [docs] added bit-manip unit to file hierarchy list

[33mcommit f7afa9ba452a0b0f790113fcca4b06ef7405149b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 11:38:12 2021 +0200

    [rtl] minor comment edits

[33mcommit 866ed7aba7deaa094381bc8ae8906c033748f011[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 10:48:34 2021 +0200

    [docs] added FAST_*_EN flags to SYSINFO

[33mcommit 4a2a1ac4ed9a6a7f295a65b7c77fdbbe32e08269[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 10:48:12 2021 +0200

    [sw] added FAST_*_EN flags to SYSINFO

[33mcommit a723d7342f2a8606d0108b4c390e204c6704b634[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 10:47:57 2021 +0200

    [rtl/core] added FAST_*_EN flags to SYSINFO

[33mcommit 0ab13ecfd2c7ec7f020cf33586e9ea8a165a8928[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 10:40:38 2021 +0200

    [rtl/core] minor typo fixes

[33mcommit eab859cb95751f537180109c91834d409fb5b9cf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 10:36:33 2021 +0200

    [sw/example/processor_check] minor edit
    
    using custom instruction type with invalid opcode to check for illegal instruction exception

[33mcommit 87581cb8e77d5e3779a9dbf306bfb4e5d2995b6d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 09:53:01 2021 +0200

    [README] minor edits

[33mcommit ea21c18ee8cc4da4b505d30526b898387e16d94c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 09:52:46 2021 +0200

    [docs] added FAST_SHIFT option to Zbb extension
    
    -> performance option

[33mcommit 7cd9beae06db9c9d5f9e10062352f0eb035c1a46[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 09:50:57 2021 +0200

    [rtl/core] added FAST_SHIFT option to Zbb extension
    
    set `FAST_SHIFT_EN` generic true to implement full-parallel logic for all Zbb shift-realted instructins (like clz, cpop, rol, ...)

[33mcommit e1b640f285e5d09ca70d20429b85c82d94df13e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 9 09:04:43 2021 +0200

    [docs/figures] layout fix

[33mcommit e49608bca01c6d7d152a9330b89963ae70776515[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 21:20:05 2021 +0200

    [docs/figures] updated CPU diagram
    
    added Zbb co-processor

[33mcommit 119331a56a7b848a1586d5be01bee759f7c04f51[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 20:52:36 2021 +0200

    [rtl/core] added further CPU config notifiers
    
    * perfromance options: FAST_MUL_EN, FAST_SHIFT_EN

[33mcommit 51bf46c873f2805f0525d904e991361152723280[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 20:51:33 2021 +0200

    [sim] minor fix in default extension configuration

[33mcommit 5cea7bb563a5e5deab3d683547c88b1cef5897e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 19:35:22 2021 +0200

    :sparkles: [docs] added RISC-V Zbb bit-manipulation extension

[33mcommit cc19d52202e46b40dbdd3e4f61b7b27de2d9e56d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 19:33:05 2021 +0200

    [rtl7system_integration] added bit-manipulation extension

[33mcommit a2ef87c2f810260c3a867f5193bcd15f3d5f17ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 19:32:43 2021 +0200

    [sw] added Zbb test program
    
    using "intrinsics" for the Zbb instructions since there is no support in upstream gcc yet

[33mcommit 68a86909c02bb823a84343f9d0f6dd229b780248[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 19:26:17 2021 +0200

    [sim] added Zbb bit-manipulation extension

[33mcommit 3727819ce841a161776922fd8f638a0af9e59b89[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 19:25:51 2021 +0200

    [setups] added bit-manipulation extension

[33mcommit 1209e6b8e7f0e9a578ccb9c6f07534b1a487ae60[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 19:25:24 2021 +0200

    :sparkles: [rtl] added Zbb basic bit-manipulation extension

[33mcommit d5cba96a4ae1a974736c2f9afa37c3a5a8c23e04[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 18:04:38 2021 +0200

    [sw/lib] preparing support of Zbb CPU extension

[33mcommit 71e642bc0979b112c8eaf9170e0fdc0e07a14c2e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 18:03:42 2021 +0200

    [sim/lib/source/rte] minor bug fix

[33mcommit 7739654d2f33cf36951495895a413184984688fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 17:45:35 2021 +0200

    [sim] integer overflow?! [WIP]

[33mcommit 46587fe697d01eec80f29b141fa131381af1aa12[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 17:08:04 2021 +0200

    [docs] updated B extension spec

[33mcommit 8725e4d9a40b02a5ee534e6b4a79998f15b197fb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 17:07:35 2021 +0200

    [setups/radiant] setup now using neorv32_top (no "wrapper") [WIP] #145

[33mcommit 20e9f00a022eba1cbe93442efed2e7ea72e6350c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 8 17:05:06 2021 +0200

    [rtl] fixed missing flash_sdi_i signal #145

[33mcommit f01ef87605ed41c22abddac816f7b1121bc18523[m
Merge: 8a08fd0f 66a7dc93
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 2 19:12:53 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 8a08fd0fe36a718ac77891766bf75a7e4bbcdb45[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 2 19:12:49 2021 +0200

    [docs/datasheet] added more infromation to generic's description

[33mcommit 5ffee4fc9e05df0e8a38eb126a43e5d7a17739aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 2 19:12:24 2021 +0200

    [docs/userguide] minor edits

[33mcommit 29e11bdec043996b832df319bb6f4c98bb214d7f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 2 17:55:16 2021 +0200

    [docs/userguide] added new section
    
    "Application-Specific Processor Configuration"

[33mcommit cce12eb9a0d949e88865be59b3be7431e053e87e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 2 17:21:02 2021 +0200

    [setups] minor README edits

[33mcommit 66a7dc93a6d701ead0ef198280737233dd603219[m
Merge: b562c39c 5db03c1d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 2 15:57:27 2021 +0200

    Merge pull request #144 from stnolting/terasic_cyclone_v_setup
    
    [setups] add terasic cyclone v starter kit

[33mcommit b562c39c22c2210e73ed48d38d05f22ca0b26725[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 31 18:41:10 2021 +0200

    :bug: [sw/lib] fixed bug in watchdog reset function

[33mcommit 6706f858d426dd3b762dd0157eb4ed454794feac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 31 18:40:40 2021 +0200

    [sw/lib] whitespace fix

[33mcommit 5db03c1d425629327e39bf8412d458c9506266e2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 31 16:19:17 2021 +0200

    [setups] added terasic cyclone v starter kit
    
    sources provided by zs6mue

[33mcommit 79058c17e1c1b10d1573c8712d754bd22300f0f6[m
Merge: afc5dca4 bb3b9364
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 31 13:54:29 2021 +0200

    Merge pull request #139 from zipotron/AlhambraII_and_ULX3S_boards
    
    Added AlhambraII and ULX3S boards

[33mcommit afc5dca4840171510082f88cd53de9f544f4a83a[m
Merge: 6939e819 f4813c57
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 30 21:08:58 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit bb3b9364efe653bb045b2ff2e2e7b9124effa9ca[m
Author: zipotron <zipotron@gmail.com>
Date:   Mon Aug 30 19:01:56 2021 +0300

    ULX3S rewired reset button, and code cleaned

[33mcommit 58d20c5bb101135098a62fece38fc3eaf4ffe546[m
Author: zipotron <zipotron@gmail.com>
Date:   Sun Aug 29 23:41:14 2021 +0300

    AlhambraII working, UART issue fixed, ULX3S dirty code cleaned

[33mcommit 195adb7086f6db408094594d4dce2f4441db0820[m
Author: zipotron <zipotron@gmail.com>
Date:   Sun Aug 29 23:17:40 2021 +0300

    ULX3S Working, hart led blinking, UART connection suscess and tx and rx reacting

[33mcommit f4813c577b0ea77c6885e51111997b640d0460ed[m
Merge: 8bc7d23a f56d7d64
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 29 14:44:32 2021 +0200

    Merge pull request #143 from henrikbrixandersen/doc-fixups
    
    SYSINFO documentation fixups

[33mcommit f56d7d64a258959ac8b6ce4ab385fd14a65a735c[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Sat Aug 28 15:12:13 2021 +0200

    [docs] remove double documentation for SYSINFO_FEATURES_HW_RST
    
    Remove the double documentation for bit 15 (SYSINFO_FEATURES_HW_RST)
    of the SYSINFO_FEATURES register.

[33mcommit 6c38fb734ec748a066a545e47ebfc040bbc49e15[m
Author: Henrik Brix Andersen <henrik@brixandersen.dk>
Date:   Sat Aug 28 15:10:52 2021 +0200

    [docs] add missing formatting to the CPU_SYSINFO register table
    
    Add missing formatting (leading underscores) to the CPU_SYSINFO
    register table.

[33mcommit 6939e819c062fc6274a74e31c651dfb058083320[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 13:06:05 2021 +0200

    Update run_riscv_arch_test.sh

[33mcommit 744d79eef10ca8f709ca4a675361a5c8d4cb9c06[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 12:53:36 2021 +0200

    :construction: [riscv-arch-test] minor edits (E extension)

[33mcommit 9249abcc68d4969764cc5fbc06300a2f1755a657[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 12:41:41 2021 +0200

    [riscv-arch-test] added rv32e tests :construction: [WIP]

[33mcommit b2d5fa7b4620ed9b8317895ca2022fc400f061b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 12:40:37 2021 +0200

    updated riscv-arch-test submodule to latest

[33mcommit 8bc7d23a9037e108612d97f5de1e4e7b65d60b67[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 12:11:25 2021 +0200

    [rtl] trigger reset (also) via bitstream

[33mcommit 32e28e15977e9192ba32a13b81a22053c6f480c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 12:10:09 2021 +0200

    [rtl] minor edit

[33mcommit 43266951e2805de5c5ec43582d7ac6c5c0dc4097[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 20 09:59:26 2021 +0200

    [.github] fixed Windows workflow

[33mcommit 67ef58fc5c7335320a99a09ee092c616ecfc1097[m
Author: zipotron <zipotron@gmail.com>
Date:   Thu Aug 19 22:54:27 2021 +0300

    Bugfix - UART TX and RX constrains was fliped

[33mcommit 2b8daf677e7c88fa474799dc3ea6851b247b48a6[m
Merge: a2e02a17 e59683e5
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 17:43:27 2021 +0200

    Merge branch 'master' into pr/139

[33mcommit e59683e5ceff410d915b459b1c5b64673200386c[m
Merge: d657e7d8 6f3b0051
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 17:41:18 2021 +0200

    Merge pull request #142 from stnolting/split_templates_folder
    
    split rtl/templates folder

[33mcommit d657e7d8ef5b9bed8480f55c8b1dfe046dcb02ce[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 13:40:34 2021 +0200

    [docs] removed custom `mzext` CSR
    
    moved information flags to new `SYSINFO_CPU` register

[33mcommit b4a2b39bdb733acec693ce3764039a954438c2de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 13:36:27 2021 +0200

    [rtl/core] updated bootloader image

[33mcommit e4b29674a25ddf50789f38fe0ed99de83963f1c3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 13:32:05 2021 +0200

    [sw] removed custom `mzext` CSR; moved info o new `SYSINFO_CPU` register

[33mcommit ee69d0678230c9e57a40e8c9adbf893f43de12ea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 13:31:07 2021 +0200

    [rtl/core] moved mzext flags to new SYSINFO register

[33mcommit c078e0e1e5dbc338e0ea631eeab10c1837ac3169[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 13:30:48 2021 +0200

    [rtl/core] removed custom `mzext` CSR

[33mcommit 6f3b0051e97526e3d2cb39aba5fa53a0531eb548[m
Merge: 8c344f54 48fe277e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:47:23 2021 +0200

    Merge branch 'master' into split_templates_folder

[33mcommit 48fe277e68e81ab63a1851f782844114d8814b93[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:35:20 2021 +0200

    version update

[33mcommit 17e7651c61c23d0f0f89038cea7c2ca8a85a12d6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:35:07 2021 +0200

    [sw] RTE typo fix

[33mcommit 5d548f43ea452326b422cc831086a543bcf3afbb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:29:31 2021 +0200

    [sw] remove USER_CODE generic

[33mcommit fd31b5b4ccfe7d80f7118801e6872729ff0462e6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:29:15 2021 +0200

    [sim] remove USER_CODE generic

[33mcommit cc055bce9fcb82577e6286e4a4b0fe1a00a08414[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:29:00 2021 +0200

    [setups] remove USER_CODE generic

[33mcommit d83eefde7047a87ebc96ada77d13264c7b2c42cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:28:48 2021 +0200

    [rtl] remove USER_CODE generic

[33mcommit 8a9b5fb31ea53a70d6eb55724f203fbcc8e498ad[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 12:28:33 2021 +0200

    [docs] remove USER_CODE generic

[33mcommit e467decb43bab51663cb736145991501394564c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 11:50:21 2021 +0200

    [bootloader] added option to disable SPI module
    
    and related bootloader options (like load/storing executables from/to SPI flash; SPI auto-boot)

[33mcommit f490c76c9cdfac9def91c2e0236834eacf3a8ed1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 19 11:43:40 2021 +0200

    minor edits

[33mcommit de708791f06475117fd5d35eb08c0edccae65c77[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 18 20:51:37 2021 +0200

    [sw, sim] fixed Zifencei riscv-arch-test test

[33mcommit a23592d37c1d58f72738617e11f8d1b30c2a72f6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 18 20:41:22 2021 +0200

    [.github] added Zifencei test to riscv-arch-test workflow

[33mcommit 6719a9f6fcc3c8b132d3a41af73f6246e25ea79d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 18 20:40:57 2021 +0200

    [sim] added pre-initialized IMEM (RAM) module
    
    :warning: for simulation only!

[33mcommit 42129abcded89fa4b5a621a8ef7354c8147a7aeb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 17 15:38:59 2021 +0200

    [sw/lib/include/neorv32_twi] added missing prototype

[33mcommit 8c344f54f5035677c41f717179e8aa29c64e7d8c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 20:35:06 2021 +0200

    [setups/osflow] path fix

[33mcommit a972b2cf15d385a8ebe33cc8470cf743120e915b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 20:31:00 2021 +0200

    [sim] adapted new rtl folders

[33mcommit 2a00b99ea87cab2781a74b2d33accdc73bf1872c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 20:30:46 2021 +0200

    [setups] adapted new processor_templates folder

[33mcommit 9627be7db7cafa56490ced7479d519cd4072b732[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 20:30:18 2021 +0200

    [doc] adapted new rtl folders

[33mcommit fe64b1cd09a035bf2c9841b2285dc2587aa685f8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 20:10:29 2021 +0200

    [rtl] split templates folder

[33mcommit a2e02a17fdca6330cf314ca5138750235a30c1a3[m
Merge: 00255cca 46ca6c71
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 18:59:50 2021 +0200

    Merge remote-tracking branch 'origin/master' into pr/139

[33mcommit 46ca6c71b3b57377f1740a3d1fce00c5031bdbfc[m
Merge: 522bd3ce 9ff3e040
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 18:53:32 2021 +0200

    Merge pull request #137 from stnolting/move_osflow_examples
    
    moved osflow "examples" folder

[33mcommit 522bd3cef8606356276f9e4b0f7329696f1c1a17[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 18:29:29 2021 +0200

    [sw/image_gen] renamed *.cpp -> *.c

[33mcommit ac41ecf07120fb97e79789f2a19b0721cb825a1f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 15:50:04 2021 +0200

    [rtl] minor CPU control logic optimizations & comment edits

[33mcommit 0b1a478a0f07be25c325896ffbe2e63fa0d58150[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 15:16:29 2021 +0200

    [sw/example/processor_check] added MRET test

[33mcommit 15385d79d7d0ab7f5e81ce64cb408cfad6cb3493[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 15:16:02 2021 +0200

    [sim] adapted number of expected test cases

[33mcommit 00255cca9500c35e6d704a9c6e1caf26fadeae34[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 12:53:31 2021 +0200

    removed PLL, added simple reset generator

[33mcommit 50fc80a7e121d1084726e4712ef6132d19cab64c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 16 12:49:29 2021 +0200

    minor edits

[33mcommit 7259831b9a806819569b8c784241a401aae57776[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 15 16:18:18 2021 +0200

    [sw/example/dhrystone] minor edits

[33mcommit 6abfc3df6487eb1e58e83ae501fb95f972335b17[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 15 13:43:45 2021 +0200

    [sw] added Dhrystone benchmark port
    
    :construction: work in progress

[33mcommit 8a4c0c6d838085ba4d1c8aae8ed738c00cee2f44[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 15 10:58:38 2021 +0200

    :bug: [rtl] fixed bug in MRET instruction
    
    * bug caused an exception if user mode was not implemented
    * bug caused by modifications in v1.5.8.8

[33mcommit 0000e7baf92aa2be09cfee6e4f0ef6004332d598[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 15 10:55:53 2021 +0200

    [sw/lib] minor edits
    
    * ensure that read-only memory-mapped registers are identified as such by the compiler

[33mcommit 9ff3e040ca0c02fa0b1e618e954f6cd8b7ef42fb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 15 09:54:40 2021 +0200

    minor README edits (wip)

[33mcommit ded8d73c65bf2c1e6e08f90611bfef3dcc26337c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 21:45:53 2021 +0200

    [CHANGELOG] added new test setups

[33mcommit 71e29451a5cd9b4fe6a951e79e70fd950e0f70cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 21:45:23 2021 +0200

    [rtl] minor comment edits

[33mcommit 9b6303c5715481c6ece4cb047788f61ec62ee4c4[m
Merge: 8bf4b702 a482d215
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 19:37:01 2021 +0200

    Merge pull request #136 from stnolting/add_rtl_test_setups
    
    ✨ add rtl/test_setups

[33mcommit b38b2aa7375f4e426020811068246122a25e4593[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 19:04:59 2021 +0200

    reduced mem sizes: IMEM=4kB, DMEM=2kB

[33mcommit a482d2153c00b7fa47a63209e0e06f93c97118d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 09:25:30 2021 +0200

    [vivado] now using new neorv32_test_setup_bootloader.vhd

[33mcommit c2b1bfff549f4050c31c5768dece92bf1f3c6c5b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 09:25:05 2021 +0200

    [quartus] now using new neorv32_test_setup_bootloader.vhd

[33mcommit 795a40e8b20e3cc95ac1a4ff4d69f9966d81542c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 09:24:42 2021 +0200

    [rtl/test_setup] typo fix

[33mcommit 31475399bcb58619f0b69c8ffda18294e3b9c477[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 09:22:08 2021 +0200

    [rtl] removed deprecated templates/test_setup
    
    rtl/test_setups/neorv32_test_setup_bootloader.vhd provides the same functionality and is illustrated in the user guide

[33mcommit 9c593549c1e1e288cce50abba0e5c2a0e2e7be21[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 14 09:08:27 2021 +0200

    [userguide] added link to new test_setups folder

[33mcommit 8bf4b702f6a64cd1c71f6cdab3a1f49526b0cbf8[m[33m ([m[1;33mtag: v1.5.9[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 13 14:37:58 2021 +0200

    preparing new release [v1.5.9]

[33mcommit 397296f064551f2797951e6f471bfd39cd5672f2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 12 17:35:05 2021 +0200

    [setups/README] added PR's boards

[33mcommit 354dd37c3b75b2de53c132b1e780f9357e67b488[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 12 17:23:20 2021 +0200

    typo fix

[33mcommit 04af9fb1c595c5dc9dfed83c1c5fe7929f835041[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 12 17:15:35 2021 +0200

    removed PLL, minor signal renaming

[33mcommit 8570a27b902802d06fd287d6d3fad09eb22707b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 12 17:13:52 2021 +0200

    renamed UART signals

[33mcommit faa904f2c108ae9aae7f680d070122cef6f8f24f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 12 17:05:33 2021 +0200

    [.github] added ULX3S implementation

[33mcommit bc400b4902f10afe9d5e8982add4ebf43b872238[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 12 13:20:08 2021 +0200

    [sw/common] added RISCV gcc config output to info target

[33mcommit 87525f3e50ded5bf1152f25f366a4e5974606b2e[m
Author: zipotron <zipotron@gmail.com>
Date:   Wed Aug 11 22:02:22 2021 +0300

    ULX3S Synthesis fix

[33mcommit bff5293681395ae2d17e5dce558f5b1cb27bd090[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 11 17:28:32 2021 +0200

    fixed memory HDL include

[33mcommit 20b219c8340da66f8aaefa1a678e3b55664d2949[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 11 17:22:35 2021 +0200

    DMEM = 2kB

[33mcommit 8903de5d830c74adff8122614d4f85d67377053e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 11 17:18:42 2021 +0200

    8kB IMEM; 4kB DMEM

[33mcommit e777498690fd98f77e60cdc5f6571873c4d776ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 11 17:07:38 2021 +0200

    removed RGB drive; added on-board LEDs

[33mcommit 071b63d04e9071f7cc6c44651ab2790e5565f463[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 11 17:06:48 2021 +0200

    removed SPI pins; added all on-board LED pins

[33mcommit 2e5c8847d7a72214785fd4e67ed74265f98ed64d[m
Author: zipotron <zipotron@gmail.com>
Date:   Wed Aug 11 15:56:05 2021 +0300

    PMOD references removed from .pcf file. Unexistent USB port removed for top of VHDL

[33mcommit 39dc746d7eb1a8733f1770c5290ad0eddd42712b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 10 19:28:07 2021 +0200

    removed HF_OSC; using external clock [TEST]
    
    seems like there is no HF_OSC primitive in the iCE40HX family; revert this if I am wrong ;D

[33mcommit 0bd9865a44221974e9137b4740ffb3742d6e1197[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 10 19:03:01 2021 +0200

    [.github] added AlhambraII board (PR) to Implementation workflow

[33mcommit f1f25c58f97566d1df7f4fabb6f8df1e0a4a5414[m
Author: zipotron <zipotron@localhost.localdomain>
Date:   Mon Aug 9 21:44:59 2021 +0300

    Added AlhambraII and ULX3S boards, Alhambra failing in the design, maybe optimization, ULX3S Failing for some misconfiguration in Makefiles

[33mcommit dd55e1aa3f6e8e8169510416fe79238a213c7617[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 18:56:22 2021 +0200

    [CHANGELOG] added SemVer note (wip)

[33mcommit 19c5110e0ca2ea484db5db2fcfe280ebc799902a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 18:48:45 2021 +0200

    [CHANGELOG] minor edits

[33mcommit 980b385b660c923855c9d5247d0ab5a7520e8904[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 18:42:37 2021 +0200

    [docs] minor citing edits

[33mcommit b3797af5790a8c547e41d5d983f3677c1cd860b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 18:06:32 2021 +0200

    [docs/userguide] added note: (too fast) external clock

[33mcommit 7c3967f074e3a3c9dbe0a33ab1926b1dc71dc2a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 17:54:20 2021 +0200

    [docs/userguide] added signal polarity note

[33mcommit 8e02127153055ff51d1b371f582a4c54c2b17f40[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 17:42:07 2021 +0200

    [rtl/test_setups/README] fixed links to user guide

[33mcommit 7c6fafcb4390b8e08eac762124e6cc696069da00[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 17:31:44 2021 +0200

    [setups/osflow] fixed verilog module path

[33mcommit fc9cb6d15c9046d37f6c44ebf383bd6e43112e57[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 17:30:09 2021 +0200

    [setups/osflow] removed obsolete variables

[33mcommit 7aacec16ac3bde0776096e84123451dfc249a5c6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 9 17:29:46 2021 +0200

    [setups/osflow/README] comment on "partial makefiles"

[33mcommit 49fdd28e706f56e3b8aaa3665b3625849fbcf04d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 8 17:02:17 2021 +0200

    [rtl] updated pre-built images

[33mcommit 7cbcec9ce3dcd834e94142703c3e5a91abaeaa83[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 8 17:01:08 2021 +0200

    [rtl] reworked register file x0 logic
    
    * any write access to x0 will be masked to actually write zero
    * no special treatment by the CPU control unit required anymore
    * first instruction after hardware reset should write _any_ value to x0 (implemented in start-up code crt0.S)

[33mcommit 1610281c4efb4adbea6e8a8712c83a5321db0b6f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 8 16:45:00 2021 +0200

    minor edits

[33mcommit e145d3c20fb24b71ad98a887e3bf8f5c429e8f3a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 22:22:33 2021 +0200

    [setups/osflow] added new "board_top" dir to README

[33mcommit 8a46f27b25a6cfb576d19993b79d87ebc79dfc5e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 22:22:10 2021 +0200

    [.github] adapted "Implementation" workflow

[33mcommit a369598ba4c27b704fc8fa86f0dac7086f069aaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 22:17:53 2021 +0200

    [setups/osflow] re-added makefile

[33mcommit 419ee0058e4ad89e1308291e6138fdfba144a252[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 22:17:29 2021 +0200

    [setups] moved `examples/*` to `osflow/board_tops/*`
    
    * removed original examples/README.md (there was no new information in it)

[33mcommit 70edf18fa3859be2d9c433a731547b1d6a0274f9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 22:06:50 2021 +0200

    [docs] rework: "execution safety" -> "full virtualization"

[33mcommit 3815cd78b2fd6737e8bcf816e3c1f49acd4f78aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 21:51:33 2021 +0200

    [setups/osflow] edited README (WIP)

[33mcommit f8205daeaf07e610ee6a96f0627e943301abed8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 21:49:33 2021 +0200

    [setups/examples] added help target (=default)

[33mcommit 106cd06934d322e92eb6462722747a07f0498c89[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 21:27:11 2021 +0200

    minor typo fix

[33mcommit 0bd95a3591943e3bd06fe443ab3b548f2574a8ef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 19:59:00 2021 +0200

    minor edits

[33mcommit 8cb3d4cf1f015fa8da926f399dab3b9869629fd4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 18:05:23 2021 +0200

    :bug: [rtl] fixed DRET and MRET trapping
    
    * `dret` has to trap if executed outside of debug-mode
    * `mret` has to trap if executed in privilege modes less than machine-mode

[33mcommit b961af3c79f16ae13d8d0298373323c8c4fe576b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 16:58:09 2021 +0200

    minor typo fix in README

[33mcommit 837e1f173ddc28961c861852636487ec32dd5237[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 16:44:02 2021 +0200

    [docs/datasheet/overview] added test_setups folder

[33mcommit 80f7f2c00f26a023acb201e8b303dd01c3025577[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 16:40:57 2021 +0200

    [rtl] added test_setups to rtl/README.md

[33mcommit c05b9f05c9fd4a90abd2dd238f3b57843e08f1e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 16:38:00 2021 +0200

    [docs/userguide] now using test_setups + minor edits

[33mcommit da7a59986ec7b273341de22c1ea1e177ff64017d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 16:33:04 2021 +0200

    added rtl/test_setups
    
    * README to illustrate test setups
    * test setups VHDL files (can be uses as TOPs)
    * adapted GHDL setup script

[33mcommit 6331568fc1925ab4190875159f2ea9ae67d45be2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 16:02:28 2021 +0200

    minor README updates (wip)
    
    :warning: some READMEs are work-in-progress

[33mcommit fb51738c4c35135a535011559a6e0e75843feab1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 7 15:59:54 2021 +0200

    [setups] reworked README

[33mcommit cfc442700b1fb4b7252a27e49b05a6cc8dd33afb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 5 17:31:39 2021 +0200

    [docs] added mstatus.SD and mstatus.FS bits
    
    used to control the state of the FPU; if mstatus.FS = 00 all FPU instructions and related CSR accesses will raise an illegal instruction exception

[33mcommit 5d39d65e8c8179b62b29d658a59e4c17b4c824cc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 5 17:31:08 2021 +0200

    [sw] added mstatus.SD and mstatus.FS bits

[33mcommit 32826d2183998dd86ddd97c475680ce2db88da54[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 5 17:30:09 2021 +0200

    [rtl] added mstatus.SD and mstatus.FS bits
    
    these bits are hardwired to zero if CPU_EXTENSION_RISCV_Zfinx = false

[33mcommit fb70f518648dd3715a1e0553931297ec53383484[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 5 16:52:24 2021 +0200

    [docs/datasheet] clean-up of folder overview

[33mcommit 01ec016926f7c7ae6b95ef78a37711462bcfca9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 5 16:21:56 2021 +0200

    [docs/ug] added Xilinx IP block designer

[33mcommit 4588d991eb73980aeb49ac31df1ce87e2470948a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 5 16:21:26 2021 +0200

    minor edits

[33mcommit 81c9d44db90317a694a8bf022fe1d48d22e1416e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 3 20:23:57 2021 +0200

    [sw] added (debugging) symbols to linker script

[33mcommit 95194aee83657e04a7e038c9a47a8185c5a1655e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 3 20:23:24 2021 +0200

    :bug: #134 was a bug

[33mcommit ff055298fb5d2d5ccee30b67eff1488296e8d667[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 3 16:37:43 2021 +0200

    [sw] clean-up of linker script #134

[33mcommit 28b9ff05089d00c5c11ef8b68ac4cc11bbe09652[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 3 16:36:31 2021 +0200

    added 'mconfigptr' CSR
    
    * RISC-V priv. ISA spec. v1.12
    * CSR is not actually used yet (always zero)

[33mcommit 5f2425971d87a3adbd437778d76dcdcb331f41dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 2 13:12:05 2021 +0200

    [docs/figures] minor update

[33mcommit 5dcf1cac87bf54ea6def080b2912cd9d2e00a51b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 30 16:17:24 2021 +0200

    [sw/image_gen] updated (legacy) UART upload script #133

[33mcommit ff75c2fe9c00784f55d1c0d5ba57d0c08f4052ac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 30 16:12:33 2021 +0200

    [rtl] fixed minor top entity bug #133
    
    Vivado "issue": generic defaults need a _fixed-size_ intialization value

[33mcommit 9e2d0f245de1439ec17d3852262e7d4626b57044[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 26 15:07:42 2021 +0200

    :bug: [rtl] fixed **major bug** in CPU interrupt system
    
    interrupts during memory accesses (load/store instruction) terminated those memory accesses violating the crucial "instruction atomicity" concept: traps (interrupts and exceptions) must only intervent _between_ instructions

[33mcommit 55198c617088e8a6a7d644867be35022fb82b1f0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 26 12:49:37 2021 +0200

    [sw] typo fix (in comment)

[33mcommit 1f416745e5d38417ead598f961111a1d6b1c91cb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 25 20:48:33 2021 +0200

    [rtl,sw,docs] added 'mstatus.TW' CSR bit

[33mcommit df6a45e0191bb945552284d778a2f0bc28c72fdd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 25 13:27:47 2021 +0200

    :bug: [rtl] fixed bug in 'E' ISA extension
    
    extension could not be enabled due to missing generic propagation

[33mcommit c5fb92c155d56debe730ea7f7c85f5f5fbdd1255[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 25 13:27:04 2021 +0200

    [rtl] clean-up of generic defaults
    
    this is a legacy thing. now only the processor top entity provides defaults for (all) the configuration generics

[33mcommit 7378e75fadb661c2bbef3976cf08ef24357d9c15[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 25 11:09:51 2021 +0200

    [docs] minor edits: HPM counters are accessible in m-mode only

[33mcommit 4830a5bbd01d2106a9c5e57c5022ef197b910dda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 24 21:03:53 2021 +0200

    fixed template wrappers

[33mcommit c6cf5d75fc8f1afce64809eeae84dd8fa0f4d8f6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 24 20:20:45 2021 +0200

    [rtl, docs, sw] exposed advanced external interface configuration options as new generics

[33mcommit 80fd4c39f21eded360a3a9883661b0a97578a784[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 24 20:20:13 2021 +0200

    [rtl] exposed advanced external interface configuration options as new generics

[33mcommit b453d9024cbca4c61962f628e59dcf374836fee3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 24 19:49:00 2021 +0200

    machine-level interrupts (top entity) are now edge-sensitive

[33mcommit d9c76143c38cdb041f04ec88acc2399f74129058[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 24 19:07:53 2021 +0200

    [ocd] minor clean-ups (thx Niko)

[33mcommit 984966cf8a50749214c4d6ca866a39d2f0b57626[m[33m ([m[1;33mtag: v1.5.8[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 22 12:03:23 2021 +0200

    preparing release 1.5.8

[33mcommit 74ea46a4172e2717eb122f56c05213bcf9983451[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 22 11:10:49 2021 +0200

    minor edit in start-up code
    
    very first instruction is now "lui zero, 0" (just another dummy)

[33mcommit 8b8da121c8a01814f6dfcca072cac4840fd07418[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 22 11:07:28 2021 +0200

    (re-)added mstatush CSR
    
    CSR is r/w, but writes are ignored and reads always return zero (CSR is implemented / CSR address is assigned for RISC-V priv. arch. spec compatibility)

[33mcommit 179045698473437e8c3613a0321655ddeb6836ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 21 21:01:31 2021 +0200

    [docs] reworked NEOLED module
    
    * simplified IRQ system - now using FIFO half-full condition
    * added top generic to configure TX FIFO depth
    * added option to send RESET (strobe) command

[33mcommit b1c1dd992321b5ecde8fbe8caf9e1e90dee38580[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 21 21:00:23 2021 +0200

    [sw] reworked NEOLED module

[33mcommit 9e68afa69279f16edab2708ecf79d2c15abc3c63[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 21 20:59:29 2021 +0200

    :sparkles: [rtl] reworked NEOLED module
    
    * simplified IRQ system - now using FIFO half-full conditions
    * added top generic to configure TX FIFO depth
    * added option to send RESET (strobe) command

[33mcommit 2602de0e652153c2008f79f95d136e9348bddb48[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 21 19:23:42 2021 +0200

    :bug: [rtl] fixed minor bug in SLINK code
    
    simulation issues due to missing signals in sensitivity lists

[33mcommit 5ef3fc672deb6147ec5233e4aa370896b3652450[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 18 13:34:46 2021 +0200

    [rtl] added top generic to configure CPU's instruction prefetch buffer size

[33mcommit cea7a618258952e6fde975d1de426939dd8ada3c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 18 13:13:01 2021 +0200

    clean-up of processor top entity #128
    
    using default for ALL generics and input signals;
    * generics are "off" by default
    * input signals are 'L' for control signals and 'U' for data signals by default

[33mcommit 245e701a05e98e0498fc1eff2b59afbba7a3519b[m
Merge: ad03afa8 1a3173ca
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 14 22:08:41 2021 +0200

    Merge pull request #116 from umarcor/ci-containers
    
    [ci] add workflow 'Containers'

[33mcommit ad03afa8780969edd0f69dac5a2cab4a5f0c4f93[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 14 21:49:36 2021 +0200

    [docs] reworked SLINK interrupt concept #122

[33mcommit 2ed8dffa60e407ec4d06f81cd83272988880a90b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 14 21:49:17 2021 +0200

    [sw] reworked SLINK interrupt concept #122
    
    added functions to check FIFO half-full fill level

[33mcommit 1705d42ce0a7776a138d8d4cf631be5ccb849293[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 14 21:48:32 2021 +0200

    [rtl] reworked SLINK interrupt concept #122
    
    SLINK now provides FIFO half-full interrupts

[33mcommit b95258a6a859bdea3bcff836610acaf9a5d1a8db[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 14 21:37:54 2021 +0200

    [sim] minor bug fix

[33mcommit a590b8175668b2575118e1fe05b398f885de6dfb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 14 21:35:54 2021 +0200

    [rtl] added level output to processor FIFO component

[33mcommit 0251fe4180da0672125eed36e100c88ac13b9fbc[m
Merge: 1afbf514 1b23e7ce
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 13 20:00:22 2021 +0200

    Merge pull request #127 from umarcor/ci-win
    
    [ci/windows] fix RISCV_PREFIX

[33mcommit 1b23e7ce61bd13af73ad57f05877a6b5cdff297a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Mon Jul 12 18:36:28 2021 +0200

    [ci/windows] fix RISCV_PREFIX

[33mcommit 1a3173ca215c9f936bfcf00abf1aaffb2679640c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jul 13 17:47:49 2021 +0200

    [ci] use custom container 'sim' in workflow 'Processor'

[33mcommit 008e429dc4840817ef476f858dbc6f75612f21ea[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jul 13 17:47:24 2021 +0200

    [ci] use custom container 'impl' in workflow 'Implementation'

[33mcommit e99a1777dde1d7286ed98fa033cb356eb49945e4[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jul 13 17:46:40 2021 +0200

    [ci] add workflow 'Containers' for building custom images on top of the ones from hdl/containers

[33mcommit 1afbf51489e195553c5c442288f026ba6e96d4ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 13 16:40:40 2021 +0200

    [rtl/templates] fixed typo (#126)

[33mcommit e6da52c4f16e0ebd4888953b7e58c147e544e6b0[m
Merge: 3c4d8047 2684e0b4
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 13 16:19:09 2021 +0200

    Merge pull request #124 from umarcor/sw-parallel
    
    Split ISA test suites in multiple jobs and rework makefiles

[33mcommit 3c4d804794121ca8820e3f959ed430286924b2e7[m
Merge: 8ba1e8df 325bf2cc
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 13 15:24:23 2021 +0200

    Merge pull request #125 from jeremyherbert/patch-1
    
    Fix source/sink mixup in SLINK docs

[33mcommit 8ba1e8df3ea676292fa5ed78c48d2069295675e1[m
Merge: dd079dfd 9375c8e5
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 13 15:10:35 2021 +0200

    Merge pull request #123 from umarcor/docs-isa-test
    
    [docs] update references to isa-test

[33mcommit 9375c8e53efd71fb7c82ad42aec6d5e28562eecf[m
Merge: 1d80b4b1 dd079dfd
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 13 15:10:20 2021 +0200

    Merge branch 'master' into docs-isa-test

[33mcommit 325bf2ccd82e9857527e521957ab7fb3c691bdff[m
Author: Jeremy Herbert <jeremy.006@gmail.com>
Date:   Tue Jul 13 14:00:30 2021 +1000

    add link to AXI4-Stream documentation

[33mcommit 2684e0b4f04acc316e04be61cc6ed9812696c823[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 07:11:06 2021 +0200

    [sw/isa-test] move RUN_TARGET to common.mk

[33mcommit da5711a30ac7bd3f80b23f70c48ffbd8bd0f601f[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 06:39:33 2021 +0200

    [sw/isa-test] use top-level generics instead of sed

[33mcommit 1ad87022ff29571031b0e67934b983c4e6faf56c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 05:59:04 2021 +0200

    [sw/isa-test] add common.mk

[33mcommit 064e5c84902896f0ee6818d594741d77382bce8a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 05:12:43 2021 +0200

    [ci/riscv-arch-test] split test suites in multiple jobs

[33mcommit 6d596414936334ec93197ec9e19befb2ab1f9538[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 02:45:52 2021 +0200

    [sim] split ghdl setup and run

[33mcommit dd079dfdbb00cf2fbeff70736942e341bee49a91[m
Merge: 207122d4 5b380dc5
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 18:47:23 2021 +0200

    Merge pull request #118 from umarcor/sw-common
    
    [sw/example] add common.mk

[33mcommit 5b380dc56b54a9ab4575d88f99ac848344eae9e8[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Mon Jul 12 13:16:54 2021 +0200

    [sw/example] define NEORV32_HOME

[33mcommit 8eb3dad8e1d8f68d379636515ea0ce0f9bcc1b58[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 12:28:46 2021 +0200

    [docs] minor edits: central makefile for SW

[33mcommit c849e2b5eb1b5d42556438fd4c86ff2fe6eef92f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 12:28:07 2021 +0200

    [sw] added usage of central makefile

[33mcommit 0fcb1d57c87cc76c220885e9ef2214072ac30134[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 12:27:03 2021 +0200

    [sw] moved central makefile to common

[33mcommit 7d4f89fd368cfe41b93203f990fcc848cd9e8daa[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 19:27:00 2021 +0200

    [sw/example] add common.mk

[33mcommit 1d80b4b1629f43ba0ab7f1582aa6f44d70bd0528[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Mon Jul 12 16:54:08 2021 +0200

    [docs] update references to isa-test

[33mcommit 207122d45a7774adbd3e70f98544c8f267b21f58[m
Merge: fd51fe7b 773686de
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 17:23:54 2021 +0200

    Merge pull request #119 from umarcor/test-time
    
    [riscv-arch-test] measure execution time of each test

[33mcommit fd51fe7b08a7f0e1d6e8a4c58c507ab4da7c648d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 17:07:32 2021 +0200

    minor edits and updates
    
    due to moved riscv architecture test folder(s)

[33mcommit 773686de981a0a1a15f67f09933f51f3b401544a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 20:59:38 2021 +0200

    [riscv-arch-test] measure execution time of each test

[33mcommit aa769fa21021e7d2d5f704e6dc6528b734ece029[m
Merge: 68ebc325 53662299
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 16:39:20 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 53662299f4e42edb1a712b3d32732a97a789d39d[m
Merge: 5832ce53 f77f5e25
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 16:38:45 2021 +0200

    Merge pull request #117 from umarcor/sw-move
    
    mv riscv-arch-test sw/isa-test

[33mcommit f77f5e25d80af26407eda7f316c7a8320caef63b[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 05:45:03 2021 +0200

    [sim] mv rtl_modules/neorv32_imem.vhd neorv32_imem.simple.vhd

[33mcommit 822a559375230c3540bef4836cdbf9f2a570bb83[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 05:31:03 2021 +0200

    [riscv-arch-test] update readmes

[33mcommit 87236b4e5de61755fb99633ad9e66811e56f60cc[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 05:22:22 2021 +0200

    mv riscv-arch-test sw/isa-test

[33mcommit 8cb0df7cea28d610ea290fc5efdaf02b994a0512[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 04:21:14 2021 +0200

    [riscv-arch-test] move run script to subdir 'sim'

[33mcommit 11c1f90b677389b6fe21b48f70415e661bb641a9[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 03:54:33 2021 +0200

    [riscv-arch-test] move submodule out of subdir work

[33mcommit e4e9393de64b934b9c17e9dc4976ac02597b621b[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 03:16:45 2021 +0200

    [riscv-arch-test] simplify hierarchy of port-neorv32

[33mcommit 5832ce5316d2572e977b15210dfc3225c1a9432f[m
Merge: e4bc862d efaa87d6
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 12 13:37:55 2021 +0200

    Merge pull request #120 from umarcor/sim-workdir
    
    [sim/ghdl] use --workdir=build

[33mcommit 0c4b2af18b332a5158c39f8caf57e7aa738da853[m
Author: Jeremy Herbert <jeremy.006@gmail.com>
Date:   Mon Jul 12 19:40:55 2021 +1000

    Fix source/sink mixup in SLINK docs
    
    Also explicitly state which end controls which signal

[33mcommit efaa87d6b0a7e94ff42060195f1b1c8f1d26b6f0[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 11 23:26:32 2021 +0200

    [sim/ghdl] use --workdir=build

[33mcommit e4bc862d1a807998beeb092176c1489feeb07a31[m
Merge: 4f55c581 ab215d43
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 11 15:07:11 2021 +0200

    Merge pull request #114 from umarcor/sw-prefix
    
    use RISCV_PREFIX instead of RISCV_TOOLCHAIN

[33mcommit ab215d437c61d9e3370ebdc141c556800a982387[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jul 10 03:05:19 2021 +0200

    [sim] cleanup

[33mcommit ca8927e55836d6bd57bea65faf9d1381ede53195[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jul 9 23:23:46 2021 +0200

    [sw] use RISCV_PREFIX instead of RISCV_TOOLCHAIN

[33mcommit 62a2e3b77b09cdc1903fa96bc22ca05d528c2053[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jul 9 22:19:30 2021 +0200

    [riscv-arch-test] use RISCV_PREFIX

[33mcommit 4f55c58133813fb146cd84408f2f78f92b526ce3[m
Merge: a38e4fef df52b501
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 9 18:21:51 2021 +0200

    Merge pull request #109 from umarcor/arch-test-cleanup
    
    riscv-arch-test script cleanup

[33mcommit 68ebc325902ec06b0fc45db6f93818586226f311[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 9 17:34:19 2021 +0200

    [docs] added note: priv. arch. extensions in MARCH #112

[33mcommit a38e4fefca9ca84972b4b4cfc1e471f36f1a809d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 9 12:58:09 2021 +0200

    :bug: [rtl] fixed minor bug in FIFO component
    
    might have caused mapping issues if FIFO_DEPTH = 1

[33mcommit c541967e3aff99cd64d224078b811ae45f1dbe85[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 9 12:57:17 2021 +0200

    :bug: fixed broken freeRTOS makefile #112

[33mcommit 8109de55907cc54679eba3ee4cf207cb241fad72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 9 12:49:05 2021 +0200

    minor edits

[33mcommit df52b501f4a027a2a4b59cae1a63013050a5bc60[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jul 8 14:59:59 2021 +0200

    update gitignore files

[33mcommit caf6924a8c6168c3986162f10fc06c2b29e799c5[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jul 8 06:08:49 2021 +0200

    [riscv-arch-test] run script cleanup

[33mcommit bebe4a89b4aae5054cb6befc5dbdbb2eaf4e619c[m
Merge: a22a32f8 6f839348
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 8 13:26:14 2021 +0200

    Merge pull request #107 from umarcor/orangecrab-constraints
    
    [setups/osflow] update OrangeCrab constraints file

[33mcommit 6f8393480cadbae49adc3ca55f80fbaa0576facb[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jul 8 02:25:06 2021 +0200

    [setups/osflow] update OrangeCrab constraints file

[33mcommit a22a32f8fbb9bb6a0a856773078171fa149dbb51[m
Merge: 07184119 a9df3076
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 7 18:06:31 2021 +0200

    Merge pull request #104 from umarcor/upduino
    
    [setups/osflow] cleanup

[33mcommit a9df3076daf89da0c3e08fbebc46981c49364324[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jul 7 02:49:58 2021 +0200

    [setups/osflow] cleanup

[33mcommit 071841192595f8c050a7471afb079fdaca9365d6[m
Merge: 84711a5e 301fd786
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 7 16:55:30 2021 +0200

    Merge pull request #98 from umarcor/orangecrab
    
    add OrangeCrab

[33mcommit 84711a5e1aa01912d5866360e8f1ff3924bda452[m
Merge: 5c825dae ca719d78
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 7 16:45:00 2021 +0200

    Merge pull request #100 from umarcor/doc-makefile
    
    [docs] move Makefile from project root to subdir 'docs'

[33mcommit 301fd7860e26c0aff653e9203be21d744297e6d4[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jul 7 16:09:33 2021 +0200

    [setups/osflow] add '--compress' to ecppack

[33mcommit 386647799122baa3e9d10d62b0f92288992d941a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jul 6 20:51:54 2021 +0200

    [setups/osflow] support 'svf' target for ECP5 devices

[33mcommit 0fa53c619206d7b46ee177d485fcc5f2cb8ca234[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jul 6 19:48:11 2021 +0200

    [setups/osflow] add ECP5 components
    
    Co-Authored-By: Jeremy Herbert <jeremy.006@gmail.com>

[33mcommit dd58c8e5a1b19c8f4a5591ca47c2c153f35194d0[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 13:58:48 2021 +0200

    [setups/examples] add OrangeCrab MinimalBoot
    
    Co-Authored-By: Jeremy Herbert <jeremy.006@gmail.com>

[33mcommit aa9d93fdb0f615818a5425cafc79c0be0cf8a3ea[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 13:59:02 2021 +0200

    [setups/examples] support OrangeCrab

[33mcommit d9ebdae84905bfc610f23afd6c3e14528347ef9d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 20:37:03 2021 +0200

    [setups/osflow] add OrangeCrab

[33mcommit ca719d78768743101596ef1c0ac08b0621cd13d7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 21:52:43 2021 +0200

    [docs] updated path to documentation makefile

[33mcommit c0f485cc6df2056ecfed40788e1f93406b5a3155[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 23:25:05 2021 +0200

    [docs] move Makefile from project root to subdir 'docs'

[33mcommit 5c825daeb59e8ba45df4ecbbf78ad8f3ca5fd378[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 16:54:37 2021 +0200

    [docs] minor fixes

[33mcommit a93424ca637a6d42c96d19cf366b27b52ac8e7c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 16:47:03 2021 +0200

    [setups/*/README] minor edits

[33mcommit 61fda32d3184decfbd96da7f84abc980540ae09c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 16:36:54 2021 +0200

    [docs] minor typo and link fixes

[33mcommit 9b4d130d6f6b28c6356e775d53bfac0cbfa3761a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 16:36:31 2021 +0200

    [docs] updated performance and FPGA results

[33mcommit d6fa36a1ff7ec2b14608231da265418315a1944a[m
Merge: 6375439d a87e8047
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 15:37:47 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 6375439d17ae42e2358e8400d708c68a00691185[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 15:37:28 2021 +0200

    minor edits, typo fixes and clean-ups

[33mcommit 8f4951af0ff836a4ea72b4bc861e4dd41865ebb1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 15:35:58 2021 +0200

    [docs] clean-ups; added application makfile hex target

[33mcommit 805ddc4f00223e775f9b4052793d8ef15d0f435b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 15:35:22 2021 +0200

    [sw] clean-up of makefiles; added "hex" target

[33mcommit 5606ff01a90f5f45853e19e99134d67da8ef6ba7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 15:22:56 2021 +0200

    [sw/image_gen] added option to generate plain hex memory init file #35

[33mcommit a87e8047e6485997a6eb176cd002847ee0a0eb50[m
Merge: 87ef9eff 4e371c63
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 6 15:21:25 2021 +0200

    Merge pull request #99 from umarcor/osflow-rework
    
    osflow rework

[33mcommit 4e371c636e1f79ea04d1e841f3d56d743770d3d7[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 20:37:03 2021 +0200

    [setups/osflow] rework: remove board Makefiles; create subdirs 'boards' and 'constraints'

[33mcommit c5c1c26acc18a35dc67ee2e6802dedd9e297a97d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 14:03:17 2021 +0200

    [ci/Windows] support prjtrellis; remove MINGW32

[33mcommit a356c24124dcf094ecf6a870fd39f64b42f69dc6[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jul 4 13:34:37 2021 +0200

    [setups/osflow] prepare for supporting ECP5

[33mcommit 87ef9eff369e9458f9ef5d94abac0fa4c6b25232[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 4 10:58:07 2021 +0200

    removed stale-bot

[33mcommit b1ed2beed6532fc2a28cf7864e6ea020739614e2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 19:08:26 2021 +0200

    [rtl/templates/system] added XIRQ

[33mcommit 1c88f0382f30a24b6a2b2f1360ef5dfd21fb56cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 19:00:12 2021 +0200

    removed README from osflow; updated setups README (#97)

[33mcommit 0df68cf96f2c19ff64c7ba81f7ee2f8b557d846f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 18:59:28 2021 +0200

    [sw] minor typo fixes

[33mcommit a89a09d688b942a7c43673cd83032d3e2a4fd62e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 16:14:00 2021 +0200

    added XIRQ tests to processor check environment

[33mcommit d111d65e9063d79f4ce1783337ccad85b364de4f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 16:12:40 2021 +0200

    added example program for new XIRQ controller

[33mcommit 10a433e032d8df81406adad5d80c2178a4722415[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 16:04:08 2021 +0200

    [docs] added new module: external interrupt controller (XIRQ)

[33mcommit 98e49609224afef8a862e35a23c048b2230d0f31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 16:00:17 2021 +0200

    :sparkles: [docs] added new module: external interrupt controller (XIRQ)

[33mcommit 81487b0c1f34720063525dbe108317f045a489be[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 15:59:16 2021 +0200

    :sparkles: [sw] added new module: external interrupt controller (XIRQ)

[33mcommit 4f339aae55f3f2f46244865bda1d7c59435d429c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 3 15:57:41 2021 +0200

    :sparkles: [rtl, setups] added new module: external interrupt controller (XIRQ)
    
    * up to 32 external interrupts
    * configurable trigger (via generics)
    *prioritized or non-prioritized servicing

[33mcommit b595f73d7018a5bbd5f81167d99b4d9d2560edeb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 20:06:06 2021 +0200

    [rtl/core] removed unused CPU FIRQ ack signal

[33mcommit 9e037d643f11ed8abd42fdcba9b2f223d239244e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 20:05:33 2021 +0200

    [rtl] relocated WDT and TRNG base addresses

[33mcommit f9578729c50b5b65c68b5a72016b4b727f60ffee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 20:05:19 2021 +0200

    [sw] relocated WDT and TRNG base addresses

[33mcommit 0857c14cd41ae9b42097f676b39a6f863a9701bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 20:05:08 2021 +0200

    [docs] relocated WDT and TRNG base addresses

[33mcommit fdde86811ddd5e3bbc032c4bf35f7038b84f1f4d[m
Merge: 8636de75 9bb3f497
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 12:42:33 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 8636de752a93387447eb64f710867bafa4fc08e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 12:42:30 2021 +0200

    fixing VUnit windows workflow

[33mcommit 2dfcf447b2bd2305cc5d0dfa2fbf710e1ce94666[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 12:31:49 2021 +0200

    [sw] minor edits

[33mcommit 5c12a9d78901547b3d041d62866c16c799fd03a0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 2 12:31:35 2021 +0200

    [rtl/templates] added stream link interface ports

[33mcommit 9bb3f497fa37f6a7715bbf5f25cbfd59603e02b8[m
Merge: 9f7022ec 2ad3f268
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 1 20:15:25 2021 +0200

    Merge pull request #95 from LarsAsplund/master
    
    Add loopback test for SLINK

[33mcommit 2ad3f2686f72aa95fc63107895661a70ad4b93a5[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Thu Jul 1 09:48:55 2021 +0200

    Added random stalls for SLINK tests.

[33mcommit 90c9918a7819d5f81254a57792c17014ec1af591[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Wed Jun 30 22:19:57 2021 +0200

    Add loopback test for SLINK

[33mcommit 0ec1bac9170e7d7c0a2283be3711591f59c54717[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 18:40:56 2021 +0200

    [docs] minor edits #91

[33mcommit 9f7022ecb1bb3bbf7adb8692c905af207ff449c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 18:30:51 2021 +0200

    [docs] [rtl] increased GPIO ports from 32-bit to 64-bit; removed pin-change interrupt

[33mcommit b6595c010c1acb2a540af0d48e4bd7c812d9021b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 18:27:01 2021 +0200

    [sim] [rtl] increased GPIO ports from 32-bit to 64-bit

[33mcommit b49edc6b48bffbb7ca3f714dce05552aeb4f236d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 18:26:43 2021 +0200

    [sw] increased GPIO ports from 32-bit to 64-bit; removed pin-change interrupt
    
    * removed sw/example/demo_gpio_irq (obsolete now)

[33mcommit 974c8399b743504333957278c286bd622d388b10[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 18:25:54 2021 +0200

    :warning: [rtl] increased GPIO ports from 32-bit to 64-bit; removed pin-change interrupt

[33mcommit 6819fdedfc283fc65e7c8ebb71eb7b2e6f636140[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 18:23:59 2021 +0200

    [rtl/core/slink] minor fix

[33mcommit e8891e6ea63a13ad511ce7e5fafa40698d6db3a0[m
Merge: 8caf190a d0a52aa7
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 16:46:16 2021 +0200

    Merge pull request #93 from umarcor/a7-filesets
    
    [setups/vivado] arty-a7-test-setup: define multiple filesets, set board, set language

[33mcommit 8caf190ad69cdb8572415d94f43e5b906e449f1a[m
Merge: faa9721e c588303f
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 30 16:27:23 2021 +0200

    Merge pull request #92 from umarcor/sim-cleanup
    
    sim: update readme

[33mcommit d0a52aa76d541a51ef5037ade455263f266b6bd3[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 13:49:51 2021 +0200

    [setups/vivado] arty-a7-test-setup: define multiple filesets, set board, set language

[33mcommit c588303f2a824bb7dd81a88900c2338f132afcff[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 30 11:10:51 2021 +0200

    sim: update readme

[33mcommit faa9721e1c9e7855c78e2a07322aef7b8e457ad8[m
Merge: 65e1b066 47fd64bc
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 19:35:35 2021 +0200

    Merge pull request #90 from LarsAsplund/master
    
    Make uart_rx a VUnit verification component. Step 4.

[33mcommit 47fd64bc27d6dbc5734644637540c4baca6e7b44[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Tue Jun 29 18:36:27 2021 +0200

    Make uart_rx a VUnit verification component. Step 4.
    
    Collect all generics into a single handle generic. A single generic is easier to pass
    around and makes it easier to update the VC without changing the user code.
    Also using a `check_uart` procedure to raise the abstraction of the API.

[33mcommit 65e1b066437cdccc5008f6ba07f3a2083cf6acbe[m
Merge: cddf8b9d db0f54de
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 18:13:32 2021 +0200

    Merge pull request #89 from LarsAsplund/master
    
    Make uart_rx a verification component

[33mcommit db0f54ded6c4888396ea872bd294c7991d8d2446[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Mon Jun 28 10:42:39 2021 +0200

    Make uart_rx a VUnit verification component. Step 3.
    
    Use standard message passing interfaces for handling synchronization with uart_rx
    and determine when to terminate the testbench.

[33mcommit f623de895c633a0f05ab730a722e29a542f75149[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sun Jun 27 23:22:11 2021 +0200

    Make uart_rx a VUnit verification component. Step 2.
    
    Create a cleaner interface to uart_rx and prepare for uart_rx to handle several different types of messages.

[33mcommit 2d0cc36aa8651630ffbf54c030dbfc9ff9b50ea2[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sun Jun 27 23:07:23 2021 +0200

    Make uart_rx a VUnit verification component. Step 1.
    
    Setup message passing communication to uart_rx. Message passing is asynchronous which
    means that the two uart_rx instances can be commanded to receive the expected data
    concurrently

[33mcommit cddf8b9d0f28d1f663bb4d62bfafc0d5969003f0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:50:34 2021 +0200

    [sw/example/processor_check] minor typo fix in UART1 test code

[33mcommit 2e35094d7ac98ea2f1154bfb68d22f508234a90e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:42:31 2021 +0200

    [docs/figures] minor edit

[33mcommit b7a25826bfb13dba7be5905bc7ad21d78c78920b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:42:11 2021 +0200

    [sw/example/processor_check] added SLINK check; added minimal UART result report output #89

[33mcommit 0c2ba8dfe2d9201069d332b541e6accb23c30a52[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:29:13 2021 +0200

    [sim, setups] added new stream link components

[33mcommit 054904de616cf7fa2028566b6b1eb0cdffe83a78[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:28:43 2021 +0200

    :sparkles: [docs] added new component: stream link interface (SLINK)

[33mcommit b8064a71e9c0ff2be54d4dd0bfc8964228594f6e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:28:06 2021 +0200

    [sw] added new component: stream link interface (SLINK)

[33mcommit 158c9ed721e85ca91896ab4497053ccf288f570a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:27:30 2021 +0200

    :sparkles: [rtl] added new component: stream link interface (SLINK)

[33mcommit 5264235edf48795d7581ae2395d159c861b36c8a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 15:18:36 2021 +0200

    [rtl/core/CFS] comment typo fixes

[33mcommit f76b2f62c1021b997ff629347f82d2d64b0247e7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 14:44:41 2021 +0200

    changed default GHDL sim time to 10ms

[33mcommit 8e5a78de172f299e6e98547a92f39b3d7d0464a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 14:22:55 2021 +0200

    [sw/lib] minor fix in UART simulation mode

[33mcommit d3078aea0e2b4e8c7f4f933e80ce757eebd971dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 14:21:42 2021 +0200

    [sw/lib] added missing prototype of after-main handler

[33mcommit b692848922621e476ef0202eaf91020a5312fdc2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 14:03:48 2021 +0200

    [setups/quartus/README.md] added FPGA utilization

[33mcommit 454d343d56e2ae76c48718bc4b23ac836edca639[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 13:58:55 2021 +0200

    [setups/radiant] clean-up; now using pre-defined top template (#77)

[33mcommit e407d3e79af7514c6e7383c54a5f223282cda245[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 13:08:26 2021 +0200

    [setups/quartus] corrected project script to use pre-defined top-template (#77)

[33mcommit 048e252fb6b54dafe7b5c8d4920e9c0fde11f2aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 29 12:56:55 2021 +0200

    [setups] removed obsolete UPduino_v2 setup

[33mcommit 5af685ba8111d7927040983b8f447ecf079f7504[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 17:19:47 2021 +0200

    :bug: fixed bug in CFS address mapping

[33mcommit 3541407c3e2797050b05a375bdb7e00f4c13140f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 14:54:06 2021 +0200

    minor edits and clean-.ups

[33mcommit a7f3c8d35c12d191de3e5a3a7c9e3bd4c9ce5fe5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 14:53:46 2021 +0200

    [sw] removed numerically-controlled oscillator (NCO) IO module

[33mcommit 7674ed12be98364c0e9defad9869f6d5a4de50c1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 14:53:02 2021 +0200

    [docs] removed numerically-controlled oscillator (NCO) IO module

[33mcommit 02e03d24ac7085e745ce138280721ae25c0fa4c2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 14:52:09 2021 +0200

    :warning: [rtl, sim, setups] removed numerically-controlled oscillator (NCO) IO module

[33mcommit 0be769bf93a433f50044f4f9caf4c90e7224f4cf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 12:02:30 2021 +0200

    [docs] removed top's FIRQ inputs; added new FIFO component

[33mcommit f269884902fb847fff5cdd1195b43e40b2ecbe17[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 12:01:44 2021 +0200

    :warning: [rtl,setups,sim,sw] removed top's FIRQ inputs
    
    see CHANGELOG.md for more details

[33mcommit 8dc0b30e3bac738f12e5dea8eadbe25ba23af483[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 27 12:00:00 2021 +0200

    sourced-out FIFO(s) into new HDL module

[33mcommit 16cfbb63a0aa105411e9dbc64b56e76f7a970881[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 26 20:03:10 2021 +0200

    :sparkles: added console options to configure/customize build-in bootloader
    
    * updated according sections in data sheet and user guide

[33mcommit 2f15bf655c103f68e2cc52d387b4ce0e51ed17ef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 26 15:39:01 2021 +0200

    [sw] minor edits

[33mcommit 33c841f3a3ef561ce41f47f141f9474b2471482e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 26 15:38:40 2021 +0200

    edit of previous update: trapped loads do NOT alter dst register

[33mcommit 7bd101b9c5d9766b997ea68405560772ceb78749[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 26 11:20:33 2021 +0200

    changed function type of "after main handler"
    
    void -> int

[33mcommit d68db0fe0b224f0574c0858fbf8eaef2365304a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 25 18:54:06 2021 +0200

    [sw/example/processor_check] clean-up...

[33mcommit dec4da45520c7e4ba77be04427f20281c703329b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 25 18:53:43 2021 +0200

    [core/rtl] added size check to memory init function

[33mcommit 6170ed03813806c28e762623d25f19175304ea86[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 25 18:02:42 2021 +0200

    [sw/examples/processor_check] minor edits
    
    * changed PMP tests
    * added security failure checks (make sure that invalid loads/reads do NOT return any relevant data)

[33mcommit f6457a52fcb3599f1016ea4aabea7c37a5812bbe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 25 18:01:48 2021 +0200

    [rtl/core] simplified execution control fsm
    
    * less hardware utilization
    * :lock: now ensures to write ZERO to destination register if there is an exception during a load operation

[33mcommit 6cb945f7c4cc084c2a3d5ef76c27da886b0e009f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 25 18:00:37 2021 +0200

    made bootloader even more HW configuration independent
    
    * GPIO is optional
    * MTIME is optional
    * SPI is optional
    * UART is optional but highly recommended
    
    general clean-up of bootloader source code

[33mcommit 93388084b94010da3d11328a162a5161ed8d0d67[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 25 17:50:01 2021 +0200

    minor edits

[33mcommit ba52c75be4db5617813e8deda3e473ddedd5d127[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 24 16:32:32 2021 +0200

    [rtl] added VHDL report to show CPU/processor-IO configuration

[33mcommit 2dbfb85561c36fdced9d190e16c5f92380c57c98[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 24 16:28:48 2021 +0200

    [docs] added impressum
    
    according to § 5 TMG

[33mcommit 05e47d6fb495fae67c7121ae7e995c0008dd3a96[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 24 16:16:46 2021 +0200

    [sim] Zmmul should be disabled for full-scale simulation

[33mcommit f2e69a36b3351567e34e07a425b3be53ddd08661[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 24 15:58:16 2021 +0200

    :sparkles: [docs] added RISC-V "Zmmul" ISA extensions
    
    * implement mltiplication instructions only (no division instructions)
    * sub-extensions of M extensions
    * for size-constrained setups; requires ~50% less hardware than M extensions
    * div[u] and rem[u] instructions will raise an illegal instruction exception

[33mcommit 27070f7ea7a15cbdf180daecd9d8fd7b77e4586c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 24 15:56:57 2021 +0200

    [rtl, sw] added "Zmmul" RISC-V ISA extensions
    
    * implement mltiplication instructions only (no division instructions)
    * sub-extensions of M extensions
    * for size-constrained setups; requires ~50% less hardware than M extensions
    * div[u] and rem[u] instructions will raise an illegal instruction exception

[33mcommit da135374ed46c338b074251ea705aeaa6cd49a80[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 23 18:02:51 2021 +0200

    added DOI

[33mcommit 569e8d799a7e76eb7b30b78d9bc09a0047290f6a[m[33m ([m[1;33mtag: v1.5.7[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 23 17:34:28 2021 +0200

    preparing new release (v1.5.7)

[33mcommit 35e3d5381bc5da0be4b522aed5a436705bf63b7a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 18:08:50 2021 +0200

    :bug: fixed bug in debugger park loop
    
    missing fence.i isntruction to resync with DM's program buffer if i-cache is implemented

[33mcommit 997f91b1df7f48d65cbf75c0e5eac37f59e82c1f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 18:07:10 2021 +0200

    minor clean-up

[33mcommit c797e33c66aeb41e0997736ece51798e43b97fba[m
Merge: fb7a8e25 a23103b7
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 17:17:37 2021 +0200

    Merge pull request #82 from umarcor/PR2
    
    [sim] use custom VUnit loggers for better verbosity control

[33mcommit fb7a8e254933c310d8c380f5095ed115f524429d[m
Merge: 1927deeb a9403963
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 17:14:20 2021 +0200

    Merge pull request #83 from umarcor/osflow-verilog
    
    [setups/osflow] support optionally using Verilog sources and add Fomu MixedLanguage example

[33mcommit 1927deebde9f27b5efeffecca5c41925f6734563[m
Merge: 27b59b5b b2da250a
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 17:08:18 2021 +0200

    Merge pull request #84 from umarcor/icesugar-pcf
    
    [setups/osflow/iCESugar] update PCF

[33mcommit 27b59b5b844f42ab2d0716b7ea970ff75b63c63c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 16:51:42 2021 +0200

    removed obsolete TINY_SHIFT_EN generic; added new shifter co-processor

[33mcommit d7be0a37cc8ad17d5d7b5a5ceaefa508ce796da0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 16:51:01 2021 +0200

    added new VHDL source file (shifter co-processor)

[33mcommit 470832ea42c6fe96282a0e69e6f57a614ee6c912[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 16:50:19 2021 +0200

    removed obsolete TINY_SHIFT_EN generic

[33mcommit 89668c76bb0e63835b3065f802e5ee73e6998d4f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 21 16:49:35 2021 +0200

    CPU ALU code clean-up
    
    * removed TINY_SHIFT_EN generic
    * ALU shifter is now a dedicated co-processor
    * removed CSR-read "dummy co-processor"
    * simplified multi-cycle instruction decoding logic

[33mcommit a9403963dc3aedc926e98094b7437a54bca291f5[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jun 20 18:17:33 2021 +0200

    [setups/examples/MixedLanguage] it's SB_HFOSC, not SB_HFOSS

[33mcommit b2da250a932fbd64ce1dce0e1e17ad09eaea25c1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jun 20 14:51:35 2021 +0200

    [setups/osflow/iCESugar] update PCF

[33mcommit ec9722ee55d6b391fb693b2af62298f85c689694[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jun 20 14:17:12 2021 +0200

    [setups/examples] add Fomu MixedLanguage

[33mcommit 49d58fed44f4ea15ce869c422400c81bbac4941f[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jun 20 13:33:12 2021 +0200

    [setups/osflow] support optionally using Verilog sources

[33mcommit a23103b7ca6a185aa6b58c84920c6160ca37d29c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 19 13:57:43 2021 +0200

    [sim/VUnit] make both UARTs visible

[33mcommit 23f17f4ef535cb33c299d7498fba421a985c4869[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sat May 15 12:28:10 2021 +0200

    [sim] use custom VUnit loggers for better verbosity control
    
    Rather than a separate logging to show what's being produced on a UART output the checking of that value can log passing checks.
    Separate loggers for UART0 and UART1 means that this can be configured for UART0 only where the interesting stuff happens.

[33mcommit e0cc32fe3f8399a37195b2f5a0bcc7a5da73cb93[m
Merge: 6b89ae46 807cecbf
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 19 23:47:59 2021 +0200

    Merge pull request #80 from LarsAsplund/master
    
    Added option to print a selected subset of information from processor…

[33mcommit 6b89ae46fe8f22c66fcdac41432fb56194b323f9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 18 19:32:11 2021 +0200

    clean-up of CPU co-processor system
    
    * removed unused co-processor slots 4,5,6,7

[33mcommit c7c7b5bbb9e4244c5aac1ef67ebded834d9a7416[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 18 15:40:42 2021 +0200

    [CHANGELOG] typo fix

[33mcommit 807cecbfff80619dfc62dbb089956c391c1aa534[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 18 15:20:13 2021 +0200

    fixed expected UART output
    
    2x 0x00 from UART0/1 hardware tests

[33mcommit 260e2cf39b38aed699ce64ed8bb9de0f713ee3a7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 18 15:11:55 2021 +0200

    reworked processor check program
    
    * using macros to redirect prints
    * minor edits to allow execution on real (but constrained) hardware
    * minor edits to avoid execution of tests that might cause dead locks if certain HW modules are not implemented

[33mcommit 0eecbda6300f165e435f721af64e8f06943acf3f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 17 20:56:19 2021 +0200

    added UART1 simulation mode enable

[33mcommit b4ff38d088eece93f6b4a5d5d848e7622bf628b0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 17 20:55:19 2021 +0200

    going back to 16kB IMEM

[33mcommit 8e519470dd40fbd7c2a941ee5a3b00b93ff30a56[m
Merge: 098030d3 89a18071
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 17 17:18:11 2021 +0200

    Merge pull request #79 from umarcor/add-mailmap
    
    add mailmap

[33mcommit 917b7c8d1247c774225de21ef8548ec1f01e2705[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sat Jun 12 18:09:11 2021 +0200

    Added option to print a selected subset of information from processor_check to UART0 during simulation.

[33mcommit 89a180718c6ad85f5c496bb39005e3ebc5f93520[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 21:37:01 2021 +0200

    add mailmap

[33mcommit 098030d314a0d358fa155a09b451981e7f3776d7[m
Merge: 43c418a8 c8d614a7
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 16 17:00:28 2021 +0200

    Merge pull request #76 from umarcor/icesugar-minimal
    
    [setups/examples] add iCESugar Minimal

[33mcommit 43c418a8b9f5315a5aab16027ab9eab9ed7f84b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 16 16:54:48 2021 +0200

    [docs] made boot configuration more explicit: DIRECT and INDIRECT

[33mcommit 4314c3e50012743d1089efa5907851594404ba77[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 16 16:28:49 2021 +0200

    made bootloader more configuration independent
    
    bootloader only uses the _first_ 512 bytes of the DMEM; hence, DMEM size is irrelevant as long it is >= 512 bytes

[33mcommit 09dceaa0c59156af61d4b63c1ace517e908f10fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 16 16:23:56 2021 +0200

    added internal DMEM size notifier

[33mcommit fe2adda2624e6610ae525f997e889c4a570e698d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 16 16:23:25 2021 +0200

    added UART and GPIO top signals

[33mcommit c8d614a773c1c70255e6dc8ce8a61b9a1f971cec[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 16 02:36:00 2021 +0200

    [setups/examples] add iCESugar Minimal

[33mcommit 17d3a3fc2cc321cd587654335dfb89daf2dbb09b[m
Merge: 91415cd4 11967543
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 16 14:05:56 2021 +0200

    Merge pull request #75 from umarcor/icesugar
    
    iCESugar

[33mcommit 11967543c927a8f518c00453d566c824864447cb[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 16 00:57:57 2021 +0200

    [setup/examples] add iCESugar MinimalBoot

[33mcommit 2ab6d1dc2714027e8da41b1be6eb096176cf76f4[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 16 00:56:01 2021 +0200

    [setups/examples] support iCESugar

[33mcommit c7c60d8f6bdb798c893e4b88ac29b38f6b934efd[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 16 00:55:39 2021 +0200

    [setups/osflow] add iCESugar

[33mcommit 91415cd48d1e903b913f04b866377a5030cd36b3[m
Merge: 7aa5812c 0f1c3dd7
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 15 22:02:48 2021 +0200

    Merge pull request #74 from umarcor/fix-artifacts
    
    [ci/generate-job-matrix] fix UPduino_v3 artifact (bitstream) extension

[33mcommit 0f1c3dd7a89e475437b096dceb4de565cacbf65c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 15 21:51:02 2021 +0200

    [ci/generate-job-matrix] fix UPduino_v3 artifact (bitstream) extension

[33mcommit 7aa5812c0bbbe1e232fd1770e50263b7416fbe73[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 15 16:55:48 2021 +0200

    updated riscv-arch-test to new project folders/fils
    
    :warning: zifencei test is currently disabled: this tests requires a different aproach to get the executable as IMEM has to be implemented as RAM to support self-modifying code

[33mcommit e3fc8806914042c2516c92711febdd9f669c816c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 15 16:14:23 2021 +0200

    fixed UART tests
    
    * both UARTs are correctly initilized
    * complete backup of original config during explicit low-level UART tests

[33mcommit d2c600a178f6178d6738c5f5bf25880673818d05[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 15 16:13:29 2021 +0200

    added explicit (re-)enable functions for IO

[33mcommit c7a2b9ddf343c09e8b7809b204093fab8f4a7020[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 22:49:05 2021 +0200

    [rtl/core] minor edit

[33mcommit a9be63728852d3521e6b52e2fc87e608a48f2a0d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 22:48:39 2021 +0200

    [sw/lib] minor edit

[33mcommit c41580b0883268865871b406032ec3c3e12daa5a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 22:34:08 2021 +0200

    :bug: [sw/examples/processor_check] fixed bug in UART tests
    
    corrupted UART1 configuration

[33mcommit 1d0b03f3d0b133ed654be141802783da0e5fe344[m
Merge: 574b9696 f49a108c
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 20:26:56 2021 +0200

    Merge pull request #73 from umarcor/sim-exec
    
    [sim] make ghdl_sim.sh executable

[33mcommit 574b9696a59011216ce6a3c857e27aeaf0fb1ab2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 18:50:17 2021 +0200

    :rocket: [docs/userguide] reworked and updated entire user guide
    
    :construction: a few sections are still work-in-progress

[33mcommit be24a4a2e0b310a84bae33d67b80753c7e442565[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 16:51:36 2021 +0200

    fixed missing if-gen ELSE

[33mcommit afb93dadb9b5487765c1ba16daecbf7ce263a41a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 16:14:51 2021 +0200

    added if-gen (for large ext mem b); using variables for external IMEM & DMEM

[33mcommit 5a5f4e1ba253f9287ccd62e8f8dbcbe09d4c83a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 15:59:29 2021 +0200

    [core/rtl] minor clean-up

[33mcommit 5db75c72b57b164d4a0c495fe8b69597bcc8563a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 15:51:49 2021 +0200

    [docs/datasheet] reworked "Executable Image Format" section
    
    + minor clean-ups and fixes

[33mcommit 4931e9768408d030972c5d16bf4ed90c92eae4f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 14 15:50:45 2021 +0200

    :sparkles: reworked IMEM.ROM and BOOTROM memory concept
    
    * bootloader can use up to 32kb - synthesis will create an accordingly sized ROM
    * linker script: now using logical instruction address space size of 2GB - bootloader or hardware sanity checks test if the application executable fits into the actual physical memory size, which is configured via the top generics

[33mcommit f49a108c6dc7a7ead9d54c35fb38893598f774cc[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun Jun 13 23:39:57 2021 +0100

    [sim] make ghdl_sim.sh executable

[33mcommit 434cbfec7fe7fdb72f600835df6c7d3ddf7a251e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 23:48:16 2021 +0200

    [sim/ghdl_sim.sh] fixed path

[33mcommit 93697d88425e64e4f21ede3d7b20842730e5ae1f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 23:45:29 2021 +0200

    [.github/workflows/Processor] (re-)added explicit script permissions
    
    * gihub desktop (on windows) seems to have issues when uploading file permissions :thinking:

[33mcommit 771f8169ff6fc6a56dccc5985167cd6cbf27bd89[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 23:36:50 2021 +0200

    [docs/datasheet] minor fixes and clean-ups

[33mcommit 6eff0dfea90e72d9ea5d60a61ba66060b182ae2d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 23:36:19 2021 +0200

    [sim} clean-up
    
    * removed vivado folder
    * moved ghdl script to sim folder
    * removed ghdl folder

[33mcommit b6564bd924e5fb1d4de0e12877379db45a866a28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 23:35:03 2021 +0200

    [.github\workflows\Windows] removed push trigger

[33mcommit d7b3e34b00c517d45f959ec2304c82e00bfbb006[m
Merge: 8ad06576 faeba4b8
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 20:42:00 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit faeba4b8379c2a28f1becf2a78dcd5b581a3b1f5[m
Merge: 97189e33 560d3a00
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 19:58:49 2021 +0200

    Merge pull request #71 from umarcor/ci-msys2
    
    Add MSYS2 jobs to continuous integration workflows

[33mcommit 8ad065762b580d6acdac75db15dc142b6c9efe1e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:47:39 2021 +0200

    [README] minor edits

[33mcommit d441377a4916e541be56fe201da32dff48281ca7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:45:26 2021 +0200

    [docs/datasheet] minor updates, fixes, clean-ups

[33mcommit 97189e3385c0c48155acfa92fb2267e80e7fed0e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:21:28 2021 +0200

    [sim] fixed missing size in meory init function

[33mcommit 0eca8ff4970c0a9a01253986b1c36370e2b1c025[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:14:03 2021 +0200

    [sim/rtl_modules] clean-up

[33mcommit 79eda513ebc6e37040c019b2234fe1e5691791ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:13:45 2021 +0200

    [sim] fixed memory init function name

[33mcommit 6e1be7c3944b8b8f0fe3b28f9b0f2018ec4c1e7a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:13:29 2021 +0200

    :books: [docs/datasheet] rework (see commit description)
    
    * updated (boot config) generics
    * added section "Memory Configuration"
    * added section "Boot Configuration"

[33mcommit 3539dbf99b5fce7f7946dda0b5bdffc994a94a21[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:10:38 2021 +0200

    [docs/figures] scaled images

[33mcommit b6bee3b9b975c5d41f8a84e14ef706ad9ef95a73[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:10:06 2021 +0200

    [sim] fixed bug in memory signal declaration

[33mcommit 9d578d03d1af15fb33a5dbf86a51498227ae5c73[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 17:01:35 2021 +0200

    [ice40up memories] fixed bug in sanity checks

[33mcommit 1dcd0fd1071132fed8cd85354b6194ed028dce24[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:55:02 2021 +0200

    [CHANGELOG] added v1.5.6.9

[33mcommit db5f1caf3c5af20187a0f46ae1eeb260d1e0e2c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:54:41 2021 +0200

    [sim] updated testbench
    
    * updated new/changes boot config generics
    * now using memory types from package
    * minor edits and fixes

[33mcommit 9230b92e8eb206bfa4f5d040f60e9920e8e3e6c7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:53:46 2021 +0200

    [setups/osflow] updated generics

[33mcommit 476314c8ee00e5a977dc41f8524d283e6bda00d6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:53:29 2021 +0200

    [setups/radiant] updated generics; reverted technology lib unification (#67)
    
    Lattice Radiant cannot resolve iCE40 library if not explicitly assigned.

[33mcommit 70de3bbf9076f09bd96da87619659724dd7d2966[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:51:54 2021 +0200

    [rtl/templates] updated (boot config) generics

[33mcommit 77b3c1ecc1650b12b32ca64ab3f4f243d67e6299[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:50:32 2021 +0200

    reworked processor boot configuration / generics
    
    * removed MEM_INT_IMEM_ROM generic
    * removed BOOTLOADER_EN generic
    * added unified INT_BOOTLOADER_EN generic
    * updated SYSINFO flags + software library + bootloader

[33mcommit b905bffd5b99f1b57ad0f7ae9717efc65c20235b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:48:40 2021 +0200

    updated image_generator
    
    * now using unconstrained arrays as initialization image
    * unified memory/image types

[33mcommit 64788b9b5bef34798d4ee71bd357d28bc14bf9ef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:46:52 2021 +0200

    [rtl/core/imem] added missing global enable

[33mcommit a0850048bb9958b11ece27dbedeb8e4144efc428[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 16:28:11 2021 +0200

    reworked processor-internal memories (HDL description)

[33mcommit 673a07472ca1a1883dda13205b0d580cf8c44a65[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 13 15:50:49 2021 +0200

    [sw/example/processor_check] fixed intermediate UART enable/disable

[33mcommit d4ca543ef13916cad97fbb8e6f6f82518dc6b030[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 12 22:02:17 2021 +0200

    [docs/userguide] added note to add neorv32 package when instantiating neorv32 top

[33mcommit 6a190cfcd98e732b6daf185ce40fc99473e8afa7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 12 22:01:51 2021 +0200

    [docs] added notes regarding platform-specific memory mapping optimization

[33mcommit 65b11162011255c0a6cde5d5ff27296220573cf2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 12 21:38:17 2021 +0200

    :bug: [rtl/core] fixed bug in instruction cache
    
    cache controller might have missed resync/"clear-and-reload" requests from `fence.i` instructions

[33mcommit 560d3a00ffc2b0954b2e6ca689a072bf68c3baa6[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 12 20:13:44 2021 +0200

    [ci] move Windows/MSYS2 jobs to a new workflow named 'Windows'

[33mcommit 80355a54bf01577b7788c9c62927cc68da1bcb26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 12 20:16:06 2021 +0200

    [setups/radiant/UPduino_v3] removed bitstream

[33mcommit bcb5ba5b109eaeb8774dc4e13ba0394a3eee30a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 12 20:07:28 2021 +0200

    [docs/datasheet] added IMEM/DMEM address space note #62

[33mcommit 9bbfee4240b8142380eeb9a54e623e9de9ab977a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 17:45:47 2021 +0200

    [ci/Implementation] generate example matrix in a single step

[33mcommit 25feb84ab230b51b45a7b549a0820ab0f05918c8[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 17:32:17 2021 +0200

    [ci] add MSYS2 jobs to Implementation and Processor workflows

[33mcommit 561b920d741f957f738c74440747a2f773ff6912[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 12 18:33:12 2021 +0200

    [riscv-arch-test] removed legacy files
    
    files for the old/deprecated test framework

[33mcommit 3b4ea762a9f1208287faa08a4bbc699a947646a7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 22:21:44 2021 +0200

    [sw/example/processor_check] re-added FENCE.I test
    
    currently, this cannot be covered by the riscv-arch-test due to boot concept issues #62

[33mcommit f66a333c64a1fdb01ed897e2f153923051a45494[m
Merge: 7e2936ac 0d5303f8
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 22:02:21 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 7e2936ac6840bde1dc4394730e1f4340775f76f8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 22:02:17 2021 +0200

    :warning: [riscv-arch-test] disabling ZIFENCEI test for now
    
    due to boot cofniguration issues #62

[33mcommit 43d91b1001acad12ddeee450c8cd7ea8f2ef6c67[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 21:09:56 2021 +0200

    [docs/datasheet] minor edits

[33mcommit ebd298da11c71eb4f4f071213ca0f7e862efd810[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 19:38:17 2021 +0200

    [sw/example/blink_led] simplification
    
    executable is now less than 4kB

[33mcommit 978bb3896f7a515fd53e42228b1a86ea627ec861[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 19:29:57 2021 +0200

    [rtl/core] minor edits

[33mcommit 0d5303f81ccc8daaf392e219e29d89891131042b[m
Merge: 66b032eb ccb6eb73
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 19:13:12 2021 +0200

    Merge pull request #72 from umarcor/ci-cleanup
    
    [ci/riscv-arch-test] make test script executable

[33mcommit 66b032eb5e13cee52adab104f717c7a4824bbf6e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 19:06:06 2021 +0200

    [riscv-arch-test/port-neorv32] fixed default location for SIM UART output

[33mcommit ccb6eb730156e4acfdcf7ea3ef398b3e093de485[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 18:50:34 2021 +0200

    [ci/riscv-arch-test] make test script executable

[33mcommit 167c019af27012ef90e1d50cbd2fd54345a42b0c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 18:02:39 2021 +0200

    [sim/ghdl] fixed #70

[33mcommit 3e65e80114259bf546b9e87cdb4c8c86d461dd5b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 17:56:21 2021 +0200

    [sim/vivado] updated pre-defined waveform config

[33mcommit 3e128162d3b9abe8f4ae039af424f0c2974a8953[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 17:56:00 2021 +0200

    [docs/datasheet] fixed default testbench name

[33mcommit b53feb7f6db68dd0ae8b6ca48f9ef44cbc29aa11[m
Merge: 7ef6848d 0dac2cf9
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:48:41 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 7ef6848df0bbe46e0954bc9fdafe6525969c25ac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:48:21 2021 +0200

    [riscv-arch-tests/poer-neorv32] fixed sim data output file generation

[33mcommit 0dac2cf9556d053b4ace0afac3f075ed7ef5f464[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:37:13 2021 +0200

    [workflows/riscv-arch-test] fixed GCC installation

[33mcommit 89bcb207cfe9dc4628521eed46eac10624f639eb[m
Merge: 4455f5f9 b39686f4
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:33:45 2021 +0200

    Merge pull request #69 from umarcor/vunit-checking
    
    VUnit checking

[33mcommit b39686f4ff5a6180300b586e78b2c65460e055ac[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Fri Jun 11 14:33:51 2021 +0200

    [setups/vivado] update simulation fileset

[33mcommit ef752aa87dcadd231bdf99479d4f5e98bcf626fd[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Fri Jun 11 14:33:11 2021 +0200

    [riscv-arch-test] update name of simple tb file

[33mcommit 2a7cc87cde09195fd9cdecc7c9fc9d58972e61e0[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Mon May 17 11:21:12 2021 +0200

    [sim/uart_rx] replace VHDL assert and report with basic VUnit logging and checking
    
    Using basic VUnit logging and checking provides (in this case):
    
    * Simpler calls for common assertions like checking equality.
    * Color coded logs that makes it easier to spot log entries with different severity levels

[33mcommit f5858ac5ae7ad8924bc5fb107ed921c8de20876e[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 15:26:44 2021 +0200

    [sim] use uart_rx_simple in uart_tb_simple

[33mcommit 145d49c24cc95afcfb69fa004e62f2003b573421[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:15:28 2021 +0200

    [docs/datasheet] fixed link to user guide

[33mcommit 07a80ed97675d50b51d60dba5a4c3b0a432c89f1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 15:46:17 2021 +0200

    [ci/Processor] split VUnit tests to a job

[33mcommit 4455f5f9daf171cd3173c3bb1d7576dc68f5eb73[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:09:03 2021 +0200

    [docs/datasheet, README] added project rationale (first draft)

[33mcommit 4739468841395f1b7c1e7b0ce1d8f545d48dcdc8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 16:07:11 2021 +0200

    [docs/datasheet] added ISA extensions to overview

[33mcommit 2d8fa93b415f9b3f233aff0d3ffdf696fffab449[m
Merge: 8597274a c6796bdd
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 14:21:06 2021 +0200

    Merge pull request #67 from umarcor/ice40
    
    rename logical library 'iCE40UP' to 'iCE40'

[33mcommit 8597274a54180905d45f15fad56c9d527e971454[m
Merge: f2e6e468 dcc6513f
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 14:06:39 2021 +0200

    Merge pull request #66 from umarcor/PR1
    
    [sim/VUnit] support CLI argument for selecting the expected UART responses

[33mcommit c6796bdda686dd0199f2cce877ad64f8e65846db[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 11:59:04 2021 +0200

    rename logical library 'iCE40UP' to 'iCE40'

[33mcommit c7dbf488907079de50d3551add7e69477ea8b4b1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 01:46:41 2021 +0200

    [gitignore] update path to examples

[33mcommit 67ef2f9d0689f32fcd1602516962f86550e426f0[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri Jun 11 01:46:03 2021 +0200

    [gitignore] ignore generated SVG files in docs/figures

[33mcommit dcc6513f2c11ea9111e0e47f93efc795a40012ae[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sat Jun 5 14:47:44 2021 +0200

    [sim/VUnit] support CLI argument for selecting the expected UART responses

[33mcommit f2e6e4688da32953a3421a4ef5b758d4f19345e6[m
Merge: a795497f 289b6551
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 11 07:39:33 2021 +0200

    Merge pull request #65 from umarcor/sim-vunit
    
    Add VUnit

[33mcommit 289b6551d263299ef2bc6d039a632e92e11e82e7[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 22:14:40 2021 +0200

    [ci/Processor] clean simulation artifacts between runs

[33mcommit ca7df7f7831d1fc3ef5258ae333ac6f241063c48[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 21:55:06 2021 +0200

    [sim] make VUnit script executable

[33mcommit 76df52155f4078ba939a35a629065457700aa3c2[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sat Jun 5 14:47:44 2021 +0200

    [ci/Processor] run VUnit test(s)

[33mcommit b8a77576a9f4aba2da912384e11fa61e444b9b11[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Sat May 15 12:28:10 2021 +0200

    [sim] create VUnit run script and testbench for using VCs and other advanced verification features

[33mcommit a795497fb110b228d6f13f006c25c4dc27edd48a[m
Merge: 94f9aba3 07f9123a
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 10 21:40:31 2021 +0200

    Merge pull request #64 from umarcor/sim-uart
    
    [sim] split UART logging component, make self-checking

[33mcommit 07f9123a930cd67118fb078cbf7ce15716db0135[m
Author: Lars Asplund <lars.anders.asplund@gmail.com>
Date:   Thu Jun 10 20:35:50 2021 +0200

    [sim] split UART logging component, make self-checking

[33mcommit 94f9aba382078f61d21055dbbc803e69f7635573[m
Merge: 16e27df2 b14135db
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 10 21:30:17 2021 +0200

    Merge pull request #63 from umarcor/add-fomu
    
    Add Fomu

[33mcommit b14135db4fb9bacd708ac7df5c4f6106757b9c11[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 9 10:52:40 2021 +0200

    [setups/examples] override imem source for Fomu Minimal

[33mcommit 691fd566cc08ffb96ad71bc7ddd521d0f27603d1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 9 10:49:21 2021 +0200

    [setups/osflow] allow overriding the sources of imem and dmem

[33mcommit bf21a4939099b0f8d51b7adcd9c03b1bd8b3c150[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 00:24:48 2021 +0200

    [setups/osflow] update 'clean' targets

[33mcommit e6ff2a52e2781d08c13ca705edaa7393a1803b8d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 9 10:02:25 2021 +0200

    [setups/examples] add Fomu Minimal

[33mcommit 80865906f0b1890cd6e73885d91392cc788053df[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 9 09:59:26 2021 +0200

    [setups/examples] add Minimal

[33mcommit 0bf917c1d8f700702e6472874f092b242d7442d1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 9 09:55:03 2021 +0200

    [rtl/templates/processor] add 'Minimal' Processor template

[33mcommit 81f52e6413f27d970f6fc64fc98debb43de00ccc[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 9 09:52:09 2021 +0200

    [rtl/templates/processor] MinimalBoot: export BOOTLOADER_EN, hide ON_CHIP_DEBUGGER_EN

[33mcommit d895553d66956d76155c8c5eaf5ddd622517347a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Mon Jun 7 10:13:13 2021 +0200

    [setups/examples] add Fomu UP5KDemo

[33mcommit 073264c34b5a93ff6d9edfdae152622b5f663666[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 00:29:16 2021 +0200

    [setups/examples] add Fomu MinimalBoot

[33mcommit 98161fa37b7b740f654d21a9849ff0b0edfee9fe[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 16:06:30 2021 +0200

    [rtl/templates/processor] style

[33mcommit 16e27df2b64423a5b1b80370e8ac52b3b842a206[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 10 16:44:20 2021 +0200

    [README, docs] updated links to setups folder

[33mcommit 4a49850db65f61c543abad6cbef0fc9ac4babaff[m
Merge: 74ee0261 ac931dbd
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 10 15:47:52 2021 +0200

    Merge pull request #61 from umarcor/vivado-run
    
    Reorganise setups

[33mcommit ac931dbd3b77994d75b5b14572a9bae6f6b460b3[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 03:32:52 2021 +0200

    [ci/Implementation] fix typo in pull_request path filter

[33mcommit 4e46ef03ff66746eb8029442f0e9d701a63d2243[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 03:03:27 2021 +0200

    [setups/vivado] add README

[33mcommit d8afe8eb303a26df262a83deb9fcd0019a3e9785[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 02:52:12 2021 +0200

    [setups] create subdirs 'vivado', 'quartus' and 'radiant'

[33mcommit 44089a2ae92ad730088562af3fd244cb165aee28[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 02:25:31 2021 +0200

    [setups/boards/arty-a7] support generating bitstream in batch mode; use single script for multiple board models

[33mcommit 01c3135af78edf589fa86ea37e36993a59829493[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 10 02:27:27 2021 +0200

    [setups/boards/nexys-a7] support generating bitstream in batch mode; use single script for multiple board models

[33mcommit 74ee02612e7f484c1ce077ec104f0560f9a970f3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 20:14:26 2021 +0200

    [setup/board] fixed copy error... (last commit)

[33mcommit 66a29d9a1ff4f3ba2639acdf433cff50eadf8afd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 20:00:01 2021 +0200

    [setup/boards] updated sanity checks of ice40up SRAM memory components

[33mcommit 00e62512fb5eaea0955d67f0c4af571ce81c6bda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 19:04:28 2021 +0200

    [sw/lib] fixed minor bug in "debug handler" of run-time environment ( #56 )

[33mcommit f4cb52ad4a9510eee94b50d1ca9e819712a7b509[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 16:53:16 2021 +0200

    minor edits and path fixes

[33mcommit 46badfa3a824b642b9448d04355a0d1f886ac524[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 16:48:45 2021 +0200

    [setups] path fixes

[33mcommit d086fc66daf594925d8c6dc5c646b10fdf1d1d34[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 16:47:58 2021 +0200

    [docs] minor link fixes

[33mcommit 789b6b2d6d44dcea6e94f47b2219a7451dc5b173[m
Merge: b5638c8a e848a7ab
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 16:23:10 2021 +0200

    Merge pull request #59 from umarcor/add-examples
    
    Add examples

[33mcommit e848a7ab34f59079aeb92a7b3763d098910c7141[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 15:50:05 2021 +0200

    [setups/examples] support Fomu

[33mcommit 2f571c346ec359f8bc16ce606b8425af3b89f343[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 04:06:49 2021 +0200

    [setups/boards/osflow] add Fomu
    
    * PCF and boards.mk brought from the 'hdl' subdir in im-tomu/fomu-workshop

[33mcommit 2e08d600da9b5d44619ce784957fbf73bfc1b3f7[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 13:09:10 2021 +0200

    [setups/examples] support loading through arg TASK=load

[33mcommit a7a501c800771c5a00c3ff639f1bef6015020f3a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 14:31:04 2021 +0200

    [rtl/templates/processor] UP5KDemo: expose more generics and ports

[33mcommit 4a081dc5b720f356a8020650b271dbfbb97f279d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 14:23:40 2021 +0200

    [rtl/templates/processor] MinimalBoot: expose more generics and ports

[33mcommit 48a31fa12ae48dd6131fb9af1b6db9b9d52ccdb6[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 11:53:32 2021 +0200

    [setups] partially rework READMEs related to osflow and to the examples

[33mcommit 78d5e63b62763b236cba8fb0da03f555f804c399[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 11:53:32 2021 +0200

    [setups] move 'boards' and 'examples' into subdir 'setups'

[33mcommit 8bfb75fc79e813dff4a2fbc89e6a2da986d675d5[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 05:44:20 2021 +0200

    update refs to the templates in the docs, boards, etc.

[33mcommit f04d5689ef42630ffaa61c75d2fc0a4e5f224a15[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 05:47:45 2021 +0200

    update .gitignore

[33mcommit 161bafe63f27af0303fa4d183a7cc6f0b8fdf68f[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 03:22:38 2021 +0200

    [examples] add UPduino_v3 MinimalBoot

[33mcommit 359bef93426ce9d28d4e0e37d5ae530536363701[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 03:39:56 2021 +0200

    [rtl/templates/processor] add 'MinimalBoot' Processor template

[33mcommit 3d26eccbbb691445b1d7529cbc4589471747ceb5[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 13:40:00 2021 +0200

    [rtl/templates/processor] cleanup UP5KDemo

[33mcommit 534d6e13c2cafa71cb24e1a71ee8f37a4f28efff[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 02:53:49 2021 +0200

    create subdir 'examples'
    
    * [boards/osflow/synthesis.mk] support both DESIGN_SRC and BOARD_SRC
    * [boards/osflow/UPduino_v3] move BoardTop and ProcessorTop to examples and rtl/templates/processor, respectively
    * [ci] update 'Implementation' workflow

[33mcommit aff6b1999b9fa04c02834ed40ca683e24f1447b0[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 02:40:28 2021 +0200

    [rtl] rename 'top_templates' to 'templates'; create subdirs 'processor' and 'system'

[33mcommit cc98fcbdf5a9697d745ac76d8fb88ea9c957b6af[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 02:03:03 2021 +0200

    [boards/osflow/UPduino_v3] split 'top' into 'ProcessorTop' and 'BoardTop'

[33mcommit 9546401ca14c4b4a304bcce0f9864fb5501698d5[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 01:50:08 2021 +0200

    [boards/osflow/UPduino_v3/Makefile] update target 'clean'

[33mcommit 7c263b5815d02fb8b6440ecc9b67fca4034b6ef2[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 01:34:41 2021 +0200

    [boards/osflow/UPduino_v3] rename constraints file, it's not for neorv32 only

[33mcommit 785c298e4937aaecaffc7fe082111809dafa117f[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 01:27:04 2021 +0200

    [boards/osflow] split 'PnR_Bit.mk' from UPduino_v3 Makefile

[33mcommit abc3ff91b19ab6f869de69a77167787f99779772[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 01:28:51 2021 +0200

    [boards/osflow/UPduino_v3] remove bitstream, which is available as CI artifacts

[33mcommit 6bca3b75d9be8d1f254fbdd2b7f67ad555c42cf1[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 00:52:05 2021 +0200

    [boards/osflow] split 'synthesis.mk' from UPduino_v3 Makefile

[33mcommit 7b6b33bfaf3143a898d9145c8280fc84c3badaaa[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 00:45:15 2021 +0200

    [boards/osflow] add 'tools.mk' to allow overriding/wrapping EDA tools
    
    * Subdir 'hdl' from fomu-workshop was used as a reference

[33mcommit bf02db9b06f67cf39fa57d8c54e64593eee4bae6[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 00:37:36 2021 +0200

    [boards/osflow] split 'filesets.mk' from UPduino_v3 makefile

[33mcommit f5cf1377af09cf57f4210cf6b685455c898b8291[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sat Jun 5 00:39:17 2021 +0200

    [boards/osflow/UPduino_v3/Makefile] support Yosys with built-in GHDL by making '-m ghdl' optional through GHDL_PLUGIN_MODULE

[33mcommit 24caa01d64741be943ee365f32cc36e1e558825c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 06:50:06 2021 +0200

    [boards/osflow] create subdir 'devices/ice40'; move dmem, imem and component sources there

[33mcommit 3b2aab65a7e2123f0485d643fa610d09439f6397[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 06:48:53 2021 +0200

    [boards] create subdir osflow; move and rename UPduino_v3_ghdl-yosys-nexpnr to osflow/UPduino_v3

[33mcommit b5638c8a63d2828eed7644796c3393851cba9617[m
Merge: 03b94dbc 5b9316f5
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 12:01:56 2021 +0200

    Merge pull request #60 from umarcor/doc/common-include
    
    [docs] add attrs.adoc and attrs.main.adoc

[33mcommit 03b94dbc2a463b69825bfc92b5aafb631a0ac183[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 11:48:36 2021 +0200

    [README] minor edits

[33mcommit fc4ba0f2e9d27e05bc45f5963d02c4755bc95b72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 8 11:40:54 2021 +0200

    v1.5.6.7: added option to configure "async" Wishbone RX path
    
    * clean-up of Wishbone interface module (dead code removal)
    * added new package constant `wb_rx_buffer_c` to configure SYNC (default) or ASYNC Wishbone RX path (allows trade-off between performance/latency and timing closure

[33mcommit 5b9316f5e77d1c32ecd444093c25a1e7fd60be3c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue Jun 8 10:18:09 2021 +0200

    [docs] add attrs.adoc and attrs.main.adoc

[33mcommit 529267dff57eaa9f179433da5e386a1f0106a63f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 22:41:28 2021 +0200

    [docs/datasheet] added OCD security note

[33mcommit 527d0fbc07f768667f9b8730ef036ffdfc3f9e7e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:41:07 2021 +0200

    [rtl/core] updated bootloader image (due to modified crt0.S)

[33mcommit d5b8c776a663e691bf2593a9c25499d1ca74108d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:40:11 2021 +0200

    [sw/example/processor_check] added "after-main" handler; minor edits

[33mcommit 02e3a7d930d0987cf73c14c7181dc55bb59723d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:39:42 2021 +0200

    :sparkles: [sw/common, docs] added option to register "after-main handler"; crt0 clean-up

[33mcommit 6d348950cc8e524967b0c28dc6a4472ad30fdf49[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:20:51 2021 +0200

    [docs/datasheet] minor fixes

[33mcommit ac12d838729967a61c417972ae63348c98c60e6c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:20:04 2021 +0200

    [sw/common] minor linker-script clean-up

[33mcommit 94034acd36f74d0c984036c6c93bdabe8ebf53ad[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:19:44 2021 +0200

    [sw/lib] clean-up of MISA extension definitions; added C++ guarding

[33mcommit 31eb2af6ab8fbd445b1efd6aaa53511eab0959b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 19:18:34 2021 +0200

    [rtl/core] minor comment/typo fixes

[33mcommit 3cdb9aebb26f14ef40ddb19e1b196ae8faa43c35[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jun 7 16:33:57 2021 +0200

    [sw/lib/include/neorv32_intrinsics] C++11 fix

[33mcommit 9bc305917c525ef5c2260ceaa940b75fb70dc253[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 18:09:52 2021 +0200

    [sim/testbench] enabled i-cache

[33mcommit be10f94a1d044d3d778c98c86f67851ed9e9eec5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 18:09:19 2021 +0200

    [sw/lib/source/rte] mindor edits

[33mcommit 494ec083ab269448c435b9881e0920249d368e6a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 18:08:59 2021 +0200

    [CHANGELOG] added version 1.5.6.6

[33mcommit ed7cb9ee1c34c49f71e9427d2d54d951d59f89a4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 18:08:32 2021 +0200

    :bug: [sw/lib] fixed PWM base address

[33mcommit 39552d46208319aec160a0ecc0b925022141cc51[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 18:07:51 2021 +0200

    hardware perf. counters (HPM): removed user-level access
    
    * removed `hpmcounter*` & `hpmcounter*h` CSRs
    * according `mcountern` bits are hardwired to zero
    * only machine-mode can access HPMs

[33mcommit a8daf129109bc2754ca6699a698a82787eed9baa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 13:38:39 2021 +0200

    [rtl/core] clean-up of assert notes

[33mcommit cae2e689f51dc6d5c03661a2e87a4a9f9c137a75[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 13:33:08 2021 +0200

    [sw/lib/source/neorv32_cpu] minor fix

[33mcommit 15393d22470c0a87ba1aea81095367a4ae15c2a8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 00:39:33 2021 +0200

    [docs/datasheet] minor edits

[33mcommit 2ce40fc44de45b286e438b3f694ffc0fa0e37c61[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 00:38:35 2021 +0200

    [sw/example/processor_check] refined counter tests
    
    counter tests (mtime, cycle, instret) now check for low-to-high overflow

[33mcommit 979084caf2467d83b483278c6e87819d04329e2b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 6 00:37:33 2021 +0200

    [rtl] reworked 64-bit counter overflow logic
    
    `cycle`, `instret`, `hpmcounter` + `mtime`: overflow logic: now using dedicated CARRY chain instead of overflow detector

[33mcommit 652a022669ffd2272276dea2c05b2783de8fe3bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 5 11:32:49 2021 +0200

    [sw] fixed return value of all example programs
    
    return 0: successful execution
    return !=0: error during execution

[33mcommit 9989cd765470b3fc60b403fa3ba807ce3229f4e4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 5 10:55:29 2021 +0200

    removed debug mode `stepie` flag
    
    used to allow interrupts during single-stepping. stepie support in GCC/gdb is very poor + the debugger can emulate interrupt behaviour

[33mcommit c590cf16adde907ff0654afaf0a0834395eb8864[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 5 10:54:42 2021 +0200

    removed debug mode `stepie` flag
    
    used to allow interrupts during single-stepping. stepie support in GCC/gdb is very poor + the debugger can emulate interrupt behaviour

[33mcommit f46fedf1feca3537c74df8bd9de87d5dbd3b9860[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 5 00:55:46 2021 +0200

    [rtl/core] fixed mtime_o low word to high word overflow inconsistency #58

[33mcommit a5e4ab2f32c9a8fb4005a502581d7c3ec390ee56[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 5 00:32:27 2021 +0200

    [docs/datasheet] minor typo fix

[33mcommit 83fce8252cde0c85a7f70f8a6cd77a118fb4a44d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 22:29:54 2021 +0200

    [sw/example/processor_check] minor edits

[33mcommit aaab4fce48e1d1005435cd53c79d0327f40cf903[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 22:29:32 2021 +0200

    :bug: [rtl/core] fixed bug in rtl address space layout

[33mcommit cf1492f10a94a71b387fa84d40762fd484db27a7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:35:58 2021 +0200

    [CHANGELOG] added version 1.5.6.3

[33mcommit 058b8759db00140cb6c531504baa1bf8e1ac1ca1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:35:42 2021 +0200

    [rtl/core] updated pre-built application images

[33mcommit 574934617f3e3e483a36db51ea496569aeb544ad[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:34:20 2021 +0200

    [rtl/core] minor edits

[33mcommit 3210dc98829dfc5190d8d28c3b04649cace8b06d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:34:05 2021 +0200

    [docs/datasheet] minor edits

[33mcommit fa9f8addb29429b7fec06c3c6fbc667e5fb97bbe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:32:33 2021 +0200

    [sim, rtl/top_templates, boards] updated PWM configuration generic and output signal vector

[33mcommit 0af274506bfb1c8441f4b2e51363438e554d8b8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:29:05 2021 +0200

    [docs/figure/neorv32_processor] removed B extension; added "up to 60 PWM channels"

[33mcommit cd0dbdd771b1d9aca30f2af6c34b72cdd7c2605a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:23:05 2021 +0200

    :bug: [cpu/rtl] fixed bug/typo in minstreth CSR counter logic

[33mcommit 37a105f10c4e1114d1464a67cd6f1766ef41089b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:17:30 2021 +0200

    [docs/src_adoc/pwm] updated PWM module
    
    PWM module now allows to configure up to 60 channels

[33mcommit 75b60e0080230d1d7f5dc4a4fe85f3b4ba87043d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:16:45 2021 +0200

    [docs/src_adoc] updated PWM configuration generic

[33mcommit a13c276e54300d949beba934d0aafccc857c79ac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:16:18 2021 +0200

    [sw/lib] updated PWM driver functions
    
    added function to discover number of implemented PWM channels

[33mcommit 08c89d53b541da148802fc36405b453946b03bbd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:15:28 2021 +0200

    [sw/lib] clean-up of IO address space

[33mcommit 003d60409997e18ec2efbf9b0a59f78f1fc50eff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:15:06 2021 +0200

    [rtl/core] relocated base addresses of PWM and CFS

[33mcommit 0cf1fe87d7c2796bf3ce85c8a4775b32b2e18de0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:14:43 2021 +0200

    [rtl/core] removed "IO_PWM_EN" generic; replaced by "IO_PWM_NUM_CH"
    
    IO_PWM_NUM_CH allows to configure up to 60 PWM channels; the PWM controller is omitted when IO_PWM_NUM_CH = 0

[33mcommit c596966d40697c50b32d433c3d4be462563402b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:13:17 2021 +0200

    [rtl/core/pwm] added option to configure up to 60 PWM channels

[33mcommit a8d91ebaafe30160aaddd0010e1c85b0c19bae01[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 21:12:19 2021 +0200

    [rtl/core, docs/datasheet] reverted back to 32 CFS registers; relocated CFS base address

[33mcommit 53c3d4108557ca7e2a0e44d1d1db00238e1f092c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:42:01 2021 +0200

    [README, docs/datasheet] minor typo/layout fixes

[33mcommit 10f223d27c68206bcd702c3c26e2945d4f1a550a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:41:18 2021 +0200

    [CHANGELOG] added version 1.5.6.3

[33mcommit 6609b984bb0c79a0a8dcf19017f81db26d78e3e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:40:53 2021 +0200

    [sw/lib] typo fix

[33mcommit 0138f0f8ff38fe73b713a584636bdbd9a61096c2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:39:36 2021 +0200

    [rtl/core/cfs] minor comment updates

[33mcommit 8ab6d21bb7a8e8d3a686aba57cced8945219b247[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:39:13 2021 +0200

    [docs/datasheet] increased CFS register space to 64x4 bytes

[33mcommit d375a4c223eb6797f43d17dc3b2d394f75b4d086[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:37:41 2021 +0200

    [sw/common, sw/lib, rtl/core] increased processor-internal IO space

[33mcommit c203ee7d95f1322453595f35bd0535267a1d5710[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:36:38 2021 +0200

    [rtl/core] minor edits

[33mcommit 92422f097eaa2bd841dab7f04843387c1d48dccc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:36:01 2021 +0200

    [docs/datasheet] added sections to processor generics

[33mcommit d6d634b62d23da67dc5d0eaaac0921d127b5a56e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 16:35:39 2021 +0200

    [docs/figures] increased processor-internal IO space

[33mcommit bd54389cdb525671d141f33b8fe74a44be3d16d5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 11:04:21 2021 +0200

    [README] clean-up

[33mcommit 5c6d331f8a863f893b398d67b5ed87af8db472fd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:20:38 2021 +0200

    [.github/workflow/Implementation] fxed typo

[33mcommit 4ad4c16d7e9095e672102eb892097d40b8e90922[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:18:25 2021 +0200

    [Makefile] removed doxygen from all
    
    redundant

[33mcommit 9372dfeadfec69c5d654e40594936e0847f72a7d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:17:17 2021 +0200

    [.github/worklfows/Implementation] fine-tuned trigger

[33mcommit 3b20c62151d6e9bcf8cfa62a32b4d759aeccbfd3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:11:53 2021 +0200

    [Makefile] added target to build doxygen documentation

[33mcommit d9aed6fbbbb3dbbd18e1a3c3f2f9eabed69c0cd2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:09:29 2021 +0200

    [docs/userguide] fixed (online) links; clean-up of "build documentation" section

[33mcommit 5c440d94c87055be316994417c322c954c09745a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:08:56 2021 +0200

    [docs/datasheet] fixed link

[33mcommit fdeb2b6221db359d299177e63c80c4afae393d08[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 10:08:37 2021 +0200

    [README] fixed links to new user guide

[33mcommit 9b833acbfa811f6df58c85ee9c34b260e1f1c746[m
Merge: 9f838c18 e78e43d1
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 4 09:43:22 2021 +0200

    Merge pull request #53 from umarcor/doc/ug
    
    docs: split User Guide

[33mcommit e78e43d15b20763819df8c8174f5d7e2d29b89d8[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Wed Jun 2 05:34:00 2021 +0200

    docs: split User Guide

[33mcommit 9f838c18617393885dd5b154803f6c8eca252d93[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 20:49:31 2021 +0200

    :warning: removed B (bit manip.) ISA extension from project
    
    This extension will be deleted from the project for the time being.
    
    see https://github.com/stnolting/neorv32/projects/7

[33mcommit 70915c9c061e0ff172e6e4a8be9c56510cb36d39[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 20:44:17 2021 +0200

    [boards/UPduino_v2] setup is obsolete!

[33mcommit a935db26eaa8d4a2ffc6a701b48d05a9ad5170bf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 19:10:04 2021 +0200

    [CHANGELOG] added version 1.5.6.1

[33mcommit 064ac9a0ca161c9743acbd2a372cf58424663400[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 19:09:49 2021 +0200

    [README] minor edit

[33mcommit 82523811a18d9f60e3c10a5f01bddc48c62c965f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 19:09:34 2021 +0200

    [docs/src_adoc] CPU/HPM counter size configuration can now also be 0-bit (not counters implemented at all)
    
    for area-constrained setups the CPU counters (cycle and instret) can be completely removed by setting CPU_CNT_WIDTH generic to 0

[33mcommit f647277aa2769d8647258b7d10fc30b73534b85b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 19:08:01 2021 +0200

    [sw/lib] minor edits in compiler ISA flags notifiers

[33mcommit 3aec51238d2ad3a9fb2ea94f49afefc45e0d0b9b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 19:07:38 2021 +0200

    [rtl, sim] fixed CPU/HPM counter size configuration generics comments

[33mcommit da08c08906957dba4a78eafb1e6543779a8fe0d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 19:06:54 2021 +0200

    [rtl/core] CPU/HPM counter size configuration can now also be 0 bit (no counters at all)

[33mcommit eb93247c02d39733415d490578ee84fddefbe5af[m
Merge: 0fe6fa2a 437d0280
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 3 17:57:00 2021 +0200

    Merge pull request #54 from umarcor/ci/cleanup
    
    [ci] cleanup

[33mcommit 437d02803c76a72964a5474a1107050cb3468e60[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 05:00:39 2021 +0200

    [ci/Processor] use LLVM backend

[33mcommit 8061d1789c01b920744224de77d983b219722466[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 04:49:42 2021 +0200

    [sim/ghdl] use bash for loop

[33mcommit b46958ad612208aea4d83d5e21de767c1f8ba335[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 04:44:06 2021 +0200

    [sim/ghdl] use import and make, instead of analyze and elaborate

[33mcommit d692097626591e841556f0a4bc8dc39691f90d3e[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 03:44:39 2021 +0200

    [ci] cleanup shell scripts

[33mcommit a942d35db55eb50888fc3f98e2ef82c9ef802e7d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 03:55:37 2021 +0200

    [ci] make shell scripts executable

[33mcommit 0c44d16417c21b1e4307c93d7c52928ef6efbc3f[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Thu Jun 3 03:56:39 2021 +0200

    [sim/ghdl] make script executable

[33mcommit 0fe6fa2a2224bbf7dc60a6884c73940879d1b362[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 2 19:41:10 2021 +0200

    [README] rework #42

[33mcommit b5c4d9a5e963df7fa3fc0a3bb9130402a1106cbc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 2 19:40:02 2021 +0200

    [docs/src_adoc] moved 'X' ISA extension to according section

[33mcommit 40a24766f9867cb708c50cff43ee13170a26f4fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 2 18:55:58 2021 +0200

    [docs/src_adoc] citing - added BibTeX #42

[33mcommit 2723525a564b0569c619c3588188590cd14d67b3[m[33m ([m[1;33mtag: v1.5.6.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 20:00:15 2021 +0200

    preparing release v1.5.6.0

[33mcommit 210d8b5fe1dd785dc57252767f42c85097d837cc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 19:54:08 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit 3eb9bea523f127f2fde4859799a750d46c09f33c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 19:53:50 2021 +0200

    [README] minor edits

[33mcommit 5717fda549900bbe66b2a4561356d54a0fe1850f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 18:36:21 2021 +0200

    [README] minor updates and clean-ups

[33mcommit b1775767b63faabb45f66761cd4effed4ebc0659[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 18:36:03 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit 2f163720f5ec5206f79e8af5092af57b65940b79[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 18:35:45 2021 +0200

    [docs/src_adoc] added "legal" overview to its own section

[33mcommit 9e09f315f51f2ce6b48f9ec45219f075cebab523[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:47:32 2021 +0200

    [CHANGELOG] added version 1.5.5.13

[33mcommit ca21559e518d2b98304f58d62bc03d5a357792bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:47:02 2021 +0200

    [sw/examples/floating_point_test] fixed broken function call; now also using "RUN_CHECK" compile switch

[33mcommit 7a2017cd25ef4eb26440627ae609b398de0e0490[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:37:14 2021 +0200

    [docs/src_adoc] fixed external memory interface endianness configuration #50

[33mcommit 960471087e191c91e8ed0b3d366a69ad4fc870a4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:36:29 2021 +0200

    [docs/src_adoc] CPU/processor/project is LITTLE-ENDIAN #50

[33mcommit 2cbb7cce8044bde95e7362621e65f2b8a4a11df8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:35:33 2021 +0200

    [docs/src_adoc] removed mstatush CSR and mstatus.ube flag #50

[33mcommit e728d288f32919a6f1763c44bdb4be7d51821fc7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:34:38 2021 +0200

    [sw/lib] fixed RTE's endiannes/architecture info output

[33mcommit 200a47c8594eb6045922e5db70f4fc37465149e7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:33:24 2021 +0200

    :warning: [sw/bootloader, rtl/core] bootloader now uses LITTLE-endian byte-order for executables

[33mcommit bf154017b0121214a7848e5788581c28f0f91670[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:32:36 2021 +0200

    :warning: [sw/image_gen] image generator now generates LITTLE-endian EXECUTABLES (neorv32_exe.bin) #50

[33mcommit 7aca4658db40638e503f8e35db56a27a69f27fda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:26:11 2021 +0200

    [rtl/core, sim] fixed endianness configuration of external memory interface #50

[33mcommit 3ac468921aff4d162da54a68e546e4cd1e5f290d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:25:21 2021 +0200

    [rtl/core] removed mstatush CSR and mstatus.ube flag

[33mcommit 0deddc4253660ff495b474015a9e5b9d7e4633e0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:24:06 2021 +0200

    [sw/lib] removed mstatush CSR and mstatus.ube flag #50

[33mcommit cb81ea51705ee4b804f6cc3c5a12859c16c05e26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 1 17:19:14 2021 +0200

    [sw/lib] minor edits

[33mcommit 2be3da7f1ef60e3ab2125b25de913e1063ffa0f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 19:17:04 2021 +0200

    [docs/src_adoc] added citing information

[33mcommit 8dbe35a55654fd91dc86ad50400e4b740e4ba840[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 19:16:44 2021 +0200

    mret instruction now clear mstatus.mpp CSR bits
    
    according to newest RISC-V priv. spec.

[33mcommit ffd92a0ea8bd13ac50b0f2bf06d4976df54c8654[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:52:16 2021 +0200

    [CHANGELOG] added version 1.5.5.11

[33mcommit 8ad8c003873537cd947f941ed2941738f31a9ec0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:45:18 2021 +0200

    [.gitignore] added docs/NEORV32.pdf

[33mcommit 49f68e1205618f05a54610afb1f8fdc9dbcd6c26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:43:56 2021 +0200

    [rtl/core] mtval CSR is now read-only; fixed default value of dcsr_prv if user mode is not implemented

[33mcommit e5b0c59af9017c5b3169f79184780bcd8d59fafc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:34:17 2021 +0200

    [sw/lib] mtval CSR is now read-only

[33mcommit b11919d667a480658e615adf9ef85a222378eb0f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:33:58 2021 +0200

    [docs/src_adoc/cpu_csr] added missing "mcause" CSR; added links to CSR overview; "MTVAL" CSR is now read-only!

[33mcommit 8e2883aa81298dd6f12aa35fb3be295eb09536b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:33:18 2021 +0200

    [docs/src_adoc/soc] minor edits

[33mcommit 16d45f85ea95fd26c6420dac3011b379a2c38537[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:32:59 2021 +0200

    [docs/src_adoc/cpu] minor fixes

[33mcommit c26524d2968242aa37ec985c779922ebf5488be0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 31 13:02:45 2021 +0200

    [Makefile] added (default) help target

[33mcommit 7deabfac75a3d44ab2303d3d48f0abcf07af0f48[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:33:32 2021 +0200

    [README] minor edits

[33mcommit 17570972e639dcb09e77b2d396d4d008b69ce772[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:31:17 2021 +0200

    [CHANGELOG] added version 1.5.5.10

[33mcommit 54183aedc5ffbc2a757930d22e63822df9dc8cd7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:28:58 2021 +0200

    :bug: [rtl/core/neorv32_top] fixed bug in SoC reset system; code clean-up
    
    * fixed bug in reset generator: SoC reset was stuck on zero when on-chip debugger not implemented
    * reworked reset generator (simplified)
    * reworked SoC bus infrastructure (module's "response" bus)

[33mcommit 5031b51a24059313d50755c1f36dd32e836e861a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:26:56 2021 +0200

    [sim/neorv32_tb] minor edits

[33mcommit 2c1188255c601b5df26dd5c4ce9105fdfc302aba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:26:32 2021 +0200

    [rtl/core] code clean-up

[33mcommit 06eef197b2be59e4042ff8d0ecd253cd50d80ab9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:26:17 2021 +0200

    [rtl/core] minor edits

[33mcommit 2747546f659a1e13abb79662498550069464387f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:25:47 2021 +0200

    [rtl/core] renamed "and/or/xor all bits" functions; code clean-up

[33mcommit 74456158dd5a6d356e9cab97afe1ff6db5f10eb6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:25:05 2021 +0200

    [rtl/core] renamed "or/and/xor all bits of vector" functions

[33mcommit c4f2aca1662ba2d0eaa7e069dc677d355344830b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 30 18:18:26 2021 +0200

    [sw/ocd-firmware] minor comment endits

[33mcommit 6748b0ff2a71dfd78849d5897bbcb694620d7c17[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 29 13:25:07 2021 +0200

    [README] minor edits

[33mcommit f0bbba35202e916d11438c20b94cbf72ebe8c327[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 29 13:17:29 2021 +0200

    [docs/src_adoc/overview] minor edits in example FPGA setup results

[33mcommit d40879900048ba9f27703680b588ca7b479422fa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 29 12:41:03 2021 +0200

    [docs/src_adoc/getting_started] removed OCD tutorial's "work in progress note"

[33mcommit ebc83ba02a0bbae54c3b997a9ed16fbf9ba3a6d1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 29 12:40:32 2021 +0200

    [docs/src_adoc/cpu] added missing FIRQ channel numbers

[33mcommit 24be15fa19e54abff339612a0d68943ee9ee9f2a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 19:29:06 2021 +0200

    [README] layout/content experiments

[33mcommit 7cb9f35d4f83dc21ce8d25b740055fa1a46877c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 19:27:02 2021 +0200

    [sw/common/crt0.S] moved WFI instruction into endless wait loop

[33mcommit 1a3503d774f33a1ea92dae70ae7436d5df395534[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 16:30:04 2021 +0200

    [docs/figures] minor layout edits

[33mcommit 0f3020d20d8eba241a2b3a68eb375abeb37afeb3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 16:24:00 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit 29c61645b934c6f3fcc3496769562e23afc37813[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 13:12:22 2021 +0200

    [docs/src_adoc/cpu] added note: how can the CPU discover ISa extensions

[33mcommit 2bea835d204329777aaf8b43f33a8665a90631b0[m
Merge: 6108870d 9474a3db
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 12:41:25 2021 +0200

    Merge pull request #40 from umarcor/feat/docs/diagram
    
    Use asciidoctor-diagram with wavedrom support

[33mcommit 6108870d9180742cba37aeac1812fc7fad41548b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:09:28 2021 +0200

    [CHANGELOG] added version 1.5.5.9

[33mcommit c84c182543d2e793da92b15cbb71f5554ab29e3b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:09:04 2021 +0200

    [docs/src_adoc] integrated DBMEM into DM

[33mcommit a690c3fb6bee3f0e5e9d8b5fdf05ad837f6f86a7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:08:41 2021 +0200

    [docs/src_adoc] version update

[33mcommit 839d49174142e1de9b52926225af83218cec6849[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:08:24 2021 +0200

    [docs/src_adoc] executables can also uploaded via GDB

[33mcommit bc67116a81398af3b84fb2e686c7f699f35d64ef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:07:56 2021 +0200

    [docs/src_adoc] if Zifencei is disabled a fence.i instruction executes as nop

[33mcommit 21ec3103d5872af7022d355934ae30ef957aad85[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:07:15 2021 +0200

    [docs/figures] removed now-obsolete DBMEM VHDL component

[33mcommit 31bd7b27da35626102345307695343f33d8e3463[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:06:48 2021 +0200

    [docs/src_adoc] removed now-obsolete DBMEM VHDL componen

[33mcommit 4bdaea6ec4cecd4e1fc2eeb88c110fafc113e64f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:06:24 2021 +0200

    [sim/ghdl] removed now-obsolete DBMEM VHDL component

[33mcommit 328700cf6ccc7f216dd4d116e4ba636d35d0fa15[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:05:58 2021 +0200

    [rtl/core/neorv32_cpu_*] minor edits and code clean-ups

[33mcommit 9c205645116fa55f7594d37851df91517f4901e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:05:19 2021 +0200

    [boards/*] removed now-obsolete DBMEM VHDL component

[33mcommit 47c8fb4721d89fcb35759b3cfb38fd4a388d3587[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 28 11:04:39 2021 +0200

    [rtl/core] integrated DBMEM into DM
    
    integrated the fomer "debug memory (DBMEM)" into the "debug module (DM)"; neorv32_debug_dbmem.vhd is no longer needed

[33mcommit 232057f3bd65206d5a5464e4ffdb9cc8354e65e0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 18:10:53 2021 +0200

    [docs/src_adoc] fixed including of images into tables

[33mcommit 15f281dcc2804e96b996e650c9b875dfab52160a[m
Merge: caef3a24 72aa786d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 17:43:18 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit caef3a242dbd81ca317cba204cd73c008e37a353[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 17:43:14 2021 +0200

    [README] added quick-start/quick-links sections

[33mcommit 1c63ebeb3eb3a752bbd92312d5bea2f87e96840a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 17:42:52 2021 +0200

    [docs/src_adoc/on_chip_debugger] added UNIMPLEMNTED debug mode CSR bits

[33mcommit fe3c18042e23d80fe2719cc931ffeb4f4a70a261[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 17:42:20 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit ffc7df45ab84b437db8b02da41719b43515c5726[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 17:42:05 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit 9474a3db0fe86e195a6a23510b59fb62505509dd[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Mon May 24 02:05:29 2021 +0200

    [docs] use asciidoctor-diagram with wavedrom support

[33mcommit 4a3ad9f591d452418300e276f8eedb305483aeed[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun May 23 23:38:37 2021 +0200

    update gitignore

[33mcommit 72aa786de911bc21856b23af849f359681f6b487[m
Merge: 8433e89d 2e918e5f
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 26 16:08:22 2021 +0200

    Merge pull request #44 from umarcor/ci/fix-tag
    
    [ci] prevent tag 'nightly' being used in revnumber

[33mcommit 8433e89d27c3fe644188e07a876972cc5f0d25ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 18:57:55 2021 +0200

    :sparkles: [docs/src_adoc/getting_started] added on-chip-debugger + gdb tutorial

[33mcommit ec08215c8b5eb7e3efa96cc0ad8219fe6a3677bf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 18:57:23 2021 +0200

    [docs/src_adoc/on_chip_debugger] minor edits

[33mcommit fc56f697bca5d93eab0437c70a0c64c631f7e7b6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 16:52:04 2021 +0200

    [sw] added OpenOCD configuration file for on-chip debugger

[33mcommit 82cbed8e016bfa4bf7ba366bdc5d3aa2c33cfa39[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 16:51:40 2021 +0200

    [docs/src_adoc/overview] added openocd folder

[33mcommit d1d168336b97d4c10c2608220221209ee6802de9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 16:30:50 2021 +0200

    [docs/src_adoc] renamed folder "on-chip-debugger" -> "ocd-firmware"

[33mcommit 9f013d12a8bcdbd8686bbeaa524f4b40291d0a07[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 16:29:57 2021 +0200

    [sw] renamed "on-chip-debugger" -> "ocd-firmware"

[33mcommit 2e918e5f151b25022d4118bd06a6d7a4da9f92a8[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 25 16:14:04 2021 +0200

    [ci] prevent tag 'nightly' being used in revnumber

[33mcommit 83dbb3c12cc88c888afe162eeb1de59b38697f7e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 16:26:57 2021 +0200

    [docs/src_adoc] added OCD "park loop" code reference + note

[33mcommit d1d1b8058aa354f135a34dec37f2107ff3822a2f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 16:26:21 2021 +0200

    [sw] added sources for on-chip debugger's "park loop" code

[33mcommit 8a5a1904febb93f06931fa86a828980a757ac7ab[m
Merge: 47c339f4 0e91d253
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 15:51:55 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 47c339f485e09dd713828b95dd80ae2f6108c2d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 15:50:18 2021 +0200

    [docs/src_adoc] started writing HOWTO: debugging with openocd + gdb

[33mcommit 0e91d253899a362cd7efcdc573c8d2726eb51ad8[m
Merge: cf861e8f 3ee1ac5d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 15:46:28 2021 +0200

    Merge pull request #43 from umarcor/patch-1
    
    [ci] set fetch-depth to 0, for git describe to work

[33mcommit 3ee1ac5df065237d6df22b701901737c052e21ca[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 25 15:02:43 2021 +0200

    [ci] set fetch-depth to 0, for git describe to work

[33mcommit 663a40aee3f13f4a61ab1e8c2432ed602e9cce80[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:47:37 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit cf861e8ff63cd5da19943e6f3ca5c2a274e7df77[m
Merge: a6d4e182 d06e1830
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:31:46 2021 +0200

    Merge pull request #38 from umarcor/feat/docs/favicon
    
    [docs] add favicon

[33mcommit 8471e18480f4e25badc9dc09a8ea362300c07b3b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:27:35 2021 +0200

    [riscv-arch-test] minor edits - now using git submodule #34

[33mcommit a6d4e182b61cc8940378d987239f207b503578ec[m
Merge: a9f80b2b c5bc9777
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:25:34 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit a9f80b2b324b850473ee45b1a3cc3a9cab34e2e0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:25:06 2021 +0200

     [riscv-arch-test] minor fix

[33mcommit 39e16b66442abf6085be808d1ad50c25a69c73f2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:13:59 2021 +0200

    [riscv-arch-test] now using submodule for RISC-V architecture test framework

[33mcommit 77af6ea0e20017398aa1fe3fc219781d0cdd35ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 14:13:23 2021 +0200

    [.gitmodules] added; added riscv-arch-test

[33mcommit d06e1830d36f2ef60dbdb1944ce9f8831461007f[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun May 23 01:58:33 2021 +0200

    [docs] add favicon

[33mcommit c5bc977714189a73c6165a76a3c70663992aa42a[m
Merge: 46d79d90 be20472b
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 13:47:18 2021 +0200

    Merge pull request #41 from umarcor/feat/docs/revnumber
    
    [docs] set revnumber through git describe

[33mcommit 46d79d90a4516024f2b9c9f0623831394d4e8fd2[m
Merge: f9d3b8c1 050125a6
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue May 25 13:26:22 2021 +0200

    Merge pull request #39 from umarcor/feat/ci/implementation
    
    Add GitHub Actions workflow 'Implementation'

[33mcommit be20472b8ef83d4ea30df730f6c751d0e1238814[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Mon May 24 03:28:31 2021 +0200

    [docs] set revnumber through git describe

[33mcommit 050125a669f08bcbdf0292bfa5c13d43239c2412[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun May 23 18:04:46 2021 +0200

    [ci] rename workflow 'Processor Check' to 'Processor'

[33mcommit cf3f4a8754d77aaaad1eaf23d61de061df5ef9aa[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Sun May 23 13:58:46 2021 +0200

    [ci] add GitHub Actions workflow 'Implementation'

[33mcommit f9d3b8c12f5c9d24404b2af92b675433797762df[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 16:23:38 2021 +0200

    [docs] removed NEORV32.pdf

[33mcommit 7f2944e79047694cbd1fda1d2f637116370bb773[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 16:23:17 2021 +0200

    [README] minor typo fixes

[33mcommit 343046db5670e4a3db6402d97dbc4229a4632c3e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 14:17:44 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit a39abfcfd958aff9c1c6bbcb59be9f5244f375d8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 14:17:33 2021 +0200

    [README] heavy clean-up; added lots of links to the online documentattion

[33mcommit a9a7c81a14890ddb8859722eaad45ef42e0df0aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 12:32:08 2021 +0200

    [CONTRIBUTING.md] reworked

[33mcommit 7354b98d9aa9a4afd62d2be113e59de8da858cb4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 11:29:31 2021 +0200

    [docs/src_adoc/on_chip_debugger] updates and fixes

[33mcommit f72ed7b327999a45fa577edba336d5727f633f62[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 11:27:52 2021 +0200

    [docs/src_adoc/getting_started] updated instructions on how to bul documentation(s)

[33mcommit 335bab883e3ac5e6752cbb046399492148291d25[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 11:27:16 2021 +0200

    [boards/UPduino_v3] added on-chip debugger modules & signals

[33mcommit 03e02b4665535653990c25ad689f78df85a66ee3[m
Merge: e094c3ab d476a07d
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 08:40:03 2021 +0200

    Merge pull request #32 from umarcor/ci/tip
    
    [ci] use eine/tip for uploading artifacts to (pre-)releases

[33mcommit e094c3ab11608e781fcbbe3b028ba22faead06ec[m
Merge: a18c153b c6621978
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 23 08:19:37 2021 +0200

    Merge pull request #31 from tmeissner/upduino_v3_yosys
    
    Add UPduino v3 variant using open source toolchain (2nd try)

[33mcommit c6621978f2e2d5dc1bf2f63925d8f577783baffe[m
Author: Torsten Meissner <programming@goodcleanfun.de>
Date:   Sat May 22 20:15:03 2021 +0200

    Add debugger ports

[33mcommit d476a07d4e8b685815375a508f738fdd91a7c60d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri May 21 01:51:50 2021 +0200

    [docs] style

[33mcommit 5d3d00272f522747887f0dc66b8690662a100777[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri May 21 01:46:46 2021 +0200

    [docs] move bitmanip-draft.pdf into references

[33mcommit 5220450476b8d956c1cf27eca887f9180b818122[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri May 21 02:04:26 2021 +0200

    [ci] add figures to HTML artifact

[33mcommit 3cfcb33c388bf57649a07bec2fbdfbad82204d6a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri May 21 02:02:36 2021 +0200

    [ci] do not deploy from PRs or non-master non-tagged branches/commits

[33mcommit d25527c6b36cb5eaf99913db0e1b0e49c8043d6d[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Fri May 21 01:31:11 2021 +0200

    [ci] use eine/tip for uploading artifacts to (pre-)releases

[33mcommit 40007bca339466d427c47c66612321c401103df3[m
Author: Torsten Meissner <programming@goodcleanfun.de>
Date:   Fri May 21 19:01:19 2021 +0200

    Incorporate comments of PR review
    
    * ICE40: Remove unused Radiant component declarations
    * PLL: Remove system_pll unit, instead use SB_PLL40_CORE in top level
    * Makefile: remove Yosys synth_ice40 -abc2 option
    * Update FPGA binary file to above changes

[33mcommit 31d9475b4a842b5c620352530cbe78995fc29ccc[m
Author: Torsten Meissner <programming@goodcleanfun.de>
Date:   Thu May 20 20:25:45 2021 +0200

    Add FPGA FW binary file

[33mcommit f05c2ad665d9bfe3b7b86c5fb4ca3bde17ac2562[m
Author: Torsten Meissner <programming@goodcleanfun.de>
Date:   Thu May 20 20:07:11 2021 +0200

    Add UPduino v3 variant using open source toolchain (GHDL, Yosys, nextPNR)

[33mcommit a18c153bc666dd3cddb6411c8776c1ba3865426a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 19:16:21 2021 +0200

    [rtl/core] updated pre-built application/bootloader images

[33mcommit 79dfd222d9d05144da6eeeec4992bb0a8f3bdc3c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 19:15:55 2021 +0200

    [sw/bootloader] minor clean-up

[33mcommit 83463f82be2c8f291e99c35229df8d821152de71[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 19:15:07 2021 +0200

    [docs/src_adoc] minor typo fixes

[33mcommit 8d14e482e5934bc072d8df6c50c7f67de13b2040[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 19:14:43 2021 +0200

    [sw/lib/source/mtime] minor edits to prevent/reduce race conditions when updating MTIMECMP

[33mcommit 5c3b5aa7ab39e8430d8dd8d3f3487b8f9bce6c64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 18:08:38 2021 +0200

    [docs/figures/neorv32_processor] reworked figure format; added on-chip debugger complex

[33mcommit b127f35713a19ea5c834b17164551c23b051a253[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 17:11:42 2021 +0200

    [docs/src_adoc/OCD] added debug module (DM) documentation

[33mcommit dca939ce03cc36f830884775fe0aec10514021c5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 17:11:04 2021 +0200

    [docs/src_adoc/overview] minor edits

[33mcommit 180e8c481661a85075a6ac24e650378e9aba5c64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 17:10:48 2021 +0200

    [CHANGELOG] minor edits

[33mcommit b2fea3ac2719a54a0e56a618d8919f7b6f06f6eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 17:08:09 2021 +0200

    [docs/src_adoc/overview] added on-chip debugger hardware ressources

[33mcommit 038786c0e5c27c5004fcf84ef35038a387de01a8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 17:07:43 2021 +0200

    [docs/arc_adoc/soc] added JTAG interface and OCD configuration generic

[33mcommit 984f3a3dc29202af14a690fed0d3bdf8c780a5c4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 17:07:02 2021 +0200

    [docs/src_adoc] updated version

[33mcommit 7915cdba0edd6f1181464895a2ac6d9cb539c545[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 16:56:33 2021 +0200

    [rtl/core/neorv32_debug_dbmem] updated "park loop" code

[33mcommit 69213e241f3576af6a1277f26d9234205bff10b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 16:56:01 2021 +0200

    [CHANGELOG] added version 1.5.5.7 and 1.5.5.8

[33mcommit ee5bd804ccb6c40ca914bea69d1649cdf51c5989[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 16:54:49 2021 +0200

    [sim/ghdl] added on-chip debugger debug module (DM)

[33mcommit 085d8a936ac1ba79beaf86d0aa1ffb965eb66bed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 16:54:31 2021 +0200

    [sim/testbench] added JTAG interface and configuration generic for new ON-CHIP DEBUGGER

[33mcommit 60bca42eb43a6abb7144ee9414feeeab41a0765b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 16:54:16 2021 +0200

    [rtl/top_templates] added JTAG interface and configuration generic for new ON-CHIP DEBUGGER

[33mcommit 00e469f42f92b31b886344c285b4aa7f8f12b516[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 16:53:26 2021 +0200

    :sparkles: [rtl/core] on-chip debugger: added debug module (DM)
    
    :bulb: the on-chip debugger is operational now - but still in an experimental stage :wink:

[33mcommit d566d45f8e35368c834c4c8705f7e2907f410b5e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 15:47:24 2021 +0200

    Revert "Revert ":bug: [rtl/core/bus_keeper] fixed bug that caused permanent CPU stall""
    
    This reverts commit 04b3b922293ad2d9a898f34a90977b8cdfad34c4.

[33mcommit 04b3b922293ad2d9a898f34a90977b8cdfad34c4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 15:42:43 2021 +0200

    Revert ":bug: [rtl/core/bus_keeper] fixed bug that caused permanent CPU stall"
    
    This reverts commit 9fa11e67ed0dbc4706bf4bc6faaefcff9f2b37f0.

[33mcommit 9fa11e67ed0dbc4706bf4bc6faaefcff9f2b37f0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat May 22 14:58:39 2021 +0200

    :bug: [rtl/core/bus_keeper] fixed bug that caused permanent CPU stall
    
    fixed bug in internal memory monitoring: if accessing an unused address which is not re-directed to the external bus interface (because WISHBONE module is disabled) caused the CPU to stall since that bus access was not correctly monitored and aborted by the BUS_KEEPER

[33mcommit 37ba4b36f786245a6ff97921adb38e86452411a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 21 18:43:41 2021 +0200

    [CHANGELOG] added version 1.5.5.6

[33mcommit 1cd850ddfa28a5a7df36e8f45dc9f7a4d600368f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 21 18:43:22 2021 +0200

    [rtl/core] updated version

[33mcommit 58a4e9050420ab99fca59c22206608e84af3b723[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 21 18:23:26 2021 +0200

    [docs/src_adoc] added further details on upcoming on-chip debugger; minor clean-ups, typo fixes and layout edits

[33mcommit 0299639c4e4cfe229c8d7fd6253efa2eeaf13514[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 21 16:13:12 2021 +0200

    [docs/src_adoc/index] added RISC-V logo

[33mcommit 6d872b2d8c222dd4947f9b00ed67002f7bad6136[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 21 16:12:31 2021 +0200

    [sim/ghdl] added debig transport module hardware module

[33mcommit f54175b741044eed26699e4e80031a4a7dd3b5aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri May 21 16:12:10 2021 +0200

    :sparkles: [rtl/core] on-chip debugger: added debug transport module (DTM)

[33mcommit 2c1e75b9b72d3b2907e7c1fad70d4458bd616cba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 19:20:49 2021 +0200

    [bootloader image] reduced size to fix #30

[33mcommit 37e86dda939f035133656945785e32e247c6e950[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 18:55:33 2021 +0200

    [CHANGELOG] added version 1.5.5.5

[33mcommit 6a824fdea5dfe6acd1b8bfe7d5452c71faff7c40[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 18:53:51 2021 +0200

    [rtl, sim, boards] added system time output "mtime_o"
    
    https://github.com/stnolting/neorv32/discussions/29

[33mcommit 4def5b02b11866821a7144d87d84175883cccba3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 18:52:54 2021 +0200

    [docs/src_adoc] added system time output to processor top entity
    
    https://github.com/stnolting/neorv32/discussions/29

[33mcommit 0218604475b076d896f13fb15413bf04ec1566dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 18:50:26 2021 +0200

    [README] added on-chip debugger (WORK IN PROGRESS); added status not (active development)

[33mcommit fb3d4e650eeb3a238c4996cb196bf62dc2d55d9e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 15:47:21 2021 +0200

    [CHANGELOG] added version 1.5.5.4

[33mcommit d1ed7635026dd169e6a905253ef10ac4bca876cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 15:47:07 2021 +0200

    [docs/src_adoc] added OCD debug memory; minor edits

[33mcommit 3a160288f90cc5e3e641622227d46b043c4981da[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 15:46:19 2021 +0200

    [sim/ghdl] added new debug memory module

[33mcommit 5f8bae3787c177b9971fa7cf3694b9b6b1d4cf9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 15:45:53 2021 +0200

    :sparkles: [rtl/core] on-chip debugger: added debug memory (DBMEM)

[33mcommit 7d50d0893427516e42871821d26eae6be898118c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 13:39:17 2021 +0200

    [docs/src_adoc/ocd] updates, fixes, ...
    
    :construction: still under construction

[33mcommit fb2f8c18d54d75429d20a718d1be5b5a2e21e04e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 13:38:28 2021 +0200

    [docs/figures] added on-chip debugger complex figure

[33mcommit 0fb60445b0eaa424a5359fd27a1c4a236392c839[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 13:37:59 2021 +0200

    [README] fixed links

[33mcommit 70a01e03f0c8b8447d984dbbb42b99573bbe4f80[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 13:37:35 2021 +0200

    [docs/src_adoc/software] fixed link

[33mcommit 51ef9898da2c4ca349b5418d94e570c1721cff18[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 13:37:20 2021 +0200

    [docs/src_adoc/inde] minor edits; added logo, added link to github repo

[33mcommit 944d808d9bf9b1dc3b692e4386c8f24ad57d281d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 09:17:33 2021 +0200

    [CHANGELOG] added version 1.5.5.2

[33mcommit 22ad4909a44b2d34a5de46ba646a9f1ebd3e0bd0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 09:17:00 2021 +0200

    [docs/src_adoc] minor updates

[33mcommit 375607f5d7315f13cfc91b059f5875d62476eecb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 09:16:33 2021 +0200

    [docs/arc_adoc] added flag to discover on-chip debugger; added missing i-cache flag

[33mcommit 1a4bf26536b5dd27d4be1e8a505c75dc69c388fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 09:15:54 2021 +0200

    [sw/lib] added flag to discover on-chip debugger

[33mcommit 8db7fccdbf02bb0254acc92af15062349e1a7b8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 09:15:29 2021 +0200

    [sw/example/processor_check] minor edits

[33mcommit 188eb5c7186aaf911ae8f28a3be63cae51a9214d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 09:15:08 2021 +0200

    [rtl/core] added flag to SYSINFO to allow software to check if on-chip debugger is implemented

[33mcommit f468859606e0cefd20faeded8e85a13dd8601609[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 07:28:08 2021 +0200

    [.ci/sw_check.sh] updated compile switch

[33mcommit 968d2f7be654d0d3da9dcb86a658ec0cb74432b2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 07:21:41 2021 +0200

    [sw/example] renamed cpu_test -> processor_check

[33mcommit 78ee8efcd98e4a7eaf6be4803961b89242d1313a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 07:20:38 2021 +0200

    [sw/example] renamed cpu_test -> processor_check

[33mcommit 1b83443cf3c1cb01fe68648be096a3bbb698431c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 20 07:15:15 2021 +0200

    [docs/src_adoc] fixed including images for html deployment

[33mcommit 5ed53de19636c44cd1de7f7a0cc88ff2de688dd9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 20:57:16 2021 +0200

    [core/rtl] updated pre-build application images

[33mcommit 8f7688c537dae63ab94d0069bf87fd31eda10c40[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 20:56:02 2021 +0200

    :lipstick: [docs/figures/neorv32_bus] minor edits

[33mcommit 5c4d51f273f52c24d3989e8ff4126b23794df23c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 20:55:30 2021 +0200

    [CHANGELOG] fixed link to documentation

[33mcommit d7a59cbbccb5a77b0b787e53945ba2e3b0bb7f44[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 20:04:23 2021 +0200

    [docs/src_adoc] added documentation related to new "CPU debug mode"

[33mcommit 3ec57c1d5f7f7a5f7e998c824a3441158e3f83f0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:55:14 2021 +0200

    [README] added CPU debug mode - :construction: work in progress

[33mcommit 8a6a70bae4880a199652ae36c2979840c369c6a8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:49:28 2021 +0200

    [CHANGELOG] added version 1.5.5.2

[33mcommit 2e4b7a4eaac9aabd3f24000fc3a8f87fe6603bd1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:49:09 2021 +0200

    :sparkles: [rtl/cpu] added CPU debug mode
    
    comatible to the RISC-V debug spec 13.2

[33mcommit 022a73691c16862eff5f8d4cf9c22cb7c334aba1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:48:23 2021 +0200

    [sw/lib] added flags for SW to discover CPU debug mode

[33mcommit 701f325cd728b6fda7719c34434289e0628be935[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:43:23 2021 +0200

    [README] minor clean-up

[33mcommit 61bd0d77b0f097124e9113952e3f5326f00a6524[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:32:04 2021 +0200

    [docs/src_adoc] added links to online documentation to front

[33mcommit 78808fd6e6ead37d7d25b6f535bd9138b64c7587[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:26:24 2021 +0200

    [README] fixed/updated links to online documentation
    
    :construction: work in progress

[33mcommit edfd4ebe1aa63ac913be9536e61d6ceacb239d3f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:09:39 2021 +0200

    [docs/src_adoc] fixed links to SW documentation on github pages

[33mcommit ab8e651c510289b94895fe45004eabf796843cec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:07:15 2021 +0200

    [docs/reference] added RISC-V debug spec
    
    :sparkles: for upcoming NEORV32 *on-chip debugger* :wink:

[33mcommit 049b388ac12c2de4de23936788997201a7e3661a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:06:24 2021 +0200

    [sw/lib/source/neorv32_mtime] fixed comments

[33mcommit 7bd31eebbae95215b5b30937534e86fcb47e28e7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 18:06:04 2021 +0200

    [sw/common/crt0] added NOPs right after HW reset

[33mcommit 4b7344eced197c832069dd78c9aad19b0c646ad3[m
Merge: 183783e5 539660e0
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 19 17:07:30 2021 +0200

    Merge pull request #26 from umarcor/ci-tweaks
    
    Build HTML datasheet and publish to GitHub Pages along with Doxygen output

[33mcommit 539660e0c5b272307fc40f63de7a6577aca49d85[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 05:43:04 2021 +0200

    docs: add shields/badges to HTML datasheet

[33mcommit 1748ea045a849e13ef69c07da0c73c93def08e0a[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 05:14:23 2021 +0200

    readme: update shields/badges

[33mcommit 762d1a1ca99a7eb18e67b57854102bbfe5f269e3[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:59:10 2021 +0200

    ci: deploy datasheet (HTML and PDF) to GitHub Pages, along with Doxygen output

[33mcommit 445032679477aeaccad846cc57ff135490a93d15[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:36:19 2021 +0200

    add Makefile

[33mcommit 6a2d00f4fd0b8875f53174f7f8d25ebf5f6b4a42[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:03:31 2021 +0200

    docs: add HTML datasheet

[33mcommit 8c6887d59e01f2a00f1288919ad2a0df9324ee5c[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:03:31 2021 +0200

    ci: merge workflows 'gh-pages' and 'build_datasheet' into 'Documentation'

[33mcommit 413d0534b063979043c0c889275102f26ac8b645[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:42:09 2021 +0200

    ci: remove redundant Action

[33mcommit 3eb863d474a0bbce088768e3ffe8b78278c9d78e[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:40:50 2021 +0200

    ci: cleanup

[33mcommit 87b4777b969cd4f8a01c0d6dcfae5c5c50945a7e[m
Author: Unai Martinez-Corral <unai.martinezcorral@ehu.eus>
Date:   Tue May 18 04:29:17 2021 +0200

    docs: create subdir 'references'

[33mcommit 183783e5bea1031e5b24c1e071afb1817e87d485[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 16 18:17:55 2021 +0200

    [README] minor edits

[33mcommit 8af59129e9b801107845fd69b9c8062ce9202c46[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 16 18:17:43 2021 +0200

    [boards/UPduino_v3/README] minor corrections

[33mcommit 8358a8af41904a2cd1017416784409aaaaba9ebc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 16 18:15:47 2021 +0200

    [rtl/core/neorv32_wishbone] fixed minor sanity check error

[33mcommit 88d565f9866da4bffda4304db3cbebe17e3fc630[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 13 17:11:39 2021 +0200

    [docs/NEORV32.pdf] rebuild

[33mcommit 07eb6cdfb16c2bd7c2d8ef2cdcc244e468928943[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 13 17:11:22 2021 +0200

    [docs/src_adoc/neorv32] added front notifier: project aims to be stable even while working on new features

[33mcommit 8eab4f8b66064993050011067960009ff38547e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 13 17:10:33 2021 +0200

    [docs/src_adoc/soc] added note regarding disabled optional modules/extensions

[33mcommit d446a0ced87c233e810df2303003b3eb18474a8a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 13 12:41:37 2021 +0200

    [CHANGELOG] added version 1.5.5.1

[33mcommit 6eec917879633dad0da167953052038153db0964[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu May 13 12:41:04 2021 +0200

    [rtl/core/wdt] changed signal name since it is a reserved keyword in vhdl-2008 (fixing #24 )

[33mcommit c1bd8d8e705557a9224789ec39ada0e52a3038d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 12 23:59:32 2021 +0200

    :sparkles: [boards] added UPduino_v3 example setup; UPduino_v2 is obsolete

[33mcommit 56d3f7e77a1888d23c3ec673524224e0ff5ca113[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 12 23:57:24 2021 +0200

    :sparkles: [boards] added UPduino_v3 example setup

[33mcommit 58eb4d55a62b2d518b2ad484fbe8b4cca4a97295[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 12 21:41:25 2021 +0200

    [README] mip CSR is read-only now

[33mcommit 276c9f73fd33ee0b299997f7fbf63372b88a6621[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 12 21:01:37 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit 9fbfad5f6b69364f8a8bd4460d36d835fac1dfe8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed May 12 21:00:58 2021 +0200

    [boards/UPduino_v2] updated bitstream

[33mcommit 45c2282f353fc0bc237dfbccf4eaf9586918ce53[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 20:29:38 2021 +0200

    [CHANGELOG] added latest release

[33mcommit a2e306ebfd8be0ad19212703fd24e9a4b2caa9bc[m[33m ([m[1;33mtag: v1.5.5.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 20:17:25 2021 +0200

    preparing new release

[33mcommit 9b5c17d5e4aca130b3a01e778d799bae51294023[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 20:16:15 2021 +0200

    [README, docs/src_adoc] added warning that mip is read-only (not fully RISC-V compliant)

[33mcommit 76e3cd4b5a1e792e1f02c7b4fd870ab7be30d464[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 20:15:39 2021 +0200

    [sw/example/cpu_test] minor edits

[33mcommit 64f6836a8c83f9d5bc3ce04aad60c0aef25a51c3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:23:18 2021 +0200

    [rtl/core] updated prebuild application image
    
    * build from sw/example/blink_led
    * updated due to modified RTE sources (added NMI support)

[33mcommit fc630a3c445ae6817ee50a880b4eaf46482cdd47[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:22:13 2021 +0200

    :books: [docs/NEORV32.pdf] rebuild

[33mcommit 8a8a4206953772ec525b1bbd3277353301ede220[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:21:37 2021 +0200

    [boards/UPduino_v2] added NMI (unused)

[33mcommit 17d8657f64ccd5dc67fb4b5cbf38c729570edfa7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:20:14 2021 +0200

    [README] added new non-maskable interrupt

[33mcommit 23e746617ad6bf9a1975c04724cb77ba8388b630[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:19:56 2021 +0200

    [docs/src_adoc] mip CSR is now read-only

[33mcommit 75f308a7d0de65733ec216252bd3bba748a9fc5c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:19:37 2021 +0200

    [docs/src_adoc] added non-maskable interrupt

[33mcommit 0e42ca858c4a9211ea618ec924c5894398eae858[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:18:07 2021 +0200

    [docs/figuresneorv32_processor] added new non-maskable interrupt input

[33mcommit 7b2afee4c4abff17a819308bc197549a3d5ff0cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:17:02 2021 +0200

    [CHANGELOG] added version 1.5.4.12

[33mcommit 9ecbdd92f310d44ad9f662f8ae4ef91aa4d2c182[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:10:03 2021 +0200

    [sw/example/cpu_test] removed mip test, added NMI test

[33mcommit 67a1cd7b393f946b8f26253fd3d828b45a2706ab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:09:39 2021 +0200

    [sw/lib] added trap code for new non-maskable interrupt
    
    * added NEORV32 runtime environment support for new non-maskable interrupt

[33mcommit 3dfc30d5b0cf9f7e8a122c7bafa6729c9c00a557[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:08:33 2021 +0200

    [sim/neorv32_tb] added simulation trigger for new non-maskable interrupt

[33mcommit e5d7e6ff56a68338e1a5e59f30e64fb00088570a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:07:54 2021 +0200

    [rtl/top_templates] added new non.maskable interrupt input

[33mcommit 8a9e68307148c1974c569efc0dd6ec3890fc1693[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 17:06:13 2021 +0200

    :sparkles: [rtl/cpu] added non-maskable interrupt input
    
    minor edit: trying to write to MIP will trigger an illegal instruction exception

[33mcommit d8f4ed011ccd3e26ce6fa6c302345d72f9ad8ad6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 10 16:14:05 2021 +0200

    :warning: [sw/lib/include/neorv32, rtl/core] mip CSR is now read-only
    
    pending IRQs can be cleared by disabling the according MIE bit

[33mcommit 57941301bf480d7abe69172ed8ee138fa052acc5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 18:33:23 2021 +0200

    [boards/UPduino_v2] updated bitstream (now including TRNG)

[33mcommit 9cfc88e8939bb616873590bcf941755c98c2386a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 18:33:05 2021 +0200

    [boards/UPduino_v2/.gitignore] minor edits

[33mcommit a06b34307f15aaa0852a11f0b826b990732b62b3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 18:32:42 2021 +0200

    [boards/UPduino_v2] added TRNG, updated synthesis results

[33mcommit 53dfa68d7b0be26d3f96073be98e4cfc1eb70705[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 18:07:19 2021 +0200

    [boards/UPduino_v2] added pre-compiled bitstream

[33mcommit 94ddb13941e5fc3bbebd3710734081a92fecc83d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 18:03:53 2021 +0200

    :sparkles: [boards] added UPduino v2 (lattice ice40 ultraplus 5k) FPGA example setup

[33mcommit 5013e7d5397d3f6c0b3f1c7ff9bcfb592077a6b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 18:02:51 2021 +0200

    [boards/UPduino_v2] typo and broken link fixes

[33mcommit f3d3ad0ab56c256e9f29607f00c3fa1199b594a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 17:55:15 2021 +0200

    [boards] added UPduino_v2 (Lattice Ice40 UltraPlus) example setup

[33mcommit 1d2f61a1731e7852061feabdadb514fe6a413be6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 12:01:28 2021 +0200

    [sw/common/crt0] minor edits

[33mcommit 2dc65cba94d920c427fee2e3e5074ae105abcb86[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 12:01:04 2021 +0200

    [doc/src_adoc/cpu_csr] added details when HPM/PMP CSR accesses trap

[33mcommit 6d98d52fba3a9286fd06b05ad6ec22c40e59db54[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:58:04 2021 +0200

    [docs/src_adoc/neorv32] updated version number

[33mcommit 7fabdaa68589c57548d939fcfb0aebec144086ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:57:14 2021 +0200

    [CHANGELOG] added version 1.5.4.11

[33mcommit 3b23b01b9ffba2d7ff7fa71005beca6f4f2ef055[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:55:45 2021 +0200

    [docs/src_adoc/soc] added note regarding new mzext CSR flags to check if PMP/HPM is implemented at all

[33mcommit 12f6374a811edf5abe6cd60df32c0fc5393d8336[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:55:04 2021 +0200

    [docs/src_adoc/cpu_csr] layout edits

[33mcommit c8430858901e59a831052c9ca8f8a2447c7bfd78[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:54:37 2021 +0200

    [sw/example/cpu_test] added "extension-available" guards for some tests

[33mcommit 2095a4a328bcb7b04d1affadd940fe08886dda5b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:53:48 2021 +0200

    [sw/lib/source/neorv32_cpu] added mzext check to test if HPM/PMP is implemented at all

[33mcommit a079e1a5f6707bec245c13806e92e5ec1e82fb94[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:53:15 2021 +0200

    [sw/lib/include/neorv32.h] added new mzext flags to check for PMP/HPM

[33mcommit 4901259abd9148ece54833e67c0d1c3d81e099f0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:52:36 2021 +0200

    [rtl/core] added flags to mzext CSR to check if PMP / HPM is implemented at all

[33mcommit 573c7f1bbf7c3000eb0f2014d5e615d927f6b020[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun May 9 07:51:33 2021 +0200

    [sw/lib/source/neorv32_rte] mino edits

[33mcommit 06ae329143154922e12701a326ce4f94c8381cbc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 18:53:31 2021 +0200

    [CHANGELOG] added version 1.5.4.10

[33mcommit c3c0f012866404fa2b649cfe8586ca0a6a5c2b60[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 18:53:16 2021 +0200

    [docs/NEORV32.pdf] rebuild

[33mcommit 7bc2d48ccbf0341821d25e9984d13368c369a5e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 18:53:01 2021 +0200

    [docs] removed obsolete NEORV32.legacy.pdf

[33mcommit 8969f5dedfd93bd543c3e5ace8b5e99c3090717b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 18:52:14 2021 +0200

    updated version number

[33mcommit a8e6b0b8e270914e9b24bfba8867828e92844591[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 18:51:57 2021 +0200

    [rtl/core] moved FIRQ sync FFs to top, removed sync FFs of processor-internal IRQ sources

[33mcommit 341f0a3321a0332d607eba418f6d6ccd07a3f443[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 18:51:29 2021 +0200

    [rtl/core] minor comment edits

[33mcommit b25fdc644d0fac9147eb061173ffe4d489a577ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 16:32:04 2021 +0200

    [rtl/core] minor code clean-ups

[33mcommit 09138246b486583eec3701f6389f0ae0a6bf8976[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 16:31:38 2021 +0200

    [docs/src_adoc] updated processor implementation results

[33mcommit 49007de1f4f6d7a8da6e00b4f0ab52ecb4939e97[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon May 3 16:31:21 2021 +0200

    [README] updated processor inmplementation results

[33mcommit 873036e3b466cf66f3049a03c3b32dcc6294d4bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 21:12:43 2021 +0200

    [CHANGELOG] added version 1.5.4.9

[33mcommit 03e05a9f0d7340af5c53d04def0a01290836ce9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 21:12:26 2021 +0200

    [rtl/core] updated pre-build images

[33mcommit 54b0c4c4f0331335f5332bb7c7ffc498c30dce85[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 21:11:44 2021 +0200

    [rtl/core] reworked CSR access system (reducing area footprint)
    
    highly reducing decoding logic overhead

[33mcommit 3b8b2d596daeefcb87472c36247afa0971777427[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 18:39:45 2021 +0200

    [sw/common/neorv32.ld] minor comment edits

[33mcommit 8cb619d6e8fc096595fde110e21ae568f3f5db59[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 18:39:16 2021 +0200

    [sim/neorv32_tb] minor edits

[33mcommit df6650ec24fae3e1a5609611b5023f5462b08606[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 14:50:37 2021 +0200

    [sw/lib/source/neorv32] minor Hw info output formatting

[33mcommit 2b15348213830554d8d1e3aa64e5dbfc5a792a5f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 14:35:13 2021 +0200

    [README] minor edits

[33mcommit 75a1b9c9ae1aa289d2b00c217e68c61ad10bfc3f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 14:34:53 2021 +0200

    [docs/src_adoc] minor edits

[33mcommit be2441a24eb6625084e49c94f6e7d8910df9a157[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 14:34:34 2021 +0200

    [docs/ard_adoc_software] added more details regarding linker and crt0

[33mcommit a25c58681d11eff810de108ef493f8be2b9eacab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 14:34:07 2021 +0200

    [docs/src_adoc/soc_pwm] fixed link

[33mcommit 426f292e067105d13e4b8396964a968e5a3dbf95[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 30 14:32:09 2021 +0200

    [sw/common] moved IO space definitions from crt0 to linker script

[33mcommit 367cd25b241d27adfaca086293d64a571ad13771[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:37:14 2021 +0200

    [docs/NEORV32.pdf] rebuild

[33mcommit e8c639ed8ae4391957d7e59c38c2b9bdccb25a7a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:36:57 2021 +0200

    [docs/src_adoc] typo and layout fixes

[33mcommit 276562630e6a5426cd755501ccd3e56d017c2904[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:34:43 2021 +0200

    [docs/src_adoc/soc_sysinfo] added "dedicated HW reset flag"

[33mcommit c6403072a49766bbfb45c1b2ebdfb63c5310ab7c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:34:03 2021 +0200

    [.github/workflows] minor edits

[33mcommit a8c4493b4b769ceb6eb4dde8029ec142e9726dc1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:29:19 2021 +0200

    [CHANGELOG] added new SYSINFO flag

[33mcommit 6e8301be89723d76556b60b1619696dafa6851ae[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:28:59 2021 +0200

    [docs/Doxyfile] minor title edits

[33mcommit d1f7886fa6fe79738aded98dc69fbc71a3a697d7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:28:13 2021 +0200

    [sw/lib/source/neorv32_rte.c] minor updates

[33mcommit 1173ac83245acf258dde09b4a06fea101b031133[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:27:47 2021 +0200

    [sw/lib/include/neorv32.h] added "dedicated HW reset" flag

[33mcommit c4feab43c553a7d60cdd0d8cea5b741548f968f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:27:22 2021 +0200

    [rtl/core/sysinfo] added flag to check if a dedicated HW reset of all core registers is implemented

[33mcommit c0feddc5386c5cded90c65a59135415436f1e48a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:11:58 2021 +0200

    [CHANGELOG] added version 1.5.4.8

[33mcommit 4940ae5d3629c50ef91075184630d64b699184ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:10:43 2021 +0200

    [rtl/core] reduced processor INTERNAL bus timeout to 15 cycles

[33mcommit 6a73211d7d62d343543fb997fb4513b519fcc747[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 12:10:17 2021 +0200

    [rtl/core] minor edits of CPU instruction fetch engine

[33mcommit d537a69e176e4ca4554b620b5e300382cefd9321[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 11:12:50 2021 +0200

    [docs/figures/address_space.png] reworked figure

[33mcommit 0ce72fa2c2c5578d4253703e2bcd1cee3a841284[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 11:10:26 2021 +0200

    [docs/figures/neorv32_bus.png] added bus keeper

[33mcommit c1456fc774560fa8949c90b4a13f501e61ba1a23[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 29 10:25:42 2021 +0200

    [docs/arc_adoc] clean-up

[33mcommit 842e88d70a50738a2bbdffd1ae389b5222844148[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:22:51 2021 +0200

    [docs/NEORV32.pdf] rebuilt data sheet

[33mcommit 7d6514b165ebf8f52e1c3046c580730ac3a2deee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:18:37 2021 +0200

    [README] minor edits

[33mcommit e3533518fefca7c1a6e6d093292a86298e951105[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:13:36 2021 +0200

    [docs/src_adoc] updated version

[33mcommit 84e4cde0b7e8a83d2b2d3aa60497826e6d27ee6f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:13:18 2021 +0200

    [.github/workflows/build_datasheet] fixed trigger paths

[33mcommit 1ae61708199d9772f3c35f65c4879b0f663ad060[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:12:50 2021 +0200

    [README] minor edits

[33mcommit 618d10145fc29194d0e4289010dbc8e32a822bc3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:12:04 2021 +0200

    :bug: [CHANGELOG] added version 1.5.4.7

[33mcommit d968c297043143d50a13432515c1b5a4802c1306[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 19:11:46 2021 +0200

    :bug: [rtl/core] fixed bug in iCACHE (for configuration with ICACHE_ASSOCIATIVITY = 2)

[33mcommit bb1af754f431adc770f1479349bd380507664f67[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 18:08:11 2021 +0200

    [CHANGELOG] typo fix

[33mcommit 2a790e53b789b5c8267dd0cf87fa0998783e8b01[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 18:05:21 2021 +0200

    [README] typo fixes

[33mcommit 82fb86d758948f12d79eef4f128510995baccf81[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 18:01:53 2021 +0200

    [docs/src_adoc] minor layout and typo fixes

[33mcommit a61a09becf6f1f51593d421b2c1e3cc58dd8e95b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 18:01:26 2021 +0200

    [README] added links to new "build data sheet" workflow

[33mcommit a6463bbc6404dbb4051762b4a1fc8c85d099e067[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:40:19 2021 +0200

    Update build_datasheet.yml

[33mcommit a8badf0370e183a5dbbe9243593fa6ce5d85e66e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:38:35 2021 +0200

    Update build_datasheet.yml

[33mcommit 631f0b5704ddf1f1f2a0e29451ab797376309369[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:37:26 2021 +0200

    Update build_datasheet.yml

[33mcommit dc852e467674dbf03193f24abfb555f12f2d171a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:34:47 2021 +0200

    Update build_datasheet.yml

[33mcommit 904ce405bad32f5b4161b8e9b3fcb97f8ffb9883[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:32:08 2021 +0200

    Update build_datasheet.yml

[33mcommit 560d5efa6b01b18efcd2f98cdc0ffc9592d02878[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:30:59 2021 +0200

    Update build_datasheet.yml

[33mcommit 6c4018c99b11eb0c31a025d676e0c5dc61f137b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:29:32 2021 +0200

    Update build_datasheet.yml

[33mcommit fc4d2bd41599466d484d6f6f0c6f2bedf384caf2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:16:10 2021 +0200

    Update build_datasheet.yml

[33mcommit 25ef9f37126ba93f4ead830cf7dc266f636b0e9a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:08:08 2021 +0200

    Update build_datasheet.yml

[33mcommit 33752b60f52cf698952515698c61ecd71b3e1915[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 28 16:05:47 2021 +0200

    Create build_datasheet.yml

[33mcommit 9dcc367a6837d3c0e7db2a05dbde4fd505580016[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 27 18:11:21 2021 +0200

    [docs/src_adoc] minor typo and layout fixes

[33mcommit 13a02731f8b357c9600a067ad81226a77b6ecaf7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 27 18:08:06 2021 +0200

    [CHANGELOG] typo fix

[33mcommit c9c676f1c8b562075752c463f4953a4ea8fdb43c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 27 17:01:03 2021 +0200

    [docs] rebuild NEORV32.pdf

[33mcommit de369366138218df199a71525c40f9c83693c5ff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 27 16:56:32 2021 +0200

    [docs/src_adoc] minor typo and layout fixes

[33mcommit 7d7518871eb1a0a2e1066a6dfc6d51d2d10dc49c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 27 16:55:29 2021 +0200

    [docs/src_adoc/soc_icache] removed obsolete timeout configuration

[33mcommit feb9c911afaed660034044aa9e0768cf165417d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 26 20:30:37 2021 +0200

    [CHANGELOG] added hardware version 1.5.4.6

[33mcommit 2217eb087051542137f46b4d80c727fec9d9a120[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 26 20:30:15 2021 +0200

    [docs/src_adoc] updated HW version

[33mcommit 2b893bcd824be0e0d752e382596822fe9fc3dbef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 26 20:29:50 2021 +0200

    [rtl/core/cpu] optimized instruction fetch unit
    
    * less overhead for branches
    * reduced unit's hardware complexity

[33mcommit 30e314828e1042b05de6672890ecb6326adb18a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 26 20:16:15 2021 +0200

    [rtl/core/bus_keeper] minor comment edits

[33mcommit cd024dfe6848f34a7b02edc1c8c267bd68a58d37[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 26 17:10:52 2021 +0200

    [docs/data sheet] layout and typo fixes

[33mcommit 34653a703df7c17b82a0b9ef925981149a48cc7d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 18:54:11 2021 +0200

    [sim/ghdl/ghdl_sim.sh] increased default simulation time to 8ms... :wink:

[33mcommit e14721ab9211e6e98200397e9c28c3ce5c3cceed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 18:44:54 2021 +0200

    [sim/ghdl/ghdl_sim.sh] changed default simulation time to 7.5ms

[33mcommit c06dd34a98747ef6a06688d7a7e945a7069e8f32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 18:27:16 2021 +0200

    [.ci/hw_check.sh] removed simulation time override

[33mcommit 10475a4dc143fc4125dbfeb0368818c66e48303b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 18:07:17 2021 +0200

    [README] typo fixes

[33mcommit fd1616b7937cfe60665f41e3d8d824d55cb38f1e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 18:07:04 2021 +0200

    [docs/NEORV32.pdf] re-built from sources

[33mcommit ef380edc20108f4bda83723d3378cc17afcc520a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 18:06:43 2021 +0200

    [docs/src_adoc] updated version number

[33mcommit 51397da1e100740e99217f61ff0d6a36f51b13ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:55:47 2021 +0200

    [docs/src_adoc] added new generic "MEM_EXT_TIMEOUT"; removed obsolte CPU/bus timeout configuration

[33mcommit d02207f99f6606c26cd0e594620d5953a690bc8c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:54:53 2021 +0200

    [docs/src_adoc] removed CPU's cancel signals; removed CPU bus timeout

[33mcommit 2eb2303718c6e22ebf3dbbc5494975b508e6b580[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:50:50 2021 +0200

    [CHANGELOG] added version 1.5.4.5

[33mcommit 6a57a9fb5989b82bfaf12a496baf0a247e666206[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:49:56 2021 +0200

    [sim/ghdl/ghdl_sim.sh] added new rtl module "bus keeper"

[33mcommit 7778217b335104dbd01d59e75bf33f62a7311c52[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:49:30 2021 +0200

    [sim/neorv32_testbench.vhd] minor update

[33mcommit 43b0334d305cc5d26c890ed065a61c5d28efd285[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:49:04 2021 +0200

    [rtl/top_templates] added new generic "MEM_EXT_TIMEOUT"

[33mcommit 09a4e4db205f1d18a461b362c068ba4f3ff88634[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:48:41 2021 +0200

    [rtl/core/neorv32_package] cleaned-up and updated

[33mcommit f765799e7b67b60e8826bc503f0024a0c8320ffd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:46:17 2021 +0200

    [rtl/core/neorv32_top] removed processor-internal cancel signal; added new generic to configure Wishbone auto-timeout

[33mcommit 27d7ad812ca1daf0ac763226d6b58bc2580d4557[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:45:24 2021 +0200

    [rtl/core/wishbone] removed cancel signal; added option to configure Wishbone access timeout
    
    Wishbone access timeout is OPTIONAL

[33mcommit e36820c78a12644b68157188e908605a7d3c9aea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:44:43 2021 +0200

    [rtl/core/cpu_control] reworked pipeline front end: instruction fetch unit
    
    modified due to reworked bus interface (removed 'cancel' signalw)

[33mcommit beb1d31f959c6e496d5c7d23269c91d9dc9c1093[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:42:34 2021 +0200

    [rtl/core cache & busswitch] removed obsolete "bus access cancel" signals

[33mcommit 4bbb1ce684c62b743f08e103fdbd5f1c92e863d8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:41:54 2021 +0200

    [rtl/core/cpu_bus] removed timeout counter; removed "cancel" bus signals

[33mcommit 78d79a6abaf6549fd6f3a412b23ec32c2ab1d86c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:41:03 2021 +0200

    [rtl/core] added new component: bus keeper
    
    this unit keeps track of all INTERNAL bus actions and asserts a bus error if any accessed device is not repsonding within a specifc time window

[33mcommit e1533b0523f99516a69baddd4fb899c5398dbb41[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 17:39:33 2021 +0200

    [README] added build instructions for data sheet & SW documentation

[33mcommit 31b9a6f66cf5f4d7910413289bf90fa01b923dce[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:56:28 2021 +0200

    [CHANGELOG] new data sheet sytel; added asciidoc sources

[33mcommit 71c8056fba357009ca5f473be705bb11261335c3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:56:00 2021 +0200

    [README] minor edits

[33mcommit b5ee9aa31b5c1e65c4d12c2d9f8c262bf6471027[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:51:12 2021 +0200

    :sparkles: :lipstick: [README] added link to new asciidoc sources of data sheet

[33mcommit 244333fbf6da1b43e85c9e29fe403d8a8f6b6361[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:50:42 2021 +0200

    [docs] added asciidoc generator script for NEORV32.pdf

[33mcommit 26b2e97fc8a8ef10d7f9365962287ac616917c44[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:49:46 2021 +0200

    [docs/figures] added NEORV32.pdf figures

[33mcommit 072bc93871122133c1247ef955e0c6891a108066[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:49:15 2021 +0200

    [docs] added asciidoc sources of NEORV32.pdf data sheet

[33mcommit 24d5c100346806ebaafcea36cb74031783d75982[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 25 12:44:28 2021 +0200

    [docs] current style of NEORV32.pdf is DEPRECATED now; added new style version

[33mcommit ddd8b83c527d2ae19560a615761493c6c0da1cf0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:35:25 2021 +0200

    [CHANGELOG] added version 1.5.4.3
    
    :warning: reworked atomic memory access system

[33mcommit 1b0c23542abf29700b8c5de15cb04dc6e453c53b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:34:07 2021 +0200

    [docs/NEORV32.pdf] reworked CPU and SoC sections regarding atomic memory access
    
    * updated CPU diagram
    * updated CPU interface signals
    * updated Wishbone bus signals and description

[33mcommit b06b25f3f89ead375ded67bff561cd97c4287eb0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:33:15 2021 +0200

    [README] minor edits

[33mcommit ece60507642ceec7d6a1caafd70a2fcb6c0a6421[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:33:00 2021 +0200

    [sim/neorv32_tb.vhd] updated Wishbone bus network

[33mcommit e1e2e0b7dfa668cad4767d4d28d823ae8476e202[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:32:34 2021 +0200

    [rtl/top_templates] updated Wishbone bus signals due to reworked atomic memory access system

[33mcommit 29ab15dab08d2b6deeb65443f6f06dd852aa4a05[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:32:06 2021 +0200

    :bug: :warning: [rtl/core] reworked "atomic memory access" system of CPU due to conceptual design errors
    
    * pruned bit 3 of top's wb_tag_o signal
    * removed top's wb_tag_i signal
    * added top's wb_lock_o signal

[33mcommit 61326e80d6eff19fed0caa30557b56f88ebbb0d1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 19:31:02 2021 +0200

    :bug: :warning: [rtl/core] reworked "atomic memory access" system of CPU due to conceptual design errors
    
    updates:
    * LR/SC combinations will fail if there is an exception/interrupt (e.g. from a context swtich) in between)
    * SC will NOT write to memory if exclusive access fails

[33mcommit 946dee12adfceb255018be5d2d091073c4139f55[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 18:15:34 2021 +0200

    [sw/lib/include/neorv32_cpu] added "compile guards" to atomic memory access functions

[33mcommit c185aa6bdcd165d394471761d9ec2c6797e04e58[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 18:10:11 2021 +0200

    [sw/example] minor edits

[33mcommit 7a7af35dfca164ec90e32197398f608328fb9b72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 18:09:52 2021 +0200

    [sw/lib/include/neorv32_cpu.h] added primitives to atomically load/store from/to memory space

[33mcommit 72f8c188af4834462dfe0a80be876c021fb32f0c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 18:08:49 2021 +0200

    [sw/lib/source/neorv32_cpu] removed atomic compare-and-swap function

[33mcommit ff3cc48b240bd4048d750d681828cc9320ec75bf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 17:58:35 2021 +0200

    [rtl/core/bootloader_image] updated pre-built bootloader memory image

[33mcommit dab1d660e157ba44dedd321b784b58fa98de0977[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 17:57:40 2021 +0200

    [sw/bootloader] added check to test if SPI is implemented at all

[33mcommit 36d96cbff98e93e2d760cb3cd6fee73b84570f02[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 17:22:32 2021 +0200

    [docs/NEORV32.pdf] minor edits and clean-up
    
    * removed "fpga_specifc" rtl folder
    * removed note ragarding Lattice Radiant - LSE (lattice synthesis engine)

[33mcommit c7a175f03840afeae40ae1b1b2d59349861c031f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 17:21:32 2021 +0200

    [rtl/README] removed fpga_specific folder

[33mcommit 0bec57961ffa452545663e6292869fd612ede927[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 21 17:21:12 2021 +0200

    :warning: [rtl/fpga_specific] removed; iCE40 UltraPlus memory modules will be re-added in upcoming example setup (in boards folder)

[33mcommit fb83541e3e62bb7fb7a8da526e046737df9d62e3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 19 17:19:23 2021 +0200

    [CHANGELOG] added version 1.5.4.1

[33mcommit 20f0bc9a4ebf58e5e20d343e7439fc3b2bf33470[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 19 17:18:00 2021 +0200

    [README] added link to processor check to status section

[33mcommit eaa57a3f0e829a7b3664df906f3b03c0fb2e99ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 19 17:01:02 2021 +0200

    [sw/example/cpu_test] minor edit to check correct MTIME low-word-to-high-word overflow

[33mcommit 1c44357970b7afac7ac58b98ec9b4b9a1a492c35[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 19 16:56:25 2021 +0200

    [rtl/core/top] minor edit

[33mcommit e1c98835df20caab97449ea207faf881f48e4be7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 19 16:56:04 2021 +0200

    [rtl/core/mtime] added register stage to <mtime> write access for improved timing

[33mcommit 53b29e2c610e43593eede09fcfca116efe29ae5e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 18 19:01:14 2021 +0200

    [rtl/core/neorv32_package.vhd] added missing default value of top's "wb_tag_i" signal

[33mcommit e4036f6fec8db12ff14296b84c72c8c9b65e4df1[m[33m ([m[1;33mtag: v1.5.4.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 17 17:33:35 2021 +0200

    :rocket: preparing new release

[33mcommit 65f1dbc5620f42d8e98c70bf2084597d0ffc6797[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 17 17:23:42 2021 +0200

    [sw/example] minor edits

[33mcommit 84d25ecdd3137a9dbde01cf912ede65120dda978[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 17 17:23:23 2021 +0200

    [sw/lib/include/neorv32_cpu.h] added simple functions to load/store word/hald/byte from/to memory system

[33mcommit 3b013b56fa47fe34fc0445834c8208f503ae1b4b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:53:36 2021 +0200

    [CHANGELOG] added version 1.5.3.13

[33mcommit 7a7535f7778d9de83afc31361bf65b96e21530c6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:52:56 2021 +0200

    [docs/NEORV32.pdf] added new generic "TINY_SHIFT_EN"
    
    if enabled, the ALU's shifter unit is implemented as tiny single-bit (iterative) shifter (slow!); dedicated for highly area-constrained setups

[33mcommit 1921f71c33e9cd19446cc27b64a0c2b04a260da9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:47:21 2021 +0200

    [sim/neorv32_tb] added new TINY_SHIFT_EN generic

[33mcommit 358b444952e6bbfc92dd81184b4042ae58d26ad8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:46:44 2021 +0200

    [rtl/top_templates] added new generic to configure a TINY shifter unit for CPU's ALU

[33mcommit 1ffd6422f55bf9d5542bd275355ce633e182ef76[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:44:30 2021 +0200

    :warning: [rtl/core] added new generic to implement CPU ALU shifter unit as iterative single-bit unit (to reduce hardware footprint)

[33mcommit d7b127eccda6c71f819b447ae6cdbc7c161b8d13[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:09:17 2021 +0200

    [CHANGELOG] added version 1.5.3.12

[33mcommit 935724b4ac91be8bc9c00afcd480dc631fabe378[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:08:55 2021 +0200

    [docs/NEORV32.pdf] added new section "2.11 CPU Hardware Reset" explaining the reset system and configuration options for a "real" dedicated register hardware reset

[33mcommit 176623cf080dee3a3e9384f0d08e75eba5a0a9ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 16 17:07:07 2021 +0200

    :sparkles: [rtl/core/cpu*] reworked CPU-wide reset system; added option to configure a DEDICATED RESET  for all registers
    
    by default, most registers (= "uncritical registers") **do not** provide an initialization via hardware reset; a **defined reset value** can be enabled by setting a constant from the main VHDL package (`rtl/core/neorv32_package.vhd`): `constant dedicated_reset_c : boolean := false;` (set `true` to enable CPU-wide dedicated register reset); see new section "2.11. CPU Hardware Reset" of NEORV32.pdf for more information

[33mcommit 089a2e7dadfde78fd9e04bf31509efc5ec222efa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 14 20:13:33 2021 +0200

    [CHANGELOG] added new version fixing some issues; edits to allow synthesis using [`ghdl-yosys-plugin`](https://github.com/ghdl/ghdl-yosys-plugin) (:construction: work in progress :construction:)

[33mcommit 354610abd05ff1168c145dadc66c002055863d19[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 14 20:13:09 2021 +0200

    [rtl/core/neorv32_package] minor fixes
    
    edits to allow synthesis using [`ghdl-yosys-plugin`](https://github.com/ghdl/ghdl-yosys-plugin) (:construction: work in progress :construction:)

[33mcommit 8cfbf764bfb444defa7bb0c5e3d5e66c7f6a18fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 14 20:12:39 2021 +0200

    [rtl/core/neorv32_top] added missing default value for IO_CFS_CONFIG generic

[33mcommit 9c35511f4a64223978bb366d2a114c31a97f0b00[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Apr 14 20:12:13 2021 +0200

    [rtl/core/neorv32_uart] added synthesis guards for simulation-only UART SIM MODE
    
    edits to allow synthesis using [`ghdl-yosys-plugin`](https://github.com/ghdl/ghdl-yosys-plugin) (:construction: work in progress :construction:)

[33mcommit 6c3ccba25120143112c030aa4adf19aa3d498db8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 20:28:26 2021 +0200

    [CHANGELOG] added version 1.5.3.10

[33mcommit c7d925ed63197f33cad879e9225ea058f9a89976[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 20:28:12 2021 +0200

    [docs/NEORV32.pdf] minor updates
    
    * added new generic "CPU_CNT_WIDTH"
    * added new <mzext> flags "Zxscnt" & "Zxnocnt"
    * minor edits and typo fixes

[33mcommit 2f4ada9add30ea09f56b0084c442b639d6877ae7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:56:03 2021 +0200

    [rtl/core/neorv32_cpu_control] fixed some issues with CPU/HPM counter size configuration

[33mcommit 30315204517f7998e59a3bf69ebd666ea0e04e73[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:40:09 2021 +0200

    [sw/example/cpu_test] minor fix in counter setup

[33mcommit 1ad93d53480eca2d832b6232794f665aa5c1ec4c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:33:44 2021 +0200

    [sw/example/cpu_test] minor debugging fixes

[33mcommit 947421cdccc76420b6647657599802952d503539[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:24:31 2021 +0200

    [sw/example/cpu_test] minor edits to allow program to run even if CPU_CNT_WIDTH generic = 0

[33mcommit ae2719642358a94bcf3afbf9a6b2df34095299aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:23:50 2021 +0200

    [sw/lib/source/neorv32_rte] added counter check (effects of CPU_CNT_WIDTH generic configuration) to hardware info

[33mcommit 00f45e065452c610e49263b8a41c44d40cce50db[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:22:31 2021 +0200

    [rtl/core] updated pre-built bootloader memory image due to bug in crt0.S

[33mcommit fa03adbe075a7f7c3ecefecc1ed7baf15f754c69[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 17:21:12 2021 +0200

    :bug: [sw/common/crt0] fixed bug in SP setup
    
    stack pointer (SP) has to be initialized before an exception can occur

[33mcommit 4aea001b50a971e7cda5fa3a2f5cb57c731a6ba9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 15:08:21 2021 +0200

    [sw/lib/include/neorv32.h] added <Zxscnt> and <Zxnocnt> flags to indicate configuration via new CPU_CNT_WIDTH generic

[33mcommit 90d4317e14a624a69028c169c9fd67533580923b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 15:07:37 2021 +0200

    [sim/neorv32_tb.vhd] added new generic "CPU_CNT_WIDTH"

[33mcommit 9fd89397bff7eff0fd159a7fd7720e278927a979[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 15:07:17 2021 +0200

    [cpu/top_templates] added new generic "CPU_CNT_WIDTH" to configure <cycle> and <instret> CPU counter size

[33mcommit 303eb26a6217c349bb1ba37c642c56bfac2d8a90[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 15:07:01 2021 +0200

    [cpu/rtl] added new generic "CPU_CNT_WIDTH" to configure <cycle> and <instret> CPU counter size

[33mcommit 9308bcf2fe7985227ff92b4917e7416d2e2409f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 15:06:17 2021 +0200

    :bug: [rtl/core/control] added new generic to configure size of cycle and instret CPU counters; fixed bug in HPM counter size configuration

[33mcommit 7a10af989cab55501df9055857c7d245357f495a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Apr 13 12:53:09 2021 +0200

    [rtl/core/mtime] minor comment edits

[33mcommit 117a355be271d4a0d4f5c05e92b61082adaf072e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 12 20:04:00 2021 +0200

    [rtl/core] updated pre-built bootloader memory image

[33mcommit 3d9cc123c390e7567cc3fa04f3d3c7fb4965ae85[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 12 20:03:43 2021 +0200

    [sw/bootloader] code clean-up

[33mcommit 40f41b7f02d6f16189fb1adde45ca6b30c6757ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 12 20:03:15 2021 +0200

    [rtl/core/neorv32_cpu] minor code clean-up of sanity checks

[33mcommit d27b8c656acdbcc408af5628fe165c9061244983[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 11 19:10:51 2021 +0200

    [sw/lib] delay function no longer needs CYCLE CSRs
    
    instead an ASM loop is used

[33mcommit e5794fc40b58df400ffd950db10b73f9532b58aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 11 17:00:50 2021 +0200

    [CHANGELOG] added version 1.5.3.9

[33mcommit 4e3b88d16bd134939bd84b9e556ee74c5a0f268d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 11 17:00:33 2021 +0200

    [docs/NEORV32.pdf] added new section (2.11) regarding CPU hardware reset

[33mcommit 2777249b654e83506b22c1ac24a5e8f753ed83f3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 11 16:59:25 2021 +0200

    [rtl/core] reworked reset system
    
    default reset: most register are "initialized" with '-' (don't care) since no real reset is required; however, a "real" reset can be configured using the packages 'def_rst_val_c' constant that defines the reset value for all "uncritical regsiter" (see NEORV32.pdf)

[33mcommit 71238e46ea89baa5444b617e8f41c51229c81380[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Apr 11 09:35:41 2021 +0200

    [boards/de0-nano-test-setup/README] fixed uart0_rxd_i header pin number

[33mcommit def9ed96d77af18668590eb8a16ed013795ddfaf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 10 23:18:38 2021 +0200

    [boards/de0-nano-test-setup/README] minor typo fixes

[33mcommit 972a20453f66ccd0164c789f8806d4f6f43fdf3b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 10 18:58:07 2021 +0200

    [READMEs] minor edits / clean-up

[33mcommit d069dd2bdb83d8d0fe0f1838e59744e0d1e6f71c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 10 18:00:31 2021 +0200

    [boards/README] added new Terasic DE0-nano setup (Intel Cyclone IV FPGA)

[33mcommit 72175d76eb75904bf8fda33b709ad1a8ce2626a6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 10 17:59:49 2021 +0200

    [boards] added new example setup for the Terasic DE0-Nano boards (Intel Cyclone IV)

[33mcommit c9c1602c9a51300b9c45e929a86a81adbc20c99b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Apr 10 17:57:12 2021 +0200

    [baords/README] fixed setup links to be repo-local

[33mcommit 87df1595c8facad77ba81ca130af061ef48c5bdc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 9 17:34:08 2021 +0200

    [CHANGELOG] added version 1.5.3.8

[33mcommit 9c3a8557ef2fcdbc410711682a028c156e67ce72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 9 17:33:51 2021 +0200

    [docs/NEORV32.pdf] updated MUL/DIV instruction timings

[33mcommit d37c890fcb2563dfca8433b686f266841f610dc4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 9 17:33:24 2021 +0200

    [rtl/core/MULDIV] optimized processing delay
    
    divisions and multiplications are 2 cycles faster now

[33mcommit ffd7d29469815fd01400ba92fef20f8b0708a7f1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 9 17:32:34 2021 +0200

    [rtl/core/neorv32_cpu_control] minor optimization
    
    register write back during multi-cycle ALU operation only when result is really available (reducing switching activity; avoids possible source operand corruption)

[33mcommit f411b7a85618d56c2e1af5c4c979365290947282[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:45:28 2021 +0200

    [CHANGELOG] added version 1.5.3.7

[33mcommit 3e0f3e01a9a859c89001f59aaec620d21debd1b0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:44:21 2021 +0200

    [docs/NEORV32.pdf] minor edits
    
    * updated after-reset-value of CSRs
    * typo and layout fixes

[33mcommit 5869c26fe57286b1173363a2d16f7741741dc5b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:42:50 2021 +0200

    [rtl/core] updated pre-built application images due to modified crt0
    
    * ...bootloader_image -> made from sw/bootloader
    * ...application_image made from sw/example/blink_led

[33mcommit 13ed13c8e06c22e7f14929caa3f63c8709a62f2c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:41:20 2021 +0200

    :warning: [sw/common/crt0.S] crt0 now perform an initialization oll all CPU core CSRs
    
    this is required since most core CSR are NOT reset by hardware

[33mcommit 8b93501aa61f8899a91705f9bcd79778db0866a0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:39:46 2021 +0200

    [sw/example/cpu_test] minor edits
    
    * removed TRAP_RESET mcause code check
    * instret CSR check test: increment between consecutive reads has to be ==1

[33mcommit 8c08c1256f609c73681a1d204cb1bda75f16f1b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:38:38 2021 +0200

    [README] removed obsolete TRAP_RESET mcause code

[33mcommit e378b2f16b4b59601b204d49176a2440e830cc22[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:38:14 2021 +0200

    [rtl/core/i-cache] minor edits to allow better FPGA BRAM mapping

[33mcommit c014649a21367fc79a5c72c550ef6033ab7d6deb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:37:48 2021 +0200

    :bug: [rtl/core/neorv32_cpu_control] bug-fix in HPM event configuration; :warning: code clean-up (:warning:)
    
    * removed TRAP_RESET mcause value (obsolete, was not RISC-V compliant)
    * fixed bug in HPM event configuration via `mhpmevent*` CSRs - there was a CSR address decoding overlap between the HPM event registers and the machine trap setup CSRs (introduced in version 1.5.3.6)
    * :warning: reworked CPU core CSRs: most CSRs are not reset by hardware and need explicit initialization (done by crt0.S start-up code)

[33mcommit 09cea0b7f275c4f30a0e536ae07d73cee38a0121[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Apr 8 18:34:30 2021 +0200

    [sw/lib/include/neorv32.h] removed TRAP_CODE_RESET (obsolete)

[33mcommit e597ec3fbf7dbc717062299bd3b910cff4e14ece[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 5 20:40:29 2021 +0200

    [rtl/core/icache] minor edits

[33mcommit fa79ac40dd2c6a50c3e2402ae867af47a222ddc8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Apr 5 20:40:07 2021 +0200

    [sw/lib/source/neorv32_rte] minor edits

[33mcommit c701cd65fcb83f463ced2391ce95c194d929dde7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 2 16:24:14 2021 +0200

    [CHANGELOG] added version 1.5.3.6

[33mcommit 0c18d9be3c9637d326ccb0a1f62b3e5b92575f28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 2 16:23:46 2021 +0200

    :bug: [rtl/core/neorv32_wishbone.vhd] fixed bug in external memory interface
    
    bug caused bus exceptions when using external memories with very high access latencies (race condition in bus timeouts)

[33mcommit ce6be78372219690be7c5070265ebadc808dd8e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 2 16:22:45 2021 +0200

    [sw/example/cpu_test] RTE setup should be done as early as possible

[33mcommit 866fff90a0b7421ae194a0c83aff70061c973890[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 2 16:22:16 2021 +0200

    [sw\lib\source\rte] minor code optimization (print HW info)

[33mcommit 407ffd3ff4ff3490dcc3f92c4710830cb885969b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 2 16:21:47 2021 +0200

    [sim/neorv32_tb.vhd] minor comment edits

[33mcommit 9d32bc7b7fafc4b8cdd3040390ad3e09d6567063[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Apr 2 16:21:25 2021 +0200

    [sim/ghdl/ghdl_sim.sh] minor edits

[33mcommit 3f9f7682130439b60c6196869876efa4bd8183d5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 19:39:56 2021 +0200

    [rtl/core/neorv32_spu_control.vhd] code clean-up

[33mcommit 8806df9377d855b0261a32eb7adc023e72a64b07[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 19:39:18 2021 +0200

    [rtl/core/package] minor comment edits

[33mcommit 715c4b4fe05891623107c05810cd19ada3c322b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 18:59:42 2021 +0200

    [rtl/core/cpu] minor sanity check edits

[33mcommit c70dff6f1ee6310c2d8a9973aa8dd4c125004d92[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 18:59:21 2021 +0200

    :lipstick: [README] minor format edits

[33mcommit 61fe42d9356d7e55611036b61135917b7e90fe04[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 15:55:03 2021 +0200

    [rtl/core] updated pre-built application images due to mofified crt0.S
    
    * botloader_image: from "sw/bootloder"
    * application_image: from "sw/example/blink_led"

[33mcommit 2abf90df081701996d3aa73d0c570773fcb5db02[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 15:51:05 2021 +0200

    [sw/lib/source/neorv32_cpu] "neorv32_cpu_delay_ms" now makes sure CYCLE CSR is actually running #19

[33mcommit 3ab623bb9d17ba80f5eaf0feaf8e9c3d5e5e5da2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 31 15:50:23 2021 +0200

    [sw/common/crt0.S] now enables cycle and instret counters #19

[33mcommit 16a1a99e2c39a772c2130d93c3ddf158a0c3a6de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:16:35 2021 +0200

    [CHANGELOG] added version 1.5.3.5

[33mcommit e85d3e802051782cf571a8cd12c7e6402767f9a4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:12:10 2021 +0200

    [sw/example/cpu_test] minor edits

[33mcommit e6b935bd6d1f8885193f8431c2caabf73c871770[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:11:40 2021 +0200

    [docs/NEORV32.pdf] added new HPM_CNT_WIDTH generic

[33mcommit 7eeba45af308b8c5264de70af781beee91bf01d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:08:05 2021 +0200

    [sim/neorv32_tb] added new top generic: HPM_CNT_WIDTH

[33mcommit 2ef9e8d2dbdd7a9040c85511544a9ed19377d27d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:07:41 2021 +0200

    [rtl/top_templates] added new top generic: HPM_CNT_WIDTH
    
    allows to specify the total HPM counter size (low word CSR + high word CSR), min=1, max=64; default=40

[33mcommit 3deccde08f2136673d552f22d9954630f27aacda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:06:53 2021 +0200

    sw/lib/include/neorv32.h] added missing <mcounteren> & <mcountinhibit> CSR bit definitions

[33mcommit 566356390879ce9315e748593a69282b257934c4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:05:42 2021 +0200

    [rtl/core] added new top generic: HPM_CNT_WIDTH
    
    allows to specify the total HPM counter size (low word CSR + high word CSR), min=1, max=64; default=40

[33mcommit 25c0a82c8fc7d27772b060c25a856246e839e4c3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:04:25 2021 +0200

    [sw/common/crt0.S] after crt0: all counters are stopped, only machine mode can access counters

[33mcommit 275c8a41d08e30648dbc676fdf5c8fc4289a38da[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:03:16 2021 +0200

    [sw/lib/source/rte] HW info now also shows size of HPM counters

[33mcommit 4ea1f7dd949116b0195e5c874401d9108c16dc1f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:02:50 2021 +0200

    [README] typo fix

[33mcommit fa975698af09d2dc975d62efd5c7e9ea3ca8eefc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 18:02:28 2021 +0200

    [sw/lib/*/cpu] added function to get size of HPM counters

[33mcommit 085761b2e078931040bfa92cdf2d422a41527618[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 13:26:55 2021 +0200

    [boards/README] minor edits (added author links)

[33mcommit cbf20e3e29d26a860fde28ae81bdffda01562625[m
Merge: 0a653199 cfa4384c
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 13:18:03 2021 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit cfa4384ce9c0b40a6851fe45044a62eab882359a[m
Merge: b7a96d55 8669a2e1
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 30 13:16:19 2021 +0200

    Merge pull request #18 from AWenzel83/nexys_a7_example
    
    added Nexys a7 example

[33mcommit 8669a2e1d1eafb993d5e5a0d563e998e8ebabb0c[m
Author: Andreas Wenzel <Andreas.Wenzel@hs-wismar.de>
Date:   Tue Mar 30 09:08:38 2021 +0200

    Update README.md

[33mcommit 8cfd5500fbd3eba94b89b03a3a1b5a8e2de97537[m
Author: Andreas Wenzel <Andreas.Wenzel@hs-wismar.de>
Date:   Tue Mar 30 09:01:13 2021 +0200

    Update README.md

[33mcommit fc09d603881f66f782deb09b9ad6fe08bc7681a3[m
Author: Andreas Wenzel <Andreas.Wenzel@hs-wismar.de>
Date:   Tue Mar 30 08:48:25 2021 +0200

    neuer Branch

[33mcommit 376dfdd723e0a8a1d594792b5499c12036c60abc[m
Author: Andreas Wenzel <Andreas.Wenzel@hs-wismar.de>
Date:   Tue Mar 30 08:41:37 2021 +0200

    renamed folder

[33mcommit af211292d5a7a2be560c071e71b51e7cd3ffd4b3[m
Author: Andreas Wenzel <Andreas.Wenzel@hs-wismar.de>
Date:   Tue Mar 30 08:39:44 2021 +0200

    Example projects for Digilent Nexys Boards

[33mcommit 0a653199c19637bb7f7ba4421d0dec170df48857[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Mar 29 11:10:31 2021 +0200

    [sw/example/floating_point_test/README] added some handy online resources

[33mcommit b7a96d5578a22c0c68971518948656778c916066[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Mar 29 11:01:48 2021 +0200

    [sw/example/floating_point_test] added one instruction of each "class" to execution time test (hw vs. sw)

[33mcommit 63096cc8d1f3aeb3b1f0e5cc3af5436a0f6a782b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Mar 29 10:23:42 2021 +0200

    [boards/README] minor edits

[33mcommit db6b9a72540815a80e6cdfbe57f3b41e7be194e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Mar 29 10:23:26 2021 +0200

    [boards/arty-a7] fixed broken link

[33mcommit 19521cbfdf011eb216602c0f2755c489f2725e55[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 21:22:18 2021 +0200

    [sw/example/floating_point_test] added simple HW vs. SW speedup test

[33mcommit 4d0057296f6e5e0b193a33d1488d930b4769cd61[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 21:21:52 2021 +0200

    [README] :lipstick:

[33mcommit ec25d50e75b1b2310c13b8750df08b0016534ac1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 21:21:35 2021 +0200

    [sw/example/floating_point_test] minor optimization of intrinsic library

[33mcommit 892a7fd2fc1b1907b2bbffbce1cebb54f2b793bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:23:16 2021 +0200

    [boards/arty-a7] typo fixes

[33mcommit aa097adaed81e396eed3803e067e8163ff1127fd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:21:08 2021 +0200

    [boards] fixing typos and broken links - part 2

[33mcommit 7218dc1f97e124eeecabf2e27cae5f51c4c7ee91[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:19:27 2021 +0200

    [boards] fixing typos and broken links

[33mcommit 93e8e01e2f69560764e7e43fe8e2b0ddf3b5a019[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:13:54 2021 +0200

    :sparkles: [boards] added Digilent Arty A7-35 demo project

[33mcommit c4a724f4164812545ea5786bd30bfe623b0f39dd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:09:16 2021 +0200

    [CHANGELOG] added version 1.5.3.4

[33mcommit 75edfac2f5fd0ed0e8c4ea3474339913c9fcfcf1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:08:44 2021 +0200

    [doc/NEORV32.pdf] added new "boards" folder

[33mcommit 7e5dd6527642f710feb942af1a4f9fca3e73a4bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 20:08:24 2021 +0200

    [rtl/core/package] updated version number

[33mcommit fad92d68723733f313fdc0c67a6d64935e269161[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 19:56:18 2021 +0200

    [README] minor edits

[33mcommit 2979075f1bb081d89b00bf0af1b1d098c6e464c7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 18:47:35 2021 +0200

    [rtl/top_templates/test_setup] disabled PMP; now implementing 4 HPM counters

[33mcommit 0912fc2f103a762f5480a0514a8f0f1216e1f67b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 18:41:08 2021 +0200

    [.github/stale] decreased issue timeout to 30 days

[33mcommit b48d29d5952c3fd3fa7523d4f4d8821fb38d062d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 18:39:43 2021 +0200

    [README] added link to exemplary FPGA setups folder (boards)

[33mcommit 2ff5e65e94b5387c6a475503353b915dce992ee6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 17:44:49 2021 +0200

    added folder for exemplary FPGA setups - #7
    
    this folder is still work-in-progress :construction:

[33mcommit bfeb53dc7376dae6703a0187fa90706c3349fbff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 28 10:51:34 2021 +0200

    [docs/NEORV32.pdf] minor rework of basic hardware/software setup sections
    
    targeting [discussion #17](https://github.com/stnolting/neorv32/discussions/17)

[33mcommit 411cb9db15a8272775d44fb57a3a154668d4caca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 19:41:18 2021 +0100

    [sw/example/floating_point_test] updated intrinsic library
    
    intrinsic library now inlines Zfinx floating-point "instructions" (nearly as fast as "native" zfinx support by the compiler)

[33mcommit 9a147543d173ffede14368c59bd09be97e8ae9d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 19:32:07 2021 +0100

    [sw/example/floating_point_test] typo fix

[33mcommit a42aa82e5f51f15a0dfecc6d1264203ad500ab55[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 18:46:38 2021 +0100

    [CHANGELOG] added version 1.5.3.3

[33mcommit a434fa14d69dfea4db96cca96a48a417af75647f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 18:45:21 2021 +0100

    [rtl/core/package] minor edits, version update

[33mcommit 079aba668aa78185daf6d723cae547fedc64371b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 18:40:28 2021 +0100

    [rtl/core/CPU_control] minor optimizations (reducing hardware footprint)

[33mcommit 3bd71e65d9de8fddb7f1f09119112e9091871af0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 18:38:39 2021 +0100

    [rtl/core/FPU] FPU now uses comparator results from main ALU (reduces hardware footprint)

[33mcommit d1722afbe22bcbef33bbc4cbcd7140fb9dc23c58[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 18:36:34 2021 +0100

    [sw/example/floating_point_test] minor edits

[33mcommit f7790d0a7847efa1758a995919d176a7abd76e55[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 17:18:46 2021 +0100

    [sw/example/floating_point_test/README] added current "FPU limitations"

[33mcommit 07a210a23acdc23da574f791dcbaaf197f3c4baf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 27 17:17:54 2021 +0100

    [README] minor fix in CPU synthesis results

[33mcommit afe13d3c731a43eb46ed52bdec9dd1adc5d8e1b2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:59:54 2021 +0100

    :sparkles: [rtl/core] added Zfinx floating-point unit (stable)

[33mcommit b55e8122dfe1c54ccf3ca514d6ede2ebfe4b7758[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:47:48 2021 +0100

    [CHANGELOG] added version 1.5.3.2

[33mcommit 3c3a229d7ddbb7cae1ac3b3eb61dccbc2ca47fc5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:44:29 2021 +0100

    [README] updated synthesis results; FPU is no longer work-in-progress :sparkles:

[33mcommit e70fd41435abb7636679b3eb2f5ebaf99295bbac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:43:04 2021 +0100

    [docs/NEORV32.pdf] added Zfinx FPU; updated synthesis results

[33mcommit ed7ec0cf54eab1972d8f393cbc89da39d02a3a32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:42:38 2021 +0100

    [rtl/core/package] added switch to enable co-processor timeout counter (default=disabled)

[33mcommit 2e46e78d0758536b7c9ccbbb0b66048cae5e8621[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:41:52 2021 +0100

    [rtl/core/cpu] updated CPU configuration sanity checks

[33mcommit c5eeca897a25e19cb2fcb375ebf20d84f19dd1d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:41:19 2021 +0100

    [rtl/core/cpu] added timeout counter to auto-terminate co-proceesor operations (for debugging/simulation only!), default = deactivated

[33mcommit 14db698f10c44b8702ff5725a8c64ec91da81204[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:20:34 2021 +0100

    [sw/example/floating_point_test] removed timing test; added compile guard

[33mcommit 8325ff33ffd054aea3f1966c5468934acae94377[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:05:28 2021 +0100

    [sw/example/floating_point_test] updated Zfinx README

[33mcommit 51ea4ff3b8c5415420c0d42ac12a61fad2c0629f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:05:06 2021 +0100

    [sw/example/floating_point_test] updated Zfinx verification framework

[33mcommit 1f62d253c09042f45963c99905794fab173c6142[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 16:04:18 2021 +0100

    Revert "[.github/stale] reduced issue timeout to 30 days"
    
    This reverts commit eb6e8e119beb3005704eafe3938b19a8119e08e9.

[33mcommit ce66253b74b08f79ed4ba5b28bb0cc1366566063[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 26 15:33:28 2021 +0100

    [sw/example/floating_point_test] updated Zfinx intrinsic library
    
    * intrinsics and emulation function now have the same "interface" (arguments and return value)
    * added "flush_to_zero" function so arguments get "de-denormalized" before doing actual computations

[33mcommit eb6e8e119beb3005704eafe3938b19a8119e08e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 17:09:05 2021 +0100

    [.github/stale] reduced issue timeout to 30 days

[33mcommit 8de15f6c445e504f0abdfe5b410fbab00285e709[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 16:22:24 2021 +0100

    :sparkles: [sw/example/floating_point_test] added Zfinx verification framework

[33mcommit 7724da3f8eb4ef4df417d82a5bd770d7b6325a38[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 16:22:11 2021 +0100

    :sparkles: [sw/example/floating_point_test] added Zfinx intrinsic library

[33mcommit 463992547aac33f46cdb12cf83ca47ff59312567[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 16:04:50 2021 +0100

    :sparkles: [sw/example/floating_point_test] added showcase project for the Zfinx floating-point extension

[33mcommit 9e561f04afe736d4690a27a3ece334e25b53a1ff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 14:12:12 2021 +0100

    [sw/bootloader] added -mno-fdiv switch to makefile

[33mcommit 02e192f13d53ca5fa0a4ead5afe70e414721ef86[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 14:06:52 2021 +0100

    [sw/example/*/makefile] added compiler switch to avoid floating-point division/sqrt instructions
    
    these operations will be emulated using gcc builtin functions

[33mcommit 0c0585d25479a27561d0eeefd3782502d4e1592b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 10:18:53 2021 +0100

    [CHANGELOG] added version 1.5.3.1

[33mcommit f2cb9ad11f84178146a956605171c9305a7cc391[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 10:17:23 2021 +0100

    :bug: [rtl/core/cpu_control] fixed in invalid floating-point instruction detection
    
    * caused CPU to stall if executing an invalid floating-point operation

[33mcommit 7239a6a23d6a97c92a84d857ebf01e5414ee1fcd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 10:14:17 2021 +0100

    [sw/example/bit_manipulation] clean-up of intrinsic library

[33mcommit 04c8db35a250aa6d39d233712fc7ec7224219601[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 10:13:39 2021 +0100

    [sw/lib/source/neorv32_uart] minor edits (if using unsupported format specifiers)

[33mcommit 355a5641684ab73ae2c5fb5bdb0492331e0e45a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 10:13:10 2021 +0100

    [sw/lib/include/neorv32_intrinsics] clean-up; added r3 instruction

[33mcommit 3ccc350737645baaa0f09ce8a7d868ce7c2f10dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 25 10:12:40 2021 +0100

    [README] minor edits

[33mcommit c1880353017ab7c4ab77604af80459b030822cf6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 21:13:05 2021 +0100

    [sw/example/cpu_test] minor edits (to reduce executable size)

[33mcommit dd07075c79d6667a2e44c49ccdfbc50a3405c802[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 21:12:28 2021 +0100

    [README] fixed broken link

[33mcommit 925436648c476f7bafbcbcc5cafbc30042a22bfe[m[33m ([m[1;33mtag: v1.5.3.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 19:31:42 2021 +0100

    [CHANGELOG] added version 1.5.3.0

[33mcommit 7623ee06611a02912b1d77b1a945f84eb3db412c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 19:21:16 2021 +0100

    [sw/lib/include/neorv32_intrinsics] removed dummy "nop" operations again

[33mcommit c72f0308843f8e24f4e6e0394e201799c3ada34e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 19:20:44 2021 +0100

    [docs/NEORV32.pdf] updated version for next release

[33mcommit 14b664264f2dd9f66563e3352f636577c2fc2fd6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 19:20:29 2021 +0100

    [rtl/core/package] updated version for next release

[33mcommit dad1c315c18d5b27df88398e8ad76f78763ae734[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 18:31:22 2021 +0100

    [rtl/core/cpu_control] fixed bug in floating-point exception flag update logic

[33mcommit 98aa0dd2443873ba384f9607724b8a324f046612[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 24 18:30:53 2021 +0100

    [docs/Doxyfile] changed title

[33mcommit b2235b5e952d9d4d5ee01f3cc7e5441d2ba371af[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 21:37:24 2021 +0100

    [README] minor edit

[33mcommit 3818a241bec6f09de47eeb00378ee7372cdc534f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 21:37:13 2021 +0100

    [sw/lib/include/neorv32_intrinsics] added "nop" instruction to prevent GCC ".constprop" optimization

[33mcommit 904816c1f6cf0408d9df35404b2b9be6be505f1b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 18:31:47 2021 +0100

    [sw/lib] added function to check if certain Z* extension is available

[33mcommit a749f06c102ab29ac378ce4251e07ea8c4333d96[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 18:14:26 2021 +0100

    [rtl/core] minor edits (comments)

[33mcommit 1149fb6846a67f52a57c3aaadb9254b42cfc6fa4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 18:14:07 2021 +0100

    [README] fixed broken Zfinx link

[33mcommit aa6e310ae1291466b668b5a1688ef23033c8060e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 18:10:23 2021 +0100

    [rtl/core] updated pre-built apllication image
    
    due to minor core library changes

[33mcommit 55f305dcbf789750027659e37042c1ee08ea8846[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 18:09:12 2021 +0100

    [CHANGELOG] added version 1.5.2.9

[33mcommit 7d504dcf14ed779d2cca1b149a7826a642fd906e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:59:09 2021 +0100

    [docs/NEORV32.pdf] added Zfinx
    
    * added Zfinx instructions
    * added Zfinx enable generic
    * updated CPU and processor figures
    * updated bootloader console example

[33mcommit 23497263a9580aebb4483dcc55793bb833d164fa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:41:59 2021 +0100

    [README] added current state of to-be-released Zfinx extension
    
    :warning: floating-point extension is NOT operational yet! the actual FPU core will be released soon

[33mcommit 933838ce66f5e229850c6cfdac20c5a2fe2b1e2a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:40:41 2021 +0100

    [sim/neorv32_tb.vhd] added Zfinx extension
    
    :warning: floating-point extension is NOT operational yet! the actual FPU core will be released soon

[33mcommit 5e43d64ce53db13c7885673cc91dc556975c4f95[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:40:12 2021 +0100

    [rtl/top_templates] added new generic to enable Zfinx floating-point extension
    
    :warning: floating-point extension is NOT operational yet! the actual FPU core will be released soon

[33mcommit 1abe98b14b3495e661ee1a958d8089501cd95bca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:39:46 2021 +0100

    :sparkles: [rtl/core/neorv32_top] added new configuration generic to enabled Zfinx floating-point extension
    
    :warning: floating-point extension is NOT operational yet! the actual FPU core will be released soon

[33mcommit b9c4232a76d7edfcdbe819fbc5be32630c27b176[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:38:09 2021 +0100

    [docs/figures/neorv32] removed F extension, added Zfinx extension

[33mcommit 5a6bc86a0a25cd048dfa2416a0cebb4abd0687aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 23 17:36:57 2021 +0100

    [bootloader] now also shows available Z* extension
    
    from mzext CSR

[33mcommit 7b447d082e90d49474934dde38e69546c2519555[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:32:25 2021 +0100

    [README] minor clean-up

[33mcommit ac4bd6764e0b77dc07e613a8bfe17869afa7f19e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:21:58 2021 +0100

    [CHANGELOG] added version 1.5.2.8

[33mcommit 0a9ddd84d9c5f69b11cc661317f801957c6da4dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:20:52 2021 +0100

    [docs/NEORV32.pdf] added new intrinsic core library; added floating-point CSRs

[33mcommit 845aad6f6ab76733b28b4340603bb82eed4cc228[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:20:21 2021 +0100

    [rtl/core/neorv32_package.vhd] added floating-point-related global definitions

[33mcommit fc8fc000ad24c0a780ab7144203b875382fad9bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:19:33 2021 +0100

    [sw/example/bit_manipulation] removed intrinsics helper macros; they are located now in a new file in sw/lib/include

[33mcommit a7971065a2e7bfcf8ad5cf621cb49fc9f5046de5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:16:40 2021 +0100

    [sw/lib/include] added new library file: helper macros for custom intrinsics/instructions
    
    intrinsic support libraries of CPU extensions, which are not yet supported by the upstream GCC, are based on these macros

[33mcommit 9bd43e157081ed1b4ba99aad1ed966cc718a4479[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 17:15:33 2021 +0100

    [sw/lib/include/neorv32.h] added floating-point CSRs
    
    -> fflags, frm, fcsr

[33mcommit 7a4428e56be0f8a34fe610387f6605278d3fc82e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 16:54:54 2021 +0100

    :bug: [sw/*/makefile] fixed problem with linking math.h library

[33mcommit 4a406ba24152543dec90283f5baf3b1e364db42d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 21 15:57:57 2021 +0100

    [README] typo fix

[33mcommit 4405fc4efe6d65e35c4d83b6cf2fe2328361fe9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Mar 19 16:44:16 2021 +0100

    [README] minor layout edit

[33mcommit b96f7d2b3232f14eb995b7dff8089f524131b331[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:19:46 2021 +0100

    [CHANGELOG] added version 1.5.2.7

[33mcommit b60fa638192f021932aa85cb649e42ff9e4ff8f5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:18:15 2021 +0100

    [docs/NEORV32.pdf] minor edits (moving from F extension to Zfinx extension)

[33mcommit 7b046f74568f3338a4fc39acbc4b5e9e9c13c80c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:17:49 2021 +0100

    [README] updated floating-point extension text
    
    moving from F to Zfinx

[33mcommit 09fcac3473b817244767ba4e1586650324e85e7c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:17:02 2021 +0100

    [sw/example/cpu_test] minor edits
    
    to keep application image size below 16kB :wink:

[33mcommit 1e8873d5cb2f8b1b7854b021db64ab39190d39e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:16:19 2021 +0100

    [sw/lib/source/neorv32_rte.c] added Zfinx flag to hardware configuration output

[33mcommit ebe9b14c3d440c9da9de1c63d203c9827b051d9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:15:48 2021 +0100

    [sw/lib/source/neorv32_cpu.h] added compiler warning when using floating-point ISA string configuration

[33mcommit 606ae41f81ae5fde26c29f274568234ffa7009bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:15:09 2021 +0100

    [sw/lib/include/neorv32.h] added Zfinx flag to mzext CSR
    
    to discover if Zfinx floating-point extension is available

[33mcommit 19a20739512ff87c3f6c3e1ba5824d203c3138da[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:14:34 2021 +0100

    [sw/common/crt0.S] removed floating-point register file initialization
    
    Zfinx floating-point extensions uses the integer register file

[33mcommit 4c951c7521b7404975f2af088b241fb9f04908ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:13:51 2021 +0100

    [cpu/core] changed upcoming floating-point extension from "F" to "Zfinx"
    
    Zfinx uses the integer register file for floating-point operations; hence, dedicated floating-point load/store and move operations are obsolete
    
    see Zfinx RISC-V spec: https://github.com/riscv/riscv-zfinx

[33mcommit 1b273fa700ab99387c5c0a23b878759e549d7061[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 18:12:18 2021 +0100

    [rtl/core/neorv32_cpu_decompressor] removed compressed floating-point load/store instructions

[33mcommit 5008490caec48ace0a5d8a8dded76231a760adaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 11:43:13 2021 +0100

    [rtl/core/memory images] updated pre-build application/bootloader images due to bug in crt0.S start-up code

[33mcommit d2ce4a7c6b31b159c32ec5e1f291faabbbb4dfc6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 18 10:00:46 2021 +0100

    :bug: [sw/common/crt0.S] fixed bug in dummy trap handler
    
    -> wrong order of register push/pop

[33mcommit 8c9952b7a3f0446a6613e2a05bef87dcde563cf4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:31:34 2021 +0100

    [CHANGELOG] added version 1.5.2.6

[33mcommit 122328470bf65c5818fa24c0548c35be81b3f8fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:30:54 2021 +0100

    [docs/NEORV32.pdf] updated CPU/top signals; reworked excluisve memory access interface

[33mcommit 717ee7838d028f698feba1f3a81b4d51fed162bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:12:04 2021 +0100

    [sim/neorv32_tb] added simple model to test excluisve access reservation
    
    this mechanism is used to test the atomic memory access instructions using the sw7example/cpu_test/main.c ctest program

[33mcommit 71aa6f8102eb5e90c3351e2150e9b22bb5f00012[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:06:31 2021 +0100

    [sw/example_cpu_test] minor edits for new excluisve memory access interface

[33mcommit cdb4278d6803406fa179fd550dcedc603793a121[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:06:02 2021 +0100

    [rtl/top_templkates] added "exclusive" attribute to Wishbone request tag singal; removed obsolete lock signal

[33mcommit cc2fc0dfe945b4748009c25c7bb6df32bc0e5e73[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:05:18 2021 +0100

    [rtl/core/processor] reworked excluisve access interface/protocol of external memory access
    
    * removed obsolete wb_lock_o signal
    * added "exclusive" attribute to acceess request tag
    * added repsonse tag signal (1-bit to indicate if the reservation of an exclusive access was still valid for a store-conditional instruction)

[33mcommit 60575871d9388fb44d01c24ca486f94451a93a84[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:03:22 2021 +0100

    [rtl/core/i-cache] removed obsolete "lock" signal
    
    * instruction fetches cannot have "locked" or excluisve attribute

[33mcommit fe26c2106725a31f6d2a4e7ec35c47318f1c0f3e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 16 17:02:40 2021 +0100

     [rtl/core/cpu] reworked CPU's atomic/excluisve memory access system
    
    * removed "lock-based" excluisve access scheme
    * removed reservation termination via bus exception - there is now a dedicted response channel for that

[33mcommit f9919b28f50f6e8d1dd1d2274bf2da0ab01c1dc5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:33:10 2021 +0100

    [CHANGELOG] added version 1.5.2.5

[33mcommit d7d86f97c9d8637b066467d5c922925e2411fcb3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:31:21 2021 +0100

    [docs/NEORV32.pdf] added B.Zba instructions

[33mcommit 94a64fcf541c86e521e0752ea64665546bad2018[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:29:44 2021 +0100

    [README] added B.Zba extension (shifted-add instructions)

[33mcommit 25bdeaaccb43609028e85bb321a7ca050ab66cb6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:29:17 2021 +0100

    [sw/example/bit_manipulation] added Zba intrinsics, emulation function and tests

[33mcommit 240f82929b919ee20d7d96a15915a695e49af234[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:28:44 2021 +0100

    [sw/lib/source/neorv32_rte] added Zba flag to hardware check; fixed/updated ISA check (compiler's ISA string vs. MISA CSR)

[33mcommit 2266d2cec03fa025d588b00eb90e25e19d791cf2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:27:44 2021 +0100

    [sw/lib/include/neorv32.h] added Zba flag to mzext CSR

[33mcommit a830f89fb837d6599f00828532812c4cf289f510[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:27:10 2021 +0100

    [rtl/core/bit_manip] added Zba (shifted-add) sub-extension
    
    Bit-manipulation `B`:
    * sub-extension `Zbb` (shifted-add)
    * SH1ADD
    * SH2ADD
    * SH3ADD

[33mcommit afc94afeb350e019a42dc17ceace6666b3f2a2e8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 9 20:04:52 2021 +0100

    [sw/lib/source/neorv32_cpu.c] added warning when compiling for not-yet-supported extensions

[33mcommit 33cac05b4973887fe5b737c7f9ec7f2203236d41[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 16:03:01 2021 +0100

    [README] added new NEOLED module (NeoPixel(c)-compatible hardware interface)

[33mcommit c686d07f174225a9b635625743fb30e526d34e3f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 16:01:52 2021 +0100

    [CHANGELOG] added version 1.5.2.4

[33mcommit e4738e06c0bb86430d05a7b3c2fb9055ff3190d5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 16:01:29 2021 +0100

    [docs/NEORV32.pdf] added NEOLED module
    
    * new section for NEOLED IO module
    * updated FIRQ assignment list
    * updated processor top signals
    * updated processor top generics

[33mcommit c6076c2313965c8907cf063d6d157c7a922a5fde[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 16:00:39 2021 +0100

    [docs/figures] updated processor diagram
    
    added NEOLED module

[33mcommit 8ba6405989d66c1240bcb0374daf4d976a0c998b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 16:00:06 2021 +0100

    :sparkles: [sw/example] added NeoPixel demo for new NEOLED module

[33mcommit ed84a2e926181fbe11b25b399011c9aa01367ae4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 15:54:31 2021 +0100

    [sw/lib/source/rte] added NEOLED to hardware info check

[33mcommit b4d7c315fb432713b50e6f297dda29654781f76f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 15:54:12 2021 +0100

    [sw/lib] added new NEOLED module (register definitions + SW driver files)

[33mcommit c181b16161aaac8a7fad35eeb8dad974629a42bf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 15:53:38 2021 +0100

    [sim] added new NEOLED module

[33mcommit d15f766fba2c9cd0cd17f7a49b14b649cd91ec1e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 15:53:00 2021 +0100

    [rtl/top_templates] added new NEOLED top generic and signal

[33mcommit ee41ef2b077f85740bad7cc81fa242711503792f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 15:52:30 2021 +0100

    [sw/lib/source/spi] typo fix

[33mcommit ea70c789675eee3a206b2b231c73d164361b8ef7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Mar 7 15:51:47 2021 +0100

    :sparkles: [rtl/core] added new IO/peripheral module: smart LED interface (NEOLED)
    
    * compatible to WS2812/WS2811 LEDs (used in the awesome Adafruit NeoPixel products1 :wink: )
    * supports 24-bit and 32-bit mode (RGB & RGBW LEDs)
    * hardware TX buffer to relax CPU time constraints
    * new top signal: `neoled_o`
    * new top generic: `IO_NEOLED_EN`

[33mcommit 40dc7ae496c33d3c180f38f2c442b0dfe1854155[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 6 22:02:26 2021 +0100

    [.ci] updated to latest pre-build tool chain

[33mcommit 36db64b37eab804d33eecf15c0c710c179c71762[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 6 17:01:38 2021 +0100

    [CHANGELOG] added version 1.5.2.3

[33mcommit 8c60413d8b7831d3d202002490d21cda5b93c334[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 6 16:58:52 2021 +0100

    [rtl/core] control code clean-up
    
    * fixed minor bug in F-exension's instruction decoding
    * changed coding style for CSR write access (old version might have caused "inferring latch..." warning in Intel Quartus)
    * fixed default values for CSRs when according extensions are disabled

[33mcommit dfaa0156a56f2c227a56265fbc2f44bd1a9aad06[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Mar 6 15:58:13 2021 +0100

    :bug: [sw/examples/demo_gpio_irq] fixed GPIO pin-change FIRQ chanel (#14)

[33mcommit e76906c9f7894979e087672408c808addd18c891[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 12:20:21 2021 +0100

    [riscv-arch-test/README] typo fixes

[33mcommit 05804c6dac0bce2370621e34c74256f86cebf75e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 12:14:15 2021 +0100

    [README] minor edits; updated riscv-arch-test workflow

[33mcommit 27a5fcbe1704665ea3546cc42265a402e127043a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 11:48:28 2021 +0100

    [riscv-arch-test/run_riscv_arch_test.sh] minor edits

[33mcommit 8944fc48412da91243a269b93ba3e3a4e3f9fd68[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 11:23:51 2021 +0100

    [.github/workflows] renamed and adapted RISC-V architecture test workflow

[33mcommit 00efb6e6bfceff83280fa9d2b0bf2a04b341db16[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 11:22:31 2021 +0100

    removed riscv-compliance folder; replaced by up-to-date riscv-arch-test folder
    
    Adapted to new name of the RISC-V architecture test repository

[33mcommit cf35d19057590aae947443dbdeaf99ed290dcc9e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:32:42 2021 +0100

    [CHANGELOG] added version 1.5.2.2
    
    implementing #13

[33mcommit 2077b644a1a4791d483ca39ab8626556784c20cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:32:15 2021 +0100

    [docs/NEORV32.pdf] added new CFS IO configuration generics
    
    * added to "processor generics" section
    * added to "custom functions subsystem (CFS)" section

[33mcommit 06d49a4e7697447449f82fe8c59586564754a108[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:30:20 2021 +0100

    [sim/neorv32_tb.vhd] added CFS IO configuration generics

[33mcommit 2b00f51a33279cd20df5e89b537ae66f5b2d54e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:29:55 2021 +0100

    [rtl/top_templates] added new CFS IO size configuration generics

[33mcommit 302e9da8ece495f911eca950df0b23c93b6cdde0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:29:25 2021 +0100

    [rtl/core] added CFS IO configuration generics (implementing #13 )
    
    * IO_CFS_IN_SIZE defines size of cfs_in_i signal
    * IO_CFS_OUT_SIZE defines size of cfs_out_o signal
    * generic type is "positive"
    * default value for both generics = 32

[33mcommit 7960ca1f31d62bc02fe7303eb4d6b31251f83da0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:23:13 2021 +0100

    [rtl/core] completed implementation of F extension's CSRs
    
    :warning: the F extensions is not yet operational!

[33mcommit aadf4ba97fde2f3ce2c2f4f2e050a17e4b2095ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Mar 4 10:21:16 2021 +0100

    [README] minor edits

[33mcommit 3236bfb696048f1496fec9c4c099d6ed22f4de0f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:55:21 2021 +0100

    [docs/NEORV32.pdf] added notifier regarding F extension (WORK-IN-PROGRESS)

[33mcommit faabf02417d97fb135b4be8b8f858ef4428a5a5e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:54:54 2021 +0100

    [README] added current state of F extension-related work

[33mcommit edebf5fdb37e1473de89a7a3ba433923446e6e4d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:53:59 2021 +0100

    [CHANGELOG] added version 1.5.2.1

[33mcommit d5b15bfb993e3955c8cd77c57b8d1de8826f42ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:52:06 2021 +0100

    [sim/ghdl] added new FPU (blank) HDL source file
    
    update for **upcoming** F extension; modifications have NO actual impact yet

[33mcommit 2cee0dee0681bfa77db293c7dbc45d4847b22798[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:51:42 2021 +0100

    [rtl/core] added FPU HDL file; **blank template only**
    
    update for **upcoming** F extension; modifications have NO actual impact yet

[33mcommit 93da5b0419e4d3bf1e8068746a5de3456a2b5bbc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:49:53 2021 +0100

    Update neorv32_cpu_decompressor.vhd
    
    added instructions:
    - C.FLW
    - C.FSW
    - C.FLWSP
    - C.FSWSP
    
    all these instructions will trigger an illegal instruction exception when executed
    
    update for **upcoming** F extension; modifications have NO actual impact yet

[33mcommit c4e83332266448c82e2289787bbccaa6c1421ba0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:48:49 2021 +0100

    [rtl/core] added CPU core infrastructure for upcoming F extension
    
    update for **upcoming** F extension; modifications have NO actual impact yet

[33mcommit 77d862da8bb2ce89a177a0db1d321556d1666a24[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:47:00 2021 +0100

    [sw/lib/neorv32.h] added alias for misa F flag

[33mcommit 57632c162df004269d28affff50e37c9409fdc47[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:46:38 2021 +0100

    [sw/lib] added WARNING when compiling with F extension
    
    update for **upcoming** F extension; modifications have actual NO impact yet

[33mcommit 16547c848d9fbff7898f6055b91c296484dc6996[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 11:45:58 2021 +0100

    [sw/common/crt0.S] added FPU register file init
    
    update for **upcoming** F extension; modificytions have NO impact yet

[33mcommit 308ebe3fa244c15f2885d631104ce6b98e5ea0ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Mar 3 10:36:11 2021 +0100

    [sw/example/bit_manipulation/README] minor edits and updates

[33mcommit c670d100580e415e4a2c63cff9a30bdb11df1990[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 2 20:46:42 2021 +0100

    [README] minor edits; added note regarding CPU's F extension
    
    :construction:  single-precision floating-point extension is **work-in-progress** :construction:

[33mcommit 481e9d71fef622837c902d07102c02dbe6c489e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Mar 2 20:44:32 2021 +0100

    [.github/stale] changed time for issues becoming stale

[33mcommit 8557ae4ef8b44301be7a28074ec3d41c9223fd15[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Mar 1 17:54:16 2021 +0100

    [CHANGELOG] added link to latest release

[33mcommit 9a2919de8ab307c57c283b2cc1fee56a1bd8980b[m[33m ([m[1;33mtag: v1.5.2.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Mar 1 16:12:39 2021 +0100

    updated version number for new release

[33mcommit 90fbda7243967c0d2b518c659e75287da8664b9b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 27 18:31:35 2021 +0100

    [README] minor edits

[33mcommit 6401f7562351d12f01c7c33b7c72fd5c1bb3a8e8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 27 18:31:23 2021 +0100

    [CHANGELOG] added version 1.5.1.11

[33mcommit 1ab9bf412a3581a1326c2dc7073f46ee42eefb99[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 27 18:30:47 2021 +0100

    :bug: [rtl/core/control] fixed several small bugs in B extension's instruction decoding
    
    not all `B` instructions triggered and *illegal instruction exception* when B-extension = disabled

[33mcommit 3954de7a8cd302df943eeb7319c71a6e9bbc0085[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 19:48:00 2021 +0100

    [docs/Doxyfile] minor edits

[33mcommit 2fb75423d5180663b0419cfded16d60b67c72b0c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 19:23:57 2021 +0100

    [docs/NEORV32.pdf] updated UART section
    
    * added text regarding UART.RX double-buffering
    * added more information regarding RTS/CTS hardware flow control handshake

[33mcommit ade56d1864a1d54cc94e6da95175901176305375[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 19:23:43 2021 +0100

    [docs/Doxyfile] fixed logo include path

[33mcommit 2252a9eb58fe880b07774a1b0e548cf0e92b7bda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 19:07:40 2021 +0100

    [CHANGELOG] added version 1.5.1.10

[33mcommit 062f3c7dadd6e94662d0f4a762733969ef733860[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 19:07:19 2021 +0100

    :bug: [rtl/core/uart] fixed bugs in RTS/CTS hardware flow control (targeting #11 ); added double-buffering to UART RX engine
    
    RTS/CTS hardware flow control was finally verified on *real* hardware

[33mcommit 8bdac989a0c2f2309ab43bc1a96240c99d17c672[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 19:05:59 2021 +0100

    [rtl/core/cpu] modified notifier when using B extension

[33mcommit 68d3f7dc3e9d76e85c2d658c0045d516b41b9c26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 25 16:26:06 2021 +0100

    [sw/lib/source/mtime] typo fix (stupib, but not critical)

[33mcommit 0f4b3fe7bb795197b15c5c81c8dbd65153d0b33f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 17:03:37 2021 +0100

    [README] instruction mnemonics clean-up

[33mcommit e478e837bf29270586fce86443bf03527a233ed2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:50:47 2021 +0100

    [CHANGELOG] added version 1.5.1.9

[33mcommit 358e01f9f099779a74d67e1f4ba2abef96ecefcd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:50:19 2021 +0100

    [sw/example/cpu_test] minor edits

[33mcommit 08c22835e05e7363829beb691791ad11d26d6b51[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:49:51 2021 +0100

    [docs/NEORV32.pdf] added Zbs
    
    * added note: mcounteren CSR is hardwired to zero if user mode is not implemented
    * mpp is read only if u-mode not enabled
    * added Zbs B sub-extension's instructions and instruction timing

[33mcommit fb16286d34c04c6971acf80075371fb1bbea3910[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:48:47 2021 +0100

    [sw/lib/source/rte] added check for Zbs extension

[33mcommit 5cd204e1597b4e390314a9995d9c106a5519ce51[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:48:25 2021 +0100

    [sw/lib/include/neorv32.h] added Zbs flag to mzext CSR

[33mcommit e241cc2a28ca99bb15ffd4e44c73ab9d73b7e87b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:47:27 2021 +0100

    [sw/example/bit_manipulation] added Zbs tests and "intrinsics"

[33mcommit cc5b27c36d3b33bfd838dc6f234487875fd659d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:46:57 2021 +0100

    [rtl/core/package] updated version number

[33mcommit 4b689206ae34c278eb39047eb2a3b1e8b509c764[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:46:12 2021 +0100

    [README] added Zbs (single-bit ops) sub-extension (B extension)

[33mcommit af7d365ba911c4aa086d847b0ccd798a11c3777f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 24 16:00:45 2021 +0100

    [rtl/core/bitmanip_extension] added Zbs sub-extensions (single-bit operations)
    
    * sbset[i]
    * sbclr[i]
    * sbinv[i]
    * sbext[i]

[33mcommit 99b1ff5141ecaaa0e4a05b72d9a7c90ca96cd98f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 22:36:33 2021 +0100

    [README] typo fixes

[33mcommit 1a44aec79049abd241151fe81cf4b427d3a123e8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 16:37:36 2021 +0100

    [README] fixed broken "A" extension link

[33mcommit cdd03ce633904f3d1760c33fb4993afaad642d64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 16:36:13 2021 +0100

    [README] fixed broken links

[33mcommit 6f127f124c34b8ed9c89bf55b8520844ac3d7bcd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 16:31:48 2021 +0100

    Update README.md

[33mcommit f2daff162c71939d915ac8ca5ac39d0dc8e467b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 16:31:21 2021 +0100

    Update README.md

[33mcommit d76106e2949bef91162c4d6771b0c40237f6a936[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 14:34:11 2021 +0100

    [README] minor edits; added link to new project boards

[33mcommit f930be6ad233d348f176cd9ef63adb059c66030c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 23 10:59:31 2021 +0100

    [README] minor edits

[33mcommit 4da405f2517d89682e22939e69d2597a2a8db2bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:26:17 2021 +0100

    [CHANGELOG] added version 1.5.1.8

[33mcommit 4d394a1f380425305d2dd53aa47b4bda5edd9cb3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:25:34 2021 +0100

    [docs/NEORV32.pdf] added UART hardware control flow to UART sections

[33mcommit 28b6a698cc1bcb16a06a1a3c253da6725d4b2dfe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:24:40 2021 +0100

    [README] added UART HW flow control: minor formating edits

[33mcommit ec48cd0c80fd8482939c3adc2acb1757a393b319[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:24:11 2021 +0100

    [rtl/core] updated pre-built application images
    
    * neorv32_application_image.vhd was built from sw/example/blink_led
    * neorv32_bootloader_image.vhd was built from sw/bootloader

[33mcommit f7d9dc18d6b3dcfe23ec6ec197b941328fc9871e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:23:14 2021 +0100

    [sw/bootloader] added UART HW flow control configuration

[33mcommit 0a00f6a5e0bcdaf1336469fe789bd452c2b46426[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:16:21 2021 +0100

    [sw/example/*] added UART hardware flow control parameter to UART setup functions
    
    * added define to ease parity configuration

[33mcommit a6c80778b9d56555a662669103b436dc5b0def85[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:15:20 2021 +0100

    [sw/lib/uart] added hardware flow control parameter to UART setup functions

[33mcommit 814fc11552f1a87135c192a49caa4c84a00a29e6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:14:52 2021 +0100

    [sw/lib/include/neorv32.h] added new UART control register flags for HW RTS/CTS flow control
    
    * added defines to simplify UART's parity and hardware flow control configuration

[33mcommit e0bcfdddeb4835c08644eaf2a964551af3184b23[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:13:56 2021 +0100

    [sim/testbench] added UART hw flow control signals (local loop-back)

[33mcommit e7347242842ae86985a2aa056246646c71ae3947[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:13:31 2021 +0100

    [rtl/top_templates] added UART hardware flow control signals rts and cts

[33mcommit 6e37e564b868ae157c871857be41b49704bc9586[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 22 19:12:57 2021 +0100

    [rtl/core] added hardware RTS/CTS UART flow control to both UARTs (targeting issue #11)

[33mcommit 6fb04e2395e4e1675c3ddb2545079546ddb8becb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 21 15:18:44 2021 +0100

    [docs/figures/neorv32_processor.png] fixed number of SoC FIRQ channels

[33mcommit 815dfbf2b976a7b42a00167684b3c8e07a406f95[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 21 11:27:07 2021 +0100

    [README] minor edits

[33mcommit 1ca82266333f5f9f7e3dc3139fb9eba7c9506e9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 20:02:26 2021 +0100

    [CHANGELOG] minor typo fixes

[33mcommit 9d64674ecff694abc28980351749b0ff10e1bc8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 20:02:03 2021 +0100

    [doc/figures] added small transparent logo (for doxygen)

[33mcommit 83e35faa1959e7be18e7c820a6664ec0ea1a6a6d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 18:22:44 2021 +0100

    [doc/figures] now using vector fonts for transparent images

[33mcommit c4b0fc046b48eca6f0a1345e41375ed225928ff9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 18:06:04 2021 +0100

    [README] minor format edits

[33mcommit a33f21652fabe14f65754414b57babd58017929b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 18:05:32 2021 +0100

    [docs/figures] minor scaling and color edits

[33mcommit b21582cd0a0bb6b91c95164267076e889f6649f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 16:44:06 2021 +0100

    [docs/NEORV32.pdf] fixed FIRQ channel numbers in peripheral sections

[33mcommit 6d5849cb14042576fff1771644df90868c69b84f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 16:05:26 2021 +0100

    [CHANGELOG] added version 1.5.1.7

[33mcommit 1fd861f9fbd23a7bb13899754018bff62d3f1d72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 16:03:59 2021 +0100

    [docs/NEORV32.pdf] modified processor FIRQ assignment
    
    * UART1 now features individual interrupt channels for "TX complete" and "RX complete" conditions
    * changed FIRQ channels for SPI, TWI and GPIO interrupts

[33mcommit 0a82b0eba423f4db4faaafd1d7fdc3d6b29b5b12[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:58:44 2021 +0100

    [sw/example/cpu_test] added UART1 RX & TX IRQ tests; minor edits to keep exe size < 16kB

[33mcommit 485eba1688f0d1fe0eca5993c75745b56f45d6ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:42:09 2021 +0100

    [sw/lib/source/uart] minor edits

[33mcommit e2f92e9145475661296ade523957a00ee7df04de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:41:16 2021 +0100

    [sim/test_bench] constraint "SoC FIRQ" to 6 channels
    
    adapted simulation trigger for SoC FIRQs 0..5 (= FIRQ10 .. FIRQ15)

[33mcommit 6536a0358c47dc9674fe8146a82119e21239e196[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:40:23 2021 +0100

    [rtl/top_templates] constraint "SoC FIRQ" to 6 channels

[33mcommit 37476101a8618574d543f680fa8c475429193b3b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:39:48 2021 +0100

    [rtl/core/neorv32_top] constaint "soc firq" to 6 channels; reworked FIRQ channel assignments
    
    UART1 now features individual FIRQ channels for "RX complete" and "TX complete" conditions

[33mcommit 605d829f3ff32c2995d50d57b9c3672dd31fb00d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:38:30 2021 +0100

    [rtl/core/package] removed CFS err_o signal; processor top's SoC FIRQ constraint to 6 channels

[33mcommit e8636710156cce7a3184a185b215b5c47bbd3642[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:09:37 2021 +0100

    [CHANGELOG] typo fix

[33mcommit 7a072392ae71527b8c1ca8e3b1999c31451ee160[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 15:06:44 2021 +0100

    [rtl/core/CFS] removed CFS's `err_o` signal
    
    If this feature is really REALLY required the implementer can chose to send no ACK for an access also triggering a bus exception after some cycles (-> package's gloabl "bus_timeout_c" constant).

[33mcommit d314e6ddf5960724776a41bf58d0fc8996314667[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 14:45:09 2021 +0100

    [README] added idea: add RISC-V cryptio (K) extension

[33mcommit cd0cf680ee40d9871bf68d6083d83d1f51df5864[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 20 14:44:44 2021 +0100

    [LICENSE] date fix

[33mcommit f179fc25e55b4f89601e0a2164ad62006152bb9a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 18 16:42:37 2021 +0100

    [sim/vivado/wave_config] updated default Xilinx ISIM wave configuration

[33mcommit abae883c460bb772bb9fe10df899c239cfc68ec9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 18 16:42:12 2021 +0100

    [CHANGELOG] added version 1.5.1.6

[33mcommit 43d33ba424078a5ff459420cb5af572e16714dbd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 18 16:41:36 2021 +0100

    [rtl/core/package] updated version number
    
    -> 1.5.1.6

[33mcommit 2a5e6eb0636c20f10998bb3e2f6475b8e725f62b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 18 16:09:11 2021 +0100

    [rtl/core/neorv32_top] added register stage to clock generator enable signals
    
    relaxes FPGA placement constraints and can result in higher f_max in some cases

[33mcommit 5edd8334f65964aa2d7cf9a8be0303c1968c0544[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 18 16:07:01 2021 +0100

    :bug: [sw/example/demo_twi] fixed error in TWI speed message
    
    factor 1/4 was missing

[33mcommit 04bd3478bbe983f3f16c36456df4f1b10cb20738[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Feb 18 16:06:04 2021 +0100

    [rtl/core/SPI,TWI] minor comment edits

[33mcommit 22b9182acb77cd64d61afb6512549a9d79bae6e4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:27:37 2021 +0100

    [.ci] changed default simulation time to 7ms

[33mcommit 94177b948cb336dc60357249ca7e936c7863bd25[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:21:32 2021 +0100

    [riscv-compliance] updated scripts due to modified UART simulation output file names
    
    compliance test framework uses primary UART (UART0) to dump test results

[33mcommit 8c7072a97db1e6a5d1356c70d1df144b98880acc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:17:38 2021 +0100

    [rtl/core] updated pre-built application images
    
    bootloader & sw/example/blink_led

[33mcommit 94e04b3f48873076931a9447ceca274be204f91f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:17:08 2021 +0100

    [sw/bootloader] bootloader uses primary UART (UART0) for user interaction

[33mcommit 3c8a26bdcb7ac562933cc211c38a5d59be14e811[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:07:38 2021 +0100

    [CHANGELOG] added version 1.5.1.5
    
    added secondary UART; reworked UART simulation output files

[33mcommit ba31f78c9f7554f483252ab0f38f87f8cf35de16[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:07:02 2021 +0100

    [doc/NEORV32.pdf] main updated
    
    * added secondary UART (UART1)
    * reworked FIRQ (fast interrupt) assignment/priority list
    * updated UART simulation output file names

[33mcommit c50d68f405d8a44c9527e8baa7e6a95afa6cfd8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:03:38 2021 +0100

    [docs/figures/neorv32_processor] added second UART (UART1)

[33mcommit 64b83778c9c875e4952588da263e19bf0d4105ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 18:03:04 2021 +0100

    [docs/figures/test_setup] renamed UART signals
    
    using primary UART (UART0)

[33mcommit cacccc389b56341ff1602d8f3ee544f94709a294[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:56:51 2021 +0100

    [sw/lib/uart] added doxygen warning
    
    UART0 (primary UART) is used as default user console interface by the whole NEORV32 software framework

[33mcommit 14e4ba8953376f6dd0400973dbf98f2044de6583[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:36:44 2021 +0100

    [.ci] modified CI scripts due to renamed UART simulation output files
    
    using new compiler flag for enabling simulaiton mode of UART1: `UART0_SIM_MODE`; however, `UART_SIM_MODE` is still supported (for compatibilits) and maps to `UART0_SIM_MODE`

[33mcommit f532481dcc3e6bc9a5714d8f34a8aa43ae49fc3c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:35:13 2021 +0100

    [sim/ghdl] updated simulation script
    
    increased default simulation time to 7ms;
    added/renamed UART0/1 simulaton output files

[33mcommit 06d9ea5740353ce769d641b180f1de289eeb5b36[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:34:08 2021 +0100

    [README] minor edits
    
    added secondary UART (UART1)

[33mcommit 05bd8c8386e016704c0cc268fd036902c7fa1e9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:33:28 2021 +0100

    [sw/example/cpu_test] added UART1 RTX IRQ test
    
    modified FIRQ tests due to modified FIRQ priority list

[33mcommit edf92636b3c5fe6e74ab1c9f5974b7fa602319b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:32:40 2021 +0100

    [sim/neorv32_tb.vhd] added secondary UART (UART1)
    
    added testbench UART receiver for secondary UART (UART1); modified simulation UART receiver output file names

[33mcommit d7ce1c9125cbb36d3981c3a4686bec55c3778efd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:31:30 2021 +0100

    [sw/example/blink_led] minor comment edits (regarding primary UART)

[33mcommit c565d8fce7de7851fa8c2714a6322f5c4b18e15f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:30:58 2021 +0100

    [sw/lib/uart] added secondary UART (UART1) driver functions
    
    added compatibility wrappers: `neorv32_uart_*` functions map to primary UART (UART0)

[33mcommit d1bada57936d8237841773141b368a8f9f754f7b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:29:50 2021 +0100

    [sw/lib/include/neorv32.h] added secondary UART (UART1)
    
    renamed register of "old" UART (-> UART0);
    added registers of new UART (UART1)

[33mcommit e4a8d9e2f865df7be28edabf24d1b5b8e3ecc910[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:28:59 2021 +0100

    [sw/lib/rte] added new secondary UART (UART1) to hardware analysis list

[33mcommit 629a1463cb444ca25e75ed507d43790624b3eadf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:28:06 2021 +0100

    [rtl/top_templates] added secondary UART (UART1)
    
    renamed primary UART interface signals & configuration generic;
    added UART1 (secondary UART) enable generic;
    added UART1 interface signals

[33mcommit 54c6ba5954168297199fe327d4d8b545e8c6d8bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:26:12 2021 +0100

    [rtl/core/uart] uart VHDL module now serves as template for UART0 and UART1
    
    updated simulation output file names (see CHANGELOG)

[33mcommit 2aa4c5cb118640cee5cf0d97ae6242f1e2a6b370[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:25:31 2021 +0100

    [rtl/core/sysinfo] added UART1 (secondary UART) available flag

[33mcommit 60263b8f853f493e6394837c01dbf5e2d818fd75[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 17:24:56 2021 +0100

    [rtl/core] added secondary UART
    
    "old" (primary) UART -> UART0
    new (secondary) UART -> UART1

[33mcommit f795afdf81b3ef0f22ce0da13ae9ac03801a2ec3[m
Merge: e927676b 8e5bf12e
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Feb 17 16:22:17 2021 +0100

    Merge pull request #10 from pcotret/patch-1
    
    Fix typo in the Readme

[33mcommit 8e5bf12e3d1063abe68e6a7fb480b681229cc313[m
Author: Pascal Cotret <pascal.cotret@gmail.com>
Date:   Wed Feb 17 16:13:22 2021 +0100

    Fix typo in the Readme

[33mcommit e927676bc6e663da77b77c469ce40db8e89e5aaf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 16:07:38 2021 +0100

    :bug: [rtl/core/cpu] fixed bug (typo) introduced with last commit

[33mcommit c5abef18e8c402c67206f6ef912a24c10423191f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 16:07:11 2021 +0100

    [CHANGELOG] minor edits

[33mcommit 980df8d0f5f6c658c6597182c466151039dc52b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 15:43:53 2021 +0100

    [rtl/core/cpu] minor edits

[33mcommit 0e36521802eb4396087ad1c1c48d4809190916bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 15:43:19 2021 +0100

    [rtl/top_templates] comment edits

[33mcommit 1deb4efada8bcb3123b6b80afdcb3533275ef720[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 14:37:50 2021 +0100

    [README] minor edits

[33mcommit eb47728a417b36ae83a7f05adeb23d684d91f9b8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 14:37:02 2021 +0100

    [riscv-compliance/README] typo fixes

[33mcommit da65a9cb96405f5d0004f3cf57bf43e10f902071[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 14 09:51:04 2021 +0100

    Update CONTRIBUTING.md
    
    minor edits

[33mcommit 1f0ef5d4d7413c7729ecef66f5651dc35e8420f1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 19:21:01 2021 +0100

    [CHANGELOG] updated to version 1.5.1.4
    
    also targeting #8

[33mcommit 2bc8754a20c9ed2e653532438e83e42173c233bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 18:54:04 2021 +0100

    [docs/NEORV32.pdf] minor updates for version 1.5.14
    
    * mret instruction now requires 5 cycles to complete
    * HW_THREAD_ID generic is now of type natural
    * updated synthesis results

[33mcommit 2825ac68e26f3239a1f39c3c9ce8511f934f6010[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 18:53:01 2021 +0100

    [rtl/top_templates] removed generic initialization using "others" (targeting #8 )

[33mcommit 410a2b483388a392806bb65a52e95a7b784882fa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 18:51:56 2021 +0100

    [sim/testbench] modified HW_TREAD_ID generic
    
    is now of type "natural"

[33mcommit c5b105b4b4b800ed5b68583f462970740c960e4b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 18:51:04 2021 +0100

    [rtl/core] modified configuration generics
    
    `HW_THREAD_ID` generic is now of type `natural`;
    removed generic initialization using "others" in top modules

[33mcommit f02c4396a44f3260883e6f3c3083b156f50ff35c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 18:49:53 2021 +0100

    [README] updated synthesis results

[33mcommit 4220f03f40a7075cc47a769692203f64892391c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Feb 13 18:49:29 2021 +0100

    [rtl/core/control] logic and timing optimizations
    
    shortened critical path; reduced hardware footprint

[33mcommit 97e20dcc887e08e310debbc82f0590ef53a3fb6c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 12 17:51:24 2021 +0100

    [rtl/core] minor edits
    
    added additional four co-processor slots - not used at the moment

[33mcommit 66d2cae03fc7836863f3eda251bb2dd9384961e7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 12 17:49:08 2021 +0100

    [rtl/top_templates/test_setup] minor generic config edits

[33mcommit 8483e1098e102db94ab42f65b6372bda22019c90[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 12 17:48:18 2021 +0100

    [rtl/core] Zifencei extension is diabled by default

[33mcommit 2f5066b3e1aa8eba8564e07e9160b707e6c1456a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 9 18:19:13 2021 +0100

    [sw/lib/source/rte] minor layout edits

[33mcommit 809af1f9804badaf6248bf0c422c01fb43afc084[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 9 16:37:10 2021 +0100

    [CHANGELOG] added version 1.5.1.3
    
    modified CPU architecture:  now using a "pseudo" ALU co-processor to get the result of a CSR read operation into data path, removing one input from register file input mux -> shorter critical path

[33mcommit 075eaf95d2904f94bde41ded55ad4d7d92b324ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 9 16:36:31 2021 +0100

    [docs/NEORV32.pdf] updated CPU data path diagram

[33mcommit 62b456a8c3a4fef7ca94b267b13623d8b7711a87[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Feb 9 16:36:07 2021 +0100

    [rtl/core/cpu]  modified CPU architecture
    
    now using a "pseudo" ALU co-processor to get the result of a CSR read operation into data path, removing one input from register file input mux -> shorter critical path

[33mcommit 4d906e8b65b8c905bee360389ef52b0f81b9a03d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 18:53:05 2021 +0100

    [README] minor edits

[33mcommit 6cee2e11b15eb5ff6fb0902638efbb2a3354d592[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 18:47:45 2021 +0100

    [rtl/core/nco] minor code layout edit

[33mcommit d8247a0db71728059d6b4933a2019f74a978090b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:28:39 2021 +0100

    [CHANGELOG] added version 1.5.1.2
    
    added new processor peripheral/IO device: Numerically-Controlled Oscillator (NCO)

[33mcommit e47780c309f6baa057ca508c380f92e334bb7b4c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:20:55 2021 +0100

    [docs/NEORV32.pdf] added new NCO peripheral
    
    added new section "Numerically-Controlled Oscillator (NCO)"

[33mcommit 1f45fa9fc46da52e71ae2e3a7c933b507d3d7591[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:14:10 2021 +0100

    [rtl/core] updated pre-built application image
    
    made from sw/example/blink_led

[33mcommit 71ec56e62acd7ed7ce0c8dd8abf875fea515ea48[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:12:21 2021 +0100

    [README] added new NCO peripheral
    
    including NCO synthesis results

[33mcommit 0e825295ad4bf2ddb88bb9360e57a0e347891b1c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:10:54 2021 +0100

    [docs/figures] added new NCO peripheral to processor block diagram

[33mcommit 6e725611305505f7e6f3692ea3016319ac7ee971[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:10:03 2021 +0100

    [sw/example] added new NCO example program
    
    added example program for new NCO (numerically-controlled oscillator) peripheral:
    interactive NCO configuration/evaluation prompt via UART

[33mcommit 4b9d54f1dca3eaf8f94b09d113a1d682d2d66ae4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:08:17 2021 +0100

    [sw/lib/rte] added NCO available check to hardware info

[33mcommit 3afc1770347bf7f11ba06203f085b023b63a94db[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:06:54 2021 +0100

    [sw/lib] added NCO hardware driver
    
    nerov32_nco.h/.c driver files

[33mcommit e2d4e1ffa42a5fce2c0714c7e420eb1acae0b09e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:06:17 2021 +0100

    [sw/lib/include/neorv32.h] added new NCO
    
    added new NCO's (numerically-controlled oscillator) register and bit definitions

[33mcommit 6e39343148ddc83f1ecb1a44b52af0d11414b53c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:05:17 2021 +0100

    [sim] added new NCO
    
    added new NCO (numerically-controlled oscillator) to testbench, GHD simulation script and default Xilinx ISIM waveform configuration

[33mcommit 3fc4fbe8a0afd51444a248508c55fe6a230c6130[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:04:18 2021 +0100

    [rtl/top_tempates] added NCO
    
    added NCO enable generic (IO_NCO_EN) and NCO output signal (nco_o); minor comment fixes

[33mcommit 439d9da11b98432d1c5a121e826c160ad83d61eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 17:03:19 2021 +0100

    [rtl/core/processor] added new perpheral module: NCO
    
    numerically-controller oscillator with 3 independent channels, 20-bit phase accu and tuning words, 8 selectable clock sources (prescalers), fixed 50% duty cycle mode or pulsed-mode

[33mcommit d277a8d465ef76044309e5f7a2c998cc1942fbf7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Feb 8 16:53:06 2021 +0100

    [rtl/core/cpu_control] minor edits

[33mcommit e99daf59a7ea12b907f9da9ab21f432a83ded8a2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 7 08:56:05 2021 +0100

    [CHANGLOG] added link to latest release (1.5.1.0)

[33mcommit 70d26615fba16ab97df4b1441d240d5294080de4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 7 08:55:42 2021 +0100

    [sw/lib] added simple README

[33mcommit 0a21b7ca6c0ce0df1d43f5bad7024212141e9594[m[33m ([m[1;33mtag: v1.5.1.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 7 08:36:34 2021 +0100

    version ID fix (for next release)

[33mcommit 7c98c6183b01a69d7474682bc7fe9b2b10cab094[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Feb 7 08:35:58 2021 +0100

    [docs/NEORV32] minor typo / layout fixes

[33mcommit 78aeda49ef5805a128806045c43a92c658af7672[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 19:13:17 2021 +0100

    [rtl/core/processor] corrected pre-built application image
    
    default application image should be made from sw/example/blink_led

[33mcommit eabb6d922e0fc273e91c8b82e24a61bedee3ef33[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 19:11:27 2021 +0100

    minor edits

[33mcommit 8f2a7b230d3f59b81ccc77a6148af3a7ec6fb469[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 19:10:46 2021 +0100

    [CHANGELOG] added version 1.5.0.11

[33mcommit 9d6fa5a875709e81f22cd50d1ebfe7e471dc261d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 19:10:27 2021 +0100

    :bug: [rtl/core/cpu] fixed error in atomic LR.W instruction
    
    error caused a too early release of the locked memory access environment

[33mcommit e25378e9ff9fbc1a0cfffb61b37157cd1b88451a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 17:53:44 2021 +0100

    [docs/NEORV32.pdf] added new section "Processor Interrupts"
    
    minor typo and layout fixes;
    added more inter-section links/references

[33mcommit d807bd94cb0a60b8e0f9f8a6a109454de8750842[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 17:52:44 2021 +0100

    [CHANGELOG] added version 1.5.0.10

[33mcommit 8075d21387ba63e74f485fbb419e2d9ff820ed1f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 17:26:56 2021 +0100

    [sw/bootloader] removed obsolete IRQ config
    
    removed device-specific IRQ configuration from
    - UART setup function
    - SPI setup function

[33mcommit 3ed3fe3e237241bb20008ab2c60cead7f6d4b0aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 17:26:05 2021 +0100

    [rtl/core] updated pre-built application images
    
    application_image = blink_led example program
    bootloader_image = default bootloader

[33mcommit e241127d925d8f82bbe92555a69deadafa4b94b4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:47:28 2021 +0100

    [sw/lib/include/neorv32.h] added new FIRQ channels; removed device-unique IRQ configuration flags

[33mcommit ffcc57b4e752f632ba5e989a73e5cb0e6c6d8632[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:46:20 2021 +0100

    [doc/figures] processor now provides 8 fast interrupt channels
    
    soc_firq_i is now 8 bits wide

[33mcommit 55833332d56c0a0185d862f11eb69ed32acc5854[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:45:32 2021 +0100

    [sw/example] removed now-obsolete device-unique interrupt enable flags
    
    removed interrupt enable flags from SPI, TWI and UART modules / setup functons

[33mcommit ec6ffdc19e80154d8ebac74668627894bad2b612[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:44:31 2021 +0100

    [sw/lib] removed device-specific IRQ enable flags
    
    SPI, TWI, UART
    Interrupts are now globally/centralized enabled/disabled via the CPU's mie CSR

[33mcommit 7d76c37f7402eda75844a15a74becdcb224d5770[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:43:36 2021 +0100

    [sw/lib/source/cpu] added support for new 16 fast interrupt channels

[33mcommit a53f46769160a794d790b5ec6ea59ffdeffd0493[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:43:12 2021 +0100

    [sw/lib{rte] added support for new 16 fast interrupt channels

[33mcommit 48fa2a12f62cb1da45bd386baece935456a20da9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:42:33 2021 +0100

    [sim/testbench] added testbench trigger for all new 8 fast interrupts

[33mcommit 7d07b5d76d39396665cd61c33604fc5d909b3221[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:42:09 2021 +0100

    [rtl/top_templates] tops now support 8 FIRQs

[33mcommit 3ecad4edc07eb727057b4cf6422caf192510167b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:41:31 2021 +0100

    [rtl/core/processor] processor now features 8 user-defined fast interrupts

[33mcommit 6104591085fbb87d22a3af24734d07e97520c120[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:40:34 2021 +0100

    [rtl/core/processor] removed IO device's IRQ enable flags
    
    interrupts are now globally/centrlaized enabled/disabled via the CPU mie CSR

[33mcommit 30ff34eafa4ffb43efd90ea95d5862817224fec5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Feb 5 16:39:16 2021 +0100

    [rtl/core{cpu] CPU now provides 16 fast interrupt requests
    
    with according mie/mip CSR bits and mcause CSR trap codes

[33mcommit 108b62d6e3ed4e0429f2692099fe8919d58ede90[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 22:00:55 2021 +0100

    [README.md] minor edits

[33mcommit 2c7d8d536cacdfd7fe6d9e97ed1074cc8756f483[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:33:31 2021 +0100

    :books: [docs/NEORV32.pdf] removed CFUs, added CFS
    
    removed custom functions units 0 & 1 (CFU0, CFU1)
    added new custom functions subsystem (CFS)
    - updated processor signals & generics
    - removed CFU section
    - added CFS section
    - updated CFU and CFS related configuration flags in SYSINFO module

[33mcommit 42c04b6099934b5e766d3fa867e767e135885d71[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:30:00 2021 +0100

    [README] removed CFUs, added CFS
    
    removed custom function units 0 & 1 (CFU0 & CFU1);
    added new Custom Functions Subsystem (CFS)

[33mcommit e7741403fdc952cba41a352ba84d3ec52fadb8d8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:29:25 2021 +0100

    [CHANGELOG] added version 1.5.0.9

[33mcommit 5fc1e97ec063ae5be41e5fa06676c228a33b6104[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:29:10 2021 +0100

    [docs/figures] added CFS to processor block diagram

[33mcommit 5f5ef3972b84a69dae929e49e4d975dff14fa221[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:28:11 2021 +0100

    [rtl/core] updated pre-built application images
    
    blink_led example program -> application_image
    bootloader -> bootloader_image

[33mcommit 3b72b58538fc04556d672b6a3ba06db28ce15242[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:26:51 2021 +0100

    [riscv-compliance/README.md] minor text edits

[33mcommit 35584537b95f6aba1737e801c3ebe427000186fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:25:56 2021 +0100

    [sw/lib] added CFS hardware driver (stubs)

[33mcommit 3fc306cb7baf6d98c4e988828071d006b44e130c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:25:39 2021 +0100

    [sw/lib] removed CFUs; added CFS

[33mcommit 91c43e23408459f33f3cb937f5a4b8fb39a0d8eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:25:07 2021 +0100

    [sim] removed CFUs, added CFS

[33mcommit 334402e415aa7788ce1dae8e1d8ec89e7e57acd1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:24:45 2021 +0100

    [sw/common/crt0] increased IO area
    
    was: 128 bytes starting at 0xFFFFFF80
    is: 256 bytes starting at 0xFFFFFF00

[33mcommit 9621b78d8f696c8a2098f855455cbf5b557c138c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:23:58 2021 +0100

    [rtl/top_templates] removed CFU0 & CFU1, added CFS
    
    removed custom function units;
    added custom functions subsystem

[33mcommit d906bf634daf68601e2a9e153e00e0f6888feeee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:23:25 2021 +0100

    :sparkles: [rtl/core] added new CFS to processor

[33mcommit 1896cc3c45fb3d95053af608392f69ddca8339a5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:22:31 2021 +0100

    [rtl/core/cpu] added CPU sleep status signal

[33mcommit 7f3b0df2496d5c86838a5692c9d4feee4e10d5e0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:21:42 2021 +0100

    :sparkles: [rtl/core/neorv32_cfs.vhd] added new custom functions subsystem (CFS)
    
    provides up to 32x32-bit of memory-mapped device registers + IRQ + custom IO

[33mcommit d114f909f40c08e5ab159dd626afb3a0201a191e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 29 11:20:02 2021 +0100

    [sw/example/cpu_test] minor edits
    
    changed address of "unreachable memory-mapped register" to make sure this is ALWAYS unreachable

[33mcommit 8dc4b9e75902388f3c5b016f06e74ecef1a275b6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 22:05:01 2021 +0100

    [rtl,sw/lib] removed custom functions units (CFU0 and CFU1)

[33mcommit 6c365475363a8e6d1c96708433bb507ba85de4e2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 17:59:25 2021 +0100

    [rtl/core/cpu] minor edits

[33mcommit 31333eb047e68b3383b767477f9dd42a8be3efb9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 17:55:12 2021 +0100

    [rtl/core/cpu] added "critical limit" for number of implemented PMP regions
    
    When implementing more PMP regions that a certain critical limit an additional register stage is automatically inserted into the CPU’s memory interfaces increasing the latency of instruction fetches and data access by +1 cycle. The critical limit can be adapted for custom use by a constant from the main VHDL package file (rtl/core/neorv32_package.vhd). The default value is 4.

[33mcommit 9f4ef8454c9d56fdfb4b38e21b1278164441a382[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 17:27:38 2021 +0100

    [sim/testbench] increased number of PMP regions 4 -> 8 (for simulation only)

[33mcommit afaac5e8a2c9226fac7e17f0defc8df0af4b5066[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 17:27:00 2021 +0100

    [rtl/core] minor comment fixes

[33mcommit 6bb4132db9991bc222516d759eb67f90194f28c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 17:24:58 2021 +0100

    [sw/example/cpu_test] minor edits
    
    - fixed typo in note (misaligned instruction fetches are not possible WITH C-extension enabled)
    - removed AMOSWAP test

[33mcommit 3bf5c1a827f769d293710f2db25224853c6e131d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 13:38:42 2021 +0100

    [README] layout clean-up

[33mcommit 0338625a24ec384cd56a91ae31c29ed85efd3ce6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 13:28:42 2021 +0100

    [README] fixed links - again

[33mcommit 9b84791066143dc1fb7066c9f168e237c0fd47e3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 13:25:37 2021 +0100

    [README] fixed broken links

[33mcommit 107dbad2ccfa575c50eea7707bc355314ff4fd65[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 13:17:19 2021 +0100

    [doc, README] minor layout edits

[33mcommit 276f1143221ae8c572baaa33fbf46e725b891df3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 10:15:10 2021 +0100

    [CHANGELOG] added minor UART setup function bug fix

[33mcommit d4df6d175e016e72ff1f0e26c54d6183b44bb531[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 10:12:43 2021 +0100

    [rtl/core/processor] updated pre-built application images
    
    bootloader & blink_led
    due to fixed bug in uart configuration function

[33mcommit ae9eece63182ce57af30abed01843a757efdf214[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 10:12:05 2021 +0100

    [docs/NEORV32.pdf] fixed minor bug in baud rate computation equation

[33mcommit b57a85bb6525c6cae20ba28298402eb51c6fa334[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 28 10:11:40 2021 +0100

    :bug: [sw/lib/source/uart] fixed minor error in baud rate computation
    
    bug caused additional offset for higher baud rates

[33mcommit 5e83ee0287e9737e62cfd4839fdf2603bae9078f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:06:36 2021 +0100

    [docs/NEORV32.pdf] added new "platform fast interrupts"

[33mcommit 983217db70f665327f673218bc4fee747bc5087b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:06:00 2021 +0100

    [README] minor edits and updates

[33mcommit b3ac99cd28e22483587ff99ddf7fab96955c39b3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:05:47 2021 +0100

    [sw/example/cpu_test] added FIRQ4:7 test

[33mcommit 1e3faa286883f5d8f81576cd2eb4274649cc8c94[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:05:24 2021 +0100

    [rtl/core/processor] updated pre-built application images
    
    due to modified RTE / software framework

[33mcommit 3509aa2a0f59a8b6c90f86d7c41ec695e500d493[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:04:47 2021 +0100

    [doc/figures/processor] added new platform fast interrupt signals

[33mcommit 76204683e4b56da0fc44fc013bf22f15d6395eb8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:04:20 2021 +0100

    [CHANGELOG] added version 1.5.0.7

[33mcommit 21d79138f8d4dc1f656183c24a2e509c13354f4c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:03:54 2021 +0100

    [sw/lib] added the new four additional fast interrupt signals

[33mcommit 371f6011a12f5660ddd1a9c94764c8f29c8a16f5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:03:27 2021 +0100

    [sim/testbench] reqorked testbench IRQ trigger

[33mcommit 5a00032c7d4f8a144e79d0de032522fc57e714a6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:00:49 2021 +0100

    [rtl/top_templates] added new platform fast interrupt signals

[33mcommit 26ca0fff124db33fc4b56a555a43ce76b8ebbfbb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 27 20:00:15 2021 +0100

    [rtl/core/cpu] added four additional fast interrupt signals
    
    available for custom platform use via soc_firq_i signal

[33mcommit 9885011ade51238f08971f5e505648709a26c452[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 26 20:02:38 2021 +0100

    [README] typo fix

[33mcommit d36630e6eef56110ea66a45cfe404b5307be7356[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 26 20:02:28 2021 +0100

    [CHANGELOG] added version 1.5.0.5

[33mcommit e43c0f9cb7f3df557b0908b1cdd1723493b5e866[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 26 19:39:55 2021 +0100

    [rtl/core/cpu] fixed minor bug in branch comparator
    
    bug introduced with last commit

[33mcommit 8acbb392f7aa3a92e681c164a6e41acae345acdc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 26 18:24:32 2021 +0100

    [rtl/core/cpu] optimized comparator (for branch condition check)

[33mcommit 9118f86e572815fa775856e05d30511b4da89441[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 26 18:23:45 2021 +0100

    [rtl/core/cpu] reworked CPU's co-processor interface

[33mcommit 5c62456596ccdad49b9efbf47f4cc83d21c7bd3c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 26 17:52:33 2021 +0100

    [sw/example/bit_manip] minor output edits

[33mcommit 25ac6fab0f85aa2162be7aad4c1c1eeb4ce9149d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 25 17:37:38 2021 +0100

    [rtl/core/cpu] minor logic optimization of HPM trigger logic

[33mcommit 105bf0d36714a1a852031808a6495852c1516229[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 25 17:37:00 2021 +0100

    [rtl/core/cpu] minor optimization of B extension co-processor

[33mcommit fdb629955f65dac8156c4bf975f031a2d8da4868[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 22:30:33 2021 +0100

    [CHANGELOG] typo fix

[33mcommit e7c2d1d053eea8ee78f93353df6a0aca1b4ac4ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:16:45 2021 +0100

    [rtl/processor/trng] minor comment edits

[33mcommit 303f5865a8fd407a97c0195dde97594562fc7bdf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:13:28 2021 +0100

    [docs/NEORV32.pdf] reworked TRGN section
    
    updated section "True Random Number Generator" due to reworked TRNG hardware

[33mcommit e1c1555e5e26a012f03ad48117325c4c62148037[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:12:31 2021 +0100

    [CHANGELOG] added version 1.5.0.4

[33mcommit 86a8d1f1b70c1119258cd7010e7d4e977531e8dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:12:05 2021 +0100

    [README] minor edits

[33mcommit a9b9f38150e3e3b7031dd3053176d957f7bbb248[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:11:51 2021 +0100

    [sw/example/demo_trng] updated TRNG demo/test program

[33mcommit 7e386d92e08726e0a7c7513d4ce4b790e173f7de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:11:03 2021 +0100

    [sw/lib/trng] updated TRNG driver functions due to reworked hardware

[33mcommit 61d8aed2a72f198368f2733977b0720323a5163c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 23 19:10:25 2021 +0100

    [rtl/processor/trng] reworked TRNG hardware
    
    TRNG is now based on several simple ring-osciallators with increasing chain length

[33mcommit 0af4a973ccef83c7e3e104203f1763d9a931b623[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:52:11 2021 +0100

    [rtl] typo fixes (in comments only)

[33mcommit 6d02469cad3ef5734b8e3b9c871b5b47bb47a51a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:35:14 2021 +0100

    [doc/NEORV32.pdf] reworked section "Watchdog Timer"
    
    due to new/modified watchdog timer hardware

[33mcommit 67cb79601254b00e843d94fb371fb094118e7665[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:34:31 2021 +0100

    updated to version 1.5.0.4
    
    reworked watchdog timer (WDT)

[33mcommit d772da95f786d512a74d873e769b7c14070ed45a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:33:20 2021 +0100

    [sw/example] updated test programs due to reworked WDT hardware

[33mcommit 19befd7799686a1f9319e626ade8d729d9c329c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:32:53 2021 +0100

    [sw/lib] reworked watchdog timer (WDT)
    
    removed watchdog access password, added option to lock configuration until next system reset, changed control register bits; updated driver functions

[33mcommit 8d70487763afd2d7de0ab8a3801afed4939a0a96[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:32:03 2021 +0100

    [rtl] reworked watchdog timer (WDT)
    
    removed watchdog access password, added option to lock configuration until next system reset, changed control register bits

[33mcommit 33e78dfb46582e0d410d5987501e0fd5fe7dc39f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 22 18:24:32 2021 +0100

    :bug: [bootloader] fixed bug in IO module configuration
    
    fixed bug that caused bootloader to immediately crash if SPI or MTIME or GPIO peripherals where not implemented

[33mcommit 3237cd51d7732f2a3b478ea7dd88b611be994f92[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 21 18:03:15 2021 +0100

    [README] minor typo edits

[33mcommit 521a41ebf88220405b716aa967205b3561b06d3d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 21 18:02:41 2021 +0100

    [rtl/top_templates] typo fix in comments

[33mcommit 8bb204c43667d6022f96b885da4f05c2a168ce04[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 20 21:27:07 2021 +0100

    [rtl/processors] added "I" suffix to internal i-cache generics

[33mcommit 55f67b4501a3f49bc95a4de25fab34ae1014f4f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 19 19:49:30 2021 +0100

    [pre-built toolchain] updated to new repository
    
    old "riscv_gcc_prebuilt" repository is deprecated; new one is "riscv-gcc-prebuilt"

[33mcommit c273927b0441691c2a9933b61ef58ae79833b23a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 19 19:37:58 2021 +0100

    Update CHANGELOG.md
    
    minor edits

[33mcommit 28861a05a636ce678ffb5b1b5ead8b5da5823201[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 18 21:29:12 2021 +0100

    [rtl] clean-up

[33mcommit 00035b24ce01d9999bbbd60b3e7e17622f96a437[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 17 16:35:18 2021 +0100

    [docS/NEORV32.pdf] updated synthesis results (memory bits for register file); updated 'B' extension's instruction timing; minor formating and typo fixed

[33mcommit 52d6679625977ac0c60e078f01f8a70a07ed56a5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 17 15:50:53 2021 +0100

    [rtl/cpu.control] minor edits

[33mcommit 0dd82b276dff2c4dbfc26fa72d57f32a651d4b31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 17 15:50:20 2021 +0100

    [CHANGELOG] added version 1.5.0.3

[33mcommit 17c862e7058dee952950116b5e2d6a50041d9b7a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 17 14:47:26 2021 +0100

    [README] updated synthesis results: only half of the original version's memory bits are required now for the register file

[33mcommit 656967a43ea09f0bb8cb2dc056205d0543c786c5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 17 14:44:43 2021 +0100

    [rtl/cpu.bitmanip] logic/arithmetic operations of `B` extension only require 3 cycles now, reduced switching activity when co-processor is idle

[33mcommit 91da706d31a213d85db181ec236eb7000400e2e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 17 14:43:05 2021 +0100

    [rtl/cpu/MULDIV] modified M co-processor (due to register file read access modification), reduced switching activity when co-processor is idle

[33mcommit 946b25dfd1fffc8058fd76cbe401537a00307633[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 16 19:31:15 2021 +0100

    [rtl] minor edits

[33mcommit d41b8a44d475ce92ce0433dbfc00dce49b630631[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 16 19:30:03 2021 +0100

    :bug: [sim/rtl_modules/imem] fixed typo error

[33mcommit be6c5dfac44e4d85647d8712a0bda0cc88b24fea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 18:19:03 2021 +0100

    minor edits

[33mcommit 7dffec46ae80e61a4aabd8e2a4b2e41152aa1cf2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:15:30 2021 +0100

    [README] minor edits

[33mcommit 6ca267da61344d2151fd7c5332e6a7e81e76c45b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:15:16 2021 +0100

    [CHANGELOG] added version 1.5.0.2

[33mcommit d7f829bcfd94b69750b36e33b2e8b75a98b9ab97[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:14:44 2021 +0100

    [docs/NEORV32.pdf] added new i-cache associativity configuration options

[33mcommit f59c8b0711752fc0aa38d823535c11babdefcd14[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:14:10 2021 +0100

    [sw/lib/rte] added replacement policy ouput to hardware config info

[33mcommit f4fe40417d08d06cd5bc8683c75c062cf74dd89b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:13:38 2021 +0100

    [sw\lib] added SYSINFO flags to determine i-cache associativity and replacement policy

[33mcommit 20f8a53c3dce9a42f0ff6ef663cbb587425daad2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:12:36 2021 +0100

    [sim/testbench] added new i-cache associativity configuration generic

[33mcommit 334f80f73fe6419ac6daf8a45cb7c43ed599c048[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:12:16 2021 +0100

    [rtl/top_templates] added new i-cache associativity configuration generic

[33mcommit fca687f065d8a47aa677048e792662f245315a22[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:11:50 2021 +0100

    [rtl/processor] added instruction cache associativity configuration (via `ICACHE_ASSOCIATIVITY` generic); allowed configurations (yet) 1 = direct-mapped, 2 = 2-way set-associative

[33mcommit e789ea34a9870a7e170b24a2d474cab35052508a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:03:17 2021 +0100

    [sw/example/cpu_test] re-added FENCE.I test

[33mcommit 710da70e9a87620c415e47ce3013e1af506e1904[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 15 13:02:26 2021 +0100

    :bug: [sw/lib/source/neorv32_cpu.c] fixed bugs in PMPCFG configuration function

[33mcommit d7b152adcf4f23a60975655d9bff7f3798c32f3d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 20:05:54 2021 +0100

    [README] typo fixes

[33mcommit a8fe7cfd5e2dffc28a0d1ac59bd75af2e305fa68[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 18:46:49 2021 +0100

    [CHANGELOG] added version 1.5.0.1

[33mcommit c1dc66c2591cb769e045a64275970e2e37abc85f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 18:46:31 2021 +0100

    [docs/NEORV32.pdf] renamed instruction cache VHDL source file: `neorv32_cache.vhd` -> `neorv32_icache.vhd`

[33mcommit 85180309343c401290aff6f3ab167dbb0d1d8b20[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 18:44:06 2021 +0100

    [rtl] remaned `neorv32_cache.vhd` -> `neorv32_icache.vhd`

[33mcommit b1e2ed84a4e1c7220348dec369d0013ec3328cac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 18:36:21 2021 +0100

    [sw/example/cpu_test,coremark] added HPM counter for 'multi-cycle ALU operation wait cycle'

[33mcommit 78c60e9caaedd0368442b99e345d5bdc79b82153[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 18:35:40 2021 +0100

    added new HPM event trigger: multi-cycle ALU operation wait cycle

[33mcommit fbb31e008a3e680a08f605749dbdd365e853e624[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 18:34:42 2021 +0100

    [rtl/cpu] added new HPM event trigger: multi-cycle ALU operation wait cycle

[33mcommit 9ca3e84fe2f558c3c8597a4ea3fc924d4b96a776[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 17:32:24 2021 +0100

    [README] minor edits

[33mcommit a72b23f7870c32309863a92b626a475aaeed752e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 17:27:01 2021 +0100

    [sw/lib] typo fixes

[33mcommit bb82f9c9c50156bb825d3840e93c035e6c417ebd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 17:26:20 2021 +0100

    [README] added SiFive toolchain (can also be used for the NEORV32), require 'imac' extensions

[33mcommit 7d8f8bb8980c15097ef8a9a3ea99c5a3aee1664f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jan 14 17:24:15 2021 +0100

    [docs/NEORV32.pdf] added SiFive toolchain (can also be used for the NEORV32)

[33mcommit d8b9a59f938236945adeb2ac98698c6e608d8638[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 13 21:07:29 2021 +0100

    [rtl/cpu/bitmanip] added register to comparator input (improving critical path)

[33mcommit 6b6091fb11e5aa9f24b56948bb66c24418bc34ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 12 17:08:01 2021 +0100

    [CHANGELOG] added latest release

[33mcommit 085fa5391ad11a155a45b0e5106a8ddae89dc539[m[33m ([m[1;33mtag: v1.5.0.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 11 16:27:54 2021 +0100

    [docs/NEORV32.pdf] added version number of supported bit manipulation spec

[33mcommit 3ee3c69a990c6c282c65ed71c6e037c7c3cec1f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 11 16:27:26 2021 +0100

    [rtl/processor] updated pre-built application images (blink_led example & bootloader)

[33mcommit 21a289e28f8ee24cb854b3b2ce70003969077be3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 11 16:26:53 2021 +0100

    [sw/cpu_test] minor text edits - to shrink executable size to 16kB ;)

[33mcommit a4bfb74ee9e3f4a50e4582928c437455c4e97368[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jan 11 15:45:45 2021 +0100

    [documentation] added current version of the bit manipulation specs supported by the NEORV32 CPU

[33mcommit 3b78dc4a5a43c926a1f3e0f26cfe1e250043e4f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 18:45:41 2021 +0100

    [CHANGELOG] added version 1.5.0.0 :sparkles:

[33mcommit 91e5914419b50700464a2656b57400f3c13017f5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 18:45:20 2021 +0100

    [README] minor edits

[33mcommit 808ae3d2854871f3ecb868248c0936a766c0eea9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 18:39:34 2021 +0100

    renamed configuration generics: '*_USE' -> '*_EN'

[33mcommit e94f7a5275a4fe47e9c0fafbbb9cba4a5c53a459[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:40:08 2021 +0100

    [github.actions] minor edits

[33mcommit 0943ecf2c2dcf75c5ed45bc31245b1daf7d2b0dd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:32:36 2021 +0100

    typo fixes

[33mcommit 352e7c5711be87ff9f0fefdfe68b6f922ac2fc7c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:24:17 2021 +0100

    typo/broken link fixes

[33mcommit 5d193abd838b30f62d46087c0e9e44c782e29583[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:17:42 2021 +0100

    [sw\bit_manipulation] minor edits

[33mcommit e0af067abb36cdd43b057743f71b5ebcd581ab8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:14:17 2021 +0100

    [sw/bit_manipulation] added bit manipulation test program (including pure-software reference implementation off all Zbb instructions; including 'intrinsic' library to use B extension's instruction in C-language code)

[33mcommit 7f7806b7898dc3c6e82507fc99ed0a96a334dd39[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:12:45 2021 +0100

    [README] added B extension

[33mcommit 2a48976523c74576f01b83057fadab27b99f8bdd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:12:16 2021 +0100

    [CHANGELOG] added v1.4.9.10

[33mcommit ffbf06fdf62eb130db71695d5e22ee0c54d18365[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:09:37 2021 +0100

    [docs/NEORV32.pdf] added B extension (generics, instructions, instruction timing, related CSR flags, synthesis results)

[33mcommit 3d5ccad5148640f02011b0e40a7e9b6630dfcf10[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:02:19 2021 +0100

    [docs/figures] added 'B' extension to processor diagram

[33mcommit b4e847c5656932caa90671c99511dd7d70285910[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:01:23 2021 +0100

    [sim/ghdl] added B extension's co-processor to GHDL simulation script

[33mcommit a503e8257b7eb344011ee18266c0685526964cff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:00:49 2021 +0100

    [sim] added generic to enable B extension to test bench; enabled for simulation

[33mcommit a9ce9e70bc2ea8db6ad85dd06c27e9e3bb7618a8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 16:00:09 2021 +0100

    [rtl/top_templates] added generic to enable B extension

[33mcommit e3fb16eeb539e56dae5821e9d216ffab56ef1391[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 15:59:45 2021 +0100

    [rtl/processor] added generic to enable B extensions (default = false)

[33mcommit d49d20a330b0fe91f800439acf52056be5d4b495[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 15:57:24 2021 +0100

    [rtl/cpu] added support for RISC-V bit manipulation ('B') extension - Zbb base subset only (version 0.94-draft, not ratified yet)

[33mcommit 321970d63d07b2a3b3dff8b80368f75031cda6eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 10 15:55:55 2021 +0100

    [rtl/cpu] minor edits

[33mcommit 127b5b724ab11361b8fc67337cb563e996c4da92[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 9 19:46:29 2021 +0100

    [sw/example/cpu_test] minor fixes to reduce size ;)

[33mcommit ec05fa71579ac09c41bc552b1ac7d2ba31e9c5c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 9 19:38:50 2021 +0100

    [sw/lib] minor updates for upcoming B extension

[33mcommit 250e737957712f01096a6282de5a11b8b7980d70[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 9 19:38:23 2021 +0100

    [sw/lib] added sanity check function to compare compiler flags (cpu extensions) with actually available HW extensions

[33mcommit 37695e804189fef459b16e85be26524a5ed6ede7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 9 19:34:58 2021 +0100

    [sw/examples] added sanity checks to compare compiler flags (cpu extensions) with actually available HW extensions

[33mcommit 60b55c8e7d1874fe7a1c89c604900d1eb06e5266[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 9 19:33:08 2021 +0100

    [sw/examples] added sanity checks to compare compiler flags (cpu extensions) with actually available HW extensions

[33mcommit 76fc1b5eb8d97a9de58bd47a79dd7cf6637155bc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 6 18:13:18 2021 +0100

    Update processor-check.yml
    
    made trigger folder-specific

[33mcommit 1d2467f25ebc456d1f5a2b22c49b24c4702f3e32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 6 18:09:40 2021 +0100

    [README] clean-up

[33mcommit f6ed65f4b9f72bb0635eb9ca4dede5d55636200e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 6 18:08:38 2021 +0100

    [github.actions] added additional triggers

[33mcommit 6acd900b5b023e1ee5fa0223a2a4c5c7c1cd5a78[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 6 11:05:29 2021 +0100

    [docs/NEORV32.pdf] minor edits (removed travis ci)

[33mcommit acb7faf487b867d4be2bfc2cc6738b9473230eb5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jan 6 11:05:08 2021 +0100

    [github/workflows] minor edits

[33mcommit 151e45b9fe610154e1d65d16d45ca4f3b6cdbbd5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 19:48:13 2021 +0100

    [github.actions] removed test workflow

[33mcommit 33c391e08bb830dbf6ac91051921215a8473dae9[m
Merge: 52ef1be0 9e20f9f3
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 19:47:39 2021 +0100

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 9e20f9f33154b6d30c4782ae86c3305d0ec266a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 19:37:14 2021 +0100

    Update test.yml

[33mcommit 740b6a5d5142cb75d8b52bd817329e62952c566e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 19:35:26 2021 +0100

    Update test.yml

[33mcommit 930372c99c7e00eb4a5942b4b06456cedc82a668[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 19:34:37 2021 +0100

    Update test.yml

[33mcommit b0c06fc4e285524f181ec96891535ed90160c33c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 19:33:59 2021 +0100

    test

[33mcommit 52ef1be06e360c1ba925735c3ac5a56c3934ce72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:30:35 2021 +0100

    [github.actions] removed automatic trigger

[33mcommit 76ad866706b533e3a1402e7f3beb30379c705cf7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:29:17 2021 +0100

    [Readme] deprecated Travis CI; now using GitHub Actions

[33mcommit 2802e923e6ca546896f967676ecbcf57c933d7b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:27:26 2021 +0100

    [ci] minor edits

[33mcommit 61ebc934535e20b3335accd0d882e4018fd4db76[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:09:45 2021 +0100

    clean-up

[33mcommit 9287381f2620f8fc46596ed13a43fae439c9173f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:07:53 2021 +0100

    final

[33mcommit dcdce77f7c3ab0b4313a9251fd9dfd245225e7c8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:06:56 2021 +0100

    Update gh-pages.yml

[33mcommit 7108b6a32766f0e17cc74d77f88bbdb2ba50e2c8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:05:13 2021 +0100

    Update gh-pages.yml

[33mcommit 743c4055e50e79c74160bf7d11148accb0225b3e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 18:01:54 2021 +0100

    Update gh-pages.yml

[33mcommit 245adfa95affc8378cfd7c835ee76e649dd8e02e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:58:17 2021 +0100

    Update gh-pages.yml

[33mcommit 2ca577b602dd1dbfc34e54a0637f8986d65f52a4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:53:39 2021 +0100

    Update gh-pages.yml

[33mcommit 047d09ef639384b6a92ee551baa1be601e602f36[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:52:03 2021 +0100

    Update gh-pages.yml

[33mcommit 6d70638faf25d67a6640dfea17915ef0eb5e89a8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:50:35 2021 +0100

    Update gh-pages.yml

[33mcommit 5d089ab6abbb1ef26e511cfaaf12192873cd1db4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:49:14 2021 +0100

    Update gh-pages.yml

[33mcommit b7df45c13bccd66a27c5c88ac89fdba221338e4c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:46:21 2021 +0100

    ...

[33mcommit 24173f0b1df4235030131ccc8616e31520fd78d9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:45:01 2021 +0100

    minor edits

[33mcommit a3f612ebe2bc169c1bbb89d95fec7ffa0dfdceb1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:42:23 2021 +0100

    Update gh-pages.yml

[33mcommit c30998e41bc4a746c0c59d6f82c7fab27fcd7d09[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:41:46 2021 +0100

    Update gh-pages.yml

[33mcommit f1cd2bfd9aabd3d886067c4ce94ca613f2e490f9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:38:57 2021 +0100

    Update gh-pages.yml

[33mcommit 85c60b7d8da135da684e7c6e53bf572601ab9b3f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:36:18 2021 +0100

    Update gh-pages.yml

[33mcommit 6ae3f3679b8db1821d828df4a02a2ac97bea559e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:34:55 2021 +0100

    Update gh-pages.yml

[33mcommit 99e1db3589b8e8d78729bcc53909f6267fff9d2e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:32:38 2021 +0100

    Update gh-pages.yml

[33mcommit 4c37e4b98e4e2c45cfa9c58b3ffdf773465492d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:31:08 2021 +0100

    Update gh-pages.yml

[33mcommit c54b2df9b2fe7b3bd40a081437653ea7d4edfc92[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:27:47 2021 +0100

    Create gh-pages.yml

[33mcommit 7f59b265a7e6282900ed03f16e717a26be2bafd6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:21:46 2021 +0100

    Delete doxygen-gh-pages.yml

[33mcommit 6e37fd0f389f665c97c15debde3498577ecb83de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:13:58 2021 +0100

    Update doxygen-gh-pages.yml

[33mcommit edb9a94c00700202d77645688f5ae755530e184c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:10:53 2021 +0100

    Update doxygen-gh-pages.yml

[33mcommit fb8930afea8db441d54eb411ab53736f8a32b225[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:08:32 2021 +0100

    Update doxygen-gh-pages.yml

[33mcommit 09a1740389bee4c60b254ab88f3998828eeb12be[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:07:32 2021 +0100

    [docs] renamed doygen makefile

[33mcommit 0a9124536f4e4d87eba6372d3a62433544ef873e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 17:01:58 2021 +0100

    Update doxygen-gh-pages.yml

[33mcommit 289e238ddf540632c6a3aad72da44ee938ea5bc4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:59:26 2021 +0100

    Create doxygen-gh-pages.yml

[33mcommit 51af864d6160b359eaa82c499490fca8ce15b982[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:45:35 2021 +0100

    [riscv-compliance] minor edits

[33mcommit 0f09e49110138413f1e634d77aa6bf8a7186fba5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:35:58 2021 +0100

    Added processor check workflow

[33mcommit ad5858e43a81dcd2dc1ddb67d722aa1caef2884e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:32:29 2021 +0100

    Added RISC-V Compliance Test workflow

[33mcommit e01d6d32ef54e2f75cce06bb50848d46cff8bf97[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:24:57 2021 +0100

    Delete riscv-compliance.yml

[33mcommit c6409d8e813cdeeea44fb98b0be22fc40e2e682f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:24:50 2021 +0100

    Delete ci-check.yml

[33mcommit dfe45b83e5d6855e540a29898a63015787127be2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:22:12 2021 +0100

    [workflow] re-init

[33mcommit 55355e0ca5792eac3c1cf13e6de89d37819a78a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:21:22 2021 +0100

    [workflows] clean-up

[33mcommit 55019e7ef8dedebf3b6ad04064c345e5a8619ada[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:05:04 2021 +0100

    Update sw_hw_check.yml

[33mcommit 55dd7eaf77a7540f0769f764623f71ef2fd7cf7d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:02:18 2021 +0100

    [sw/makefiles] changed native gcc to g++

[33mcommit b6559d6e3d80441d0c8ad3649104e29586ad9773[m
Merge: dffa2bdf 23df50c3
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 16:01:50 2021 +0100

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 23df50c3ccbd04efb797aa92dce8a98f280343a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 15:55:11 2021 +0100

    Update sw_hw_check.yml

[33mcommit 41e368746ea525e1e7e27303b0d4c1db64c5328b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 15:52:50 2021 +0100

    Update sw_hw_check.yml

[33mcommit 59a27572354b4ab5b78652d01294a57e4e9f3e23[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 15:16:18 2021 +0100

    Update sw_hw_check.yml

[33mcommit a3d52311e85741a4097197b6aae1b80a5568fdb9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 15:13:53 2021 +0100

    Update sw_hw_check.yml

[33mcommit 40c14cb22706af4ee394a41518381e5cd54cebaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 15:10:47 2021 +0100

    Update sw_hw_check.yml

[33mcommit 69c1ff03c61dd03b267f28fdbd704d1a78c569b6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 15:07:38 2021 +0100

    Update sw_hw_check.yml

[33mcommit 0c2c472b5831e53a5215aab0c4c2da4d295079ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:54:18 2021 +0100

    Update sw_hw_check.yml

[33mcommit 1ee3b084f0833b85e028e6cece43166d65278aa9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:46:59 2021 +0100

    Update sw_hw_check.yml

[33mcommit 713e3f8cdd12e87e43fbd510050b201214e7e71a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:44:18 2021 +0100

    Update sw_hw_check.yml

[33mcommit e76f600567a9448225ab82e9f71622aa33e895d1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:37:39 2021 +0100

    Update sw_hw_check.yml

[33mcommit 57029e360622a0044463284a5d8cb48879b2fcc5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:33:11 2021 +0100

    Update sw_hw_check.yml

[33mcommit a0cd322d47d4118e9abce24b459119393c4dac48[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:24:20 2021 +0100

    Update sw_hw_check.yml

[33mcommit 27ba20a3cb9b6b6ebdf0bb6f2b949e5dd4659b5f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:15:17 2021 +0100

    Update sw_hw_check.yml

[33mcommit 118c64db1d13391986c63e2ea9a10900b955e226[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:12:53 2021 +0100

    Update sw_hw_check.yml

[33mcommit 4472cfa1f2e3ed67181e08571960c41d5869a50b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:10:01 2021 +0100

    Update sw_hw_check.yml

[33mcommit 2ceee73b604af8a6b892b0f2f9f5dd4e67986b4b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:06:39 2021 +0100

    Update sw_hw_check.yml

[33mcommit 7f6d58a98a55232991c8a0c39b6e60f93945712e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 14:01:52 2021 +0100

    Update sw_hw_check.yml

[33mcommit f63d3441ceef94b9064c863e05d21a427ca9d9d4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:45:30 2021 +0100

    Update sw_hw_check.yml

[33mcommit ae6af0fdf7250693dff8ac9625d03dfb01f76023[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:38:17 2021 +0100

    Update sw_hw_check.yml

[33mcommit ba42237a2b77545ab357b7f448839184d50bf5a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:31:21 2021 +0100

    added new hw&sw workflow

[33mcommit 13c527bf4955c7fb58e1462a852bffc29fc7bfb4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:21:56 2021 +0100

    bug fixes..

[33mcommit e3076251e1526acc0326009c2e5d989737ecf5a1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:17:11 2021 +0100

    [github-actions/risc-v compliance] bug fixes..

[33mcommit 931726d1e4c07c71e8377c179533c7dc3f5bf981[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:07:09 2021 +0100

    [github-actions] added risc-v compliance script

[33mcommit 52c2dc8d0b24399776854887518323b3b4a4595b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:04:26 2021 +0100

    added test workflow 2

[33mcommit 8e19b5592aa73f02631d56b3d83883abacdcb3d5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jan 5 13:00:57 2021 +0100

    [github-actions] added test workflow

[33mcommit dffa2bdfeacbe896a9bc5aaafd0a87b226968777[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 13:06:05 2021 +0100

    [sw/lib/rte] minor output format edits

[33mcommit c901093f57697a15f169d762ef54db172ce536d4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:54:16 2021 +0100

    [CHANGELOG] added version 1.4.9.8

[33mcommit 4a647a41d4a30fb55da0ad6c160be508e3b40eb5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:53:41 2021 +0100

    [docs/NEORV32.pdf] updated coremark results; added new HPM event trigger

[33mcommit 0c0b7d7d4ae369fdd8c666abf951c43d9160dc83[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:53:14 2021 +0100

    [README] updated CoreMark results; fixed typos

[33mcommit 6dc213c8f24ce443384373eab164820152528b5b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:50:52 2021 +0100

    [rtl/processor] removed wait states; less load/store bus access wait cycles -> faster execution

[33mcommit 09eb721b6506c4f7e2f58fdad5c7b66e14e0980f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:50:04 2021 +0100

    [sw/example/cpu_test] added new HPM trigger: instruction issue wait cycle

[33mcommit dfa47fa62b2df61c1f5e72fdbe7ec3911e763fb6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:49:20 2021 +0100

    [sw/example/coremark] fixed timer overflow problems

[33mcommit 3b435a136962f6c55613a8ccbef79d3cedc6b70b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jan 3 12:48:25 2021 +0100

    [rtl/cpu] added new HPM trigger for instruction issue wait cycle (caused by pipeline flush)

[33mcommit b5a80cfde4d54c8950a4d4b43360192e92220f9f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:24:30 2021 +0100

    :sparkles: [sw\example\coremark] added performance monitors for detailled profiling (to show number of loads, stores, brnaches, traps, memory wait cycles, ...)

[33mcommit 75479185ac5fa40f3dfd93e57e85aa19e777ec5a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:20:33 2021 +0100

    [docs/datasheet] CSR section clean-up; CPU generics section clean-up; added hardware performance monitors; updated PMP features

[33mcommit 65963645804c9c7f6d1154c2620c4ce2d9d9c6a0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:19:54 2021 +0100

    [changelog] added v1.4.9.7

[33mcommit a44024a7c0bf23924f16f71f5da0bb266b449076[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:18:57 2021 +0100

    [README] added hardware performance monitor (HPM); updated PMP features

[33mcommit 50137e171a609d148b4ea7d8dd912afcf45b66a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:18:10 2021 +0100

    [docs/figures] added hardware performance monitors option to processor diagram

[33mcommit 31b7b929b9724e56db047e3704948169a95acdb8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:17:07 2021 +0100

    [sw\example\cpu_test] clean-up; added HPM tests

[33mcommit 50a777cd9b4d1bf6dc79b84ec1d077db55e459dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 20:16:36 2021 +0100

    [sw\lib\rte] typo fixes

[33mcommit cc94c761e6c3f108b73bb4e84f53b3bbe39527a2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:59:07 2021 +0100

    [sw\lib\rte] clean-up

[33mcommit a6cc720df872db0e8960d4e8a6ac6ec5754db314[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:58:32 2021 +0100

    [sw\lib] added lkow-level functions for new PMP and HPM features

[33mcommit e4a9dac68b514b2bd4dcf9b6cd8d8523f6a6d24b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:57:25 2021 +0100

    [sim] added new PMP and HPM configuration options

[33mcommit 7514018644e6f923d2dfc5f56d58ba2bee61c0ef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:56:53 2021 +0100

    [rtl/top_templates] added new generics for PMP and HPM configuration

[33mcommit 1a391a4cc08f5fce9faf01b3f9ce6b6a39ce126c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:56:29 2021 +0100

    [rtl/top_templates/README] typo fixes

[33mcommit a7d02e5ba0c3b3d753ad22c1a6d0953c43e25009[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:55:45 2021 +0100

    [rtl/cpu] added support for up to 64 PMP regions (configurable via 'NUM_PMP_REGIONS' generic)

[33mcommit 8f86adfdee490b69ec3eb63a2e1351c5ae83fa9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 19:55:07 2021 +0100

    [rtl\cpu] added RISC-V hyrdware performance monitors (HPM); number of HPMs configurable via new top generic 'HPM_NUM_CNTS'

[33mcommit 4ba8bc76e06b6ecaceb18df689d44de3b6a552c6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jan 2 14:26:10 2021 +0100

    [sim/ghdl] removed resolved CPU wrapper

[33mcommit 80feb648caa33669223789a635d0594241761983[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jan 1 19:19:02 2021 +0100

    [rtl/top_templates] removed (legacy) resolved CPU wrapper

[33mcommit 078a115fbf1646005c904f485cd8f419dd8f8266[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 20:00:19 2020 +0100

    [sw/freeRTOS] fixed typo

[33mcommit 2df5cbc2af4320f88186057d91987030152417f1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:49:00 2020 +0100

    [docs/datasheet] added new functions/register flags to UART

[33mcommit fc4da6360a68dbbbfcec12a0b6adf8e5437f2745[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:47:51 2020 +0100

    [changelog] added version 1.4.9.5 (new UART features)

[33mcommit 0098316a2161d2bd7c299d6cc397195720a0bfa2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:46:46 2020 +0100

    [rtl] updated pre-built executable images (due to new UART configuration)

[33mcommit 81e7bbad53c0b93b966241c7b28607a992feb2e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:45:51 2020 +0100

    [sw] updated UART init function call (added no-parity configuration; all examples + bootloader still use 8N1 uart configuration)

[33mcommit cf2aac9452553f6aa71f76f3a391182dabde9413[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:44:49 2020 +0100

    [sw/lib] added support for UART's new frame check and parity configuration/check features

[33mcommit 2dd47d7fac3cf46a2f3b29ca914fd95de566e20f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:44:15 2020 +0100

    [rtl/cpu] minor edits

[33mcommit 2720c3d437dd9e434ea2c04662b2218f76b299fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 29 19:43:47 2020 +0100

    [rtl/processor/uart] added frame check logic and optional parity bit configuration/checks

[33mcommit d32eb827dd034cd187476fae503aeec2026b8d5d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 27 18:24:43 2020 +0100

    [README] minor typo fixes

[33mcommit b79c5f36fff8e1da917a1ff449338400062a930b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 27 18:24:27 2020 +0100

    [riscv-compliance] now using simulation-optimized DMEM for compliance tests

[33mcommit 5527c6d601c896421002c8425af24e5dfa900775[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 27 18:23:50 2020 +0100

    [sim/ghdl] minor edits

[33mcommit 892281f7e9b88136f1592acacbc01f65ab19884d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 27 18:23:29 2020 +0100

    [sim/rtl_modules] added simulation-only (simulation-optimized) DMEM version

[33mcommit 3295fbdd0df5b3a28ebb2b6fc5879dde539f49ab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 16:38:09 2020 +0100

    :warning: renamed (C-alias) CSR bit names: CPU_* -> CSR_*

[33mcommit 1478ba8c3fb4cf7bb1bb4ee7a114ac5825aa91ae[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 10:46:09 2020 +0100

    [sim/vivado] clean-up of ISIM wave config

[33mcommit 98ef5e86f8d29cb38a511a89a0a0d7c39f7fbf26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 10:45:35 2020 +0100

    [changelog] added version 1.4.9.4

[33mcommit d7d8e3fdac685f5a8a87ddd951eca2f1402aa924[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 10:45:03 2020 +0100

    [neorv32 data sheet] added 'mcounteren' and 'mcountinhibit' CSRs; added processor-internal bus architecture diagram

[33mcommit f2942cfc0609acf97de60c27421da5a49780b46d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 10:44:24 2020 +0100

    [docs/figures] added processor-internal bus diagram

[33mcommit 6c3669ec9e0e03156f08881393fffac4f5b91c15[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 10:43:45 2020 +0100

    [rtl/processor] updated pre-built application images (bootloader and blink_led demo)

[33mcommit abb62d91d065db98df42f5975268b524ab1d5ea0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:57:01 2020 +0100

    [README] added new 'mcountinhibit' and 'mcounteren' CSRs

[33mcommit 5a928ec916e44510ba019a889e258711fd34d16d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:56:23 2020 +0100

    [sw/example/cpu_test] added tests for new 'mcountinhibit' and 'mcounteren' CSRs

[33mcommit c26b7ba8c7325e0d977391b47ac5cd6d1031d396[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:55:36 2020 +0100

    [sw/common/crt0] start-up codes enables auto-increment of cycle/instret counters (via mcounterinhibit CSR) and allows user-level code access to time/cycle/instret counters (via mcounteren CSR) before launching main application

[33mcommit 738a569e78432161204fbe0ce72ceea11592e74f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:54:08 2020 +0100

    [sw/lib] added mcounterer and mcountinhibit CSRs

[33mcommit 161149eb565a3a4ab07669ea56598659cf92ca56[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:53:28 2020 +0100

    [rtl/cpu] added added missing `mcounteren` CSR (to allow read-access from user-level code to `cycle[h]` / `time[h]` / `[m]instret[h]` CSRs); available bits: 0: `CY`, 1: `TM`, 2: `IR`; added missing `mcountinhibit` CSR (to disable auto-increment of `[m]cycle[h]` / `[m]instret[h]` CSRs); available bits: 0: `CY`, 2: `IR`

[33mcommit 32d970ae9504e96b989c29653729b82d7934882c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:52:10 2020 +0100

    [rtl/cpu] removed 'zicnt_en' option (standard RISC-V performance counter cannot be diabled any more)

[33mcommit a503c6bfb30d9dd9ea460a816cb546eed0011fd0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 26 09:51:01 2020 +0100

    [sw/lib] removed 'zicnt_en' option (standard RISC-V performance counter cannot be diabled any more)

[33mcommit 25aba844d63a6f42191386b34a40b169e01f3d0f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 17:35:01 2020 +0100

    added missing 'mstatus.ube' flag (indicates endianness for user-level load/stores, always set -> big-endian, flag is read-only

[33mcommit f2d53d075cece58f57b8e62560685b5321e2311d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 17:28:45 2020 +0100

    [rtl/core] minor edits and code clean-ups

[33mcommit 3f0455097a5aab19b1d92a58672fb793f872f861[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 12:21:10 2020 +0100

    [sw/lib/rte] marked 'zicnt' extension (since it is not a *real* RISC-V *extension*)

[33mcommit 88d9fee578fba0064bbe57b87f2ab9e50534ac4d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 12:19:26 2020 +0100

    [sw/example/cpu_test] fixed bus time-out latency estimation

[33mcommit 327ee829d2dd339dc97ba6b69d885b6a19d8988b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 08:35:51 2020 +0100

    [rtl] cache configuration now allows having just a single cache block/line/page

[33mcommit 8aa79418b4b74634453370d054be8e71f48a42b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 08:32:50 2020 +0100

    [riscv-compliance] using default (minimal!) simulation time for compliance tests

[33mcommit cb3bf516f956a0280e51a33a9f1953d0118e4e78[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 25 08:32:20 2020 +0100

    [riscv-compliance] added variable to define simulation time

[33mcommit 8ef3533d6837f377f9a27fec4100e3e05c762fb5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 19:16:54 2020 +0100

    [README] typo fix

[33mcommit 3a294d705c60a2443a3f631e003c9054791ab035[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 19:16:37 2020 +0100

    [sim/testbench] modified cache block size (to allow external memory test to succeed when accessing 64byte dummy external memory)

[33mcommit d8e50f6b1f542c4a2e4b20e7cc8b345daa116c31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 19:00:22 2020 +0100

    [sw/lib/rte] minor edits

[33mcommit b2c03e27f57fc30084873005bb61e21da19ca2aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 19:00:06 2020 +0100

    [sw/example/cpu_test] minor edits

[33mcommit c4ccf7865599092931ff7e40bc6d416f9925b80c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 18:55:24 2020 +0100

    [sw/example/cpu_test] minor edits

[33mcommit 005b496916f61a913a1da356431bd0ce0e26e12a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 18:55:02 2020 +0100

    [sw/lib/rte] minor edits

[33mcommit f5ef941bc9479d1cebff40253cbf65524e28820f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 18:49:36 2020 +0100

    [riscv-compliance] increased simulation time for I tests (to have enough time to even simulate setups with a not-so-optimal i-cache configuration)

[33mcommit 46db3a55ecab2817aefd40dae4c717cb0caf1767[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 18:22:44 2020 +0100

    [readme] minor updates

[33mcommit a6e52cc07d5a1f82f3d48682559adf2077c86542[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 18:22:26 2020 +0100

    [changelog] added version 1.4.9.2

[33mcommit 1bf99c988e1f9ffe558010beccb92b9c9fa06fe6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 18:18:07 2020 +0100

    :books: [doc/NEORV32.pdf] added section 'Processor-Internal Instruction Cache'

[33mcommit 9702b15535b7d81e7049238da50df46703ff000b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:52:09 2020 +0100

    [docs/figures] added i-cache to processor block diagram

[33mcommit 4eabcb78d6e646a761e649b9f19891450988637d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:45:47 2020 +0100

    [sw/lib/rte] added cache configuration/layout info to hardware config listing

[33mcommit 9b6ed71d243426f6a6ea947ba841fba96a6d36d1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:44:30 2020 +0100

    [sim] added i-cache to simulation framework

[33mcommit d054ef3964a01c8f420bb0c104cca59df727740e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:40:26 2020 +0100

    [rtl/cpu, sw/lib] added CACHE_INFO read-only register to SYSINFO to determine i-cache configuration by software during runtime

[33mcommit b62f84682dd6a94a288ef0d3b6bb5f95e95ac8df[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:39:28 2020 +0100

    [rtl/top_templates] added i-cache configuration generics

[33mcommit 74b2b35af41f8049d578146feb44673fa8f5f57b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:38:55 2020 +0100

    :sparkles: [rtl/processor] added processor-internal instruction cache

[33mcommit d12ba5581c2ae96ff8e825ec54f5c80fe7ed19b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 23 17:32:04 2020 +0100

    [rtl/cpu] exposed bus timeout configuration as 'BUS_TIMEOUT' CPU configuration generic

[33mcommit 401cfe2022f91131d664fc5e79c2dbfcb3742b84[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 22 14:37:29 2020 +0100

    :bug: [rtl/cpu] fixed bug in instruction fetch misalignment/bus_error ack logic

[33mcommit 9a96ee49b2ae66a15449d2367fda431dd3bc5438[m[33m ([m[1;33mtag: v1.4.9.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 20 07:16:54 2020 +0100

    [CHANGELOG] minor edits

[33mcommit 173005c39bc873479120183494c6cff760ee0c3a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 20 07:14:40 2020 +0100

    [READMEs] fixed broken links; minor typo fixes

[33mcommit 077e280bd1af5456c344ede721118a4883aeaff9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 20:20:14 2020 +0100

    minor edits

[33mcommit 92a69581bf758b7f92323351db86b27e305d4fa0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 18:36:13 2020 +0100

    [riscv-compliance/neorv32-port] increased simulation runtime to deal with nem M-tests (riscv-compliance v2.1)

[33mcommit 77d9a727067f052e91529a8161b55e599401f64f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 17:30:09 2020 +0100

    :bulb: [docs/NEORV32.pdf] highly reworked and updated data sheet

[33mcommit 599a05c0909111826ed84a6e05c667bfeed1b02b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 17:16:34 2020 +0100

    minor updates

[33mcommit abd1122c53b8925d5cfdd63f7da1b7e3d8603e41[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 17:05:29 2020 +0100

    [riscv-compliance] minor fixes

[33mcommit de95ef201924467c1949e2a7fab123d9ac36f87f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 17:04:59 2020 +0100

    [rtl] updated pre-built appication images (bootloader & blink_led example)

[33mcommit a9f7d55cf9a17fcd2a9fd4958f25382b004d58ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 17:00:23 2020 +0100

    minor edits

[33mcommit 67cc9896e5f626392094542dd8e50eb3345321e4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 16:31:43 2020 +0100

    :bulb: added 'RISC-V Compliance Test Framework' to repository

[33mcommit 635e652ab6cf5e9341e855fdf76a252ca706fa02[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 16:09:39 2020 +0100

    [README] updated FPGA implementation results

[33mcommit 696d24f39f754d6027d2eb9d10823f13b2acddcb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 16:08:56 2020 +0100

    :lipstick: [docs/figures] minor logo edits

[33mcommit f4ef49e3e56589598376d317278e0e7c34ea2a9a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 16:05:02 2020 +0100

    [sw/example/cpu_test] removed CFU tests; added MEI and MSI tests (via testbench triggers)

[33mcommit bcc10fc616410e6c414c516d26e42a67faacd635[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 19 16:03:28 2020 +0100

    [sim/testbench] added memory-mapped registers to trigger core's machine software & external interrupts

[33mcommit 3d4bc20d3bd26ba4c81198b2fcb511c4d41f646b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 18 13:21:37 2020 +0100

    updated version (1.4.8.13)

[33mcommit c6b22c6ea4c10fad37efcb1e31507c1a95bdaa76[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 18 13:21:00 2020 +0100

    [sim] Added additional simulation files: simulation-optimized IMEM-ROM (so far, this is only relevant for the *new* NEORV32 RISC-V Compliance test framework v2.0

[33mcommit dd1ace2304adf7e4101d944d4d0b39c477d5b1ef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 21:53:54 2020 +0100

    [rtl/cpu] fixed another bug in mtval csr (wrong value for breakpoint trap); fixed bug in load/store control logic

[33mcommit 148a6fb01fef9bf73f9d1a0a041cb750dd75d0a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 20:14:11 2020 +0100

    [changelog] added version 1.4.8.11

[33mcommit 8c4dffdd6d3fd4ff6043a5939fefcbbf62038586[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 20:13:54 2020 +0100

    [Readme] minor edits

[33mcommit 83a4d0afe7c2c59e22e500748271b4413cc31081[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 19:44:48 2020 +0100

    minor edits

[33mcommit d7ebef302d5bc1b80aba7d39dff53254420132e3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 19:43:21 2020 +0100

    [docs/data sheet] updated mtval value table; added mip info on how to clear pending interrupts; fixed IRQ priority

[33mcommit dde1b572efd2575f5ec0959af07c97af8fa93f09[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 19:41:22 2020 +0100

    [rtl] fixed version number

[33mcommit 58239f29dc18c639f9b557cb37ccf8a8ffdcbf26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 19:34:35 2020 +0100

    :warning: [rtl/cpu] fixed error in IRQ priority encoder (MSI software interrupt comes before MTI timer interrupt)

[33mcommit fec029b0e97497e64eebf81076f84879c5d7282c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 19:32:20 2020 +0100

    :warning: [rtl/cpu] fixed error in mtval CSR (wrong values for some traps); fixed bug in mip CSR (writing zero bits to mip now actually clears pending interrupts"

[33mcommit 86328f9776f2031cd2c7bf5926dc5e5d671e144a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 14:57:22 2020 +0100

    [rtl] updated pre-built memory images (due to updated sw library sources)

[33mcommit b08d60d143c741cfdd027a0fc44f956125860eb9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 14:48:28 2020 +0100

    minor (uncritical) edits; formats/code clean-up

[33mcommit 660188752858c3ccd2f2bacea07d8e6cf18c6daf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 16 14:46:30 2020 +0100

    [sw\lib] added NOPS after modified global interrupt enable flag in mstatus CSR

[33mcommit 8bcfa788ed3cc5bf610774fcadf4768f7fca2f3c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 15 19:13:18 2020 +0100

    [sw/example/cpu_test] added tests for testing/clearing pending IRQs

[33mcommit f70c6b7d6ed5c7b6d98d420aeef5f9426e7a27e3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 17:02:18 2020 +0100

    :lipstick: [docs/figures] minor edits

[33mcommit 5cc36ab7b854521991d779b503845175b6aef0f9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 16:44:26 2020 +0100

    [docs/NEORV32.pdf] added reset value to mcause CSR

[33mcommit 3fb4846d1466a1ad38ef613da5b69e7a90a27570[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 16:43:59 2020 +0100

    [sw/example/cpu_test] minor edits

[33mcommit 24dea9d38ce466ed8984192db8faed496bd33232[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 16:43:39 2020 +0100

    [changelog] added version 1.4.8.10

[33mcommit 5e040c849b0298a3781016cbdd3ab9a66bca5178[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 16:43:13 2020 +0100

    [sw/common] crt0.S start-up code now sets mcause to trap_reset_c after finishing hardware setup

[33mcommit 4ad3dd7e3fe5f479d3a531cac60b3964cd461d5f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 16:42:42 2020 +0100

    :warning: [rtl/cpu] fixed missing reset of mcause hardware register

[33mcommit cfd1bc85b54deb8b034493b9f5ccdc5972c50567[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 12 16:42:14 2020 +0100

    :warning: [rtl/cpu] fixed wrong encoding of 'trap_reset_c' (mcause value after reset, should be 0x80000000)

[33mcommit e226031495329ed2fb9cc9e1e3bce2da738ed01a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:38:24 2020 +0100

    minor edits

[33mcommit ad10a1fb6d6e5609e6f8eb74ec24fdba78cee20d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:34:20 2020 +0100

    [README] minor fixes and beautifications :lipstick:

[33mcommit 09367c473e2d88678327256860feccbebf5d1706[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:33:38 2020 +0100

    [docs/NEORV32.pdf] added zicnt_en_c description; added nem mtime_i top interface signal; minor edity, fixes and beautifications

[33mcommit b518b395881fce6abbe930333515502c44fcd283[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:22:24 2020 +0100

    [changelog] added version 1.4.8.9

[33mcommit 3b5bc991690a76b738758395d873c5540b7ec4ab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:16:39 2020 +0100

    [sw/example/hex_viewer] added option to perform atomic compare-and-swap operation

[33mcommit 80559900e6664f68f048081d8579c5b5bb9bcc95[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:15:54 2020 +0100

    [sw/example/cpu_test] added cycle[h] and instret[h] tests

[33mcommit c011f1be0ae277d1a033e35b46e4e8ae7b439aa5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:10:52 2020 +0100

    [sim/testbench] added new signal to processor top entity: `mtime_i`; this signal is used for updateting the `time[h]` CSRs if the processor-internal MTIME unit is disabled (via `IO_MTIME_USE` = `false`)

[33mcommit cabe5ec7e7293c4f1f1ed712399a7d35310b560b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:10:14 2020 +0100

    [rtl/processor] added new signal to processor top entity: `mtime_i`; this signal is used for updateting the `time[h]` CSRs if the processor-internal MTIME unit is disabled (via `IO_MTIME_USE` = `false`)

[33mcommit ea94711726572f3be7483bf9fd5e460080c3a474[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:09:07 2020 +0100

    [sw/lib] Software can determine state of zicnt_en_c via mzext CSR's CPU_MZEXT_ZICNT bit

[33mcommit d06e7bb5e1ed6cfc3338c7be7684df3bee497aaf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 11 14:07:54 2020 +0100

    [rtl/cpu] added option to disable standard RISC-V performance counters ([m]cycle[h] and [m]instret[h]) for size-constrained implementations; disabled by setting VHDL package's 'zicnt_en_c' constant to false

[33mcommit 8b1d1a1e2699e2c272c6d0efa1ec90cabdaa022f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:32:16 2020 +0100

    [docs/NEORV32.pdf] added mstatush CSR and information regarding endianness configuration/status

[33mcommit 631e5f430381285597fe86a80c618c65820d189e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:31:37 2020 +0100

    [changelog] added new version 1.4.8.8

[33mcommit 8249f4d8e749848cc56c0c9ae6a7bb6d0ddc24f3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:26:28 2020 +0100

    [sim/testbench] if external memory is used for boot during simulation it now uses the endianness configuration used fo the external memory interface

[33mcommit 91109d5f3c7c0f665444706b1e1a4bdc23dc097d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:25:46 2020 +0100

    [README] minor fixes

[33mcommit 44c08fd3389170564885c92e8d58bf6d080b37c1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:25:05 2020 +0100

    [sw/lib/rte] added endianness configuration output to hardware configuration overview

[33mcommit d8e7ac0d9ca8b970b69497bdcac32bda0d11d1cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:24:11 2020 +0100

    minor edits

[33mcommit abf7101f049aa5f0d865dc6f81a900e746b1279f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:23:24 2020 +0100

    [rtl/processor, sw/lib] added flag to SYSINFO to allow software to determine endianness of external bus interface

[33mcommit dd2e4eb8c03af8b5fa53e4a1c3abe2f2c5ea7f9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:22:33 2020 +0100

    [rtl/processors] added configuration option (via VHDL package) to configure external bus interface for BIG- or litlle-endian byte-mode

[33mcommit 4a38e79cc114d0138446482ace7581b2157fbca4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 21:19:47 2020 +0100

    [rtl/cpu] added missing 'mstatush' CSR; only bit 'MBE' is implemented yet

[33mcommit f56c18e3dd81434522c1d347c417387c80f2bb9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 20:43:24 2020 +0100

    [rtl/cpu] minor edits (layout, comments)

[33mcommit f94682e56b265b6cf4f887a2ec13d07903f1f869[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 10 20:23:20 2020 +0100

    :lipstick: [docs/figures] added logo version with transparent background

[33mcommit ff81bf605cc9b6c4c9396a007431b3bf9dc4602b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 21:04:10 2020 +0100

    [README] minor format edits

[33mcommit 781a989346e7e4b4c74f9072dec6150681dbaf2d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 21:01:13 2020 +0100

    [docs/figures] removed pdf icon image

[33mcommit 5bee7018a3d46f54130cacd368311178a7456815[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 20:48:14 2020 +0100

    [docs/NEORV32.pdf] added new section *Execution Safety*; added 'envrionment call from U-mode' exception

[33mcommit 27a4c1fa0cf22d541a37524e70a1c65c9d60ae27[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 20:46:39 2020 +0100

    [README] added link to changelog to hw version labels; added environment call from u-mode exception

[33mcommit 4078043184345db6f0bdad48f5bd4ceb1a855625[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 20:46:01 2020 +0100

    [CHANGELOG] added version 1.4.8.6 and 1.4.8.7

[33mcommit 1f4dc6b354e02359beddddd0383a3eded5159ef3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 20:45:21 2020 +0100

    [sw/example/cpu_test] added test for environment call from u-mode

[33mcommit 3ab48772f0edeec4ef0047c0fdaa3122b8ceb159[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 20:44:51 2020 +0100

    [sw/lib/rte] added 'environment call from u-mode' exception

[33mcommit 9ed9c1c6f417d39e554c6dbb80b583d6da601093[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 20:44:20 2020 +0100

    [rtl/cpu] added missing *environment call from U-mode* exception (via `ecall` instruction in user-mode)

[33mcommit f2adb49467ea66aa9b235136980f175af6511f25[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 19:39:04 2020 +0100

    [sw/example/cpu_test] added check to make sure that unprivileged CSR read access does not return any (secure) data

[33mcommit 5d09fe138da9c23b71fd20d1466fb0584a1107b2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 18:38:28 2020 +0100

    [rtl/cpu] added mcause ID code for hardware reset; :lock: added security feature: illegal user-level CSR read access will always return zero

[33mcommit 5fae214e85ff87f85a6bde04636a9a370911b13a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 17:47:54 2020 +0100

    [sw,rtl/cpu] added mcause trap id code for 'hardware reset': 0x80000000

[33mcommit 4b043d71f3fa62bc4b150ac1922321c508435583[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 17:05:13 2020 +0100

    :warning: [rtl/cpu] fixed bug in ALU's co-processor interface - CPU might have permanently stalled when executing an instruction from a disabled ISA extension

[33mcommit 997efdde5cddb8d5d940ffad352fb2d3db00ffeb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 17:04:43 2020 +0100

    :warning: [rtl/cpu] fixed bug in ALU's co-processor interface - ATOMIC 'A' extension could not be used without MULDIV 'M' extension

[33mcommit 5a650d2aeaade99f120953213bee6e1eda414e92[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Dec 9 17:01:15 2020 +0100

    :lipstick: [docs/figures] cosmetic updates for logos and figures

[33mcommit 510b13d731fd03f7be9628d1165132d4ce701a15[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 21:42:29 2020 +0100

    [docs/neorv32.pdf] fixed typos & layout

[33mcommit ff122007144c745381164dab0e5efa40cbd1c8a5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 21:41:49 2020 +0100

    [changelog] added version 1.4.8.5

[33mcommit 31c23120c9a68ef81ea3ee3502cbe9b2e6dd7e10[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 21:41:22 2020 +0100

    [docs/figures] beautified processor block diagram

[33mcommit 392500aec7546c5d443e473f1ea60e8cfca653d9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 21:40:09 2020 +0100

    [rtl/cpu] :warning: fixed bug in next-PC logic (introduced with version 1.4.8.1) that caused instruction fetch from memories with more than 1 cycle delay to fail

[33mcommit 521c7cfbec8654a5e4f29cb490e94f7d602849fb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 21:37:56 2020 +0100

    [sim] fixed typos

[33mcommit 0f45ed09b478c01388cd73085b11531bf8d96753[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 18:06:27 2020 +0100

    [docs/risc-v-specs] updated RISC-V base and privileged specification manuals

[33mcommit aaa837c82b6760c6e562f51cbb47027cbf921837[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Dec 7 17:40:23 2020 +0100

    typo fixes

[33mcommit b2e55b424ff97b6af0cc614fe0e8b92f27e4a384[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 6 17:35:22 2020 +0100

    [docs/neorv32.pdf] added more information to PMP section; refined 'CPU access' section

[33mcommit ebb2856f0be36182e4f7d11869e8657c5e36fd39[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Dec 6 17:34:40 2020 +0100

    [README] minor edits

[33mcommit 76c77bb67e3ebd295d41d47d2fb3f7e954a43845[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:50:45 2020 +0100

    minor edits

[33mcommit 144a837749b714b26cf3f78bc18bbabd98e6c5da[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:50:18 2020 +0100

    [changelog] added version 1.4.8.4

[33mcommit c23364f44ad3055ad8ca6dbdc6b7501e41a41179[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:49:11 2020 +0100

    [docs/neorv32.pdf] Reworked section '2.4. Instruction Sets and CPU Extensions'; clean-up of CPU/Processor generics listing

[33mcommit afc8436f4cbf5d72560b2d9bf1ebfd05c4aedbd5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:48:19 2020 +0100

    [rtl/sw/example/cpu_test] minor edits (now using dedicated PMP configuration functions fro PMP test)

[33mcommit 2c516c04eec906089ec7025e4d5ed204a7754a3a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:47:37 2020 +0100

    [rtl] removed specific PMP configuration generics (PMP_REGIONS, PMP_GRANULARITY) from top entities; configuration is now done via package constants

[33mcommit 0863844efb56cb237c5bfc95026307c55c9238d7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:38:15 2020 +0100

    [rtl\cpu] :warning: fixed bug in physical memory protection - region size configuration was incorrect

[33mcommit 8543d6289596e12034bb0e25984411ba6b860f83[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Dec 5 22:14:51 2020 +0100

    [sw/lib] added functions to configure physical memory configuration (PMP)

[33mcommit bcab5521eb754a62a96049ff809e2c15e0819c19[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 4 20:35:18 2020 +0100

    [changelog] added version 1.4.8.2

[33mcommit 95aba4f5703e3d8b18d95bb33a320de0ea50d385[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 4 20:34:45 2020 +0100

    [sw/lib/rte] minor code size optimization

[33mcommit 9de87dd468e399672bf26f805079ba740e13e218[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 4 17:47:00 2020 +0100

    [rtl/cpu] code clean-up/optimization

[33mcommit e5fd09208ecf97699cf599b92532a77c1a948f80[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 4 17:46:10 2020 +0100

    [rtl/processor] Added PMA (physical memory attribute) to processor-internal IO region: NO_EXECUTE

[33mcommit c0e7bf312c7206c5982ca326c611c8b9d6f20340[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Dec 4 17:45:31 2020 +0100

    [docs/neorv32.pdf] added 3.3.Address Space/Physical Memory Attributes (PMAs) section

[33mcommit a1105235502155ed8d2d2c9571e291765a068a98[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:55:02 2020 +0100

    [neorv32.pdf] updated text regarding customization of bootloder

[33mcommit 3dc005142eaf57cda441e126ccecd15e4485170a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:54:04 2020 +0100

    updated changelog

[33mcommit 51c52b2934745c2650868c7d6a8cb4956cc798c8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:53:12 2020 +0100

    [rtl\cpu] optimized CPU PC update logic; shortend critical path

[33mcommit 4d184650cba7b3122fe12839ff7a64ad41e2d386[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:52:38 2020 +0100

    [bootloader] minor edits

[33mcommit 2a96c2428ec8a8aa15f1d944a9ccf9980106844f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:51:53 2020 +0100

    [rtl\processor] updated pre-built blink_led applicaton image

[33mcommit 5e2d417176ab970ab451f83fdad627886d69abd1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:41:52 2020 +0100

    [sw\crt0.S] crt0 now resets CPU's cycle and instruction counters

[33mcommit 3691df734cc6688e3df77366d3490d3dd9776f16[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Dec 3 20:41:03 2020 +0100

    [bootloader] added option for boot-from-spi-flash only (simplified bootloader)

[33mcommit 9240306bf581c3fd14c3f0f86911c11b3527aba9[m[33m ([m[1;33mtag: v1.4.8.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:37:07 2020 +0100

    [data sheet] added details regarding (new) atomic 'A' CPU extensions; added infromation about bus lock signal and interconnect requirements for atomic accesses; added example waveforms for pipelined/classic Wishbone transactions; updated implementation results

[33mcommit f617c1ab3ea903b94668d15159f99eafd895f498[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:22:44 2020 +0100

    [CI] using all provided CPU extensions for compiling the CPU test program

[33mcommit 195b93afcf270512c7ac5bc8f6ba72d9aa1c0dec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:22:09 2020 +0100

    [readme] minor edits

[33mcommit 6276e609899ecc7230421ad43d06ff0926c82a60[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:21:54 2020 +0100

    [sim\tb] A extension is enabled by default now

[33mcommit 8af0aee6f4ec745bddc3ca8d0b9d3757c91b01ab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:15:10 2020 +0100

    [changelog] added version 1.4.8.0

[33mcommit e470694ec3fa1462a54aa218867aa88bdc465b88[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:13:08 2020 +0100

    [readme] added A extension; updated implementation results

[33mcommit 59ac8a01ae607dfb1ebe9a45d38c89cd5edbad31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:12:34 2020 +0100

    [sw/example/cpu_test] added tests for atomic memory accesses

[33mcommit 9fb7cc8fcd9e5f92ebd98ffb522f919997b70fae[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:12:04 2020 +0100

    [sw\lib] added atomic compare-and-swap operation (function) to implement semaphores/mutexes using atomic memory accesses

[33mcommit d42b1e9c1b93648b07d8a01cd5bb490eb866c725[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:11:19 2020 +0100

    [sim/vivado] added atomic control signals to defualt waveform configuration

[33mcommit 9e042925ca2e9df5be4ed3a96cbb6a9431f84b85[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:10:51 2020 +0100

    [sim/tb] added mechanism to support simulation/test of successful/failed atomic memory accesses

[33mcommit 9e3440869303e30fe5ba4e6f672979b8ed7b2294[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:09:58 2020 +0100

    [rtl\top_templates] added 'lock' signal to external bus interface to support atomic memory access operations

[33mcommit 1b0b8272006a3af365d6336af1a33e1a9b8fdcaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:09:21 2020 +0100

    [rtl\top_templates\AXI4-Lite] AXI4-Lite has no netive support for atomic accesses

[33mcommit 374e5987fded9ac5166328bc58991c10559fd260[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:08:38 2020 +0100

    [rtl\processor] added (Wishbone) bus 'lock' signal to support atomi memory access operations

[33mcommit 806e43fece33daca8d5875d3e2fece983920fb6f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:08:03 2020 +0100

    [rtl\cpu] added support of RISC-V CPU 'A' extension (atomic memory access instructions); only lr.w and sc.w yet; sufficient to implement semaphores/mutexes and to emulate any other AMO operations

[33mcommit 08c58d120e700207b67abb36c7e146bc3f11a008[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:06:48 2020 +0100

    [rtl\cpu] minor edits

[33mcommit 0b5d633e0d953873667057491a4c4021d7425e7f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 18:06:13 2020 +0100

    [rtl\core] updated pre-built blink_led example program image

[33mcommit 9a1527e19de5676fb46b34bfc0856386799eb6cb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 16:04:12 2020 +0100

    [sw/lib] added some more default MISA flags

[33mcommit 8961343981665e3a3e16db3e2e682cb7988825ac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Dec 1 16:00:34 2020 +0100

    [rtl/cpu] fixed minor issue in CP interface; multiplier is now slightly faster (~1 cycle)

[33mcommit ae71678264a90777b037a90267f13abed235d9e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 30 16:27:30 2020 +0100

    :warning: [rtl/cpu] fixed bug in CPU internal co-processor interface

[33mcommit 7f6d865705e2fd1d6c0777e942ba2ef7d117cdc2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 28 14:08:04 2020 +0100

    [CHANGELOG] updated

[33mcommit 88cb5742b258b68d26a34d978e3520bdea0fb1bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 28 14:07:20 2020 +0100

    [rtl\cpu] split ALU operations and shortened critical path (replaced ALU output 8:1 mux by a 4:1 mux)

[33mcommit 4888509c05440ee3daea06116eb7acc180ff84fd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 28 13:36:00 2020 +0100

    [README] minor fix in contributing text

[33mcommit b77501cd903a04f83c97636ce149f1ef97f0a920[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 28 12:58:36 2020 +0100

    [rtl/cpu] relocated some synthesis warnings (asserts)

[33mcommit b25e41bfda5bdcdf29288edea6d83a1be555c532[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 28 12:57:51 2020 +0100

    [rtl/cpu] minor comment edits

[33mcommit aefbaa3e16c418483dc3580e4f91b849a335c279[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 26 19:19:26 2020 +0100

    updated changelog

[33mcommit 77ab7ad4511360be5feb8d6251ff8d260161cfe4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 26 18:23:15 2020 +0100

    [rtl/cpu] CSR access instructions are one cycle faster (3 cycles now); system/environemnt instructions (like mret or ecall) need an additional cycle (4 cycles now)

[33mcommit d77d5bb7edeb65f336e529619f8750ddb6d442ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 26 18:22:18 2020 +0100

    [rtl/cpu] minor rtl code clean-up

[33mcommit 0575de8254d05a13180b7535d6229244bd694486[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 26 18:21:26 2020 +0100

    minor edits

[33mcommit 0fd58bc33ed892fc6efed6ffd0c7f24640599e7d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 17:04:46 2020 +0100

    [README] small edits

[33mcommit 749d4d2b8054c60e26bad46e94e4f2a4780bc883[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 17:03:46 2020 +0100

    updated changelog: v1.4.7.4

[33mcommit 990138f1da6388d31e9c3ccb37c5349c06cdaa2a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 17:00:32 2020 +0100

    [rtl/top_templates/axi4] minor edit in synthesis sanity check

[33mcommit dcd535bf1cc688c4b0c75c8c8088d85ac6deccfa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 16:57:37 2020 +0100

    [rtl/processor] minor resource optimization of external bus interface

[33mcommit 2ffab1d1d6af3298771b458bd486a0f612418193[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 16:56:55 2020 +0100

    [rtl/cpu] optimized CPU's instruction fetch interface (no more unnecessary request cancelling)

[33mcommit 316ae80dabbb989a38c07dfd44eb15832d3ca0b1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 16:56:23 2020 +0100

    [rtl/cpu] optimized CPU's instruction fetch interface (no more unnecessary request cancelling)

[33mcommit 3f5078ec43e87708cdbff663683e46e8518c0824[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 16:44:04 2020 +0100

    [s/lib/rte] minor format edits

[33mcommit 172a11201e4f8d124152c2e12b83e5240e351ea0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 15:30:39 2020 +0100

    [rtl/cpu] :warning: fixed bug in IFENCE.I instruction that caused instruction fetch problems when executing code from processor-external memory

[33mcommit e1b03af9edb4e01ffc287d26096f42b2823e6896[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 15:24:08 2020 +0100

    [sim] added another external memory to testbench; testbench now features external memories for simulating external IMEM, external DMEM and external IO; programs can now be run using external memory only

[33mcommit cd4875d52184e2bc96395787361507671f4ad690[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 14:38:47 2020 +0100

    [lib/rte] minor format edits

[33mcommit 8410f930f26484aebb526af6169b9e4721eda56d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 14:37:19 2020 +0100

    [sw/example] added example program (demo_gpio_irq) showing how to use the GPIO pin-change interrupt

[33mcommit 4c0958a477b26cd173435cf32f070b727371aeda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 25 14:21:02 2020 +0100

    [sw/lib/cpu] 'neorv32_cpu_delay_ms' busy wait function is now precise (using CPU's cycle CSR; Zicsr extension required!)

[33mcommit 08dfef69bcb152fb104ea76f01a6bcdc69ed6c24[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 16:21:56 2020 +0100

    updated changelog

[33mcommit 570fc827702d69a317dca2f4b648059fe1f634e6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 16:19:18 2020 +0100

    [rtl/processor/external_bus_interface] external bus interface now makes sure that a canceled transfer is really understood by the accessed peripheral

[33mcommit 9b8ea983bfa4a8fcc40d130c961cc303cd90d6eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 16:15:50 2020 +0100

    [rtl/core] minor edits

[33mcommit 3289c4aa749e116d6175c2e0c5fcbf2a4adef186[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 16:15:10 2020 +0100

    [rtl/cpu] :warning: fixed bug in bus unit that caused memory exceptions right after reset in some cases

[33mcommit 96844f9d0a496375cfebd468a62ec770b3250d7c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 16:12:39 2020 +0100

    [sim] added second simulated external Wishbone memory (one for simulating processor-external IMEM, one for simulating processor-external memory-mapped IO)

[33mcommit 3b7c15531e59cc9270f0e2ec2480800cef7a34b6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 16:09:43 2020 +0100

    [docs/NEORV32.pdf] added more information to GPIO pin-change interrupt configuration; added mcause value after reset (=0)

[33mcommit d8b6203087173a87c3f3796a788d849a96781945[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 13:42:58 2020 +0100

    upadted Changelog

[33mcommit 49f7d4ab7e36eb2ecd91fa91f92ac5cbccfe53d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Nov 20 13:42:14 2020 +0100

    [rtl] removed legacy (and actually unused) IMEM's 'update_enable' signal

[33mcommit ca8b4ab65cbe8d74dffc29e66547124c782f2ea6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 19 22:15:48 2020 +0100

    [README] added stuff being 'work-in-progress'

[33mcommit b4cb9454525e52ba1bc9aebe1049051e844ee9c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 18 18:14:30 2020 +0100

    minor edits

[33mcommit 2d5c474f6b81664443efcf5c2d514ad02cd5d34d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 18 18:09:06 2020 +0100

    [sw\example\cpu_test] added 'compile guard'; application has to be compiled with USER_FLAGS+=-DRUN_CPUTEST to be compile actual application

[33mcommit 8096d4cbd73b598e11a3138338bec0e30b1f4f45[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 18 18:08:12 2020 +0100

    [.ci] minor edits

[33mcommit a97504686a30c76c607c959cd7b426a47b3dc619[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 18 17:41:48 2020 +0100

    [sw/example/coremark] updated CoreMark documentation

[33mcommit 49b05917cc826a9e172dea5f9e62aa9d08460867[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 18 17:41:29 2020 +0100

    [sw/example/coremark] updated CoreMark sources (latest version from github); code clean-up

[33mcommit c81282c59532a08c90267cb754ad3bdbb25cf77a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 16 15:45:35 2020 +0100

    [sw/example/cpu_test] bus max timeout latency estimation is now an 'actual' test

[33mcommit 04812ca2bd22b8b94f7abe9f2281c8046cdf7161[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 16 15:44:14 2020 +0100

    [rtl/cpu] fixed minor bug in 'is_power_of_two' helper function

[33mcommit cc0df761b15813544a7ee5327bfefbcbc039eb46[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 16 15:43:27 2020 +0100

    [rtl/cpu] Added assert - check if number of configured instruction prefetch buffer entries is not a power of two

[33mcommit e3249d8f34c970fb06a2e976a760d090dd61ac4f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 16 15:42:08 2020 +0100

    minor edits

[33mcommit 6eafcbbb9f96af61ceee970e8434d05501fca0b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 12 21:36:23 2020 +0100

    [docs/figures] added fancy logo card

[33mcommit dbe2fdbf0ade7a34f715ca980bb1ebdc03c49f57[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 12 19:42:36 2020 +0100

    [changelog] added link to latest release

[33mcommit d27150cb17330e27394e9f5b97254b35df01f280[m[33m ([m[1;33mtag: v1.4.7.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 11 17:14:03 2020 +0100

    [sw/rte] minor ASCII logo font edits

[33mcommit 55a831fee90c9990afe46755517ba05da81c6500[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 11 16:58:06 2020 +0100

    updated changelog

[33mcommit 811d932270d1c2ab10796020e988bcff4919175b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 11 16:56:49 2020 +0100

    [sw/example] added fancy 'hello_world' example program

[33mcommit ac6ba1a04f85ba6bc468498810c3024b6f8c5360[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 11 16:56:02 2020 +0100

    [sw/example/blink_led] minor edits

[33mcommit 24231b8b020bf1b77907de355826b573084b2573[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 11 16:55:33 2020 +0100

    [doc] updated synthesis and performance results

[33mcommit 18c4d410d564bd7ebe2aac795a017e8dcc10ddea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Nov 11 16:54:47 2020 +0100

    [rtl/cpu] optimized pipeline front-end; jumps & branches are 1 cycle faster; +5% coremark performance

[33mcommit 0d1a5680fb618feb388a2e816e86853c458f4a11[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 10 20:29:10 2020 +0100

    [ci] minor edits

[33mcommit ad2ddb7333c38c18e4422c1449a5924953d5e242[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 10 20:28:41 2020 +0100

    [readme] minor edits

[33mcommit 1b3b22c7d61df378d969d73df1ecf0f0508a14d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 10 20:28:25 2020 +0100

    [logos] minor layout edits

[33mcommit 8953101b4d55873521f19c2f2cc07913788df69f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Nov 8 00:40:40 2020 +0100

    [ci] using march=rv32imc for test-compiling all example programs

[33mcommit dc10ba1ee370055d66351512c299c6b9e23b7421[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Nov 8 00:33:13 2020 +0100

    [sw/cpu_test] minor intro edits

[33mcommit 17f195b6cfb8542255794b3de83b253e8e55a30b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Nov 8 00:32:47 2020 +0100

    [sw/rte] added print_logo function; minor clean-ups

[33mcommit f5e88711b9a20367c78cb0770eecc9a04538f564[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 7 16:59:00 2020 +0100

    [doc/logos] added inverse logos

[33mcommit 001fa5e95fc47431dfa5183279855ea9e790fa17[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 7 16:23:30 2020 +0100

    added project logo

[33mcommit 1e9c35eda076f942804d76a54a3ed92683756fa7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 7 16:20:15 2020 +0100

    [doc] minor edits

[33mcommit b4eb702876c6edcb223e0eedbcfdfe08a952205d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 7 16:19:30 2020 +0100

    [doc] added project logo

[33mcommit 5a0bf953f558cf054fc86b3a5fbf7a280fd5dcc4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Nov 7 16:16:44 2020 +0100

    [bootloader] minor size optimisations; changed 'processor version' output

[33mcommit a4e0fd8ecdad2cbf7da5b985174560e0922420de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 5 21:40:21 2020 +0100

    [sw/freeRTOS] clean-up

[33mcommit 684c227fe54d367cff02194f90465528ec2f9914[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 5 21:38:58 2020 +0100

    [sw/freeRTOS] added makefile-support for FreeRTOS-Plus

[33mcommit 60f164957ab621d2224c6bda3c044084779a4c72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 5 20:57:38 2020 +0100

    [rtl] minor edits

[33mcommit 552be84e24c11774b5d3126782cdecb0bb19a772[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Nov 5 20:27:19 2020 +0100

    [doc] updated documentation

[33mcommit e4a50158d077198f6531dcf0ea5bf26d124f2467[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 3 18:50:26 2020 +0100

    [rtl\processor] added 'HW_THREAD_ID' generic to assign CPU's hartid

[33mcommit 7df72ba516736ed75ba9076be7add572c3989535[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Nov 3 18:37:32 2020 +0100

    [spi] removed SPI's buggy 'lsb-first' mode

[33mcommit 2cd24a773f84746cac3e9e8778f5a68664aeca98[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 2 20:35:45 2020 +0100

    updated changelog

[33mcommit d2daa693c93dafffd99e0302532c8cadf825ce5a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 2 20:34:57 2020 +0100

    [rtl/cpu] further rtl code optimizations to reduce hardware footprint; rtl code clean-ups

[33mcommit 3ff679895a8db24c4481696c30e064edf53187aa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 2 20:33:51 2020 +0100

    [rtl/cpu] :warning: fixed in bug CPU's illegal instruction detection logic; further rtl optimizations to reduce HW footprint

[33mcommit 391237a31f90fab574e8d2285ec4ec2024c5bdab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 2 18:21:13 2020 +0100

    [rtl/processors] minor edits (assigned static values for uninitialized and UNUSED signals)

[33mcommit 022ff0254d3988db7c713a39ef3fabc0c2647d71[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 2 18:18:40 2020 +0100

    [ci] changed output text for successful cpu test run

[33mcommit 7e4f15365f18d2431c9b9cc1d567508a6c15f677[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Nov 2 18:18:08 2020 +0100

    [rtl/cpu] fixed bug in compressed instructions decoder (introduced with last commit :( )

[33mcommit 20a55dd19c82165bab09cbd7dd9939c6d50d9ae0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Nov 1 00:38:54 2020 +0100

    updated changelog

[33mcommit 25fa693b2b46f560676300e31b26e5361115ac4f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Nov 1 00:28:56 2020 +0100

    [rtl/cpu] hardware optimization; reduced hardware footprint, shortend critical path

[33mcommit 56c9d3e150b7b224fed754135c0924e3a26f5517[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Nov 1 00:27:53 2020 +0100

    fixed bug in instret/cycle carry logic

[33mcommit 44e720ec69f2a533899357aca8a5fe05a54a04b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 29 21:28:43 2020 +0100

    [changelog] update

[33mcommit 8f0b1e6562e5d7fa7cae0db05206941ab91e0d31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 29 21:27:55 2020 +0100

    [rtl] clean-up; modified co-processors system for future extensions (looking at you bitmanip)

[33mcommit 381044745b0b22c689de106961d4c2be16a5e00e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 29 14:04:25 2020 +0100

    minor edits, optimizations and clean-ups

[33mcommit 3904793c5f0ca5d384f9e1f13267f2aec970a650[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 25 22:12:50 2020 +0100

    [sw/FreeRTOS] Added a more sophisticated demo application ('full_demo')

[33mcommit c7991b544a950b37b91ffdaa8d6f1ca79980dd26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 25 22:05:05 2020 +0100

    [doc] updated documentation

[33mcommit 0cc1c683b0620a33f9b7183e8ef27e7bbc6cca59[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 25 22:03:24 2020 +0100

    [rtl\processor] removed processor's >priv_o< signal; added >tag< signal to Wishbone bus (wb_tag_o)

[33mcommit 275da1cd16cd03a5282f53525f5594e4b904e735[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 25 22:02:03 2020 +0100

    [sw/linkerscript] minor edits

[33mcommit 1ccfc037a7afdb1e9735877bce208130747f2d87[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 25 21:09:33 2020 +0100

    [sw/makefiles] 'main.asm' debugging file now only uses .text section for disassembly

[33mcommit 7c63304c6e0e85f7f9ca9ddc4f374512065c457e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 16:41:30 2020 +0200

    minor edits

[33mcommit 9e35d4adadd667c56f43bbf992ea6ee884861b51[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 16:37:30 2020 +0200

    minor edits

[33mcommit c9d213bf94e05be7c691dac1e74f75da66ab875b[m[33m ([m[1;33mtag: v1.4.6.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 15:57:12 2020 +0200

    [doc] updated documentation

[33mcommit 0a3b269e20731c8ec9e3d940add36be3bb46ef3e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 14:55:55 2020 +0200

    [doc] updated online documentation

[33mcommit 919d733b67fcdc8241cef58e083ec6795bf043e2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 14:55:26 2020 +0200

    [doc/figurs] added exemplary axi SoC setup figure

[33mcommit c554f5d218aafacdbf01639aae357e9e0e1da26d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 13:49:19 2020 +0200

    [readmes] updates; added links

[33mcommit e5472534e38601abf6049553bce3b052d24208d0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 13:47:49 2020 +0200

    [sim/ghdl] added AXI4-Lite wrapper to simulation source list

[33mcommit 797985a151d6c2060b1fc48bc4ef3bb0f2b4d18f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 13:47:01 2020 +0200

    [rtl/top_templates] added AXI4-Lite Master Interface wrapper for processor

[33mcommit 2c686f9bd23be8845e46e950c86288aa7d34e04c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 10:23:40 2020 +0200

    [rtl/processor] updated pre-built blink_led example image

[33mcommit 50bc6b12918c04dc484601bd9c633b4a88465f61[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 24 10:22:36 2020 +0200

    [sw/cpu_test] some code optimizations

[33mcommit 4903248ba9685bb18dc468f38b69559696f8146c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 23:31:20 2020 +0200

    [changelog] updated changelog

[33mcommit 53a73e0b0c7e1c660e5016be3ca591cc4eec49bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 23:30:02 2020 +0200

    [test_bench] removed obsolete 'MEM_EXT_REG_STAGES' generic

[33mcommit 4d68f7fe204fb038941c5b4c1410c0161935d968[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 23:24:11 2020 +0200

    [rtl/external_memory_interface] completely reworked external memory interface (WISHBONE)

[33mcommit c951849d61cb41c013ae82fb18968d62b16b8057[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 23:23:17 2020 +0200

    [rtl/processor] removed obsolete 'MEM_EXT_REG_STAGES' generic

[33mcommit f6cb43bcf8fd5f9cc40e9355a49bfd9a17deeb87[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 17:21:52 2020 +0200

    minor edits/updates

[33mcommit 8bf58a3c831af644d8a2181676ede69ef5dff0f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 17:20:26 2020 +0200

    [sw/cpu_test] fixed broken CPU test program

[33mcommit ae84d57d96bc0cb6025be315a5c7d58fa41ce079[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 17:18:00 2020 +0200

    [sw/example] added new example program: hex viewer / memory inspector (read/write/dump memory locations by hand)

[33mcommit 387f412f78d55d934363de7eee0efea5bc43b8b0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 23 17:14:55 2020 +0200

    [bootloader] fixed minor bug that prevented executable storing to external memory (when no IMEM is implemented)

[33mcommit e1212d81d4e96c02cc7870bcf563121c5619b436[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 22:37:03 2020 +0200

    [twi] added control register bit to enable/allow or disabled/not_allow SCL clock stretching by peripherals

[33mcommit 3f3f79046969876a55b24677ab76d7604329ef69[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 20:29:25 2020 +0200

    [doc] updated documentation

[33mcommit eb6609ef31c9f07f4e404ff5cd0f1f4d5e90ae16[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 20:26:18 2020 +0200

    [rtl] added 'i/d_bus_priv_o' signals to CPU.top and 'priv_o' signal to CPU.top to show privilege level of bus access

[33mcommit 6eb0ad83ef7ff08fa36f53e57d7af927b09e336a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 20:21:32 2020 +0200

    [rtl] updated pre-built bootloader image (due to RTE modifications)

[33mcommit 54c203f023c8f6968cc395f2578507bbb05f328f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 20:20:45 2020 +0200

    [sw/rte] fixed print_version function

[33mcommit 1e000fe06cf1bca1683306724ed8f241f58ac5c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 20:11:39 2020 +0200

    [rtl] updated pre-built blink_led image (due to RTE edits)

[33mcommit 7965404a035f0a6b95bc5681e02b21bcac560154[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 16:47:29 2020 +0200

    modified some source code comments

[33mcommit 4f6c90dcb480c469e7943f98daee7ed88f491a3c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 16:45:57 2020 +0200

    [sw/cpu_test] improved PMP tests

[33mcommit ea03d9714873753d48fe111f8aceefd72a085ed2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 16:43:26 2020 +0200

    [sw/lib] fixed bug in goto_user_mode function (that might have caused stack corruption)

[33mcommit 38fcd8d197912495a603dadbda5359c852877f93[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 22 16:42:51 2020 +0200

    [sw/lib] minor edits (comments and console output formats); minor performance optimizations

[33mcommit b77fd782c102948266dd4f97cb48716f80c96c94[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 21:42:37 2020 +0200

    [sw/cpu_test] added FIRQ1 test (via GPIO; GPIO.out is connected to GPIO.in in testbench)

[33mcommit 71d33ef65386837faa05fe2c5e825bf3180719c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 21:22:58 2020 +0200

    [changelog] update to v1.4.5.9

[33mcommit 8c755ca3af964ecbfe60aa35a48ce053b8c2d985[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 21:21:20 2020 +0200

    [sim] increased default GHDL simulation time to 6ms

[33mcommit 040d3dd422950582c6f90abfef2428baa0288429[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 21:20:44 2020 +0200

    [sw/cpu_test] minor layout fixes

[33mcommit 65ef1027e24d4777081c76858d5f0c020d4c5b0c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 21:20:12 2020 +0200

    [rtl/cpu] fixed bug in WFI instruction

[33mcommit 2669e3c02d9dfda76a5a851e9d101b64afc3d304[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 20:11:13 2020 +0200

    [sw/cpu_test] clean-up; more specific console outputs

[33mcommit 8171ff2875db165700dcb1099b7c8732c567cba7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 19:53:51 2020 +0200

    [sw/rte] rte's hardware info function now also shows PMP functionality

[33mcommit 32385e543e4f84731afa0331b54e8a9917d8e477[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 19:53:15 2020 +0200

    [sw/lib] minor comment edits

[33mcommit 586ab79c1c70130c165fc1e103cabfc777d3b4da[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 19:52:25 2020 +0200

    [rtl/cpu] code clean-up

[33mcommit 6509cce14b3dafdd76d32a8e76e4a8d04badc67a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 19:51:55 2020 +0200

    [doc] updadted documentation

[33mcommit f3ece25e50502bfd694b0cda90eb6eaa69ec144e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 20 19:51:32 2020 +0200

    [rtl] machine timer interrupt is now available as top signal (mtime_irq_i) if processor-internal MTIME unit is not implemented

[33mcommit 9316d11cc0d0515848e099852c783c9fff0485de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 18:44:45 2020 +0200

    [sw] minor doxygen clean-up

[33mcommit bd741f12477dc6a2ec7cb78f21a103a1d44bc42b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 18:43:39 2020 +0200

    [doc] updated documentation

[33mcommit 7bb25b6410906b4983441dbc97abc154c6880fbc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 18:43:02 2020 +0200

    [rtl] minor edits

[33mcommit 61bc9dfc05006610b62af9c5bd1ec07f9cde6ada[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 18:42:38 2020 +0200

    [sw/cpu_test] added tests for (DEFAULT!) CFUs

[33mcommit 8293d40e0add93a4bb7f8ebecf2015264cc61f0c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:43:42 2020 +0200

    [sw/rte] added CFU1 available check

[33mcommit 96d33ae583670dde939ec6b452dcc5d798e14322[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:43:10 2020 +0200

    [docs] updated documentation (for new second CFU)

[33mcommit 4f77375c4ac240c032456b0462fea0b4c27da600[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:40:56 2020 +0200

    [sim] added new cfu1 VHDL file to ghdl simulation script

[33mcommit 6cabf0c0930c01755987f184e462fab42938fb28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:40:22 2020 +0200

    [rtl] updated HW version; added cfu1 component

[33mcommit b783a7140c1258f6996d15aa8e94328b021e1348[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:39:49 2020 +0200

    [rtl/processor] added CFU1 available flag

[33mcommit ee072131084696730591a93ff43cb4bee5e11a62[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:11:55 2020 +0200

    [sw/cfu] added software framework for second cfu slot

[33mcommit 6b19fc0ccae6ad251b9c67c158f697ddbb76095e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:11:06 2020 +0200

    [rtl/cpu] minor edits

[33mcommit c93272355b30b48205794c919695b71545963785[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:10:35 2020 +0200

    [rtl] added new IO_CFU1_EN generic to enabled new second CFU slot

[33mcommit 0227cc651694450b4305de93760ab578a2014283[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 18 17:09:11 2020 +0200

    [rtl/processor] added second CFU slot

[33mcommit 5db2acaa5495dac5b3f02502598d3b10e6603c10[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 22:58:40 2020 +0200

    minor edits2

[33mcommit d0c5385d8a12b77d2ca2616aa28efa962e53568a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 22:58:17 2020 +0200

    [sw/cpu_test] added test to estimate bus timeout latency

[33mcommit 326444380a4d438eb6452e2d70390fbe6e99271f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 21:32:43 2020 +0200

    [rtl/cpu] minor code edits

[33mcommit 3f89ff9b25e452eab11bd61f8260820d346f707f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 21:21:54 2020 +0200

    [sw\image_gen/uart_upload] increased delay for bootloader response check

[33mcommit 3d6bbb1f328a73c1ce693a6938958c1bd1a5e029[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 21:21:00 2020 +0200

    [bootloader] fixed missing newline in console output

[33mcommit 8a3027e30a4a7fc6ce3093c067e26a353c009c32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 19:20:02 2020 +0200

    [rtl/cpu] fixed version number

[33mcommit 70840485514a8c9daff4dc878b7532794993d2c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 19:18:04 2020 +0200

    [docs] updated documentation

[33mcommit 3de295379f93ed42c6769f3eaa1c0ecbfe01f5ea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 19:16:46 2020 +0200

    [sw/bootloader] added 'fast_upload' command to bootloader (used by makefile's new upload target)

[33mcommit 56a6089980c66a7dc6bfca928014f6f7f93231eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 19:15:09 2020 +0200

    [sw/makefile] added new target ('upload') to directly upload an executable from the command line

[33mcommit c31cf18680c965f2f6ffe070dd8204b3a533f34b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 19:14:17 2020 +0200

    [sw/image_gen] added script to directly upload an executable to the bootloader from the console

[33mcommit c18d564c6c49d65b946809e4effc6251db5a44f5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 11:24:57 2020 +0200

    [docs] updated change log

[33mcommit 1e7bd7bb9bdc8b8e01546e0d61579647cc8740f6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 11:24:11 2020 +0200

    [doc] updated coremark performance results (when using FAST_SHIFT_EN option)

[33mcommit d4842a01465395ea430b20c1be72420e1928368f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 11:22:10 2020 +0200

    [rtl/cpu] minor edits

[33mcommit 78bee91b1375ff0035e44fe2be9aa32ff07e6422[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 11:07:54 2020 +0200

    [rtl] added new generic FAST_SHIFT_EN to enable fast (but large) barrel shifter for shift operations (default = false)

[33mcommit 99c94460e15b0e6f52f82d0b2c0c4b0db676d752[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 17 11:06:27 2020 +0200

    [rtl/cpu] added option to implement fast (but large) barrel shifter for CPU shift instructions

[33mcommit f90464ae8174000bb51342c52aded868677be0e1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 16 21:24:25 2020 +0200

    [rtl] updated pre-built blink_example image

[33mcommit 077186e141aeee0ba164228112c6f2e624e8dfcd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 16 21:20:22 2020 +0200

    [doc/changelog] updated to v1.4.5.2

[33mcommit 8a54c7bdd9db0aa2843f389638ecbe076933f90d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 16 21:19:56 2020 +0200

    [doc] updated data sheet (mzext csr bits)

[33mcommit bc6b2e89b57d7876f6486b087235af7b83fdcec3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 16 20:14:05 2020 +0200

    [sw/rte] added PMP available check

[33mcommit f91779b74b13086f8ec9dd9768418a685e6d1ec8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 16 20:12:08 2020 +0200

    [sw] added C definitions for mzext csr

[33mcommit cfd0b7bcef6d2d3d004bf08ae3698832f1989473[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 16 20:11:33 2020 +0200

    [rtl/cpu] added flag to mzext csr to check if physical memory protection is implemented

[33mcommit ca4b9200f59c809fa70b60e898ffa18839113d88[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:29:17 2020 +0200

    minor edits

[33mcommit 05d06d31069f83eb3dfd7005029a54efc09bdc28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:19:16 2020 +0200

    updated change log

[33mcommit 9d38b493d64c1818d49225ad4928e3f4176cd942[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:16:51 2020 +0200

    minor edits

[33mcommit 908f5d55a33ff610778d6f74c43ee8715fa36e83[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:16:35 2020 +0200

    [sw/cpu_test] refined illegal_instruction test (now also testing mtval value)

[33mcommit f4239a7858cf8c996c5b880e38a64d4dff463e63[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:15:44 2020 +0200

    [rtl/image] updated blink_example VHDL image (due to modified RTE)

[33mcommit df7ad1793eda211e5bd5049d89940388172f3957[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:15:01 2020 +0200

    [sw/bootloader] updated bootloader

[33mcommit 963ecbc5a426b36e74a31f8557f1cdcee1eeac91[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:12:12 2020 +0200

    [sw/rte] fixed bug in RTE: debug handler was not always showing the correct MEPC value

[33mcommit 423eaaf88b14ffbb124d81d5cd70a6d88b1d9ba8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 19:10:21 2020 +0200

    fixed _unprecise exceptions_ (mepc and mtval did not always show the correct values according to the instruction that caused the exception)

[33mcommit 9f5b2a57e85bcec97c2f8f0e63d722621e2985ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 15 17:02:42 2020 +0200

    [rtl/processor] added assert.warning to show selected Wishbone protocol when using the external memory interface

[33mcommit b45c76731b0c7ed8c0f2de9ea6a3d328bf3a2a54[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 14 20:30:19 2020 +0200

    minor edits

[33mcommit d367142855c530c0170b6d54978fb96bdb9cf9b4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 14 20:29:57 2020 +0200

    fixed broken freeRTOS makefile

[33mcommit e83f0f8bf909aa2ff8cd72632bde4f5d6e1df5f8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 18:24:35 2020 +0200

    linked latest release!

[33mcommit cd48d5a7fb2ed4554ef48dacbf49f8b8f8a862f9[m[33m ([m[1;33mtag: v1.4.5.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 18:04:07 2020 +0200

    updated pre-built blink_example image

[33mcommit 3ddbcfaf02fce0ca75e311d6042411289d898b0f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 17:51:48 2020 +0200

    updated to v1.4.5.0

[33mcommit 8e6234ec8d0a40e2d5807b63657fb986219396c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 17:50:56 2020 +0200

    removed strange character...

[33mcommit 1a5105f0b58abe3e2b00e248f2298d1620d679cf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 17:50:34 2020 +0200

    added official architecture ID

[33mcommit 10dd5314341715d2dea554a556de8e21fa4c44cb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 17:48:30 2020 +0200

    the project has received an official open-source architecture ID (id = 19)

[33mcommit 53780dd4216358e287a0eedc31549cafa12b5e0d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 17:46:03 2020 +0200

    minor layout edits

[33mcommit 16f9e6bf8afa98918578f98defece99e083972ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Oct 13 17:45:31 2020 +0200

    minor edits

[33mcommit a0fd277ab70e3115545cf3c9f2a926af7d8d3958[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 18:05:48 2020 +0200

    added bit reversal function (for later)

[33mcommit 68d2987d02c14883c630121a0c6e554b64412386[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 13:13:22 2020 +0200

    updated changelog

[33mcommit ea7272e2804b6932a3061d9260d71ef86aa81d99[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 12:24:34 2020 +0200

    updated performance results; added section regarding default makefile flags (gcc switches)

[33mcommit 0b0e479f68b4d99d1c2caaa4332aa302f4ff6dc3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 12:22:33 2020 +0200

    updated version number

[33mcommit e0124269b042be97e35632cad85eb203ac284059[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 12:22:17 2020 +0200

    updated pre-built blink_led VHDL image

[33mcommit a5e281c32805286a840a7f55e64fc61645e25097[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 12:20:35 2020 +0200

    no init of ALL registers in crt0 when compiling bootloader (saves some precious BOOTROM space)

[33mcommit 45d5590931ff2565e67adfba1c13c0c50066c44e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 12:07:11 2020 +0200

    minor edits

[33mcommit 4ecb96f8a417a10f8ba98b51ce0b7e0068b40476[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 11:34:27 2020 +0200

    updated performance results

[33mcommit d55a16a8064eae2f8c1f43f7baf27db6feba52a6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 11:32:24 2020 +0200

    added notifier (assert warning) when using external memory interface: shows actual bus timeout latency

[33mcommit d83d8bb6093f94471864a2387ed742c3e74568e8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 11:31:23 2020 +0200

    makefile now enforces 32-bit alignment of branch target (only relevant when using C extension) -> faster instruction fetch

[33mcommit 5fd59c83fa0f49c234ae07c706d7f337d3d3be9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 12 11:30:22 2020 +0200

    crt0 now clears ALL registers if not using E extension

[33mcommit 2e4894933c4921502a7b0c0698ed4bc7f6b0f539[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 20:55:09 2020 +0200

    updated to v1.4.4.8

[33mcommit 0b58c6edf10bad913ff7bf74398c1b4c994f5d44[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 20:53:21 2020 +0200

    updated synthesis and performance results

[33mcommit 28073c37eefae24d28a09a3aaca0a9a12bbe2b80[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 20:53:03 2020 +0200

    added issue engine

[33mcommit aff80a8fced98af4f6d09469861a5d95e06241e0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 20:52:42 2020 +0200

    added new pipeline frontend signals (optimized IF engine, issue engine)

[33mcommit 9c738251fda97ae9660326618a1c1e5332c66d33[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 20:52:13 2020 +0200

    optimized pipeline frontend: simplified IF engine, added issue engine; less hardware utilization, faster instruction fetch after taken branches and jumps

[33mcommit 92798d0674a00fe4e7dfadca5e2f7ca504719984[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 19:42:31 2020 +0200

    whitespace removal

[33mcommit e07f69d3d9a38aced96b3a9d531945e2020a311a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 19:11:55 2020 +0200

    minor optimization in access logic

[33mcommit 488338d9580fd266794157b8e7d93097e49e9f3e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 16:46:01 2020 +0200

    updated changelog

[33mcommit c2bbdff23ff51b9a31725f11179885716edc0c5d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 16:45:50 2020 +0200

    added more details regarding wishbone protocol modes to section '3.4.4. Processor-External Memory Interface (WISHBONE)'

[33mcommit 7a9d89fadedeece933ea30370afbb05387298985[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 16:44:52 2020 +0200

    added wishbone ram support for classic wishbone transactions

[33mcommit 1de7f5d3b82864fb6aaf5ba59d28c749ebadfa1a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 16:44:25 2020 +0200

    external memory / wishbone interface operates in standard/classic wishbone mode by default now; pipelined mode can enabled via packages's wb_pipe_mode_c constant

[33mcommit e5dbabab9f3bc38a1ddfeb2ab2402faca243f12c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Oct 11 11:06:45 2020 +0200

    added PMP cfg-lock test

[33mcommit 23806cfe693a64914d93b3d288e28d186834fb41[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 10 19:47:21 2020 +0200

    instruction fetch address output is always 32-bit aligned (even for compressed instructions)

[33mcommit 84fb5c700f10301d96f63a2ba8fbb99698a89d40[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 10 19:43:06 2020 +0200

    minor edits

[33mcommit c8275554f965ccc391bd0468a0baaf59a5432120[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 10 19:40:58 2020 +0200

    minor format edits

[33mcommit c358ffe831bfda01cb94ba8fcd1fd574e87b065d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Oct 10 18:34:23 2020 +0200

    fixed typo in UART sim_mode flag; minor text edit

[33mcommit 80d2e4aa01b093c304be69011af8f122fda55886[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 9 18:29:08 2020 +0200

    added change log link; minor edits; fixed internal links

[33mcommit 1ed12fa22a225e9cb38070b29a56eda2cf02c32f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 9 18:28:28 2020 +0200

    moved project change log from NEORV32.pdf to CHANGELOG.md

[33mcommit 1dcfe287a32d0385578549c6357c3190d0804477[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 20:42:47 2020 +0200

    added new global bus_timeout_c constant to configure bus access timeout

[33mcommit 9a5a3f3017b84a3de80e3b097b3d0a137b041c8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 20:42:19 2020 +0200

    removed CPU's BUS_TIMEOUT and processors's EXT_MEM_TIMEOUT generics; bus timeout is now configured via bus_timeout_c in VHDL package file

[33mcommit 8b790ca0eafe39f38faa89219f3dad29f5836bff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 20:06:58 2020 +0200

    extended diff in time test

[33mcommit 7070abe1d3f657032aef1fed4eb7f7856c6acd24[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 19:34:06 2020 +0200

    updated documentation

[33mcommit 6f772c3d201f1c3950a001290202c6ecaa699ff1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 18:48:20 2020 +0200

    added CSR pseudo-write access test, where rs1=r0 (no actual write access performed)

[33mcommit b11103666b6e229c95c766268b7067221fa13d8b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 18:47:08 2020 +0200

    removed DEVNULL unit

[33mcommit d06fa845b8ec56b23027de7dee6a6bf13b31ab62[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 18:42:43 2020 +0200

    mcause CSR can now also be written

[33mcommit e696c70ec95c274f21ab459f10194cf18d749e9a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 18:42:01 2020 +0200

    fixed: trying to write to a read-only CSR will correctly cause an illegal instruction exception

[33mcommit a3e90ef73b41e719a44ec44c960db601ecaba8d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:52:17 2020 +0200

    minor edits; removed DEVNULL stuff

[33mcommit ee03d192adbc1dae1a36a841023e418525d36a98[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:50:50 2020 +0200

    edited default UART simulation output file names

[33mcommit 10eaa14543701ffccf1979aa0511ea808f706598[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:50:08 2020 +0200

    edited HW analysis output formats

[33mcommit 11ba18e4928afa2a872ccbef803f835526a09fee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:49:19 2020 +0200

    minor edits

[33mcommit dcf993f423600ff4e002a0b056d9ea0df5e34c9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:48:41 2020 +0200

    relocated TRNG base address (is now 0xFFFFFF88)

[33mcommit ea39d9dec73a76eb88ad89c939c20b68f83261fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:46:19 2020 +0200

    added console, text and data simulation output features

[33mcommit da3481f027c892543071aa78f52d5c973ac59519[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:45:26 2020 +0200

    removed obsolete DENULL vhd file

[33mcommit bc36cc252891dd56d8bbbc3cbc9f8198e4ac1785[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:44:24 2020 +0200

    removed DEVNULL available flag

[33mcommit ccb4a26ca23c56183cc070e608f7a435fed57ab4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:41:28 2020 +0200

    removed DEVNULL and DEVNULL enable generic

[33mcommit 2d896711016f648bbd09c1a456ee39eac71d004c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 8 17:39:58 2020 +0200

    removed DEVNULL peripheral device

[33mcommit 97d87659d5922ce8e23ecba85229896464682138[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 21:04:20 2020 +0200

    writing illegal values to mstatus.mpp will always set machine mode

[33mcommit ad481b9cd03f1590c26398c856c1e5f35d8a218c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:59:36 2020 +0200

    updated data sheet

[33mcommit 3077aefc0b8670d700895a4654614fbc6677334b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:58:09 2020 +0200

    simplified imem/dmem access logic

[33mcommit 2b5858fbbe0668ef13b5a593c50814e3db0f2332[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:54:54 2020 +0200

    updadted pre-built application image

[33mcommit b732c503e8b7239982108ad9c411af6610e02be7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:49:06 2020 +0200

    simplified ALU core function set; removed cp-mux in critical path; control code clean-up

[33mcommit ec3880f7fedf92b6110f4eeace97e828fb928dca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:48:25 2020 +0200

    code clean-up; added CSR address list

[33mcommit 5ddaf4569c04f49bf896968c6db183a07ff73a56[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:46:09 2020 +0200

    added imem/dmem alignment check

[33mcommit f45694c237cd9b6ecb08389a2212407dda1d2a2e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 20:45:17 2020 +0200

    minor fix

[33mcommit 6603a0c513785e28dcb5b53c5702d686a8ddff26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 18:53:24 2020 +0200

    added build time output

[33mcommit 85856710a59d1edd88151950b32a1c723aa119c8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 17:11:07 2020 +0200

    updated ALU input operands

[33mcommit 5deaa342a7330941efb245141ea60e2563b2b39d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Oct 7 17:00:43 2020 +0200

    minor edits

[33mcommit e203f1ffb48ec8b7293ccca2dad2c72e350f2a31[m[33m ([m[1;33mtag: v1.4.4.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 5 19:36:37 2020 +0200

    minor updates

[33mcommit 06c9fa81ba1e2609563d00e2c5be4de6aad3fe4b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 5 19:36:20 2020 +0200

    added external memory interface test

[33mcommit 3492ed77ab0baa0de339cbb6352a1f601242052f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 5 19:35:55 2020 +0200

    ACK signal has to be zero if there is not valid CYC signal

[33mcommit 43785fc05b8e5543d85c65858823da4568bf9a28[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Oct 5 19:35:29 2020 +0200

    fixed error in external memory interface (error during instruction fetch)

[33mcommit b25e152b13228861152629506760d414f566c9f9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 2 18:51:01 2020 +0200

    updated documentation

[33mcommit b06f10fd4f9040846d5848aae6c997455ca6ada2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 2 18:50:31 2020 +0200

    [m]cycleh and [m]instret CSR are finally 32-bit wide (and RISC-V complaint)

[33mcommit 64077cb4ebf5e478425469128a6ae90772b29810[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Oct 2 14:15:37 2020 +0200

    minor edits

[33mcommit b908c7f76c9c189005cb9daa577632eedc7be908[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 1 17:39:54 2020 +0200

    updated documentary

[33mcommit 785e049e21438b1361967cf2dc7ed51f7c4dd934[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 1 17:30:36 2020 +0200

    added missing ALU input signals

[33mcommit 4878e647299546e963fdfb2284121d7e36d45cec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 1 16:29:34 2020 +0200

    ALU optimization

[33mcommit e55d4003c40e0770a1b7321f28a235d329ddfe6d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 1 16:29:01 2020 +0200

    minor edits

[33mcommit bd6d0d02439aae1f0cb13630e2060a63a051524b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 1 16:28:20 2020 +0200

    minor bootloader edits

[33mcommit 04761d69884e01596bca215fef5a64215d8fb29c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Oct 1 16:17:22 2020 +0200

    added write to read-only CSR test

[33mcommit 9f39be4508114d220ebbd044d44a2bd2166fa74f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 30 23:50:18 2020 +0200

    added CPU wrapper with resolved signals to syntax check

[33mcommit 40bb7867bf1e296d3a9755f7eec594ffdc62c0c5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 30 23:49:38 2020 +0200

    added CPU wrapper with resolved port signals

[33mcommit 05bbeea43980ba9210e849dcf30dd63cc601771d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 30 22:42:01 2020 +0200

    minor layout edits

[33mcommit e523024643f19c36ae629cff033d7f1f5dacc040[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 21:11:04 2020 +0200

    added CFU interrupt capability

[33mcommit da2f300e7b13b2f85daec15100e25e78c8f06905[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 20:53:06 2020 +0200

    added CFU hw driver dummy

[33mcommit 0862b2a739cd79102810ac02b95ee36d37afffa5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 20:43:09 2020 +0200

    added (dummy) CFU hardware driver source files

[33mcommit 5135e93aa3eb6fc032f72078eb38035f51662147[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 20:40:10 2020 +0200

    doxygen comment fix

[33mcommit 95254a28f3a99a4e803ecca3c49cebdec7333eca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 19:48:59 2020 +0200

    updated data sheet

[33mcommit 9f6f0400bb912e8baf8e9f3d9cb28016981edcc1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 19:40:14 2020 +0200

    minor edits; code clean-up

[33mcommit 7eeaff8468ff8896e4c868d634ac77471473a884[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 19:38:14 2020 +0200

    updated synthesis results

[33mcommit ff1b233fb83eb5534eaaf106dddb94fa9a6d1e45[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 18:55:51 2020 +0200

    control optimizations; CSR access instruction take one cycle longer to let side effects kick in

[33mcommit c49507e674d90ea2c25a0a35c5c8094413fd6d62[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 27 18:55:14 2020 +0200

    further logic optimizations

[33mcommit af105fdfed454880db1f93bb8e31386c70c11e9c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 26 15:37:12 2020 +0200

    updated data sheet (changelog)

[33mcommit 7d21f1201a2fc816306118657e4fc453bae41ca5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 26 15:36:58 2020 +0200

    removed 0 input for ALU input mux A

[33mcommit 622222a99d407c30581adcbc24c856405557dfbf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 26 15:32:31 2020 +0200

    further ALU operand mux optimizations

[33mcommit 3d67970a7dae1cc2116c30f35e88f0512e2ebf5c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 26 14:13:05 2020 +0200

    updated new ALU operand logic; added more details (operand C mux added)

[33mcommit 7c8956deb156333a3ff8b74a00c10fc21df01292[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 26 12:18:09 2020 +0200

    improved ALU operand logic

[33mcommit aa631f44bc642cfd4ca5f5e1a3821d20f7546ba7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 26 12:05:18 2020 +0200

    fixed error in CSRRWI instruction (introduced with version 1.4.3.1)

[33mcommit 7a819a45f7b9ccca59e6c7c0fcfa89ed28f9b751[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 25 21:28:15 2020 +0200

    minor layout edits

[33mcommit 5ea21027f186e6ab13c655019ab3c2c43d6129b9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 25 21:21:04 2020 +0200

    updated data sheet

[33mcommit 5643c2927ac9be2621f7fba94c03b03f5d80b177[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 25 21:18:31 2020 +0200

    removed zero-insertion logic

[33mcommit f5f8c3b7534fb06f1a0d2c540ea71518cb720b3a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 25 21:17:53 2020 +0200

    r0 is now a physical register; no more zero-insetions logic required: smaller hardware footprint and shorter critical path

[33mcommit a22c1baa2def28503f49d8f56d6cef0db0ac432a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 25 21:16:04 2020 +0200

    added processor top wrapper with resolved port signals

[33mcommit 06234ab597428189f1c06ad813cb5da6c3473561[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 25 21:14:02 2020 +0200

    added rtl/top_templates HDL source folder to simulation check

[33mcommit 4ba218dab99c598ee6a5bd5273b355b8eb7381fd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 24 22:35:38 2020 +0200

    minor edits

[33mcommit 83e5bde937ea896fe5ec84189d94931250d026ec[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 24 22:35:12 2020 +0200

    fixed GPIO labels: in and out ports are 32-bit wide

[33mcommit e82e6358dd58958cf0af6254680b9d55fb169e4a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 24 21:46:57 2020 +0200

    minor edits

[33mcommit ac98989e19547b936563305264195a9feaded4a3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 23 20:53:10 2020 +0200

    minor size optimizations

[33mcommit ce119fc0f6314fefa305eabf69b42dc6225f7124[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 21 20:05:08 2020 +0200

    minor edits

[33mcommit 284c5a9c91da0bdced7532bae35028832961ae92[m[33m ([m[1;33mtag: v1.4.3.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 20:52:14 2020 +0200

    minor control optimization

[33mcommit f3db918d05ed9040990470e5ba925aeeb7df51f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 20:30:53 2020 +0200

    clean-up of CPU's cp interface and bit clear operation

[33mcommit a959149336be4bbfe1476f84fd49501da09a48c7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 19:17:03 2020 +0200

    updated default application image

[33mcommit 9b465c801f6d4390ee101b3ad920fe962872f8e4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 19:16:54 2020 +0200

    minor edits

[33mcommit 21aec8e06fe28a2acc52d724f32c1673987879dd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 19:10:37 2020 +0200

    updated default application image

[33mcommit f88d0af88a7c7b304e04015c3255c8f1f8c8c7d8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 19:08:27 2020 +0200

    added note to unimplemented FIRQ1(gpio) test

[33mcommit e2df91f0fa35f8fa572c12d52c8ef62398c461bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 19:04:48 2020 +0200

    RTE.setup: added warning if CSR system is not implemented

[33mcommit ab55d54882dc6e3d85243ba1e26dc483008b8689[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 18:59:33 2020 +0200

    added option to disable auto-boot sequence

[33mcommit 8e468756ea77dbdef50d23d7bb25ba36b132beac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 17 18:59:05 2020 +0200

    minor edits

[33mcommit 4e0166a04cae93371aaf57a8f24069588cadf82d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 16 20:34:01 2020 +0200

    removed obsolete memory configuration stuff from bootloader

[33mcommit b8dc3a258ea7260c56820786c3f195a47e9be56f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 16 20:33:24 2020 +0200

    updated pre-built exectuable images

[33mcommit 172cd3465a972a5422dd445d125f2e4299340bd9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 16 20:32:58 2020 +0200

    updated data sheet

[33mcommit 90b456772e2192c41d85497256fd93b353b10edd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 16 20:32:41 2020 +0200

    clean-up

[33mcommit ceb12dbed27575104773ecabd6b416e705655756[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 16 20:31:42 2020 +0200

    clean-up

[33mcommit 14a922fe1233affb947cfcd2226ed6dbe691260f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 15 20:40:25 2020 +0200

    fixed HW version number

[33mcommit 424217acb82a6e51a4625013591e1915d7637a30[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 15 19:26:42 2020 +0200

    minor edits

[33mcommit 15d45e7458a2b3ee21e70eae0c4553468aee81ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 15 18:53:00 2020 +0200

    minor optimization

[33mcommit 121e9e58c1ded854f4db1e92609921682645eafc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 15 13:48:25 2020 +0200

    added CFU

[33mcommit b33ecde17d2f2e75eee622df366a5816b6993871[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:44:47 2020 +0200

    removed obsolete memory configuration generics in processor entity

[33mcommit d26e5d4eeb4de3ba127a386c655949b8e1193324[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:12:40 2020 +0200

    updated external memory interface (wishbone gateway) for new memory configuration options

[33mcommit a4e2ab889b073d4f0132f0bbafd5ccea9779fd32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:04:51 2020 +0200

    minor updates

[33mcommit 57f007ed8df8184d3376b22ce82445b3a618ee57[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:04:27 2020 +0200

    updated SYSINFO regs

[33mcommit 3ecc240a6245efcc91b946347fe8e6bf2fe8e262[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:03:58 2020 +0200

    crt0 now uses stack configuration from linker script

[33mcommit cd848850d82055289163a03a3d284634f1032d6b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:03:29 2020 +0200

    minor edits

[33mcommit 8bb47577696dad1925d5f39c906677649486e772[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:03:09 2020 +0200

    added default CFU signals

[33mcommit dc0cbecb4f1c68458db7d8db626bff1ed90b5797[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:02:52 2020 +0200

    package update for simplified memory configuration

[33mcommit ec549ece4f4b150b8f1c5e0cbab6581a673ff667[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:02:25 2020 +0200

    sysinfo now provides info about IMEM/DMEM size instead of I/D space size

[33mcommit fa5112a4e260c6ef3a8b713202df168936cf9496[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:01:02 2020 +0200

    simplified memory configuration (removed I/D space_base/space_size generics)

[33mcommit 2ca0507410c0780aa96507313ee4b6f116e8d226[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 22:00:43 2020 +0200

    simplified memory configuration (removed I/D space_base/space_size generics)

[33mcommit fd321e33b726c0547aeee0ac8d0b92f4148b8048[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 21:59:55 2020 +0200

    added CFU interrupt (cia fast IRQ channel 1, shared with GPIO)

[33mcommit 64407d40825a4f0d3832b1852b2a0a0c33bac691[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 21:57:40 2020 +0200

    linker script now provides stack init

[33mcommit ae340b31984079e2fb2347ebe0e06e5f51931fe6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 10:03:56 2020 +0200

    updated docs

[33mcommit 8386169176379d8d7f9a55adc5ad2a31dab24f06[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 09:39:52 2020 +0200

    minor edits

[33mcommit dfcb4a8a4ebba8e64af5b119c8a75ae4f55e5564[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Sep 14 09:39:18 2020 +0200

    output format edits

[33mcommit 91fa2cbe71afc7e888f9f2e9595ad203e59387e5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 13 21:22:58 2020 +0200

    added CFU interrupt option

[33mcommit da0c35f18ac6a0787c879ffd6881ffbec83959af[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 13 20:47:12 2020 +0200

    bug fix

[33mcommit 4674aab2dc8d76f80c86bad3537efd8c8b33c1ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 13 18:33:21 2020 +0200

    added CFU regs and made GPIO_INPUT writable (for pin-change IRQ enable mask)

[33mcommit 0ea028ea19df00650b3e0b46ad44d253a9eb3a08[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 13 18:24:58 2020 +0200

    added function to configure GPIO.pin_change_irq_en_mask

[33mcommit 42706d2261477ad93944e7ec64f7541787eaddc5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Sep 13 18:24:21 2020 +0200

    added mask register to select which input pins can cause a pin-change IRQ

[33mcommit bbc824a2234ff31392f4c9c0a2e6c4a4881bdbfb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 12 20:17:21 2020 +0200

    added CFU available flag

[33mcommit 25a7bcb47f96151b56ce3a4686d8750c876c8632[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 12 20:16:28 2020 +0200

    added CFU; optimized timing of processor-internal clock generator

[33mcommit d311f177cc92af20e7a3815f95218ec52451bdac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 12 20:15:44 2020 +0200

    added CFU enable generic

[33mcommit c5d31f2b903bf18fa1e0fd869720da2d69618ccd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 12 19:51:48 2020 +0200

    added cfu to sim sources

[33mcommit 6adb3ae325becfb437176555fbe09e07ae31bd58[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Sep 12 19:51:10 2020 +0200

    added new IO module: custom functions unit (CFU)

[33mcommit a5a20e0efef3ac352f80cdaa67fe5c8b75d8c344[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 22:55:32 2020 +0200

    removed CSR_COUNTERS_USE generic

[33mcommit cfa3413d03aa99b4ebf91ef8aa31b42a8dd5f38c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 22:54:12 2020 +0200

    removed option (CSR_COUNTER_USE) to disable CSR counter since they are mandatory according to RISC-V spec

[33mcommit 1d2058e91d9986a9b9292788b6147af6e779a645[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 21:37:04 2020 +0200

    updated pre-built images

[33mcommit 622d58f8582634fccd39cede713526aeb710b72d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 21:36:06 2020 +0200

    minor edits

[33mcommit 77191df181d777442dda97e1ca8ebdb94b140c94[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 21:35:32 2020 +0200

    updated data sheet

[33mcommit 6940ea3caa112409da5df3fb4c68a3b2ae10f9bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 19:22:11 2020 +0200

    minor optimization

[33mcommit d991ae8b84ad9da9540f65186a37f5b3bdf63db8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 19:09:06 2020 +0200

    minor edits

[33mcommit 310c0247cd6f7e1bf0bf29d99b43e77abd3b97e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 18:18:42 2020 +0200

    added sources (module) to the fast IRQ display of the NEORV32 RTE

[33mcommit 6ded2853b3982a3efceae6c1b0ff274a638d9b07[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 18:00:37 2020 +0200

    modified GARO tap masks

[33mcommit 863036a81ffd945f9fbb615377dc9e83aa45a55a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 18:00:12 2020 +0200

    updated TRNG-related example programs

[33mcommit ecb803079bfc61d04f975635012bf5679497ccc1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 17:59:26 2020 +0200

    updated software components for new TRNG setup

[33mcommit 89f64c956935b3b5f90679da30d56fbc5bdd58cd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 15:03:38 2020 +0200

    reworked TRNG architecture and interface

[33mcommit c0cc9f269b827f2e232494cd02ad76851011f93d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 14:48:39 2020 +0200

    made sanity checks concurrent

[33mcommit 414b9d5024d8d954f20e7421ac6e6d7a43652eef[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Sep 11 13:24:24 2020 +0200

    minor edits to prepare CPU for B extension

[33mcommit 4560c4d72c86fff9c05f4ef5251a07f79e8f9492[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 10 16:55:02 2020 +0200

    added simple example for mixing C & ASM (assembly version of blink_led)

[33mcommit 6644b0fa091ba2eb0e67d7295d121d5ef8929ebf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 3 23:36:57 2020 +0200

    minor optimizations

[33mcommit 7c41aff32c8a88f90263cadbfaccc6566f15fce0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 3 22:42:59 2020 +0200

    minor edits

[33mcommit 00e81bdbf4b8a2b2e43badff94b55881ea97bd2f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 3 22:42:37 2020 +0200

    changed external memory interface register stages configuration to default value (=2)

[33mcommit 957ce7c82c0a002566b220ed95e9b1ea5f4f86a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 3 22:42:02 2020 +0200

    added more details to section regarding processor address space and external memory interface

[33mcommit 08ba82e08d1d3534a5b1041d2141fb5dc05693dc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 3 13:22:32 2020 +0200

    minor edits

[33mcommit be2b588cb5a9685823a16d9e496c8f35240d9a7c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Sep 3 11:13:02 2020 +0200

    typo fixes

[33mcommit ae096559cb82da1e5a4503e281430f0755ee125d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 2 21:06:00 2020 +0200

    added external memory interface signals

[33mcommit 652a63a2039fe6fa8e02aaca6f8e9e05a2d27f00[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 2 21:05:10 2020 +0200

    added more details to section 'processor address space'; fixed memory space configuration generic names

[33mcommit bcd23c1234de5aa9a90d3c4aa7d50e6ef688408d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 2 21:04:27 2020 +0200

    fixed bugs in external memory interface

[33mcommit a9ba93eed6a876957b8e1ec7147f12e08a1fa97d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 2 20:05:33 2020 +0200

    minor edits

[33mcommit 6845509b3d458e3e1542ee61591770c6141ac11b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Sep 2 20:05:01 2020 +0200

    added option to configure latency of simulated external memory

[33mcommit 4851eaabd64ece7793d5f77c5d423e75daa36f51[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 23:24:46 2020 +0200

    updated bootloader image

[33mcommit d601e560dee7ed5e8a36118ac9f7f448f0c501ed[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 23:20:50 2020 +0200

    fixed error in GPIO access functions (they were still based on 16-bit GPIO ports; ports are now 32-bit wide)

[33mcommit 04d7f25d4368a39f90336968bbff86020fe16089[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 23:15:50 2020 +0200

    minor edits

[33mcommit 10485e628ee261294a7dbe8f2fbf2cb8cffac8c1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 23:15:27 2020 +0200

    added FIRQ tests for UART, SPI and TWI

[33mcommit 7c530d6c1d81f335acdd329df48c63bb35c54ce2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 23:14:53 2020 +0200

    code clean-up

[33mcommit 4e1868a7bb0ffefb7100099ec9b92c6c7e76d2d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 23:14:31 2020 +0200

    added test-if-busy functions for SPI, UART and TWI

[33mcommit 46005681e622e41833ffcbd6a45f52b4c7d92c32[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 22:03:55 2020 +0200

    updated documentation

[33mcommit e057b213ffc5ed16a56baf433cc309dfadd4c828[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Sep 1 22:03:30 2020 +0200

    fixed issue: using regs above x15 when E extensions is enabled will now correctly trigger an illegal instruction exception

[33mcommit 92df5839a74e83c577f71dc17451d3ade1ad8af4[m
Merge: 2044eb08 c5d86669
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 31 21:08:31 2020 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit 2044eb08d5f8d8f7d5701f385bce32af038f7cfd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 31 20:41:37 2020 +0200

    added README for SW folder

[33mcommit 446532c0848e53e073aec1641439148036999c09[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 31 20:41:12 2020 +0200

    minor edits

[33mcommit c5d86669c817b663a895b13324b915104e8b4187[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 31 20:39:15 2020 +0200

    Added CONTRIBUTING.md

[33mcommit d6f1ea4bb7d78f108a56153470f3d0b091265fb0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 29 19:03:00 2020 +0200

    Minor edits

[33mcommit 1f85fe47721ce347ec77866e47c1c504bc36dddc[m[33m ([m[1;33mtag: v1.4.0.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 29 16:47:00 2020 +0200

    updated documentation for new version

[33mcommit 4671e35559426e62a2de6c47e9280651fff8036f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 29 16:46:41 2020 +0200

    updated pre-built bootloader/app executable images

[33mcommit f826692b2b46e86e27c4fb937d8104fbca81934a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 29 01:44:27 2020 +0200

    added note on conditional rom section assignment

[33mcommit da34a6714180d7eca54aef8fb45557806ad5d82a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 19:00:01 2020 +0200

    minor edits

[33mcommit a76566f647859e777e603ba655221222c776bf0d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 18:59:36 2020 +0200

    added README for rtl folder

[33mcommit 916bae5aebf2ff70291245b986648c8990f75554[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 16:54:31 2020 +0200

    minor edits

[33mcommit 9935c7943620acf4d9d317be47c1ef6c9645125c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 09:18:35 2020 +0200

    GPIO ports are now 32-bit wide

[33mcommit 34ea286c06ec7dbdaf0190d86d3c62a95eb7206a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 08:32:11 2020 +0200

    peripheral/IO devices can only be written in full word mode (=32-bit)

[33mcommit 9d70f5017af6a7a4ee40a9b68d7acd3e2f2864f0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 02:12:21 2020 +0200

    minor edits

[33mcommit faecb6c31a0d4abb54f8565975415169febe4923[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 27 01:08:30 2020 +0200

    added readme file to explain ci scripts

[33mcommit 92c4aa40d6c34ab7dbb71df684a5614fe379fbd5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 26 20:17:18 2020 +0200

    added option to disable status LED

[33mcommit 57dbd602e7e6fd6853f6de64315b03be7e881fc5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 26 11:46:05 2020 +0200

    updated pre-built executable images for default application (blink_led) and bootloader)

[33mcommit 7d03260a1e16b4ed4ed70d2b64269b239acd4019[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 26 01:13:50 2020 +0200

    faste synthesis due to less copy iterations during memory initialization

[33mcommit e6b372f8a3f588ddf116b31ef079fbbf0c4c8644[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 26 01:12:41 2020 +0200

    updates; generated image arrays are now constrained to actual executable size

[33mcommit 87ffb2e148a5ac083ff73a0b911f47e7bd0b3a72[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 26 01:11:50 2020 +0200

    fixed broken freeRTOS makefile

[33mcommit ec693bd3afe0789b6a853b2b9becdce057cefab7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 22:29:14 2020 +0200

    minor edits

[33mcommit 1d16beabc60883f45bf1bfafd27b78bfa98e74f4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 22:25:55 2020 +0200

    minor edits

[33mcommit 83af444eb8992b4c3bac4e4c8597b6c5f8a3a816[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 22:24:59 2020 +0200

    bootloader can now have .data and .bss sections; replaced pseudo global variables by actual global variables

[33mcommit 09f8942a59a28c2403624d8ba46c402f4570851d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 22:15:15 2020 +0200

    cpu now also shows project license

[33mcommit beacc708b1bae7faaf1c40c16b2356a0ee46f8a6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 22:13:06 2020 +0200

    removed bootloader-specific exclusions

[33mcommit 434e7c1d2b87ce2fba83b7fa7dcadec8cb5ce10b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 22:01:43 2020 +0200

    added function to print the project's license

[33mcommit 3823536d3bcc213ee746be89e2ef001fb962197d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 21:49:50 2020 +0200

    updated makefiles; compile flow only uses one linker script (neorv32.ld)

[33mcommit 158fa66069878956743a4a2aefab832fabf2070e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 25 21:48:50 2020 +0200

    removed bootloader-specific linker script; neorv32.ld is used now for bootloader and application

[33mcommit 3ba2101757171fb755da78c03a82da03148bda35[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 24 19:25:20 2020 +0200

    minor edits

[33mcommit 976b5581446e0919626c649d41872c14268bde60[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 24 18:53:52 2020 +0200

    added simple FreeRTOS demo

[33mcommit eafc186fc611068ca60583eb4dfcddc615d7bc71[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 24 18:41:38 2020 +0200

    minor edits

[33mcommit 76ed568ab54ba21be94f2481965383522b2509a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 24 17:58:30 2020 +0200

    minor edits

[33mcommit c5c34abd9e29a0b819f706bd700299b7d22a27fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 23 21:15:40 2020 +0200

    minor edits

[33mcommit 31826d3cfa2fd44c477cccb53be13ca9b7c6b867[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Aug 23 19:57:30 2020 +0200

    updated documentation

[33mcommit 583bbf59da71b949a3fd42d39094d0c02fd65257[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 21:24:52 2020 +0200

    updated pre-built bootloader image

[33mcommit e3ba2cf4dc1cb24f2e5d6f19db302c1266f24335[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 21:24:33 2020 +0200

    minor edits

[33mcommit 68cf5571b8127d8c52e49f1ceb455a4743ea6887[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 20:38:11 2020 +0200

    multiplier's FAST_MUL mode is one cycle faster now

[33mcommit 4ef40de246c0ec096266f2bca24640586bc91e64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 16:32:34 2020 +0200

    updated bootloader (minor edits)

[33mcommit d42b83b294cd2f79a189b23c2e1b58788530a0ff[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 16:31:42 2020 +0200

    RTE hardware info now also shows Z* extensions

[33mcommit 66c9fe3d8dd5624232ce387fd8cd25540bc3bf11[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 16:31:18 2020 +0200

    added mzext custom CSR to sw lib

[33mcommit 78d1ea8b7d7180195c7cd51c70c75d1a5b9e5f26[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 16:30:27 2020 +0200

    added custom 'mzext' CSR to check for available Z* CPU extensions

[33mcommit 02b88757680befd7f76e789e9c50e084d4ca045f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Aug 22 16:29:41 2020 +0200

    added warning when using the bootloader with a read-only IMEM

[33mcommit 335585c3c91e609251c1a60eae8bc7b85d664c1e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 20:55:11 2020 +0200

    minor updates

[33mcommit d710a8247c4e345b2ec4cd859af4c8c6ed4c1341[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 20:52:25 2020 +0200

    updated version number

[33mcommit d0e553e9147f031eb1b1f8c4c1987222857b820e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 20:52:07 2020 +0200

    minor edits

[33mcommit 76457ba43d3ec85306b78bc82005f31d16e96ae4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 20:51:34 2020 +0200

    replaced 'compile' target by 'exe' target; 'compile' is still available for compatibility

[33mcommit aad727f36f40fbe4565ca4cfca167ca9f821a3c4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 12:10:45 2020 +0200

    minor edits

[33mcommit c0ed792b93699fddcf7f82dd4a27b4294714d3d3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 12:09:47 2020 +0200

    updated pre-built bootloader image

[33mcommit 9ed7b84d1a579b9c921b9dd062f5ceec44eea95e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 20 12:07:18 2020 +0200

    added runtime note to show CoreMark has not been actually compiled

[33mcommit 69df88a4ce38845b027180ee909d045db61665e8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 22:22:54 2020 +0200

    updated due to new makefile options

[33mcommit 60ecd3a89e0758d78e52faf413dd9b19b2b5defa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 22:22:35 2020 +0200

    minor edits

[33mcommit 498e7799d1ae260078781ed192f567f928ac865b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:18:14 2020 +0200

    no more clearing of IO devices - done by std crt0.S

[33mcommit 5fef7c3567085cf225fd1c0c0ecb512f44ce5b64[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:17:45 2020 +0200

    typo fix

[33mcommit fb6ba170489a7032b9410d19c54c0bead81e37fe[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:17:07 2020 +0200

    minor clean-ups

[33mcommit 1c4adc5108160ea7e98e7a2e58fc524080a6485b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:14:53 2020 +0200

    updated makefiles; now also supports assembly and *.cpp source files

[33mcommit d1ebd62d88386066840430d77d1306a8173c6ea2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:14:03 2020 +0200

    made linker scripts more general

[33mcommit fed3c5770a3b7cdda3b08ebeaef24a558cbe5f65[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:13:41 2020 +0200

    crt0 now provides option to skip normal application preparation (for bootloader)

[33mcommit 36130292498f04ad1b227af3174a8b85d26fece1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 19 21:12:57 2020 +0200

    removed bootloader-specific; bootloader now also uses std crt0.S

[33mcommit 9bb75774f8b808273d611b870c1b1518f9de9c76[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 17 14:38:12 2020 +0200

    added probot-stale config

[33mcommit 54283c13cddfe518aa7f67d2702ba1bb2db777b5[m[33m ([m[1;33mtag: v1.3.7.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 14 18:05:00 2020 +0200

    minor updates and fixes

[33mcommit 5ddac31247a963ea89e1ad3975fe425371b1e42e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 14 17:44:10 2020 +0200

    updated performance reports for newest hardware version

[33mcommit 0b8067ec761d202fd7699c566081f8b022454d70[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 13 21:14:47 2020 +0200

    further optimized CPU's fetch and execute engines; instruction prefetch buffer (ipb) is now a true FIFO with (default) 2 entries

[33mcommit af2de3ca4837089db140ec8cfcadb7395db13a08[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 13 20:14:40 2020 +0200

    minor edits

[33mcommit 3d9be6173b25e9089651dbc31a6d99dddcac4836[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 13 20:13:23 2020 +0200

    fixed init of lowest 16 registers

[33mcommit 636b5a2f409725690269b0f1a09036e37ffe3343[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 13 20:12:58 2020 +0200

    made CPU arch output a one-liner

[33mcommit a436a47ced8a83b1581dd1fd75f624085c2d9341[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 12 21:17:53 2020 +0200

    minor edits

[33mcommit 968eb68c2cce4024ad538fce9177e7047a93dbd9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Aug 7 20:46:42 2020 +0200

    minor edits

[33mcommit f2e76176d86a4c129847df41a9c3cf921ba2fa0b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:41:07 2020 +0200

    updated documentation

[33mcommit d931430dd5251205b42ffa14f957de6839efbf60[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:32:47 2020 +0200

    removed cycle/instret CSR tests; added output to show number of executed instructions and cycles to final report

[33mcommit c5806dad9e6cb29695a03aeb704f21349d617d4d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:32:03 2020 +0200

    new HW version 1.3.6.5

[33mcommit e502bf3bb7b63269377244c2386ef0d52cf29d24[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:31:35 2020 +0200

    clean-up and minor size optimization

[33mcommit 3dc65a91aa9dc5174d6955db5be18fa1a111a9fa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:30:51 2020 +0200

    added fast_mul generic to testbench

[33mcommit b91056de34fcc43db35fe7d49dbbf9c8d192027a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:30:15 2020 +0200

    minor edits and updates

[33mcommit 8d3c57d1909c84b800f2063bc84f580652703c6a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 20:28:50 2020 +0200

    crt0 only initializes lowest 16 registers

[33mcommit 99afdd3123a24dfe28aff1aa991471671166298d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 19:09:21 2020 +0200

    shift operations are 1 cycle faster

[33mcommit c0b55f2d901af3a0b37aede1abb24e20c95a42b8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 19:07:07 2020 +0200

    removed check if E CPU (only initializing lowest 16 registers now)

[33mcommit 4f604e3a236862eefd8c067c813e640f905e0629[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 19:06:39 2020 +0200

    simplified makefiles (using implicit definition of libc)

[33mcommit b88f7a2d96ada4ab3e3938743bc26c81546c35b8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Aug 6 14:53:24 2020 +0200

    added option to add another DSP register stage for timing closure

[33mcommit e79b64265b288eb7b3ce4faf8122e45f84bae1ea[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Aug 5 20:21:09 2020 +0200

    added option to use DSPs dor M extension; fixed problem with shifter and multiplier parallel operation

[33mcommit 4708465e8a8fd6f07c60564c6522d503e841fb06[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Aug 4 22:38:04 2020 +0200

    minor edits

[33mcommit 7a760cc097f968f6b63e20f48e991f0684e88c77[m[33m ([m[1;33mtag: v1.3.6.0[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 21:45:05 2020 +0200

    minor edits and updates

[33mcommit e1fac4d0700e02318a9335bb536866cb15838e79[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 21:42:30 2020 +0200

    fixed error in goto_user_mode function

[33mcommit b034d734ddafb67c58356c8557f0d74278b86620[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 18:15:15 2020 +0200

    added RISC-V logo; minor edits

[33mcommit 4bbe7c3ecd6ea02f66065b68f18bf0aca52008a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 18:09:00 2020 +0200

    minor edits

[33mcommit c2dc3be0b2e8739110f000bcd1d8b38a4d5599cf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 18:08:38 2020 +0200

    enabled C extension by default for test setup

[33mcommit 7f865a8faad9960299e7840b1506f5eee314d4af[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 18:08:14 2020 +0200

    minor edits & clean-up

[33mcommit 26607fb69749c18b70667b00c8459bc8de00001c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Aug 3 18:07:35 2020 +0200

    relocated DEVNULL

[33mcommit eb1fc5d3126b5d0ff79f175edffb63b61f671932[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 21:25:30 2020 +0200

    minor edits and clean-ups

[33mcommit f2537df43ed4fcae442f938422dfd2de745a8aa4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 21:25:04 2020 +0200

    using explicit IRQ enable

[33mcommit 575c77900d80b2e05433d2aa183bfb0ebbf7fddf[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 21:18:59 2020 +0200

    removed automatic IRQ enable/disable functions from RTE install/uninstall functions

[33mcommit aab978a6254bff4ad9d13c7e06aeb6a52669b0a8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 21:06:39 2020 +0200

    added register stage to PMP mask computation to shorten PMP's critical path

[33mcommit 8f0cd29b2c2ec6028f16e4a6cfb83554f63982d5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 18:44:33 2020 +0200

    minor edits and updates

[33mcommit dc4aebded46dc88f4993aa8f544c539d2f2f0504[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 18:43:10 2020 +0200

    fixed bug(s) in PMP mask generation

[33mcommit 55d60c4e7c92f7e2c7486f5652dbb44bbc0038de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 30 18:42:34 2020 +0200

    misa.Z is not yet defined by risc-v specs: Z bit is read-only and read as zero

[33mcommit b37ac5196cfccf3b515268b650176610454b7a12[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 19:09:30 2020 +0200

    updated documentation for HW version 1.3.5.0

[33mcommit 10545eb447e828a7fe1202e276f0a999f95dfaaa[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 19:05:42 2020 +0200

    minor edits

[33mcommit ed85da5589d4cdee41fc529866362c0e79b5eef2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 19:03:51 2020 +0200

    added PMP test; minor edits and fixes

[33mcommit 4e561519b4f51b7c4d7c800163c1da46d3d3df21[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 17:25:24 2020 +0200

    fixed dynamic generation of pmpcfg CSRs

[33mcommit 6e5eba8d43245abe74bd36372dc3d7b9b698796c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 17:24:42 2020 +0200

    added null-range check for *_all_f functions

[33mcommit 1fe6d5a87344005ae7b24e723bcb464d0ed58a4d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 17:24:15 2020 +0200

    added PMP granularity configuration check

[33mcommit dfa53daa5ab935d5d6bda17c7cc78ae2701136fc[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 17:23:24 2020 +0200

    added missing sensitivity signals

[33mcommit bdc54e8c5ad6889d38e7c74e607c4c0c1c5a4eb3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:43:08 2020 +0200

    added PMP signals

[33mcommit be1ca4ff5a7ee86fdd22e0a2636287a79a0726fb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:42:47 2020 +0200

    added U and PMP options

[33mcommit 5bf93f6174ce8e5adb86c87b96ec60da74352643[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:42:32 2020 +0200

    added U and PMP options

[33mcommit 725ffb28948483e2f349b9f09366774567fc65c6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:41:40 2020 +0200

    added CSRs and bits for user mode and physical memory protection

[33mcommit 167fceeccc48b1295da9e7bb4843e057d4eb840b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:41:10 2020 +0200

    added generics for PMP configuration

[33mcommit 03f1d2daa4423d29be09ab216d2730abf55c326e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:40:39 2020 +0200

    added user mode (U extensions) and risc-v physical memory protection (PMP)

[33mcommit 49f547f139ee8572331adc5bead3a6360d7feca7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:37:58 2020 +0200

    simplified NEORV32 RTE debug handler output to a one-liner

[33mcommit e5cc7098b58cbbcb2b74e3d970ecee78de349008[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 29 16:37:18 2020 +0200

    added (test) function to switch to user mode

[33mcommit 819136bd4f54bc6889eef0e459d6d1869547bc31[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 14:37:38 2020 +0200

    fixed typo

[33mcommit 855c7436ca1ca142db3fd9c035e9a27a37777842[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 13:55:44 2020 +0200

    updated documentation (HW v1.3.0.0)

[33mcommit 50b5f61a8f8b8f4787c6bc465ef3792f46d9c342[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 13:54:32 2020 +0200

    minor edits

[33mcommit adf3d959572f8aa612a144d5dcd6cdb3659a9a61[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 13:54:08 2020 +0200

    fixed resolution & crappy rendering

[33mcommit ac93563ba305848cb5bb0f1dcb3f064e4015e0a6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 11:03:51 2020 +0200

    updated for new gcc-10.1.0

[33mcommit 17ad7731554ff144d441868e0343266b1d9404b7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 11:03:14 2020 +0200

    minor edits

[33mcommit 4f6cffff5b62efad30f2e7063e36073c4753a92b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 10:12:26 2020 +0200

    upgraded to newest gcc-10.1.0 rv32i(m)(c) toolchain

[33mcommit f7a8226e5636c6b6ce018a26e2db3fd191055b7d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 10:11:41 2020 +0200

    added IRQ signal, some clean-up

[33mcommit ac58a2b4a7c34e035ada49b68d64f83fc0915754[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 10:11:02 2020 +0200

    updated bootloader using newest toolchain (gcc-10.1.0)

[33mcommit c1c4e47e7e08d5fa21c167c5aba1b3215f781be2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 25 10:10:03 2020 +0200

    fixed bug in crt0.S dummy trap handler

[33mcommit 491ea7fcc107b5736da113f9e38270c58268fd10[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:45:23 2020 +0200

    minor updates

[33mcommit 651510bf0255e26e1213a2043fc50d3291ae7897[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:44:45 2020 +0200

    updated for new CPU fast IRQ channels

[33mcommit 8eb2b07e9e649a44d0d8c3abb6951eade5656b5d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:44:13 2020 +0200

    updated for new RTE

[33mcommit 2f196e31352846026188cacb22da83c716c132ab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:43:30 2020 +0200

    mcause is read-only; added 4 custom FAST INTERRUPT lines to CPU

[33mcommit 11f30c788561240e4b2b3fdc0f9c3230580ffc9e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:41:55 2020 +0200

    updated and optimized RTE - smaller, faster, better ;)

[33mcommit 8ade8ad17029dea06a077702e6614ff198c6a878[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:40:52 2020 +0200

    updated bootloader

[33mcommit 2f04d774949024e22d3514e6ec4b99263b8e87bb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:38:29 2020 +0200

    removed CLIC module

[33mcommit 17bc12a1ea26f738aca300c7a3233b6f2b267c9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 23 21:13:24 2020 +0200

    update to use most recent pre-built toolchain

[33mcommit b0e94fcebff2c96548072fe90eb1c55d619d9a56[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 21:37:03 2020 +0200

    updated documentation

[33mcommit 4266091267267d6b153d93cb344f3c0bddcbfc0e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 21:36:43 2020 +0200

    minor edits

[33mcommit 90c698c76a0e787587b0637f7c92a0cd49b3ec96[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 21:36:22 2020 +0200

    fixed bug in bootloader autoboot timer

[33mcommit b0d0287773ab289d320388e56a74db8c04fe444d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 20:46:28 2020 +0200

    minor edits and optimizations

[33mcommit 0338f9f00ef24d83db41efc0422bf742184abbc8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 19:42:16 2020 +0200

    optimized fetch unit; faster instruction issuing for compressed instructions

[33mcommit ceb5009b60b27b93491d6e29b1709c05938ca93f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 19:41:00 2020 +0200

    minor edits

[33mcommit 581535009ae5768a4545d2f7d7bfafb0158598b0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 19:39:45 2020 +0200

    minor formatting edits

[33mcommit 3786bfe06fce6d34acb9a621a76e208353426dba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 19:39:21 2020 +0200

    no more writes to dummy register x0 - not really relevant, but maybe saves some energy

[33mcommit 819f0488c304b0b52d0e369d6224309b6c332b57[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 21 19:38:12 2020 +0200

    added Zifencei extension option

[33mcommit cc336101be3ab2e7bd22eeac7fdb17fc7170d51c[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 22:34:56 2020 +0200

    removed std libs stdlib, string and stdbool

[33mcommit a8e6ad0546bf2a3acbcbea7deb67b90325d65e82[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 22:32:54 2020 +0200

    added test for illegal compressed instruction exception

[33mcommit b665c2876b20feb66ed19f4fc72ea36108bd9f09[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 22:32:30 2020 +0200

    cpu goes to power-down mode if bootloader's boot attempt fails

[33mcommit 4a2f859e0769da6a398253129e75a56eb4467637[m[33m ([m[1;33mtag: v1.2.0.5[m[33m)[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 15:03:19 2020 +0200

    updated documentation

[33mcommit 2f04af6af8d2645d43d2bc2bb9c684dbb52cc6e9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 14:49:03 2020 +0200

    less cycles required fro filling the instruction prefetch buffer(s)

[33mcommit 40f8ad1b5ba4a73895dbf6b4a48132e265f7b041[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 14:21:50 2020 +0200

    minor updates

[33mcommit 09fe942d9c3304c630cb23df3d6a299d10aaa104[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 14:21:35 2020 +0200

    bootloader now shows user code instead of hart id

[33mcommit a2a3bd54932aec0c25855857162cd27ed8695254[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 13:43:55 2020 +0200

    minor edits

[33mcommit 72a49f94a019930f482c4dd571aca2303f91ca48[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 13:43:33 2020 +0200

    added SYSINFO module

[33mcommit 407be9856db0395c4bf86df26e058eb978477214[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 13:42:59 2020 +0200

    minor edits

[33mcommit 24331f874331c60d4e8d99012c81331c2772f805[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 13:42:40 2020 +0200

    minor edits

[33mcommit c55e3d18fb7a8819a223e2ea66be7ee25dfb5a7e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 20 13:42:10 2020 +0200

    added custom USER_CODE generic; can be checked by software via SYSINFO

[33mcommit 03b1bb1a9e936181451272c946c4e494ef8d352e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 18 14:10:01 2020 +0200

    minor edits

[33mcommit c6896830e490a9feb6171a4cfc5afbdb4ec5cae9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 18 14:09:09 2020 +0200

    fixed bug in bootloader 'r' command

[33mcommit 804dd97015f027841c382f593be51727cf2ca8b5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:41:04 2020 +0200

    added SYSINFO module

[33mcommit 8430617464a5982f2eaf02a1c252a604c2858f8d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:40:50 2020 +0200

    crt0 now takes HW info from SYSINFO module

[33mcommit d098e1d6b1d33570092bb840ebf36e835874cbc9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:40:10 2020 +0200

    updates

[33mcommit 33d74f46043c6542539e8609a33f089056ffa25b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:39:54 2020 +0200

    minor updates due to new SYSINFO component

[33mcommit 24aaad85f596f67078019dedf871e54707df6f46[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:39:11 2020 +0200

    minor updates

[33mcommit da5b86463c51eb7779beffb44e3110ab44a9beab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:38:40 2020 +0200

    processor-specific information is now obtained from the SYSINFO IO module

[33mcommit 8ae891352a9506089dc0977f94d4c43a196cdb21[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:37:52 2020 +0200

    removed custom CSRs - all processor-related information can now be obtained from SYSINFO IO device

[33mcommit 5895df0f03fdcd7535c294c0594742479be94179[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 23:36:42 2020 +0200

    fixed bug in bootloader trap handler

[33mcommit 4c491d9326d143406d4e5aed7b511ed9b0557bb0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 17:23:09 2020 +0200

    updated cpu overview - CPU now has separate I and D interface busses

[33mcommit 92397b1e8b5558f12f6c1cd3921683a1129ee0cb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 17:22:24 2020 +0200

    added busswitch; CPU I and D interface ports can both access the procesor bus; data accesses have priority

[33mcommit 9859878aab4821fa5e0e1f17f7dd5b1f34d00a06[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 17:21:35 2020 +0200

    CPU now uses separate instruction and data busses; [m]cycleh and [m]instreth are contrained to 20-bit

[33mcommit ceeffb3587a2bd43de838692135e2110aab5692f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 17:19:37 2020 +0200

    doubled speed of ALU.shifter unit (again)

[33mcommit 9510f52d1caabb66eea4f2688d7044c4e31bc438[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 17:19:04 2020 +0200

    added experimental switch to enable DSP based multiplication

[33mcommit ee790a973d5956488b8c16c1462109842d3769c0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 17 17:17:56 2020 +0200

    coremark now only uses performance data from the timed core to compute the average CPI

[33mcommit 42901e2f16118e8476817899219c51d36ba806e4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 16 19:40:18 2020 +0200

    bootloader now resets mcycle[h] and minstret[h] right before booting the actual application

[33mcommit 5db0155c064693ff0be1af1bf2f261af57ae6109[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 16 16:14:48 2020 +0200

    fixed wrong argument types

[33mcommit 192b9a3b1ada575fdd6711b8921ed3a2cc7cc603[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 14 13:54:48 2020 +0200

    updated doc for new hardware version

[33mcommit 0080bf17e262543a3d7403b2d8e7e009a0b44426[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 14 13:54:27 2020 +0200

    hardware optimization

[33mcommit 66308d77a010d8942294bbe78a74369dfc580ca1[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:19:47 2020 +0200

    removed HART_ID generic

[33mcommit 6f68b6461b4d0274b5ce0ec0d479cc632a2dc1eb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:05:02 2020 +0200

    removed software interrupt function

[33mcommit 9792a59a04cace034c6398609b30fff1bb3776a0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:04:27 2020 +0200

    updated for new hw version

[33mcommit 3be3ff2f2288dc73761f5aea9953799f514f4315[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:02:44 2020 +0200

    removed software interrupt test

[33mcommit eeb10b882d3de0fa89092c9ccbef0aeecd14e482[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:02:13 2020 +0200

    added alignment for trap root

[33mcommit 77b7666614c8651713a4a35c6a5e2c18592d3b07[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:01:10 2020 +0200

    removed HARDWARE_THREAD_ID generic

[33mcommit 4f5fc1af9facc695b1995555ec8ec76e6fea6272[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 13 20:00:39 2020 +0200

    updated bootloader

[33mcommit 2f592c18ccd4cdde778c7d1887e62a762921b619[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 12 22:42:57 2020 +0200

    added trap (mcause CSR) code defines

[33mcommit 1413865ced21b5076aaa9ca11078af11d517155f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 12 22:42:16 2020 +0200

    minor edits (added comments; clean-up)

[33mcommit b37c0646d77322c42ef94f1768a6b5d3125314de[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 12 22:40:33 2020 +0200

    minor updates

[33mcommit a1aeb68464c0778ca9ac18a61d3ff0df250e0f3b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 12 22:40:00 2020 +0200

    added function to read system time (MTIME) via time[h] CSRs

[33mcommit 13f0c7f034bf70554f8fb6731de07a90af0fd726[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 12 20:10:22 2020 +0200

    added *optional* attributes for improved timing

[33mcommit efb6e68d463756447de9bf67310676323b228292[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 12 19:35:06 2020 +0200

    doubled the processing speed of the alu shift unit

[33mcommit 160007ee6b2032813ca731b33130170958175ea6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 11 14:02:45 2020 +0200

    added fence[i] status output to external memory interface

[33mcommit 273efb91596206a138c48457b036fa263f76b866[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 11 14:01:46 2020 +0200

    typo fix

[33mcommit b3655e5afb795aca3bd38ff1f9d10146f1b3639d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 11 13:07:30 2020 +0200

    added [m]instret[h] and [m]cycle[h] access functions; make mtime read access roll-over safe

[33mcommit 16fd74de3f5e8845aa8a245dc881383cb9a72c79[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jul 11 13:06:52 2020 +0200

    using only instret[h] and cycle[h] for performance evaluation

[33mcommit 48ae50154a2a92f5c3f8578c62d945a25b784d6a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 10 19:50:33 2020 +0200

    added further tests

[33mcommit a50816b71fbd5a16273e457deb4932c7930ab2ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 10 19:48:52 2020 +0200

    updated documentary

[33mcommit 1504d16b28048871aa5789d42d3cfbc29de18f23[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 10 19:13:37 2020 +0200

    updates

[33mcommit d1720c51a58e930c78bfc2117f4412070b43a07b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 10 19:10:51 2020 +0200

    minstret[h] and mcycle[h] now can be written; faster non-taken branches; WFI bug fix

[33mcommit 16501cdc93c192fb967979430de802253bcf22a9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jul 10 19:09:35 2020 +0200

    time[h] csr now correctly reflects system time from MTIME

[33mcommit 6125732b33e7bf1f5fc5d7643b0fba1a3b6decab[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 16:53:28 2020 +0200

    updated documentation

[33mcommit 54489ae6c0e5b14f6e58c04d14b9a898776264ae[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 16:53:06 2020 +0200

    minor updates

[33mcommit d2799f7e2e5cb9d2e2ecef06dd361cffee243eda[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 16:52:43 2020 +0200

    updated bootloader

[33mcommit aada5202c4c662a2a2b54fc45a7ee5cac0351ed7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 15:53:55 2020 +0200

    MTIME.time is now r/w (again); MTIME regs only allow full-word writes (to reduce hardware footprint)

[33mcommit 227516009b3a81f6b0ae4692dab7406bd2fca830[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 14:21:13 2020 +0200

    misa CSR is read-only! mtval and mcause CSRs can now also be written by user; fixed error in bus unit - controller can now force bus unit reset and bus transaction termination

[33mcommit 338bde36d824905ab160bab836d079fbc3363cb3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 14:15:36 2020 +0200

    fixed bug in wishbone gateway: outgoing signals are now stable until ack (for at least 1 reg stage)

[33mcommit 662a3ce7d26b35678f7da8dc09bcc4cf57043deb[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 14:14:28 2020 +0200

    new default bootloader spi flash boot address is 0x0080_0000

[33mcommit 674e8bef9ef6b20eea133fd8768c78b79c4c67b3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 14:13:36 2020 +0200

    added README to introduce the different top templates

[33mcommit 4406132c5a12c930ecfa5f8bb8fea4c559d1fdac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 9 14:13:16 2020 +0200

    minor edits

[33mcommit 05cd40830108ee927276aa985ae77cf8e85db820[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 8 16:18:12 2020 +0200

    csr.misa.x (non-std extension flag) is now zero

[33mcommit 9dbea7b544a18024045663705e1793c098d89d2f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 8 16:17:23 2020 +0200

    removed pointless version number

[33mcommit 2ff653f600f7d2a4d9583006b2929377ff2a190f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 8 16:16:51 2020 +0200

    cpu test now also prints project credits

[33mcommit dfdb1544edd66a431610cd71ba8664bc825e2ed4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 8 16:15:52 2020 +0200

    added function to print project credits

[33mcommit f8dd93f849a7d45a8757cdc21ade5bf9b1df2492[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 8 16:10:48 2020 +0200

    added simplified cpu diagram

[33mcommit a8c8c5bd7d88530259abc3b69afe79c33ea35b53[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 8 16:10:32 2020 +0200

    renamed processor overview figure

[33mcommit c925e926ae71f633a82e64181bd06670e65bf5c6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 22:22:31 2020 +0200

    pretty project status ;)

[33mcommit a3aaca62ccf1eb25916c8cd50414d49881d17750[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 22:22:10 2020 +0200

    text re-formatting

[33mcommit 4d3ed4c26a12f818a8f81ceeca9ec5657852b720[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 22:07:23 2020 +0200

    changed doxygen build folder name

[33mcommit 9c79dc268a50efe2c9abf150f7846e4602177023[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 22:06:33 2020 +0200

    added link to deployed doxygen-based SW-documentary

[33mcommit 9356ed9cf5166222a0d562cc6b0396c4870aed8b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 22:05:27 2020 +0200

    removed debugging output

[33mcommit aaf88b92b1656db969e2da7271740bc02f14f35a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 22:05:10 2020 +0200

    changed default doxygen build folder

[33mcommit 86f55fa74524ab04a00d34b0f76a80423a86f0e0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 21:55:54 2020 +0200

    testing travis ci: automatic doxygen doc deployment- part 5

[33mcommit 1d53c334f0051cffee2d015823ba98860e3056f7[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 21:44:34 2020 +0200

    testing travis ci: automatic doxygen doc deployment- part 4

[33mcommit c8dbc9738056d804304eac6512e16f0e7b878ec4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 21:39:40 2020 +0200

    testing travis ci: automatic doxygen doc deployment - part 3

[33mcommit 349c7db54808b416437b8e2db3cb2a34450b4427[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 21:23:49 2020 +0200

    testing travis ci: automatic doxygen doc deployment- part 2

[33mcommit 960770c8d81566234cc68e6e03db5ebabf8e12ac[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 21:17:04 2020 +0200

    testing travis ci: automatic doxygen doc deployment

[33mcommit aec70507d6c2f1da4068c9b3f78390035dbdf252[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 20:40:41 2020 +0200

    updates and clean-ups

[33mcommit 4c7f49bfdc45046d78793a8b320cabc8770dc7c3[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 20:40:28 2020 +0200

    added some comments

[33mcommit c2edb1c885860a5b79e1c10fb4c435293d94d1ba[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 20:39:35 2020 +0200

    fix in auto boot timeout output

[33mcommit e6437512446107e81da8314da8b2a6304e799270[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 19:14:00 2020 +0200

    added function for enabling/disabling CPU extensions (experimental!); optimized some functions to be always inlined

[33mcommit 5e8b2429110dd082cc50f7c7d09f4529de76517a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 19:13:13 2020 +0200

    minor edits

[33mcommit abeb1200d10411619d5e0f997b55dc527eb0b98b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jul 7 13:02:09 2020 +0200

    doc layout and typo fixes; minor updates

[33mcommit d4e18558ace79cc58a5e81421d094af51905190a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 6 21:46:10 2020 +0200

    added simple fence/fence.i test

[33mcommit 4e09225558c8b2616320da552906ddb888938b4d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 6 21:45:42 2020 +0200

    added missing FENCE instruction; added new generic to implement Zifencei CPU extension for instruction stream snc. (FENCE.I)

[33mcommit 783dd11467bc1345f5551680e5756bfab80b6d89[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 6 19:32:51 2020 +0200

    minor edits

[33mcommit d38509fc92cfd3520f2ce8c71126f4421c2edd22[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 6 19:32:20 2020 +0200

    removed mtinst CSR since it is not ratified by the official RISC-V specs yet

[33mcommit 16636c4353af9eddbff92976de2f1b3bb28b8409[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Mon Jul 6 13:30:00 2020 +0200

    minor edits and layout modifications

[33mcommit d99ea599c5768b0b7c2ecbf562c103b611797dd8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 23:34:09 2020 +0200

    updated documentary for new processor version 1.0.0.0

[33mcommit 28d4d3f6ef3e9529f40fea764a3391af4b8b6b68[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 23:33:33 2020 +0200

    updated bootloader image

[33mcommit f57e780bd54e2e2616924485f408b556961b9b12[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 23:15:12 2020 +0200

    now also using default ghdl sim script for ci

[33mcommit a3cb008f8ac2af3d4d7792c132a64d4f2d7e139f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 23:14:46 2020 +0200

    bug fixes

[33mcommit 3fb83fa8c5ac9e8f3a5ded002b8aaddb27c3922f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:39:08 2020 +0200

    minor edits

[33mcommit 940a5d80baa9d973bf93268502cab6c8a9466db0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:31:06 2020 +0200

    added misa csr bits

[33mcommit e77b150600871a4116ac182f8c0fdf6b15669f2f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:30:32 2020 +0200

    added new generic to turn on/off implementation of performance counter CSRs; changed SPI signal names

[33mcommit 602f82ea2f6eb3f7fe074afecc0588e98e6dae50[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:29:31 2020 +0200

    signal renamings

[33mcommit 85546e853ccf282a6f056acdd81f76f1ea0bd321[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:29:18 2020 +0200

    new cpu architecture: 2-stages multi-cycle pipeline; improved execution speed

[33mcommit 241192a357e4626ce747224eee2de7a887d50a47[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:24:57 2020 +0200

    minor comment fixes

[33mcommit fa01809f60363df1cf42a56aa678f3e26441d6c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:24:26 2020 +0200

    updates due to new rte functions

[33mcommit b31a77ecad4df48f643bb7f55e5cdda968e23277[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:24:08 2020 +0200

    updated neorv32 rte

[33mcommit fd16f64704a6213ebabe01049116dee5c8e2bd97[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:22:59 2020 +0200

    using csr pseudo-instruction for easier csr access

[33mcommit 75d71a036c894d5bcea19b7896479d265d101704[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:22:21 2020 +0200

    updated makefiles

[33mcommit eba90c7aa1bd36db82d1059082cc6fde3f25e304[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jul 5 22:20:01 2020 +0200

    added option to pass simulation time as first argument

[33mcommit 6177a8b895326f661e846e68dc14de5e9132cee4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 21:19:48 2020 +0200

    removed legacy stuff from doxygen makefile

[33mcommit b66dd3fefd1da7f8e9e55935859a0f8412e3db17[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:59:05 2020 +0200

    added new generic to core sw library

[33mcommit dc507a089391321c67a4fff780f249987329a973[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:58:30 2020 +0200

    added new generic to enable/disable performance and counter CSRs

[33mcommit 6f11761648a69d5188e6de4da4ad943fd57b3d91[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:55:35 2020 +0200

    modified ci framework to use new cpu_test project for verification

[33mcommit d7cff7622a27963de62f1d506d6a2e97cbbc7da8[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:55:00 2020 +0200

    removed hw_analysis and exception_test projects; they are replaced by the cpu_test project

[33mcommit dbf1d47b01d25ff98fdda7abf5cba1f3432039d2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:54:17 2020 +0200

    added simple xilinx vivado simulator waveform configuration file

[33mcommit b0d72b8e372bcc3e25f5b52fb793364be24103df[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:49:43 2020 +0200

    added 'show hardware config' function to RTE; removed mscratch as return address buffer in crt0.S

[33mcommit 4b8d10938875949dfa1b22357184fcca930dd9ee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jul 2 20:48:08 2020 +0200

    bootloader now also shows mhartid

[33mcommit 73444ac2bcdbf3e7a92c837c2e3be5a24879049b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 1 21:47:39 2020 +0200

    minor edits

[33mcommit d16d45321d3a32f1724a1efe5cffcda784cb821d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 1 21:45:20 2020 +0200

    minor edits

[33mcommit a220448f82683f586b63bcf378aae10f521a31db[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 1 21:44:51 2020 +0200

    minor edits

[33mcommit f69ec5db3c406db3c8167f01b73b05445123369f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jul 1 21:42:58 2020 +0200

    fixed error in MTIME.time high word incremenet

[33mcommit e43f6f06549a8c99333f8e3b84252be2e66a6c79[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 28 22:16:18 2020 +0200

    minor edits

[33mcommit 462a91b384ec8a6a958bd16ffcc502ec990b3f48[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sun Jun 28 22:15:55 2020 +0200

    DEVNULL simulation output now also dumps full 32-bit written data in hex form

[33mcommit b781c9d4f520fa3351751686a2605174bde2e50e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 14:07:47 2020 +0200

    fixed bootloader bug introduced with last commit

[33mcommit 0567703e773530cb42b2c7c47025b14f0635c52a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 13:56:25 2020 +0200

    updated documentary!

[33mcommit b5c8420e66a38e3ee3029c072b769ed4f3645c04[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 13:54:58 2020 +0200

    minor edits

[33mcommit 8d52e445e7056c28dadab0f363b70fd03addf157[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 13:54:41 2020 +0200

    hardware optimization

[33mcommit b99f9cbfe559a02b502da667a0de5f80677b84c9[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 13:54:13 2020 +0200

    minor edits

[33mcommit 480ad78a84a7cf9948893f107a029fa974c7a996[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 13:53:29 2020 +0200

    MTIME.time register is now read-only

[33mcommit 6954df489db24e990452f64e906ba6458f8282bd[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Sat Jun 27 13:52:20 2020 +0200

    updated bootloader

[33mcommit ed40bdf0b72d7d2f44c1e7d2f05feae535c7984b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 26 23:59:02 2020 +0200

    minor edits

[33mcommit b032855ecaf3d6bf839a14501ee21ec71c62be52[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 26 23:56:39 2020 +0200

    added missing signals to sensitivity list

[33mcommit ba991f4b095f3404df70835e71505c3bbf136b9d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 26 23:56:01 2020 +0200

    minor edits

[33mcommit ec74f2a753c3489a7bc9c73f3c0e0e281d8b5521[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Fri Jun 26 23:55:12 2020 +0200

    minor edits

[33mcommit bc4e00b9a4a9f205c732a63c552bf32429ce4545[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 25 20:17:22 2020 +0200

    improved CI hardware test

[33mcommit 35e0f622b935b793f72fb5aafa7c539506689ad0[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 25 20:16:52 2020 +0200

    updated documentary

[33mcommit 8751207d652c799c170726d3e7aaacee6cf1935b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 25 20:16:35 2020 +0200

    added GHDL simulation script

[33mcommit def3cb5ee80b4a4f4152b6cec466ad8f4481dd89[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 25 20:16:17 2020 +0200

    minor edits and updates

[33mcommit ea0acbe2c7ab0fd0dc2a7f1ea8de55c0aeccdfe6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 25 20:15:37 2020 +0200

    added DEVNULL io device for testing and faster simulation console output

[33mcommit ddaa93b787597331c5c099ebe85b66c73bc17cb6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Thu Jun 25 19:13:55 2020 +0200

    Added code of conduct

[33mcommit d8cec1f8109657a5ea2d8c5528491c1c9c0f4123[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 17:23:15 2020 +0200

    minor edits

[33mcommit ead0be3645fff865bb7792f1c24352cc17d934b6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 17:12:45 2020 +0200

    minor edits

[33mcommit 624193de0041e3e53aee31692d44429cc7e4f75a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 17:11:18 2020 +0200

    moved (travis) CI files to .ci folder

[33mcommit 3c2819f4002e7ed2a8aeb0266e8ba17c6389f1f6[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 17:10:37 2020 +0200

    minor edits and typo/layout fixes

[33mcommit 0da666512629567c0eb090796da7036e79dd1102[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 11:30:07 2020 +0200

    added example program for showing hardware configuration

[33mcommit 3085db47b9bfc9f9d55921de9e22bf394f70271e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 11:29:35 2020 +0200

    fixed travis ci bugs

[33mcommit 558e5e6f144df9357897fba2343678c2c56801b4[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 11:22:46 2020 +0200

    debugging travis scripts v3

[33mcommit 2e5cfa7fa542623bc0e08124110a9aaf9172325f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 11:15:54 2020 +0200

    debugging travis scripts v2

[33mcommit b15d58b39fb04a5f123744b0f6ec057daf497f5e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 10:55:38 2020 +0200

    debugging travis scripts

[33mcommit 781c6320be467df042b054a5276d08fc5b14f60d[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 10:35:10 2020 +0200

    fixed typos

[33mcommit 6334ca1902d21d1f11375f3eead3fa3ccf2ef60f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Wed Jun 24 10:34:56 2020 +0200

    fixed travis ci scripts

[33mcommit ea2948cdf9225fc0391a7b4e7cf7bd5b2fd23e4b[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 22:43:59 2020 +0200

    modified travis CI test scripts

[33mcommit c6a37fcd5a46058ba9864616daaa008504b9b5a7[m
Merge: 5276e1f4 e98e61dc
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 22:03:48 2020 +0200

    Merge branch 'master' of https://github.com/stnolting/neorv32

[33mcommit e98e61dc6cb17d09f8f8a924bcb5b6a253b54e8e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 22:03:35 2020 +0200

    Update issue templates

[33mcommit 5276e1f471d8f69a1066678b689ae7e525d931ca[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 21:50:13 2020 +0200

    added *.tmp files to gitignore

[33mcommit fac878d6ce207529da43fa9eef9be6a8fdf8f69e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 21:49:54 2020 +0200

    typo fixes

[33mcommit b2861ede6764eaa778c2500f2e9108574e0e6006[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 21:00:25 2020 +0200

    fixed commit error

[33mcommit 7166dc97622a1bf7e1b8bf6145cc6b100f1e4d76[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 20:52:09 2020 +0200

    minor edits

[33mcommit 1ae6f47a97c850fb1499e23ee0f599313866ab1f[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 19:49:50 2020 +0200

    minor edits

[33mcommit fdebde63d659594801544893ec709e9735556ee5[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 19:49:39 2020 +0200

    fixed links; added PDF icon

[33mcommit 417479a18bdf3ed63f2f34b187883219c4baacee[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 19:38:35 2020 +0200

    added exceptions/interrupts test program

[33mcommit 89101cf3350d80447b203308a6e2432d821140c2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 19:38:01 2020 +0200

    minor edits

[33mcommit 5a80bb9a99688a1726be79cfc3b3edc661e203a2[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 17:55:38 2020 +0200

    minor edits

[33mcommit 420fe525cb94eac39cbf92d7927f2cf2c779089e[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 17:44:28 2020 +0200

    minor edits

[33mcommit bee421876ae20f0f702098db6eab9a957a784233[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 17:43:03 2020 +0200

    initial commit

[33mcommit c1ef76709118f9611df0f5ff07ddba309284203a[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 17:36:40 2020 +0200

    initial commit

[33mcommit 45d64ba6fe99e33c619efc06f5bfe706c44c3add[m
Author: Stephan Nolting <stnolting@gmail.com>
Date:   Tue Jun 23 17:29:42 2020 +0200

    Initial commit
