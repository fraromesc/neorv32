<<<<<<< HEAD
=======
// #################################################################################################
// # << NEORV32 - Blinking LED Demo Program >>                                                     #
// # ********************************************************************************************* #
// # BSD 3-Clause License                                                                          #
// #                                                                                               #
// # Copyright (c) 2021, Stephan Nolting. All rights reserved.                                     #
// #                                                                                               #
// # Redistribution and use in source and binary forms, with or without modification, are          #
// # permitted provided that the following conditions are met:                                     #
// #                                                                                               #
// # 1. Redistributions of source code must retain the above copyright notice, this list of        #
// #    conditions and the following disclaimer.                                                   #
// #                                                                                               #
// # 2. Redistributions in binary form must reproduce the above copyright notice, this list of     #
// #    conditions and the following disclaimer in the documentation and/or other materials        #
// #    provided with the distribution.                                                            #
// #                                                                                               #
// # 3. Neither the name of the copyright holder nor the names of its contributors may be used to  #
// #    endorse or promote products derived from this software without specific prior written      #
// #    permission.                                                                                #
// #                                                                                               #
// # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS   #
// # OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF               #
// # MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE    #
// # COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     #
// # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE #
// # GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED    #
// # AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING     #
// # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  #
// # OF THE POSSIBILITY OF SUCH DAMAGE.                                                            #
// # ********************************************************************************************* #
// # The NEORV32 Processor - https://github.com/stnolting/neorv32              (c) Stephan Nolting #
// #################################################################################################
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364


/**********************************************************************//**
 * @file p1/main.c
<<<<<<< HEAD
 * @author Francisco Javier Roman Escorza, Daniel Peinado Ramirez
 * @brief 
=======
 * @author Francisco Javier Roman Escorza
 * @brief Using the leds depending of the 3 buttoms of the iCEBREAKER associated with this configuration of the neorv:

  gpio_i(0)  <= iCEBreakerv10_PMOD2_9_Button_1; 
  gpio_i(1)  <= iCEBreakerv10_PMOD2_4_Button_2;  
  gpio_i(2)  <= iCEBreakerv10_PMOD2_10_Button_3;
  iCEBreakerv10_PMOD2_1_LED_left   <= gpio_o(0);
  iCEBreakerv10_PMOD2_2_LED_right  <= gpio_o(1);
  iCEBreakerv10_PMOD2_8_LED_up     <= gpio_o(2);
  iCEBreakerv10_PMOD2_3_LED_down   <= gpio_o(3);
  iCEBreakerv10_PMOD2_7_LED_center <= gpio_o(4);
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
 **************************************************************************/

#include <neorv32.h>


/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200
/** Use the custom ASM version for blinking the LEDs defined (= uncommented) */
//#define USE_ASM_VERSION
/**@}*/

<<<<<<< HEAD
#define BOTONES 7
//0 => reposo, 1=> leds encendidos 
char estado = 0; 
int boton = 0;
char circuito = 0;  
char cnt=0; 
int MASK_LED[3][5]={
                    {24, 6,  31, 0,  31}, 
                    {9,  17, 24, 9,  3},
                    {3,  5,  6,  3,  17}
}; 



int main() {

=======
//MASK OF BUTTOMS AND LEDS 
#define button1 0
#define button2 1
#define button3 2

#define ledLeft   0
#define ledRight  1
#define ledUp     2
#define ledDown   3
#define ledCenter 4


/**********************************************************************//**
 * Main function; shows an incrementing 8-bit counter on GPIO.output(7:0).
 *
 * @note This program requires the GPIO controller to be synthesized (the UART is optional).
 *
 * @return 0 if execution was successful
 **************************************************************************/
int main() {

  // init UART (primary UART = UART0; if no id number is specified the primary UART is used) at default baud rate, no parity bits, ho hw flow control
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
  neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

  // check if GPIO unit is implemented at all
  if (neorv32_gpio_available() == 0) {
    neorv32_uart0_print("Error! No GPIO unit synthesized!\n");
    return 1; // nope, no GPIO unit synthesized
  }

  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  // say hello
<<<<<<< HEAD
  neorv32_uart0_print("Practica 1 SEPA\n");

  while(1){
    switch(estado)
    {
      case 0:
        neorv32_gpio_port_set(0); //Leds apagados
        boton = (neorv32_gpio_port_get()&BOTONES); 
        neorv32_uart0_print("Pulse un boton\n"); 
        if (boton) 
        {
          estado = 1;
          if(boton <= 2)  circuito = boton;             //Pines 0 y 1 
          else if (boton == 4) circuito = 3;            //Pin 3
          neorv32_uart0_printf("Se ha pulsado el boton %i\n", circuito); 
          boton = 0; 
        }
        
      break; 

      case 1: 
        neorv32_gpio_port_set(MASK_LED[circuito-1][cnt]); 
        neorv32_cpu_delay_ms(500);
        if (cnt<4) cnt++; 
        else 
        {
          cnt=0; 
          estado = 0; 
        } 
      break; 


    }
=======
  neorv32_uart0_print("Led controlled by buttons \n");
 
  //Read the buttom 
  
  int output=0;
  int b1=0,b2=0, b3=0; 
  int lr=0, ll=0, lu=0, ld=0, lc=0; 
  while(1)
  {
    b1 = neorv32_gpio_pin_get(0)>>button1;
    b2 = neorv32_gpio_pin_get(1)>>button2;
    b3 = neorv32_gpio_pin_get(2)>>button3; 
    ld = b1 & b2;                         //Down Led as AND port of b1 and b2        
    lr = b1 | b2;                         //Right Led as OR port of b1 and b2
    lu = (b1 ^ b2)&1;                         //Upper Led as XOR port of b1 and b2
    ll = (!b3)&1;                             //Left Led as NOR of b3
    lc = b1 & b2 & b3;                    //Center Led as AND port of the 3 buttons
    output = ((ld<<ledDown) | (lr<<ledRight) | (lu<<ledUp) | (ll<<ledLeft) | (lc<<ledCenter)); 
    //Link all the outputs at the same vector using the masks
    

    neorv32_gpio_port_set(output);                                                        
  
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
  }

  return 0;
}

<<<<<<< HEAD
=======

>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
