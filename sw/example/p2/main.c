
/**********************************************************************//**
 * @file p2/main.c
<<<<<<< HEAD
 * @author Francisco Javier Roman Escorza, Daniel Peinado Ramirez
 * @brief Using the leds depending of the 3 buttoms of the iCEBREAKER associated with this configuration of the neorv:
=======
 * @author Francisco Javier Roman Escorza
 * @brief Using the leds depending of the 3 buttoms of the iCEBREAKER associated with this configuration of the neorv:

  gpio_i(0)  <= iCEBreakerv10_PMOD2_9_Button_1; 
  gpio_i(1)  <= iCEBreakerv10_PMOD2_4_Button_2;  
  gpio_i(2)  <= iCEBreakerv10_PMOD2_10_Button_3;
  iCEBreakerv10_PMOD2_1_LED_left   <= gpio_o(0);
  iCEBreakerv10_PMOD2_2_LED_right  <= gpio_o(1);
  iCEBreakerv10_PMOD2_8_LED_up     <= gpio_o(2);
  iCEBreakerv10_PMOD2_3_LED_down   <= gpio_o(3);
  iCEBreakerv10_PMOD2_7_LED_center <= gpio_o(4);
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
 **************************************************************************/

#include <neorv32.h>


/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200

<<<<<<< HEAD
//PINES DE BOTONES Y LEDS 
#define BOTON1 0
#define BOTON2 1
#define BOTON3 2

#define LEDLEFT  0
#define LEDRIGHT  1
#define LEDUP     2
#define LEDDOWN   3
#define LEDCENTER 4

//PINES DE TECLADO
#define TECCOL1 8
#define TECCOL2 7
#define TECCOL3 6
#define TECCOL4 5
#define TECROW1 6
#define TECROW2 5
#define TECROW3 4
#define TECROW4 3

 uint64_t TECROW[]={TECROW1, TECROW2, TECROW3, TECROW4}; 
 uint64_t TECCOL[]={TECCOL1, TECCOL2, TECCOL3, TECCOL4}; 
 uint64_t MASKTECIN = ((1<<TECROW1) | (1<<TECROW2) | (1<<TECROW3) | (1<<TECROW4));
 uint64_t MASKTECOUT = ((1<<TECCOL1) | (1<<TECCOL2) | (1<<TECCOL3) | (1<<TECCOL4));
 uint64_t MASKLED  = ((1<<LEDLEFT) | (1<<LEDRIGHT) | (1<<LEDUP) | (1<<LEDDOWN) | (1<<LEDCENTER));
 uint64_t LEDOUT = 0x00000000; 

//MATRIZ DE CARACTERES DEL TECLADO
=======
//PINS OF BUTTOMS AND LEDS 
#define button1 0
#define button2 1
#define button3 2

#define ledLeft   0
#define ledRight  1
#define ledUp     2
#define ledDown   3
#define ledCenter 4

//PINS OF KEYBOARD
#define KbCol1 8
#define KbCol2 7
#define KbCol3 6
#define KbCol4 5
#define KbRow1 6
#define KbRow2 5
#define KbRow3 4
#define KbRow4 3

 uint64_t KbRow[]={KbRow1, KbRow2, KbRow3, KbRow4}; 
 uint64_t KbCol[]={KbCol1, KbCol2, KbCol3, KbCol4}; 
 //uint64_t KbCol[]={ledLeft, ledRight, ledUp,ledDown, ledCenter}; 
 uint64_t maskKbIn = ((1<<KbRow1) | (1<<KbRow2) | (1<<KbRow3) | (1<<KbRow4));
 uint64_t maskKbOut = ((1<<KbCol1) | (1<<KbCol2) | (1<<KbCol3) | (1<<KbCol4));
 uint64_t maskLed  = ((1<<ledLeft) | (1<<ledRight) | (1<<ledUp) | (1<<ledDown) | (1<<ledCenter));
 uint64_t ledOut = 0x00000000; 

//MATRX OF KEYBOARD
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
char matrix[4][4]=
  {
    {'1','2','3','A'}, 
    {'4','5','6','B'}, 
    {'7','8','9','C'},
    {'0','F','E','D'}
  }; 
<<<<<<< HEAD
//MATRIZ DE MASCARA DE LEDS CORRESPONDIENTE A CADA TECLA
char matriz[4][4]=
{
    {5, 4, 6, 31 }, 
    {1, 16, 2, 19}, 
    {9, 8, 10, 3},
    {15, 28,12, 0}
};
char tecla=0;         //Para guardar tecla presionada

=======

char key=0;         //To save the key pressed

/**********************************************************************//**
 * Main function; use of keyboard Pmod KYPD (https://digilent.com/shop/pmod-kypd-16-button-keypad/).
 *
 * @note This program requires the GPIO controller to be synthesized (the UART is optional).
 *
 * @return 0 if execution was successful
 **************************************************************************/
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
int main() {

  // init UART (primary UART = UART0; if no id number is specified the primary UART is used) at default baud rate, no parity bits, ho hw flow control
  neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

  // check if GPIO unit is implemented at all
  if (neorv32_gpio_available() == 0) {
    neorv32_uart0_print("Error! No GPIO unit synthesized!\n");
    return 1; // nope, no GPIO unit synthesized
  }

  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  // say hello
<<<<<<< HEAD
  neorv32_uart0_print("Practica 2 SEPA: Uso Teclado y Led conjunto \n");
 
  neorv32_gpio_port_set(((1<<TECCOL1) | (1<<TECCOL2) | (1<<TECCOL3) | (1<<TECCOL4)));     //Columnas encendidas y leds apagados
  while(1)
  {     //TECLADO
    for (int i=0; i<4; i++)
    {
      neorv32_gpio_port_set((LEDOUT | (MASKTECOUT&(~(1<<TECCOL[i])))));                  //Apagar la columna i y encender los leds correspondientes
      uint64_t  INTEC = ((~neorv32_gpio_port_get())&MASKTECIN);                          //Leer y procesar los inputs para obtener unicamente el pin
                                                                                         //de la tecla pulsada a '1'
      if (INTEC != 0x00000000)                                                           //Comprobar si tecla pulsada
      {
        for (int j=0; j<4; j++)
          {
            if ((INTEC & 1<<TECROW[j]))                                                   //Tecla col = i, row = j pulsada
            {
              neorv32_uart0_printf("%c\n", matrix[j][i]);
              tecla=matrix[j][i];                                                         //Salvar valor ASCII de tecla pulsada 
              while(INTEC == ((~neorv32_gpio_port_get())&MASKTECIN));                     //Guardar mascara de leds correspondiente a la tecla pulsada
              LEDOUT=matriz[j][i]; 
=======
  neorv32_uart0_print("Keyboard and Leds active \n");
 
  neorv32_gpio_port_set(((1<<KbCol1) | (1<<KbCol2) | (1<<KbCol3) | (1<<KbCol4)));     //All colums on and all leds off
  while(1)
  { 
    //LEDs' LOGIC
    switch (key){
      case '1': 
      ledOut = (1<<ledLeft) | (1<<ledUp); 
      break; 
      case '2':
      ledOut =  1<<ledUp; 
      break; 
      case '3': 
      ledOut = (1<<ledUp)  | (1<<ledRight); 
      break; 
      case '4': 
      ledOut = 1<<ledLeft; 
      break; 
      case '5': 
      ledOut = 1<<ledCenter;
      break; 
      case '6': 
      ledOut = 1<<ledRight; 
      break; 
      case '7': 
      ledOut = (1<<ledLeft) | (1<<ledDown); 
      break; 
      case '8': 
      ledOut = 1<<ledDown; 
      break; 
      case '9': 
      ledOut = (1<<ledDown) | (1<<ledLeft); 
      break; 
      case '0':       //Circle
      ledOut = (1<<ledLeft) | (1<<ledRight) | (1<<ledUp) | (1<<ledDown) ;
      break;      
      case 'A':       //All
      ledOut = (1<<ledLeft) | (1<<ledRight) | (1<<ledUp) | (1<<ledDown) | (1<<ledCenter);
      break; 
      case 'B': 
      ledOut = (1<<ledLeft) | (1<<ledRight) | (1<<ledCenter);
      break;      
      case 'C':       
      ledOut = (1<<ledLeft) | (1<<ledRight);
      break; 
      case 'D':       //Delete
      ledOut = 0x00000000; 
      break;      
      case 'E':       
      ledOut = (1<<ledUp) | (1<<ledDown);
      break;      
      case 'F':     
      ledOut = (1<<ledUp) | (1<<ledDown) | (1<<ledCenter);
      break; 
      default:
      ledOut = 0x00000000; 

    }
    //KEYBOARD
    for (int i=0; i<4; i++)
    {
      neorv32_gpio_port_set((ledOut | (maskKbOut&(~(1<<KbCol[i])))));             //Turn off each colum i and turn on the leds 
      uint64_t  inKb = ((~neorv32_gpio_port_get())&maskKbIn);         //Read and procces the inputs to get the key pressed as '1'
      if (inKb != 0x00000000)                                         //Key pressed
      {
        for (int j=0; j<4; j++)
          {
            if ((inKb & 1<<KbRow[j]))                                 //Key i,j pressed
            {
              neorv32_uart0_printf("%c\n", matrix[j][i]);
              key=matrix[j][i];                                       //Save key pressed 
              while(inKb == ((~neorv32_gpio_port_get())&maskKbIn));   //Loop till the key is not pressed
>>>>>>> 98d2d509510bed43c7db19d86cf6ddfb20c6a364
            }
          }
      }
    }  
    
                                                            
  
  }

  return 0;
}



