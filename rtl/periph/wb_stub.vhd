-- #################################################################################################
-- # << wb_stub - Wishbone Dummy Device with Internal Handling of Address Mapping >>               #
-- #################################################################################################

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library neorv32;
use neorv32.neorv32_package.all;

entity wb_stub is
  generic (
    WB_ADDR_BASE : std_ulogic_vector(31 downto 0); -- module base address, size-aligned
    WB_ADDR_SIZE : positive -- module address space in bytes, has to be a power of two, min 4
  );
  port (
    -- wishbone host interface --
    wb_clk_i  : in  std_ulogic; -- clock
    wb_rstn_i : in  std_ulogic; -- reset, async, low-active
    wb_adr_i  : in  std_ulogic_vector(31 downto 0); -- address
    wb_dat_i  : in  std_ulogic_vector(31 downto 0); -- read data
    wb_dat_o  : out std_ulogic_vector(31 downto 0); -- write data
    wb_we_i   : in  std_ulogic; -- read/write
    wb_sel_i  : in  std_ulogic_vector(03 downto 0); -- byte enable
    wb_stb_i  : in  std_ulogic; -- strobe
    wb_cyc_i  : in  std_ulogic; -- valid cycle
    wb_ack_o  : out std_ulogic  -- transfer acknowledge
  );
end wb_stub;

architecture wb_stub_rtl of wb_stub is

  -- internal constants --
  constant addr_mask_c : std_ulogic_vector(31 downto 0) := std_ulogic_vector(to_unsigned(WB_ADDR_SIZE-1, 32));
  constant all_zero_c  : std_ulogic_vector(31 downto 0) := (others => '0');

  -- address match --
  signal access_req : std_ulogic;

  -- dummy registers --
  type mm_reg_t is array (0 to (WB_ADDR_SIZE/4)-1) of std_ulogic_vector(31 downto 0);
  signal mm_reg : mm_reg_t;

begin

  -- Sanity Checks --------------------------------------------------------------------------
  -- -------------------------------------------------------------------------------------------
  assert not (WB_ADDR_SIZE < 4) report "wb_stub config ERROR: Address space <WB_ADDR_SIZE> has to be at least 4 bytes." severity error;
  assert not (is_power_of_two_f(WB_ADDR_SIZE) = false) report "wb_stub config ERROR: Address space <WB_ADDR_SIZE> has to be a power of two." severity error;
  assert not ((WB_ADDR_BASE and addr_mask_c) /= all_zero_c) report "wb_stub config ERROR: Module base address <WB_ADDR_BASE> has to be aligned to it's address space <WB_ADDR_SIZE>." severity error;


  -- Device Access? -------------------------------------------------------------------------
  -- -------------------------------------------------------------------------------------------
  access_req <= '1' when ((wb_adr_i and (not addr_mask_c)) = (WB_ADDR_BASE and (not addr_mask_c))) else '0';


  -- Bus R/W Access -------------------------------------------------------------------------
  -- -------------------------------------------------------------------------------------------
  rw_access: process(wb_rstn_i, wb_clk_i)
  begin
    if (wb_rstn_i = '0') then
      wb_dat_o <= (others => '-'); -- no reset
      wb_ack_o <= '0';
      mm_reg   <= (others => (others => '-')); -- no reset
    elsif rising_edge(wb_clk_i) then
      -- defaults --
      wb_dat_o <= (others => '0');
      wb_ack_o <= '0';

      -- access --
      if (wb_cyc_i = '1') and (wb_stb_i = '1') and (access_req = '1') then -- classic-mode Wishbone protocol
        if (wb_we_i = '1') then -- write access
          if (wb_sel_i = "1111") then -- only full-word accesses, no ACK otherwise
            mm_reg(to_integer(unsigned(wb_adr_i(index_size_f(WB_ADDR_SIZE)-1 downto 2)))) <= wb_dat_i;
            wb_ack_o <= '1';
          end if;
        else -- sync read access
          wb_dat_o <= mm_reg(to_integer(unsigned(wb_adr_i(index_size_f(WB_ADDR_SIZE)-1 downto 2))));
          wb_ack_o <= '1';
        end if;
      end if;
    end if;
  end process rw_access;


end wb_stub_rtl;
