library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decod_pmod is
    port (
        clk_i   : in std_logic; 
        rst_i   : in std_logic; 
        integer_o: out std_logic_vector(4 downto 0); 
        row     : out std_logic_vector(3 downto 0); 
        col     : in std_logic_vector(3 downto 0);
    );
end entity;

architecture decod_pmod is
    signal decod_in     : std_logic_vector(3 downto 0);
    signal decod_out    : std_logic_vector(3 downto 0);
    signal p_decod_out  : std_logic_vector(3 downto 0);
    signal decod_inout : std_logic_vector(7 downto 0); 
begin
    decod_in <= col; 
    row <= decod_out; 

    decod_inout(7 downto 4) <= decod_in; 
    decod_inout(3 downto 0) <= decod_out; 

    comb_shifter: process(p_decod_out)
    begin
        decod_out(1) <= p_decod_out(0); 
        decod_out(2) <= p_decod_out(1);
        decod_out(3) <= p_decod_out(2);
        decod_out(0) <= p_decod_out(3);
    end process; 

    sinc_shifter: process(clk_i, rst_i)
    begin 
        if (rst_i = '1') then 
            p_decod_out <= "0111"; 
        elsif(rising_edge(clk)) then 
            p_decod_out <= decod_out; 
        end if;
    end process; 

    --CODIFICADOR COL/ROW a 

    with decod_inout select integer_o <=
        --numeros del 0 al 9
        "01101" when "01111110", --'0' col<=4  row<=1 tecla => 13
        "00001" when "11101110", --'1' col<=1  row<=1 tecla => 1
        "00010" when "11101101", --'2' col<=1  row<=2 tecla => 2
        "00011" when "11101011", --'3' col<=1  row<=3 tecla => 3
        "00101" when "11011110", --'4' col<=2  row<=1 tecla => 5
        "00110" when "11011101", --'5' col<=2  row<=2 tecla => 6
        "00111" when "11011011", --'6' col<=2  row<=3 tecla => 7
        "01001" when "10111110", --'7' col<=3  row<=1 tecla => 9
        "01010" when "10111101", --'8' col<=3  row<=2 tecla => 10
        "01011" when "10111011", --'9' col<=3  row<=3 tecla => 11
        --letras de 'A' a 'F'
        "00100" when "11100111", --'A' col<=4  row<=1 tecla => 4
        "01000" when "11010111", --'B' col<=4  row<=2 tecla => 8
        "01100" when "10110111", --'C' col<=4  row<=3 tecla => 12
        "01110" when "01110111", --'D' col<=4  row<=4 tecla => 14
        "01111" when "01111011", --'E' col<=3  row<=4 tecla => 15
        "10000" when "01111101", --'F' col<=2  row<=4 tecla => 16
        "00000" when others; 

end decod_pmod; 




