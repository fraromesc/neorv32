
/**********************************************************************//**
 * @file p2/main.c
 * @author Francisco Javier Roman Escorza, Daniel Peinado Ramirez
 * @brief Using the leds depending of the 3 buttoms of the iCEBREAKER associated with this configuration of the neorv:
 **************************************************************************/

#include <neorv32.h>


/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200

//PINES DE BOTONES Y LEDS 
#define BOTON1 0
#define BOTON2 1
#define BOTON3 2

#define LEDLEFT  0
#define LEDRIGHT  1
#define LEDUP     2
#define LEDDOWN   3
#define LEDCENTER 4

//PINES DE TECLADO
#define TECCOL1 8
#define TECCOL2 7
#define TECCOL3 6
#define TECCOL4 5
#define TECROW1 6
#define TECROW2 5
#define TECROW3 4
#define TECROW4 3

 uint64_t TECROW[]={TECROW1, TECROW2, TECROW3, TECROW4}; 
 uint64_t TECCOL[]={TECCOL1, TECCOL2, TECCOL3, TECCOL4}; 
 uint64_t MASKTECIN = ((1<<TECROW1) | (1<<TECROW2) | (1<<TECROW3) | (1<<TECROW4));
 uint64_t MASKTECOUT = ((1<<TECCOL1) | (1<<TECCOL2) | (1<<TECCOL3) | (1<<TECCOL4));
 uint64_t MASKLED  = ((1<<LEDLEFT) | (1<<LEDRIGHT) | (1<<LEDUP) | (1<<LEDDOWN) | (1<<LEDCENTER));
 uint64_t LEDOUT = 0x00000000; 

//MATRIZ DE CARACTERES DEL TECLADO
char matrix[4][4]=
  {
    {'1','2','3','A'}, 
    {'4','5','6','B'}, 
    {'7','8','9','C'},
    {'0','F','E','D'}
  }; 
//MATRIZ DE MASCARA DE LEDS CORRESPONDIENTE A CADA TECLA
char matriz[4][4]=
{
    {5, 4, 6, 31 }, 
    {1, 16, 2, 19}, 
    {9, 8, 10, 3},
    {15, 28,12, 0}
};
char tecla=0;         //Para guardar tecla presionada

int main() {

  // init UART (primary UART = UART0; if no id number is specified the primary UART is used) at default baud rate, no parity bits, ho hw flow control
  neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

  // check if GPIO unit is implemented at all
  if (neorv32_gpio_available() == 0) {
    neorv32_uart0_print("Error! No GPIO unit synthesized!\n");
    return 1; // nope, no GPIO unit synthesized
  }

  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  // say hello
  neorv32_uart0_print("Practica 2 SEPA: Uso Teclado y Led conjunto \n");
 
  neorv32_gpio_port_set(((1<<TECCOL1) | (1<<TECCOL2) | (1<<TECCOL3) | (1<<TECCOL4)));     //Columnas encendidas y leds apagados
  while(1)
  {     //TECLADO
    for (int i=0; i<4; i++)
    {
      neorv32_gpio_port_set((LEDOUT | (MASKTECOUT&(~(1<<TECCOL[i])))));                  //Apagar la columna i y encender los leds correspondientes
      uint64_t  INTEC = ((~neorv32_gpio_port_get())&MASKTECIN);                          //Leer y procesar los inputs para obtener unicamente el pin
                                                                                         //de la tecla pulsada a '1'
      if (INTEC != 0x00000000)                                                           //Comprobar si tecla pulsada
      {
        for (int j=0; j<4; j++)
          {
            if ((INTEC & 1<<TECROW[j]))                                                   //Tecla col = i, row = j pulsada
            {
              neorv32_uart0_printf("%c\n", matrix[j][i]);
              tecla=matrix[j][i];                                                         //Salvar valor ASCII de tecla pulsada 
              while(INTEC == ((~neorv32_gpio_port_get())&MASKTECIN));                     //Guardar mascara de leds correspondiente a la tecla pulsada
              LEDOUT=matriz[j][i]; 
            }
          }
      }
    }  
    
                                                            
  
  }

  return 0;
}



