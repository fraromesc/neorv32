

/**********************************************************************//**
 * @file p4/main.c
 * @author 
 * @brief 
 **************************************************************************/

#include <neorv32.h>
#include <string.h>

/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200
/** Maximum PWM output intensity (0..255) */
#define PWM_MAX 128
/**@}*/


#define ADR 0x90000000
#define WORD 4

#define PINLED1 0 
#define PINBOT1 1
#define BOTON1 2
#define BOTON2 4
#define BOTON3 8
#define BOTONES 14
#define SENSOR 1

int main() {

  // check if PWM unit is implemented at all
  if (neorv32_pwm_available() == 0) {
    return 1;
  }
  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  // use UART0 if implemented
  if (neorv32_uart0_available()) {
    // init UART at default baud rate, no parity bits, ho hw flow control
    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);
    // check available hardware extensions and compare with compiler flags
    neorv32_rte_check_isa(0); // silent = 0 -> show message if isa mismatch
    // say hello
    neorv32_uart0_print("Practicas SEPA: Proyecto\n");
  }


  uint8_t pwm = 50;
  int estado = 0;
  int s=0;
  int s1=0; 
  char uart_input[5]="....\0"; 
  char wb_pass[5]="    \0"; 
  int gpio_i = 0; 
  int gpio_o = 0; 
  uint32_t tiempo = 0; 
  uint32_t contrasena = 0x34703373; //"s3p4" en hexadecimal
  // Configuración PWM
  neorv32_pwm_set(0, 0);
  neorv32_pwm_setup(CLK_PRSC_64);

  //Configuración inicial del registro
  neorv32_cpu_store_unsigned_word(ADR, contrasena);
  tiempo = 300;   
  neorv32_cpu_store_unsigned_word(ADR+1*WORD, tiempo);   
  tiempo = 700;
  neorv32_cpu_store_unsigned_word(ADR+2*WORD, tiempo); 
  tiempo = 1100;  
  neorv32_cpu_store_unsigned_word(ADR+3*WORD, tiempo); 
  tiempo = 0;



  while(1)
  {
    gpio_i = neorv32_gpio_port_get(); 
    gpio_o = ((gpio_i&BOTONES)>>PINBOT1)<<PINLED1; 
    neorv32_gpio_port_set(gpio_o); 

    switch(estado)
    {
      case 0:
        
        neorv32_uart0_printf("Introduzca contrasena\n"); 
        estado = 1;
      break;
      case 1:
        s=0;
        for (int i = 0; i < 4; i++)
        {
          while(!neorv32_uart0_char_received()); 
          uart_input[i]=neorv32_uart0_char_received_get();
        }
        uart_input[4] = '\0';
        for (int i = 0; i < 4; i++) wb_pass[i]=neorv32_cpu_load_unsigned_byte(ADR+i);
        
        wb_pass[4] = '\0';
  
        if (strcmp(wb_pass, uart_input)==0)
        {
          estado = 2; 
          neorv32_uart0_printf("Contrasena correcta\n");
        }
        else
        {
          estado = 0; 
          neorv32_uart0_printf("Contrasena incorrecta\n"); 
        }

        strcpy(wb_pass, "");
        strcpy(uart_input, "");
        s=0; 
      break;
      case 2:
      
        if (gpio_i>0)
        {
          s=0; 
          if (s1==0) 
                    {
                      neorv32_uart0_printf("Seleccione posicion\n"); 
                      s1=1;
                    }

          if((gpio_i == 5)) tiempo = neorv32_cpu_load_unsigned_word(ADR+WORD);
          else if((gpio_i == 3)) tiempo = neorv32_cpu_load_unsigned_word(ADR+WORD*2);
          else if((gpio_i == 9)) tiempo = neorv32_cpu_load_unsigned_word(ADR+WORD*3);
        }
        else if (s==0) 
        {
          neorv32_uart0_print("Coloque pieza en la cinta\n"); 
          s=1; 
          s1=0; 
        }
        if (tiempo != 0)   
          {
            estado ++; 
            neorv32_uart0_print("Iniciando movimiento\n");
          }

      break;
      case 3:
        
        neorv32_pwm_set(0, pwm); 
        neorv32_cpu_delay_ms(tiempo); 
        neorv32_pwm_set(0,0); 
        neorv32_uart0_printf("Posicion alcanzada.\n"); 
        neorv32_uart0_print("Retire la pieza.\n El sistema se reiniciara en ...\n"); 
        tiempo = 0; 
        estado++; 
      break;
      case 4:
        for (int i = 3; i > 0; i--)
        {
          neorv32_uart0_printf("%i\n", i); 
          neorv32_cpu_delay_ms(1000); 
          }
        estado = 0; 
      default :
        estado = 0; 
    }
    
  }

  return 0;
}
