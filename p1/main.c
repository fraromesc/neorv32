

/**********************************************************************//**
 * @file p1/main.c
 * @author Francisco Javier Roman Escorza, Daniel Peinado Ramirez
 * @brief 
 **************************************************************************/

#include <neorv32.h>


/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200
/** Use the custom ASM version for blinking the LEDs defined (= uncommented) */
//#define USE_ASM_VERSION
/**@}*/

#define BOTONES 7
//0 => reposo, 1=> leds encendidos 
char estado = 0; 
int boton = 0;
char circuito = 0;  
char cnt=0; 
int MASK_LED[3][5]={
                    {24, 6,  31, 0,  31}, 
                    {9,  17, 24, 9,  3},
                    {3,  5,  6,  3,  17}
}; 



int main() {

  neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

  // check if GPIO unit is implemented at all
  if (neorv32_gpio_available() == 0) {
    neorv32_uart0_print("Error! No GPIO unit synthesized!\n");
    return 1; // nope, no GPIO unit synthesized
  }

  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  // say hello
  neorv32_uart0_print("Practica 1 SEPA\n");

  while(1){
    switch(estado)
    {
      case 0:
        neorv32_gpio_port_set(0); //Leds apagados
        boton = (neorv32_gpio_port_get()&BOTONES); 
        neorv32_uart0_print("Pulse un boton\n"); 
        if (boton) 
        {
          estado = 1;
          if(boton <= 2)  circuito = boton;             //Pines 0 y 1 
          else if (boton == 4) circuito = 3;            //Pin 3
          neorv32_uart0_printf("Se ha pulsado el boton %i\n", circuito); 
          boton = 0; 
        }
        
      break; 

      case 1: 
        neorv32_gpio_port_set(MASK_LED[circuito-1][cnt]); 
        neorv32_cpu_delay_ms(500);
        if (cnt<4) cnt++; 
        else 
        {
          cnt=0; 
          estado = 0; 
        } 
      break; 


    }
  }

  return 0;
}

